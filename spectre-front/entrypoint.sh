#!/bin/bash

echo $API_CORE_URL
echo $API_KAPI_URL

sed -i "s/APICOREURL/$API_CORE_URL/g" /usr/share/nginx/html/scripts/*.app.js
sed -i "s/APIKAPIURL/$API_KAPI_URL/g" /usr/share/nginx/html/scripts/*.app.js
exec "$@"
