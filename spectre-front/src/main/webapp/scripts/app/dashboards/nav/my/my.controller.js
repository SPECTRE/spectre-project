'use strict';

angular.module('spectreApp')
    .controller('DashboardsMyController', function ($rootScope, $scope, $state, DashboardModel) {

        $scope.initMy = function(){
            $scope.initDash($scope.initMyDashboards);
        }

        // set scope favourite dashboards to user favourite dashboards
        // set scope tab to my dashboards
        // set session dashboard to first user dashboard
        $scope.initMyDashboards = function() {
            $scope.refreshFavouritesDashboards();
            $scope.refreshMyDashboards($scope.setDefaultDashboard);
        };

        $scope.setDefaultDashboard = function() {
            if ($state.current.name == 'dashboards-my-edit') {
                $scope.showDashboard(DashboardModel.getCurrentDashboard());
            } else if ($scope.mydashboards !== undefined && $scope.mydashboards.length > 0) {
                if (DashboardModel.getCurrentDashboard() != null) {
                    $scope.showDashboard(DashboardModel.getCurrentDashboard());
                } else {
                    $scope.showDashboard($scope.mydashboards[0]);
                }
            } else {
                $scope.showDashboard(DashboardModel.getCurrentDashboard());
            }
        };

        $scope.initMy();

    });
