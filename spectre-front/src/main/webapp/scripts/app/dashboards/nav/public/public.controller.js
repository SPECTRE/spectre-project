'use strict';

angular.module('spectreApp')
    .controller('DashboardsPublicController', function ($rootScope, $scope, $state, DashboardModel) {

        $scope.initPublic = function(){
            $scope.initDash($scope.initPublicDashboards);
        }

       // set scope public dashboards to all public dashboards
        // set scope favourite dashboards to user favourite dashboards
        // set scope tab to all dashboards
        // set session dashboard to first public dashboard
        $scope.initPublicDashboards = function(){
            $scope.refreshFavouritesDashboards();
            $scope.refreshPublicDashboards($scope.setDefaultDashboard);
        };

        $scope.setDefaultDashboard = function() {
            if($scope.publicdashboards !== undefined && $scope.publicdashboards.length > 0) {
                $scope.showDashboard($scope.publicdashboards[0]);
            } else {
                if ($scope.currentDashboard != null) {
                    $scope.currentDashboard.name = null;
                }
            }
        };

        $scope.$on('$destroy', function() {
            DashboardModel.setCurrentDashboard(null);
        });

        $scope.initPublic();

    });
