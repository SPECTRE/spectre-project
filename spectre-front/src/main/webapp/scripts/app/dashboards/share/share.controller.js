'use strict';

angular.module('spectreApp')
    .controller('DashboardSharedController', function ($scope, $stateParams, $timeout, $interval, $window, DashboardModel, DashboardService, FullScreenModel) {
      
        $scope.sharedDashboard = null;
        $scope.widgetPages = [];
        $scope.widgetIndexes = [];
        $scope.Timer = [];


        //================================
        // GET DASHBOARD WIDGET AND KPI
        //================================


        var refresh = function(){


        // get widgets
        DashboardService.sharedWidgets.get({id: $scope.sharedDashboard.id })
                                    .$promise
                                    .then(function (_widgets) {
            $scope.sharedDashboard.widgets = _widgets;
            console.log(_widgets);
            
            if ($scope.sharedDashboard != null && $scope.sharedDashboard.widgets != null) {
                var widgetPagesTemp = [];
                var widgetIndexesTemp = [];

                // Round a number upward to its nearest integer
                var maxWidgetPages = Math.ceil($scope.sharedDashboard.widgets.length / 8);

                var index = 0;
                for (var i = 0; i < maxWidgetPages; i++) {
                    widgetPagesTemp.push(i);
                    widgetIndexesTemp.push(index);
                    index += 8;
                }

                if ($scope.widgetPages.length != widgetPagesTemp.length) {
                    $scope.widgetPages = widgetPagesTemp;
                    $scope.widgetIndexes = widgetIndexesTemp;

                }

                for (var i = 0; i < $scope.sharedDashboard.widgets.length; i++) {
                   
                    if ($scope.sharedDashboard.widgets[i].indicator != null) {

                        //get KPIs
                        DashboardModel.refreshWidgetData(i, $scope.sharedDashboard.widgets[i]);

                    }
                }
            }

        
            }, function (error) {

                $scope.sharedDashboard.widgets = null;
                    
        });
            }


        // get shared dashboard
        DashboardService.sharedDashboard.get({shareId : $stateParams.shareId })
                                    .$promise
                                    .then(function (_sharedDashboard) {
                                            $scope.sharedDashboard = _sharedDashboard;
                                            $scope.reachable = true;

                                             //refresh the model
                                                refresh();
                                                refresh();
                                                $interval(refresh, 600000);

                                    }, function (error) {
                                            $scope.sharedDashboard = null;
                                            $scope.reachable = false;
                                    });

        //================================
        // WIDGET COMPUTE
        //================================
                                    

        $scope.toggleAutoTuple = function (widget) {
            if (widget.slides.isAuto) {
                $scope.Timer[widget.id] = $timeout(function () {
                    $scope.slideTuple('next', widget, false);
                    $scope.toggleAutoTuple(widget);
                }, DashboardModel.getTupleRefreshDelay());
            }
        }

        $scope.switchAutoTuple = function (widget) {
            if (!$scope.isTupleOn(widget)) {
                widget.slides.isAuto = true;
            } else {
                $timeout.cancel($scope.Timer[widget.id]);
                widget.slides.isAuto = false;
            }
        }

        $scope.isTupleOn = function (widget) {

            if (typeof widget.slides == 'undefined' || typeof widget.slides.isAuto == 'undefined' || !widget.slides.isAuto) {
                return false;
            }
            return true;
        }


        $scope.slideTuple = function (dir, widget, fromUser) {
            if (dir == 'prev') {
                if (widget.slides.active == 0) {
                    widget.slides.active = widget.kpis[0].value.length - 1;
                } else {
                    widget.slides.active--;
                }
            } else {
                if (widget.slides.active == widget.kpis[0].value.length - 1) {
                    widget.slides.active = 0;
                } else {
                    widget.slides.active++;
                }
            }
            if (fromUser && $scope.isTupleOn(widget)) {
                $timeout.cancel($scope.Timer[widget.id]);
                $scope.switchAutoTuple(widget);
            }
        }


        $scope.isNumericWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'NUMERIC');
        };

        $scope.isPercentWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'PERCENT');
        };

        $scope.isMoodWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'MOOD');
        };

        $scope.isTupleWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'TUPLE');
        };

        $scope.isTextWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'TEXT');
        };

        $scope.isNumericOrPercentTupleWidget = function (widget) {
            return (widget !== undefined
            && widget.kpis !== undefined
            && widget.kpis.length > 0
            && widget.kpis[0].measure !== 'TEXT');
        };


        $scope.slide = function (dir) {
            $scope.active = null;
            $('#dashCarousel').carousel(dir);
        };

        $scope.goToTuple = function (widget, index) {
            widget.slides.active = index;
        }

        $scope.isTupleSlideActive = function (widget, index) {
            return widget.slides.active == index;
        }

        $scope.flipp = function (widget) {
            widget.isFlipped = !widget.isFlipped;
        }

        $scope.getTupleIndexTab = function (widget) {
            var tab = [];
            if ($scope.isTupleWidget(widget)) {
                for (var i = 0; i < widget.kpis[0].value.length; i++) {
                    tab.push(i);
                }
            }
            return tab;
        }

        $scope.goToFirst = function () {
            $('.carousel').carousel(0);
        }

        //================================
        // NAVIGATION METHODS
        //================================

        $scope.isLegend = true;
        $scope.isCarouselSliding = true;

        $scope.fullScreen = function(){
            FullScreenModel.fullScreen();
        }

        $('.carousel').carousel({
            pause: null,
            interval: 8000
        })

        $('#dashCarousel').on('slid.bs.carousel', function () {
            $(window).trigger('resize');
        });

        $scope.hideLegend = function () {
            if ($scope.isLegend) {
                $( ".vis-legend" ).hide();
                $scope.isLegend = false;
            } else {
                $( ".vis-legend" ).show();
                $scope.isLegend = true;
            }
        };

        $scope.playCarousel = function () {
            $('.carousel').carousel('cycle');
            $scope.isCarouselSliding = true;
        }
        $scope.pauseCarousel = function () {
            $('.carousel').carousel('pause');
            $scope.isCarouselSliding = false;
        }

        $scope.showAutoSliding = function () {
            var lastPage = parseInt($( ".carousel-indicators > li:last-child" ).attr("data-slide-to"));
            if (lastPage > 0) {
                return true;
            }
            return false;
        }
    });
