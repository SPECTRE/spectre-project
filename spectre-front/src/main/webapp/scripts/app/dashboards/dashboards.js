'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dashboards-public-show', {
                parent: 'site',
                data: {
                    pageTitle: 'Dashboards publics'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/dashboards/dashboards.html',
                          controller: 'DashboardsController'
                    },
                    'nav@dashboards-public-show': {
                        templateUrl: 'scripts/app/dashboards/nav/public/public.html',
                        controller: 'DashboardsPublicController'
                    },
                    'dashboard@dashboards-public-show': {
                        templateUrl: 'scripts/app/dashboards/dashboard/show/show.html',
                        controller: 'ShowController'
                    }
                }
            })
            .state('dashboards-public-edit', {
                parent: 'site',
                data: {
                    pageTitle: 'Dashboards publics'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/dashboards/dashboards.html',
                          controller: 'DashboardsController'
                    },
                    'nav@dashboards-public-edit': {
                        templateUrl: 'scripts/app/dashboards/nav/public/public.html',
                        controller: 'DashboardsPublicController'
                    },
                    'dashboard@dashboards-public-edit': {
                        templateUrl: 'scripts/app/dashboards/dashboard/edit/edit.html',
                        controller: 'EditController'
                    }
                }
            })
            .state('dashboards-my-show', {
                parent: 'site',
                data: {
                    pageTitle: 'Mes dashboards'
                },
                views: {
                   'content@': {
                              templateUrl: 'scripts/app/dashboards/dashboards.html',
                              controller: 'DashboardsController'
                    },
                    'nav@dashboards-my-show': {
                        templateUrl: 'scripts/app/dashboards/nav/my/my.html',
                        controller: 'DashboardsMyController'
                    },
                    'dashboard@dashboards-my-show': {
                        templateUrl: 'scripts/app/dashboards/dashboard/show/show.html',
                        controller: 'ShowController'
                    }
                }
            })
            .state('dashboards-my-edit', {
                parent: 'site',
                data: {
                    pageTitle: 'Mes dashboards'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/dashboards/dashboards.html',
                          controller: 'DashboardsController'
                    },
                    'nav@dashboards-my-edit': {
                        templateUrl: 'scripts/app/dashboards/nav/my/my.html',
                        controller: 'DashboardsMyController'
                    },
                    'dashboard@dashboards-my-edit': {
                        templateUrl: 'scripts/app/dashboards/dashboard/edit/edit.html',
                        controller: 'EditController'
                    }
                }
            })
            .state('dashboards-shared', {
                parent: 'site',
                url: '/dashboards/public/shared?shareId',
                data: {
                    pageTitle: 'Mon dashboard partagé'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/dashboards/share/share.html',
                          controller: 'DashboardSharedController'
                    },
                }
            });
    });
