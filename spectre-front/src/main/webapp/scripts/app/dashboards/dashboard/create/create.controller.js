'use strict';

angular.module('spectreApp')
    .controller('CreateController', function ($rootScope, $scope, $state, $stateParams, $timeout, Principal, DashboardService, UserService, DashboardModel) {

        //================================
        // PAGE INITIALIZATION
        //================================
        $scope.myaccount = Principal.identity();
        Principal.identity();

        // init scope new dashboard
        $scope.newDashboard = {
            name: null,
            description:null,
            reachable: false
        };

        //================================
        // METHODS
        //================================

        // toggle reachable flag for new dashboard
        $scope.togglePrivate = function(){
            $scope.newDashboard.reachable = !$scope.newDashboard.reachable;
        };

        // submit form
        $scope.submit = function () {
            // the dashboard has not yet been created:
            // call Spectre core to create the dashboard
            // set it as current session dashboard
            // go to dashboard.edit.principal state
            UserService.dashboards.create({login:Principal.identity().login},$scope.newDashboard).$promise
                .then(function(_dashboard){


                    if( $scope.futurMembers.length > 0){
                        for ( var i=0; i<$scope.futurMembers.length ; i++) {
                             if($scope.futurMembers[i].login !== $scope.myaccount.login){
                                DashboardService.members.add({login:$scope.futurMembers[i].login,id:_dashboard.id, admin:$scope.futurMembers[i].admin},null).$promise.
                                    then(function(){

                                     });
                              }
                        }

                         DashboardModel.setCurrentDashboard(_dashboard);
                         $state.go('dashboards-my-edit');

                    }else{
                       DashboardModel.setCurrentDashboard(_dashboard);
                       $state.go('dashboards-my-edit');

                    }
                });
        };

          $scope.initCreateMembersDashboard = function () {
            $scope.users = [];
            UserService.accounts.all({}).$promise
                .then(function(allUsers){
                    $scope.users = allUsers;
                });

            var monUser = {
                login : $scope.myaccount.login,
                admin : true
            };
            $scope.futurMembers = new Array();
            $scope.futurMembers.push(monUser);
        };

         $scope.remove = function (user) {
            var index=$scope.futurMembers.indexOf(user)
            $scope.futurMembers.splice(index,1);
        };


        // modify the user status
        $scope.switchAdmin = function (user) {
            var index=$scope.futurMembers.indexOf(user);
            $scope.futurMembers[index].admin = !$scope.futurMembers[index].admin ;
        };

        // add user in the array
        $scope.addUser = function(user){
            var monUser = {
                login : user.login,
                firstname : user.firstname,
                lastname : user.lastname,
                admin : false
            };
            $scope.futurMembers.push(monUser);
        };





          Principal.checkAuthentication().then(function(){
            $scope.initCreateMembersDashboard();
        });


    });
