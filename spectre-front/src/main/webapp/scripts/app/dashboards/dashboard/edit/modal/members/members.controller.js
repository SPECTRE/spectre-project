angular.module('spectreApp')
    .controller('ModalMembersWidgetController', function ($scope, $uibModalInstance, $location, dashboard, Principal, UserService, DashboardService) {

    $scope.dashboard = dashboard;

    $scope.host = $location.host();

    $scope.port = $location.port();

    $scope.myaccount = Principal.identity();


    if($scope.dashboard.shareid == undefined){
        DashboardService.dashboard.get({id : $scope.dashboard.id })
                                    .$promise
                                    .then(function (dashboard) {
                                        $scope.dashboard.shareid = dashboard.shareid;
                                    });
    }

    $scope.copyToClipboard = function (element) {
        document.querySelector(element).select();
        document.execCommand("copy");
    }

    $scope.initEditMembersDashboard = function () {
        $scope.users = [];
        UserService.accounts.all({}).$promise
            .then(function(allLogins){
                $scope.users = allLogins;
            });
    };

    $scope.togglePrivate = function(){
         $scope.dashboard.reachable = !$scope.dashboard.reachable;
    }
    // modify the user status
    $scope.switchAdmin = function (user) {
        for(var i = 0; i < $scope.dashboard.members.length; i++){
            if($scope.dashboard.members[i].login == user.login){
                $scope.dashboard.members[i].admin = !$scope.dashboard.members[i].admin;
            }
        }
    };

    $scope.addUser = function(user){
        var newUser = {
            admin: false,
            login: user.login,
            firstname: user.firstname,
            lastname: user.lastname
        }
        $scope.dashboard.members.push(newUser);
    }

   $scope.removeUser = function (user) {
        var userIndex = null;
        for(var i = 0; i < $scope.dashboard.members.length; i++){
            if($scope.dashboard.members[i].login == user.login){
                userIndex = i-1;
            }
        }
        $scope.dashboard.members = $scope.dashboard.members.splice(userIndex,1);
    };


    $scope.ok = function () {
        $uibModalInstance.close($scope.dashboard);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    Principal.checkAuthentication().then(function(){
        $scope.initEditMembersDashboard();
    });
});
