'use strict';

angular.module('spectreApp')
    .controller('EditController', function ($rootScope, $sce, $scope, $state, $document, $uibModal, $timeout, Principal, DashboardModel, WidgetService, DashboardService) {

        $scope.unusedWidget = function (widget) {
            return widget.label == null;
        };

        $('#dashCarousel').on('slid.bs.carousel', function () {
            $(window).trigger('resize');
        });

        $scope.updateWidget = function (widget) {
            DashboardModel.setCurrentWidget(widget);
            $scope.openUpdateWidgetModal(widget);
        };

        $scope.goToShow = function () {
            $state.go('dashboards-my-show');
        };

        $scope.updateMembers = function () {
            DashboardModel.refreshDashboardMembers();
            $scope.openWidgetMembersModal();
        };

        $scope.deleteWidget = function (widget) {
            var title = "Widget " + widget.indicator.project + " - " + widget.label;
            var info = "Etes vous sûr de vouloir supprimer ce widget ?";
            $scope.openDeleteModal(title, info, function () {
                    $scope.deleteCurrentWidget(widget);
                }
            );
        }

        $scope.deleteCurrentWidget = function (widget) {
            // console.log(" widget : " + widget.id + " - " + widget.label);
            WidgetService.widgets.delete({'id': widget.id}).$promise.then(function () {
                DashboardModel.refreshWidgets();
            });
        }

        $scope.deleteDashboard = function () {
            var title = "Dashboard " + $scope.currentDashboard.name;
            var info = "Etes vous sûr de vouloir supprimer ce dashboard ?";
            $scope.openDeleteModal(title, info, $scope.deleteCurrentDashboard);

        }

        $scope.deleteCurrentDashboard = function () {
            DashboardService.dashboard.delete({'id': $scope.currentDashboard.id}).$promise.then(function () {
                $scope.refreshMyDashboards(function () {
                    if ($scope.mydashboards.length > 0) {
                        $scope.showDashboard($scope.mydashboards[0]);
                    } else {
                        DashboardModel.clearCurrentDashboard();
                        $scope.goToShow();

                    }
                });
            });
        }

        // update scope project into Spectre core and set current session project to scope project
        $scope.updateDashboard = function (_dashboard, callback) {
            DashboardService.dashboard.update({
                id: _dashboard.id
            }, _dashboard).$promise.then(function () {
                if (callback) {
                    callback();
                }
            });
        };

        $scope.editWidget = function (widget) {
            $scope.openUpdateWidgetModal(widget, "Modification", "Modifier");
        }

        $scope.addWidget = function (widget) {
            $scope.openUpdateWidgetModal(widget, "Ajout", "Ajouter");
        }

        $scope.addWidgetSet = function () {
            var dashboardId = $scope.currentDashboard.id;
            DashboardService.manageWidgetSet.addPage({
                id: dashboardId
            }, null).$promise.then(function (_widget) {
                DashboardModel.refreshWidgets();
            });
        }

        $scope.deleteWidgetSet = function () {
            var widgets = $scope.currentDashboard.widgets;
            var currentPage = parseInt($(".carousel-indicators > .active").attr("data-slide-to"));
            var lastPage = parseInt($(".carousel-indicators > li:last-child").attr("data-slide-to"));
            var widgetsPerPage = DashboardModel.getWidgetsPerPage();
            widgets.sort(function (a, b) {
                return a.gridIndex - b.gridIndex;
            });
            var startIndex = currentPage * widgetsPerPage;
            var stopIndex = startIndex + widgetsPerPage;
            var needConfirmation = false;
            for (var i = startIndex; i < stopIndex; i++) {
                if (widgets[i].label != null) {
                    needConfirmation = true;
                }
            }

            if (needConfirmation) {
                var title = "Dashboard " + $scope.currentDashboard.name + " - Suppression";
                var info = "Etes vous sûr de vouloir supprimer cette page du dashboard alors que des indicateurs sont présents ?";
                $scope.openDeleteModal(title, info, $scope.deleteCurrentWidgetSet);
            } else {
                $scope.deleteCurrentWidgetSet();
            }


        }

        $scope.deleteCurrentWidgetSet = function () {
            // Get the active widget set
            var currentPage = parseInt($(".carousel-indicators > .active").attr("data-slide-to"));
            var lastPage = parseInt($(".carousel-indicators > li:last-child").attr("data-slide-to"));
            var widgetsPerPage = DashboardModel.getWidgetsPerPage();
            var dashboardId = $scope.currentDashboard.id;
            var widgets = $scope.currentDashboard.widgets;
            var widgetsList = [];

            widgets.sort(function (a, b) {
                return a.gridIndex - b.gridIndex;
            })
            var startIndex = currentPage * widgetsPerPage;
            var stopIndex = startIndex + widgetsPerPage;
            for (var i = startIndex; i < stopIndex; i++) {
                widgetsList.push(widgets[i]);
            }
            var widgetsToDelete = {
                widgets: widgetsList
            };

            // Prevent carousel from being out of bound when deleting last page
            if (currentPage == lastPage) {
                $scope.slide("prev");
                $timeout(function () {
                    DashboardService.manageWidgetSet.deletePage({
                        id: dashboardId
                    }, widgetsToDelete).$promise.then(function (dashboard) {
                        DashboardModel.refreshWidgets();
                    });
                }, 600);
            } else {
                DashboardService.manageWidgetSet.deletePage({
                    id: dashboardId
                }, widgetsToDelete).$promise.then(function (dashboard) {
                    DashboardModel.refreshWidgets();
                });
            }

        }

        $scope.isLastWidgetPage = function () {
            var currentDashboard = DashboardModel.getCurrentDashboard();
            if ($scope.currentDashboard != null && ($scope.currentDashboard).hasOwnProperty("widgets") && currentDashboard.widgets != null) {
                var widgets = currentDashboard.widgets;
                if (widgets.length <= DashboardModel.getWidgetsPerPage()) {
                    $scope.isLastPage = true;
                } else {
                    $scope.isLastPage = false;
                }
            }
            return $scope.isLastPage;
        }

        $scope.openUpdateWidgetModal = function (widget, title, button) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/dashboards/dashboard/edit/modal/update/update.html',
                controller: 'ModalUpdateWidgetController',
                size: 'lg',
                resolve: {
                    widget: function () {
                        return widget;
                    },
                    title: function () {
                        return title;
                    },
                    dashboard: function () {
                        return $scope.currentDashboard;
                    },
                    button: function () {
                        return button;
                    }
                }
            });

            modalInstance.result.then(function (_widget) {
                DashboardModel.refreshWidgets();
            }, function () {
                DashboardModel.refreshWidgets();
            });
        };

        $scope.openWidgetMembersModal = function () {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/dashboards/dashboard/edit/modal/members/members.html',
                controller: 'ModalMembersWidgetController',
                size: 'lg',
                resolve: {
                    dashboard: function () {
                        return $scope.currentDashboard;
                    }
                }
            });

            modalInstance.result.then(function (_dashboard) {
                //$scope.currentDashboard = _dashboard;
                $scope.updateDashboard(_dashboard, function () {
                    DashboardModel.refreshDashboardMembers();
                });

            }, function () {
            });
        };

        $scope.openDeleteModal = function (title, info, deleteFunction) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/modal/delete/delete.html',
                controller: 'ModalDeleteController',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    info: function () {
                        return info;
                    }
                }
            });

            modalInstance.result.then(function () {
                deleteFunction();
            }, function () {

            });
        };


    })
;



