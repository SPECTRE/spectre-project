'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('dashboard-create', {
                parent: 'site',
                data: {
                    pageTitle: "Création d'un dashboard"
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/dashboards/dashboard/create/create.html',
                          controller: 'CreateController'
                    }
                }
            });
    });
