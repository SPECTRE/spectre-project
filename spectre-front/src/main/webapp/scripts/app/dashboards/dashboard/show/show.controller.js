'use strict';

angular.module('spectreApp')
    .controller('ShowController', function ($scope, $state, FullScreenModel) {

        //================================
        // NAVIGATION METHODS
        //================================
        $scope.isLegend = true;
        $scope.isCarouselSliding = true;

        // navigate to scope dashboard main edit page
        $scope.goToEdit = function () {
            $state.go('dashboards-my-edit');
        };

        $scope.fullScreen = function(){
            FullScreenModel.fullScreen();
        }

        $('.carousel').carousel({
            pause: null,
            interval: 8000
        })

        $('#dashCarousel').on('slid.bs.carousel', function () {
            $(window).trigger('resize');
        });

        $scope.hideLegend = function () {
            if ($scope.isLegend) {
                $( ".vis-legend" ).hide();
                $scope.isLegend = false;
            } else {
                $( ".vis-legend" ).show();
                $scope.isLegend = true;
            }
        };

        $scope.playCarousel = function () {
            $('.carousel').carousel('cycle');
            $scope.isCarouselSliding = true;
        }
        $scope.pauseCarousel = function () {
            $('.carousel').carousel('pause');
            $scope.isCarouselSliding = false;
        }

        $scope.showAutoSliding = function () {
            var lastPage = parseInt($( ".carousel-indicators > li:last-child" ).attr("data-slide-to"));
            if (lastPage > 0) {
                return true;
            }
            return false;
        }
    })
;
