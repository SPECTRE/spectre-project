angular.module('spectreApp')
    .controller('ModalUpdateWidgetController', function ($scope, $state, $uibModalInstance, widget, title, dashboard, button, Principal, IndicatorService, UserService, WidgetService) {

  $scope.widget = widget;
  $scope.dashboard = dashboard;
  $scope.label = {
    'title': title,
    'button': button,
    'type': "Type d'indicateur",
    'domain': "Domaine indicateur",
    'choix': "Choix de l'indicateur",
    'value': "Première"
  }



  // init scope:
  // -projects: all available indicators sorted by project and family, depending on the projects the current user has access to
  // -indicatorProperties: measures and labels for custom indicator
  $scope.initIndicators = function () {

       $scope.projects = null;
        $scope.indicatorProperties = null;


    if(Principal.identity() != null){
        UserService.availableIndicators.all({login: Principal.identity().login}).$promise.then(function (indicatorsMap) {
            $scope.projects = indicatorsMap
            IndicatorService.properties.get().$promise.then(function (properties) {
                $scope.indicatorProperties = properties

                  if(widget.indicator == null){
                      $scope.selectAvailable = false;
                      $scope.selectedProject = null;
                      $scope.selectedFamily = null;
                      $scope.selectedIndicator = null;
                      $scope.selectedAvailable = null;


                      $scope.families = null;
                      $scope.indicators = null;
                  }else{
                        if($scope.widget.indicator.connectorType == "custom"){
                            $scope.label.value = "Prochaine";
                            $scope.selectCustom();
                            $scope.selectAvailableMeasure($scope.getMeasureFromName($scope.widget.indicator.measure));
                            $scope.widget.unity = $scope.widget.indicator.unity;

                            if($scope.widget.indicator.measure == "TUPLE"){

                                $scope.widget.data = [];
                                for(var i = 0; i < $scope.widget.kpis[0].value.length; i++){
                                    $scope.widget.data.push({'value': null, 'label':$scope.widget.kpis[0].value[i].label });
                                }
                            }
                        }else{
                            $scope.selectProject(widget.indicator.projectName, $scope.projects[widget.indicator.projectName]);
                            $scope.selectFamily(widget.indicator.family, $scope.projects[widget.indicator.projectName][widget.indicator.family]);
                            for(var i = 0; i < $scope.projects[widget.indicator.projectName][widget.indicator.family].length; i++){
                                if($scope.projects[widget.indicator.projectName][widget.indicator.family][i].id == widget.indicator.id){
                                    $scope.selectIndicator($scope.projects[widget.indicator.projectName][widget.indicator.family][i]);
                                }
                            }
                        }
                  }
            });
        },function(error){
            console.log(error);
        });
    }
  };

  $scope.getMeasureFromName = function(name){
    var measure = null;
    for(var i = 0; i <  $scope.indicatorProperties.measures.length; i++){
        if($scope.indicatorProperties.measures[i].measure == name){
            measure = $scope.indicatorProperties.measures[i];
        }
    }
    return measure;
  }

  // update scope:
  // -families: indicator families available for selected project
  $scope.selectProject = function (project, families) {
      $scope.selectAvailable = false;
      $scope.selectedProject = project;
      $scope.selectedIndicator = null;
      $scope.label.type = project;
      $scope.label.domain = "Domaine indicateur";
      $scope.label.choix = "Choix de l'indicateur";
      $scope.selectedFamily = null;
      $scope.families = families;
      $scope.indicators = null;
      $scope.selectedAvailable = null;
  };

  // update scope:
  // -indicators: indicators available for selected project and family
  $scope.selectFamily = function (family, indicators) {
      $scope.selectedFamily = family;
      $scope.label.domain = family;
      $scope.label.choix = "Choix de l'indicateur";
      $scope.indicators = indicators;
      $scope.selectedIndicator = null;
      $scope.selectedAvailable = null;
  };

  // update scope: select custom indicator
  $scope.selectCustom = function(){
      $scope.label.type = "Manuel";
      $scope.label.domain = "Domaine indicateur";
      $scope.label.choix = "Choix de l'indicateur";
      $scope.selectAvailable = true;
      $scope.selectedProject = null;
      $scope.selectedFamily = null;
      $scope.selectedIndicator = null;
      $scope.selectedAvailable = null;
  };

  $scope.selectIndicator = function(indicator){
    $scope.selectedIndicator = indicator;
    $scope.label.choix = indicator.connector + " - " + indicator.label;
  }

  $scope.selectAvailableMeasure = function(availableMeasure){
        $scope.label.choix = availableMeasure.label;
        $scope.selectedAvailable = availableMeasure;

        if($scope.selectedAvailable.measure == 'TUPLE'){
            $scope.widget.data = [{'value': null, 'label':null }];
        }else{
            $scope.widget.data = null;
        }

  };

  $scope.addTupleData = function(){
    $scope.widget.data.push({'value': null, 'label':null });
  }

  // return true if project is active project
  $scope.isActiveProject = function(project) {
      return ($scope.selectedProject == project);
  }

  // return true if family is active family
  $scope.isActiveFamily = function(family) {
      return ($scope.selectedFamily == family);
  }

   $scope.measureOptions = [{
        name: 'numérique',
        value: 'NUMERIC'
     }, {
        name: 'pourcentage',
        value: 'PERCENT'
     }, {
        name: 'texte',
        value: 'TEXT'
     }];

     // set widget data
     $scope.selectMood = function(mood){
         $scope.widget.data = mood;
     };

     $scope.isMood = function(mood){
        return $scope.widget.data == mood;
     }

     $scope.updateWidget = function() {
         $scope.widget.indicatorId = $scope.selectedIndicator.id;
         $scope.widget.label = $scope.selectedIndicator.label;
         WidgetService.widgets.update({
              id: $scope.widget.id,
          }, $scope.widget).$promise.then(function(_widget){
                $scope.ok();
          });
     };

     $scope.updateCustomWidget = function() {
         $scope.widget.measure = $scope.selectedAvailable.measure;
         if($scope.selectedAvailable.measure == 'TUPLE'){
             $scope.widget.dataMeasure = $scope.widget.dataMeasure.value;
                  WidgetService.customTupleWidgets.update({
                       id: $scope.widget.id,
                   }, $scope.widget).$promise.then(function(_widget){
                         $scope.ok();
                   });
         }else{
          WidgetService.customWidgets.update({
               id: $scope.widget.id,
           }, $scope.widget).$promise.then(function(_widget){
                 $scope.ok();
           });
         }
      };


      $scope.ok = function () {
        $uibModalInstance.close(widget);
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };

      $scope.initIndicators();
});
