'use strict';

angular.module('spectreApp')
    .controller('DashboardsController', function ($rootScope, $scope, $state, $stateParams, $timeout, $interval, $window, Principal, localStorageService, DashboardService, UserService, DashboardModel, FullScreenModel, RefreshModel) {

        //================================
        // PAGE INITIALIZATION
        //================================

        $scope.widgetPages = [];
        $scope.widgetIndexes = [];

        $scope.widgetsNeedRefresh = false;
        $scope.dashboardNeedRefresh = false;
        $scope.isFullScreen = FullScreenModel.getFullScreen();
        $scope.currentDashboard = DashboardModel.getCurrentDashboard();

        $scope.Timer = [];

        DashboardModel.subscribeDashboardEvent($scope, function () {
            $scope.currentDashboard = DashboardModel.getCurrentDashboard();
            if ($scope.currentDashboard != null) {
                DashboardModel.refreshWidgets();

                if ($scope.currentDashboard.admin) {
                    DashboardModel.refreshDashboardMembers();
                }
            }
        });

        DashboardModel.subscribeDashboardMembersEvent($scope, function () {
            $scope.currentDashboard.members = DashboardModel.getCurrentDashboard().members;
        });


        DashboardModel.subscribeDashboardWidgetsEvent($scope, function () {
            $scope.currentDashboard.widgets = DashboardModel.getCurrentDashboard().widgets;
            if ($scope.currentDashboard != null && $scope.currentDashboard.widgets != null) {
                var widgetPagesTemp = [];
                var widgetIndexesTemp = [];

                var widgetsPerPage = DashboardModel.getWidgetsPerPage();

                // Round a number upward to its nearest integer
                var maxWidgetPages = Math.ceil(DashboardModel.getCurrentDashboard().widgets.length / widgetsPerPage);
                var index = 0;
                for (var i = 0; i < maxWidgetPages; i++) {
                    widgetPagesTemp.push(i);
                    widgetIndexesTemp.push(index);
                    index += widgetsPerPage;
                }

                if ($scope.widgetPages.length != widgetPagesTemp.length) {
                    $scope.widgetPages = widgetPagesTemp;
                    $scope.widgetIndexes = widgetIndexesTemp;
                }

                for (var i = 0; i < $scope.currentDashboard.widgets.length; i++) {
                    if ($scope.currentDashboard.widgets[i].indicator != null) {
                        DashboardModel.refreshWidgetData(i, $scope.currentDashboard.widgets[i]);
                    }
                }
            }
        });

        FullScreenModel.subscribeFullScreenEvent($scope, function () {
            $scope.isFullScreen = FullScreenModel.getFullScreen();
        });

        DashboardModel.subscribeDashboardWidgetDataEvent($scope, function (event, data) {
            if (data != null) {
                $scope.currentDashboard.widgets[data.index].trend = data.widget.trend;
                $scope.currentDashboard.widgets[data.index].graph = data.widget.graph;
                $scope.currentDashboard.widgets[data.index].kpis = data.widget.kpis;
                $scope.currentDashboard.widgets[data.index].loaded = data.widget.loaded;
                $(window).trigger('resize');
            }

        });


        $scope.startAutoTuple = function (widgets) {
            angular.forEach($scope.Timer, function (value, key) {
                $timeout.cancel($scope.Timer[key]);
            });
            angular.forEach(widgets, function (widget, key) {
                if (widget.hasOwnProperty("slides")) {
                    $scope.toggleAutoTuple(widget);
                }
            })
        }

        $scope.toggleAutoTuple = function (widget) {
            if (widget.slides.isAuto) {
                $scope.Timer[widget.id] = $timeout(function () {
                    $scope.slideTuple('next', widget, false);
                    $scope.toggleAutoTuple(widget);
                }, DashboardModel.getTupleRefreshDelay());
            }
        }

        $scope.switchAutoTuple = function (widget) {
            if (!$scope.isTupleOn(widget)) {
                widget.slides.isAuto = true;
            } else {
                $timeout.cancel($scope.Timer[widget.id]);
                widget.slides.isAuto = false;
            }
        }

        $scope.isTupleOn = function (widget) {

            if (typeof widget.slides == 'undefined' || typeof widget.slides.isAuto == 'undefined' || !widget.slides.isAuto) {
                return false;
            }
            return true;
        }


        $scope.slideTuple = function (dir, widget, fromUser) {
            if (dir == 'prev') {
                if (widget.slides.active == 0) {
                    widget.slides.active = widget.kpis[0].value.length - 1;
                } else {
                    widget.slides.active--;
                }
            } else {
                if (widget.slides.active == widget.kpis[0].value.length - 1) {
                    widget.slides.active = 0;
                } else {
                    widget.slides.active++;
                }
            }
            if (fromUser && $scope.isTupleOn(widget)) {
                $timeout.cancel($scope.Timer[widget.id]);
                $scope.switchAutoTuple(widget);
            }
        }


        //================================
        // NAVIGATION METHODS
        //================================
        $scope.goToMy = function () {
            $scope.criteria = '';
            $state.go('dashboards-my-show');
        };

        $scope.goToPublic = function () {
            $scope.criteria = '';
            $state.go('dashboards-public-show');
        };

        $scope.goToCreate = function () {
            $scope.criteria = '';
            $state.go('dashboard-create');
        };

        $scope.goToEdit = function () {
            if ($scope.isMyTabSelected) {
                $state.go('dashboards-my-edit');
            } else {
                $state.go('dashboards-public-edit');
            }
        }

        $scope.goToShow = function () {
            if ($scope.isMyTabSelected) {
                $state.go('dashboards-my-show');
            } else {
                $state.go('dashboards-public-show');
            }
        }


        //================================
        // METHODS
        //================================

        // return true if selected tab is equal to tab, false otherwise
        $scope.isMyTabSelected = function () {
            return $state.current.name === 'dashboards-my-show' || $state.current.name === 'dashboards-my-edit';
        };

        $scope.isPublicTabSelected = function () {
            return $state.current.name === 'dashboards-public-show' || $state.current.name === 'dashboards-public-edit';
        };

        $scope.isDashboardSelected = function (dashboard) {
            if (DashboardModel.getCurrentDashboard() != null) {
                return DashboardModel.getCurrentDashboard().id == dashboard.id;
            } else {
                return false;
            }
        };

        $scope.canScrollUp = function() {
            return ($('#dashboards-list').scrollTop() > 0);
        };

        $scope.canScroll = function() {
            return ($('#dashboards-list')[0].scrollHeight > $('.dashboards-nav')[0].scrollHeight - 110);
        };

        $scope.scrollDown = function() {
            var currentPosition = $('#dashboards-list').scrollTop();
            $('#dashboards-list').animate({scrollTop: currentPosition + 85}, 100);
        };

        $scope.scrollUp = function() {
            var currentPosition = $('#dashboards-list').scrollTop();
            $('#dashboards-list').animate({scrollTop: currentPosition - 85}, 100);
        };

        $scope.refreshCurrentDashboard = function () {
            $scope.currentDashboard = DashboardModel.getCurrentDashboard();
        };

        // clear current session dashboard, clear scope public dashboards and favourite dashboards
        // call subInitFunction
        $scope.initDash = function (subInitFunction) {
            Principal.checkAuthentication().then(function (data) {

                $scope.mydashboards = [];
                $scope.myfavourites = [];
                $scope.myfavouritesId = [];
                $scope.publicdashboards = [];
                subInitFunction();
            }, function (error) {
            });
        };

        // get all user favourite dashboards from Spectre core
        $scope.refreshFavouritesDashboards = function () {
            $scope.myfavourites = [];
            $scope.myfavouritesId = [];

            UserService.favouritesDashboards.all({login: Principal.identity().login}).$promise
                .then(function (dashboards) {
                    for (var i = 0; i < dashboards.length; i++) {
                        var dashboard = dashboards[i];
                        $scope.myfavourites.push(dashboard);
                        $scope.myfavouritesId.push(dashboard.id);
                    }
                }, function (error) {
                });
        };

        // get all public dashboards from Spectre core
        $scope.refreshPublicDashboards = function (callbackFunction) {
            $scope.publicdashboards = [];

            DashboardService.dashboard.allpublic().$promise.then(function (publicDashboards) {
                for (var i = 0; i < publicDashboards.length; i++) {
                    var publicDash = publicDashboards[i];
                    $scope.publicdashboards.push(publicDash);
                }
                callbackFunction();
            });
        };

        // get all dashboards from Spectre core where user is member of
        $scope.refreshMyDashboards = function (callbackFunction) {
            $scope.mydashboards = [];

            UserService.membersDashboards.all({login: Principal.identity().login}).$promise.then(function (dashboardResult) {
                for (var i = 0; i < dashboardResult.length; i++) {
                    var memberDashboard = dashboardResult[i];
                    $scope.mydashboards.push(memberDashboard);
                }
                callbackFunction();
            });

        };

        //set dashboard as current session dashboard
        $scope.showDashboard = function (dashboard) {
            $scope.goToFirst();
            $timeout(function () {
                DashboardModel.setCurrentDashboard(dashboard);
            }, 600);

        };


        // remove dashboard from user favourite dashboards into Spectre core
        $scope.removeFavourite = function (dashboard) {
            DashboardService.favourites.remove({
                login: Principal.identity().login,
                id: dashboard.id
            }).$promise.then(function () {
                $scope.refreshFavouritesDashboards();
            });
        };

        // add dashboard to user favourite dashboards into Spectre core
        $scope.addFavourite = function (dashboard) {
            DashboardService.favourites.add({
                login: Principal.identity().login,
                id: dashboard.id
            }, null).$promise.then(function () {
                $scope.refreshFavouritesDashboards();
            });
        };

        $scope.isNumericWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'NUMERIC');
        };

        $scope.isPercentWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'PERCENT');
        };

        $scope.isMoodWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'MOOD');
        };

        $scope.isTupleWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'TUPLE');
        };

        $scope.isTextWidget = function (widget) {
            return (widget.indicator !== undefined
            && widget.indicator !== null
            && widget.indicator.measure !== undefined
            && widget.indicator.measure === 'TEXT');
        };

        $scope.isNumericOrPercentTupleWidget = function (widget) {
            return (widget !== undefined
            && widget.kpis !== undefined
            && widget.kpis.length > 0
            && widget.kpis[0].measure !== 'TEXT');
        };


        $scope.slide = function (dir) {
            $scope.active = null;
            $('#dashCarousel').carousel(dir);
        };

        $scope.goToTuple = function (widget, index) {
            widget.slides.active = index;
        }

        $scope.isTupleSlideActive = function (widget, index) {
            return widget.slides.active == index;
        }

        $scope.flipp = function (widget) {
            widget.isFlipped = !widget.isFlipped;
        }

        $scope.getTupleIndexTab = function (widget) {
            var tab = [];
            if ($scope.isTupleWidget(widget)) {
                for (var i = 0; i < widget.kpis[0].value.length; i++) {
                    tab.push(i);
                }
            }
            return tab;
        }

        $scope.goToFirst = function () {
            $('.carousel').carousel(0);
        }

    });
