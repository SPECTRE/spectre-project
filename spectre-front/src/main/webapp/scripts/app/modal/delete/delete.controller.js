angular.module('spectreApp')
    .controller('ModalDeleteController', function ($scope, $uibModalInstance, title, info ) {

    $scope.title = title;
    $scope.info = info;

    $scope.ok = function () {
        $uibModalInstance.close();
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss();
    };


});
