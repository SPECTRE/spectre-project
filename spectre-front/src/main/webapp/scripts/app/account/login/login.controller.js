'use strict';

angular.module('spectreApp')
    .controller('LoginController', function ($rootScope, $scope, $state, $timeout, Principal, AuthServerProvider, SessionModel) {

        $scope.errors = {};
        $scope.success = null;
        $scope.error = null;

       // Detect Internet Explorer 6-11
        $scope.isIE = /*@cc_on!@*/false || !!document.documentMode;

        $rootScope.$on('sessionExpired', function (event, isSessionExpired) {
            SessionModel.setIsSessionExpired(isSessionExpired);
        });

        $scope.sessionExpired = function () {
            return SessionModel.getIsSessionExpired();
        }

        $scope.authenticationError = function(){
            return $scope.error != null;
        }

        $timeout(function (){angular.element('[ng-model="username"]').focus();});
        $scope.login = function (event) {
            event.preventDefault();
            var credentials = {
                username:$scope.username,
                password : $scope.password
            };
            AuthServerProvider.login(credentials)
                .then(function(account){
                    SessionModel.setIsSessionExpired(false);
                    Principal.authenticate(account.username);
                    $state.go('dashboards-my-show');
                }, function(error) {
                    $scope.error = error;
                });
        };


    });
