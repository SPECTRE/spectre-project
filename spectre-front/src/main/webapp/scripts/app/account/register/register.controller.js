'use strict';

angular.module('spectreApp')
    .controller('RegisterController', function ($scope, $timeout, AuthServerProvider) {
        $scope.success = null;
        $scope.error = null;
        $scope.doNotMatch = null;
        $scope.errorUserExists = null;
         $scope.errorEmailExists = null;

        $scope.registerAccount = {};
        $timeout(function (){angular.element('[ng-model="registerAccount.login"]').focus();});

        $scope.register = function () {
            if ($scope.registerAccount.password !== $scope.confirmPassword) {
                $scope.doNotMatch = true;
            } else {

                $scope.doNotMatch = null;
                $scope.error = null;
                $scope.errorUserExists = null;
                $scope.errorEmailExists = null;

                AuthServerProvider.createAccount($scope.registerAccount,function(res){
                    if(res.success === true){
                         $scope.success = true;
                    }else{
                         if(res.data.status == '406'){
                            $scope.errorEmailExists = true;
                         }else if (res.data.status == '409'){
                            $scope.errorUserExists = true;
                         }else{
                            $scope.error = true;
                         }

                    }

                });
            }
        };
    });
