'use strict';

angular.module('spectreApp')
    .controller('ResetController', function ($scope, $location, Reset) {

        $scope.doNotMatch = null;
        $scope.success = false;
        $scope.error = false;

        $scope.resetPassword = function () {
            if ($scope.password !== $scope.confirmPassword) {
                $scope.doNotMatch = 'ERROR';
            } else {
                $scope.doNotMatch = null;
                var key = $location.search().key;
                var params = {
                    key: key,
                    newPassword: $scope.password
                }
                Reset.finish.resetPassword({}, params).$promise.then(function(data) {
                    $scope.success = true;
                    $scope.error = false;
                },function(failure){
                    $scope.success = false;
                    $scope.error = true;
                })
            }

        }
    });
