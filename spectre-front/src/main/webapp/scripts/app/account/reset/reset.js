'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('reset', {
                parent: 'account',
                url: '/reset',
                data: {
                    authorities: [],
                    pageTitle: 'Reset'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/reset/reset.html',
                        controller: 'ResetController'
                    }
                },
                resolve: {

                }
            });
    });
