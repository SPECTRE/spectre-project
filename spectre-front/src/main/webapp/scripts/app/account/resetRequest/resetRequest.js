'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('resetRequest', {
                parent: 'account',
                url: '/resetRequest',
                data: {
                    authorities: [],
                    pageTitle: 'Reset Request'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/account/resetRequest/resetRequest.html',
                        controller: 'ResetRequestController'
                    }
                },
                resolve: {

                }
            });
    });
