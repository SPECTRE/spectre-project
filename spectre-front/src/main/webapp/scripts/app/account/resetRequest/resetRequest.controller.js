'use strict';

angular.module('spectreApp')
    .controller('ResetRequestController', function ($scope, $location, Reset) {

        $scope.success = false;
        $scope.error = false;

        $scope.reset = function () {
            var request = {
                mail: $scope.email
            }
            Reset.init.resetPassword({}, request).$promise.then(function(data) {
                $scope.success = true;
                $scope.error = false;
            },function(failure){
                $scope.success = false;
                $scope.error = true;
            })
        }
    });
