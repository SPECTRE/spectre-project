'use strict';

angular.module('spectreApp', ['LocalStorageModule',
    'ngResource', 'ngCookies', 'ngAria', 'ngCacheBuster',
    'ui.bootstrap', 'ui.router', 'angular-loading-bar', 'angular-jwt','connectorParameterFilters','ngVis','FBAngular','ez.timepicker'])

    .run(function ($rootScope, $location, $window, $http, $state, Principal, ENV, AuthServerProvider) {

        $rootScope.ENV = ENV;

        $rootScope.$on('$stateChangeStart', function (event, toState, toStateParams) {
            $rootScope.toState = toState;
            $rootScope.toStateParams = toStateParams;
        });

        $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
            var titleKey = 'spectre';

            // Remember previous state unless we've been redirected to login or we've just
            // reset the state memory after logout. If we're redirected to login, our
            // previousState is already set in the authExpiredInterceptor. If we're going
            // to login directly, we don't want to be sent to some previous state anyway
            $rootScope.previousStateName = fromState.name;
            $rootScope.previousStateParams = fromParams;
            if (!AuthServerProvider.hasValidToken()) {
                if(toState.url !== '/register' && toState.url !== '/reset' && toState.url !== '/resetRequest' && toState.url !== '/release' && !/\/dashboards\/public\/shared/.test(toState.url)) {
                    $state.go('login');
                }
            }else{
                if(toState.url === '/login'){
                    $state.go('dashboards-my-show');
                }
            }

            // Set the page title key to the one configured in state or use default one
            if (toState.data.pageTitle) {
                titleKey = toState.data.pageTitle;
            }
            $window.document.title = titleKey;
        });
    })
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, httpRequestInterceptorCacheBusterProvider, jwtInterceptorProvider) {

        $httpProvider.defaults.xsrfCookieName= 'CSRF-TOKEN';
        $httpProvider.defaults.xsrfHeaderName= 'X-CSRF-TOKEN';

        //Cache everything except rest api requests
        httpRequestInterceptorCacheBusterProvider.setMatchlist([/.*kpis.*/, /.*protected.*/, /.*indicators.*/, /.*dashboards.*/, /.*connectors.*/, /.*projects.*/, /.*widgets.*/, /.*accounts.*/], true);

        $urlRouterProvider.otherwise('/login');

        $stateProvider.state('site', {
            'abstract': true,
            views: {
                'navbar@': {
                    templateUrl: 'scripts/components/navbar/navbar.html',
                    controller: 'NavbarController'
                },
                'footer@': {
                    templateUrl: 'scripts/components/footer/footer.html',
                    controller: 'FooterController'
                }
            }
        });

        jwtInterceptorProvider.tokenGetter = ['localStorageService', 'jwtHelper', '$http', 'ENV', function(localStorageService, jwtHelper, $http, ENV) {
            var token = localStorageService.get('spectre_token');
            if (token) {
                if (jwtHelper.isTokenExpired(token)) {
                    return $http({
                        url: 'http://' + ENV.coreEndpoint + '/token/basic',
                        method: 'POST',
                        skipAuthorization: true
                    }).then(function (response) {
                        token = response.data.entity;
                        localStorageService.set('spectre_token', response.accessToken);
                        return token;
                    }, function (error) {
                    });
                } else {
                    return token;
                }
            } else {
                return null;
            }
        }]

        jwtInterceptorProvider.authHeader = 'X-Auth-Token';
        jwtInterceptorProvider.authPrefix = '';
        $httpProvider.interceptors.push('jwtInterceptor');
    })
    .config(['localStorageServiceProvider', function(localStorageServiceProvider){
        localStorageServiceProvider.setPrefix('spectre');
    }])
    .config(['$urlMatcherFactoryProvider', function ($urlMatcherFactory) {
        $urlMatcherFactory.type('boolean', {
            name: 'boolean',
            decode: function (val) {
                return val == true ? true : val == "true" ? true : false
            },
            encode: function (val) {
                return val ? 1 : 0;
            },
            equals: function (a, b) {
                return this.is(a) && a === b;
            },
            is: function (val) {
                return [true, false, 0, 1].indexOf(val) >= 0
            },
            pattern: /bool|true|0|1/
        });
    }]);
;
