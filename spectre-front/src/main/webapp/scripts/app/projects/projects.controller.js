'use strict';

angular.module('spectreApp')
    .controller('ProjectsController', function ($scope, $state, Principal, UserService, ProjectModel, ProjectService, IndicatorValueService) {

        $scope.currentProject = ProjectModel.getCurrentProject();
        $scope.currentProjects = [];
        $scope.carouselInfo = {
            'indexes': [],
            'pages': []
        }

        ProjectModel.subscribeProjectEvent($scope, function () {
            $scope.currentProject = ProjectModel.getCurrentProject();
        });


        $scope.goToDashboards = function () {
            $state.go("dashboards-my-show");
        }

        $scope.goToMy = function () {
            $state.go("projects-my");
        }

        $scope.goToPublic = function () {
            $state.go("projects-public");
        }

        $scope.deleteOldIndicators = function () {
            IndicatorValueService.kpis.delete();
        }

        $scope.isMyTabSelected = function () {
            return $state.current.name === 'projects-my';
        };

        $scope.isPublicTabSelected = function () {
            return $state.current.name === 'projects-public';
        };


        $scope.bodyStyle = 'projects-background';
        $scope.initProject = function (subInitFunction) {
            Principal.checkAuthentication().then(function (data) {

                ProjectModel.clearCurrentProject();
                $scope.currentProjects = [{'name': 'ADDNEW9876'}];
                $scope.myprojects = [];
                $scope.myfavourites = [];
                $scope.myfavouritesId = [];
                $scope.publicprojects = [];
                $scope.carouselInfo = {
                    'indexes': [],
                    'pages': []
                }

                subInitFunction();
            }, function (error) {
            });
        };

        // get all user favourite projects from Spectre core
        $scope.refreshFavouritesProjects = function () {
            $scope.myfavourites = [];
            $scope.myfavouritesId = [];

            UserService.favouritesProjects.all({login: Principal.identity().login}).$promise
                .then(function (projects) {
                    for (var i = 0; i < projects.length; i++) {
                        var project = projects[i];
                        $scope.myfavourites.push(project);
                        $scope.myfavouritesId.push(project.id);
                        $scope.addToCurrentProjects(project);

                    }
                    $scope.refreshCarouselIndexes();
                }, function (error) {
                    console.log("TODO : gestion des erreurs");
                    console.log(error);
                });
        };

        // get all public projects from Spectre core
        $scope.refreshPublicProjects = function () {
            $scope.publicprojects = [];

            ProjectService.project.allpublic().$promise.then(function (publicProjects) {
                for (var i = 0; i < publicProjects.length; i++) {
                    var publicProject = publicProjects[i];
                    $scope.publicprojects.push(publicProject);
                    $scope.addToCurrentProjects(publicProject);
                }
                $scope.refreshCarouselIndexes();
            })
        };

        $scope.refreshMyProjects = function () {
            $scope.myprojects = [];

            UserService.membersProjects.all({login: Principal.identity().login}).$promise.then(function (projectResult) {
                for (var i = 0; i < projectResult.length; i++) {
                    var memberProject = projectResult[i];
                    $scope.myprojects.push(memberProject);
                    $scope.addToCurrentProjects(memberProject, true);

                }
                $scope.refreshCarouselIndexes();
            });
        };

        // remove project from user favourite projects into Spectre core
        $scope.removeFavourite = function (project) {
            ProjectService.favouritesProjects.remove({
                login: Principal.identity().login,
                id: project.id
            }).$promise.then(function () {
                $scope.refreshFavouritesProjects();
            })
        };

        // add project to user favourite projects into Spectre core
        $scope.addFavourite = function (project) {
            ProjectService.favouritesProjects.add({
                login: Principal.identity().login,
                id: project.id
            }, null).$promise.then(function () {
                $scope.refreshFavouritesProjects();
            })
        };

        $scope.isFavourite = function (project) {
            return $scope.myfavouritesId.indexOf(project.id) > -1;
        };

        $scope.slide = function (dir) {
            $scope.active = null;
            $('#projCarousel').carousel(dir);
        };

        $scope.addToCurrentProjects = function (project, force) {
            var canAdd = true;
            for (var i = 0; i < $scope.currentProjects.length; i++) {
                if (project.id == $scope.currentProjects[i].id) {
                    canAdd = false;
                    if (force) {
                        $scope.currentProjects[i] = project;
                    }
                }
            }
            if (canAdd) {
                $scope.currentProjects.unshift(project);
            }
        }


        $scope.refreshCarouselIndexes = function () {
            // Number of slides for a slides with 12 projects max
            var maxProjectsPerSlide = 12;


            var pages = parseInt($scope.currentProjects.length / maxProjectsPerSlide);
            var last = $scope.currentProjects.length % maxProjectsPerSlide;

            if (last !== 0) {
                pages++;
            }
            $scope.carouselInfo.indexes = [];
            $scope.carouselInfo.pages = [];
            for (var i = 0; i < pages; i++) {
                $scope.carouselInfo.indexes.push(i * maxProjectsPerSlide);
                $scope.carouselInfo.pages.push(i);
            }

        }

        angular.element( document.querySelector( '#spectre-page-wrap' ) ).addClass('projects-background');

    });
