angular.module('spectreApp')
    .controller('ModalAddConnectorProjectController', function ($scope, $state, $uibModalInstance, Principal, ConnectorsTypeService, project, connector, ArrayService,ProjectService, ConnectorService) {

    $scope.project = project;
    $scope.connector = connector;
    $scope.selectedType = null;
    $scope.selectedConnectorParams = null;
    $scope.carouselInfo = null;
    $scope.showHelp = false;
    $scope.isConnectorValid = false;
    $scope.connectionError = false;
    $scope.showSpinner = false;



     $scope.getImageType = function (type) {
            return "assets/images/logo-" + type + ".png"
        };

   $scope.initConnectors = function () {
        $scope.types = [];
        ConnectorsTypeService.connectors.all()
            .$promise
            .then(function (connectorsTypes) {
                for (var i = 0; i < connectorsTypes.length; ++i) {

                    var type = connectorsTypes[i];
                    $scope.types.push(type);
                }
                $scope.refreshCarouselIndexes();
            });
    };

    $scope.chooseType = function(type){
        $scope.selectedType = type;
        $scope.connector.name = null;
        $scope.connector.description = null;
        $scope.connector.type = $scope.selectedType.type;
        $scope.connector.params = $scope.selectedType;
        $scope.isConnectorValid = false;
        $scope.connectionError = false;
        $scope.connectorParams = ArrayService.paramToArray($scope.selectedType.requiredParameters);
    }

    $scope.isFormValid = function() {
        if($scope.connector.periodicity > 60){
            for (var i = 0; i < $scope.connectorParams.length; i++) {
                if ($scope.connectorParams[i].objKey.mandatory && !$scope.connectorParams[i].objValue) {
                    return false;
                }
            }
            return true;
        }else{
            return false;
        }
    };


    $scope.slide = function (dir) {
        $scope.active = null;
        $('#connectorCarousel').carousel(dir);
    };


    $scope.needHelp = function(){
        $scope.showHelp = !$scope.showHelp;
    }


    $scope.initConnectors();

    $scope.ok = function(){
        var configParams = {};
        for (var i = 0; i < $scope.connectorParams.length; i++) {
            configParams[$scope.connectorParams[i].objKey.name] = $scope.connectorParams[i].objValue;
        }
        $scope.connector.configurationParams = configParams;

        ProjectService.connectors.create({id:$scope.project.id},$scope.connector).$promise.then(function(connector){
            ConnectorService.fetch.post({id:connector.id},null).$promise.then(function(){

            });
            $uibModalInstance.close({'project':$scope.project, 'connector':$scope.connector});

        })




    }

  $scope.cancel = function () {
    $uibModalInstance.dismiss({'project':$scope.project});
  };

  $scope.refreshCarouselIndexes = function(){
      // Number of slides for a slides with 12 projects max
      var maxProjectsPerSlide = 4;


      var pages = parseInt($scope.types.length / maxProjectsPerSlide );
      var last = $scope.types.length % maxProjectsPerSlide;

      if(last !== 0){
          pages++;
      }
      $scope.carouselInfo = {
        'indexes':[],
        'pages':[]
      }

      for(var i = 0; i < pages; i++){
          $scope.carouselInfo.indexes.push(i*maxProjectsPerSlide);
          $scope.carouselInfo.pages.push(i);
      }

  }

  $scope.getHelpPath = function(){
    return "scripts/app/projects/nav/my/modal/add-connector/help/" + $scope.selectedType.type + ".html";
  }

  $scope.testConnector = function(){
        $scope.showSpinner = true;
        var configParams = {};
        for (var i = 0; i < $scope.connectorParams.length; i++) {
            configParams[$scope.connectorParams[i].objKey.name] = $scope.connectorParams[i].objValue;
        }
        $scope.connector.configurationParams = configParams;

        ProjectService.connector.test({id:$scope.project.id},$scope.connector).$promise.then(function(connector){
            $scope.isConnectorValid = true;
            $scope.connectionError = false;
            $scope.showSpinner = false;
        },function(failure){
            $scope.isConnectorValid = false;
            $scope.connectionError = true;
            $scope.showSpinner = false;
        })
  }




});
