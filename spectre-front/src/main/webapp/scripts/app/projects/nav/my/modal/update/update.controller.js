angular.module('spectreApp')
    .controller('ModalUpdateProjectController', function ($scope, $state, $uibModalInstance, $uibModal, ProjectService, Principal, UserService, project, ProjectModel) {

        $scope.project = project;

        $scope.getImageType = function (type) {
            return "assets/images/logo-" + type + ".png"
        };

        $scope.myaccount = Principal.identity();

        $scope.initEditMembersProject = function () {
            $scope.users = [];
            UserService.accounts.all({}).$promise
                .then(function (allLogins) {
                    $scope.users = allLogins;
                });
        };

        $scope.initEditMembersProject();

        // modify the user status
        $scope.switchAdmin = function (user) {
            for (var i = 0; i < $scope.project.members.length; i++) {
                if ($scope.project.members[i].login == user.login) {
                    $scope.project.members[i].admin = !$scope.project.members[i].admin;
                }
            }
        };

        $scope.addUser = function (user) {
            var newUser = {
                admin: false,
                login: user.login,
                firstname: user.firstname,
                lastname: user.lastname
            }
            $scope.project.members.push(newUser);
        }

        $scope.removeUser = function (user) {
            var userIndex = null;
            for (var i = 0; i < $scope.project.members.length; i++) {
                if ($scope.project.members[i].login == user.login) {
                    userIndex = i - 1;
                }
            }
            $scope.project.members = $scope.project.members.splice(userIndex, 1);
        };

        $scope.togglePrivate = function () {
            $scope.project.reachable = !$scope.project.reachable;
        };


        $scope.addConnector = function () {
            $uibModalInstance.close({'next': 'add-connector', 'project': $scope.project});
        };

        $scope.editConnector = function (connector) {
            $uibModalInstance.close({'next': 'edit-connector', 'project': $scope.project, 'connector': connector});
        };

        $scope.ok = function () {
            $uibModalInstance.close({'next': 'save-project', 'project': $scope.project});
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.deleteConnector = function (connector) {
            var title = "Projet " + project.name + " - Suppression du connecteur " + connector.name;
            var info = "Etes vous sûr de vouloir supprimer ce connecteur ?";
            $scope.openDeleteModal(title, info, function () {
                $scope.deleteCurrentConnector(connector)
            });
        }

        $scope.deleteCurrentConnector = function (connector) {
            ProjectService.connectors.delete({id: $scope.project.id, idConnector: connector.id}).$promise.then(function () {
                ProjectModel.refreshProjectConnectors($scope.project).then(function (_connectors) {
                    $scope.project.connectors = _connectors;
                })
            });
        };
        $scope.cloneConnector = function (connector) {
            $uibModalInstance.close({'next': 'clone-connector', 'project': $scope.project, 'connector': connector});
        };

        $scope.openDeleteModal = function (title, info, deleteFunction) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/modal/delete/delete.html',
                controller: 'ModalDeleteController',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    info: function () {
                        return info;
                    }
                }
            });

            modalInstance.result.then(function () {
                deleteFunction();
            }, function () {

            });
        };


    });
