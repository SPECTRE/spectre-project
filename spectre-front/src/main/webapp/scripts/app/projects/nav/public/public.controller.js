'use strict';

angular.module('spectreApp')
    .controller('ProjectsPublicController', function ($rootScope, $scope) {

        $scope.initPublicProjects = function(){
            $scope.refreshPublicProjects();
            $scope.refreshFavouritesProjects();
        };


        //================================
        // PAGE INITIALIZATION
        //================================

        $scope.initProject($scope.initPublicProjects);

    });
