angular.module('spectreApp')
    .controller('ModalEditConnectorProjectController', function ($scope, $state, $uibModalInstance, Principal, ConnectorsTypeService, project, connector, ArrayService, ConnectorService, ProjectService) {

        $scope.project = project;
        $scope.connector = connector;
        $scope.selectedType = null;
        $scope.selectedConnectorParams = [];
        $scope.isConnectorValid = false;
        $scope.connectionError = false;
        $scope.showSpinner = false;
        $scope.showHelp = false;


        $scope.getImageType = function (type) {
            return "assets/images/logo-" + type + ".png"
        };

        $scope.needHelp = function () {
            $scope.showHelp = !$scope.showHelp;
        }


        $scope.getHelpPath = function () {
            return "scripts/app/projects/nav/my/modal/add-connector/help/" + $scope.selectedType.type + ".html";
        }

        $scope.initConnectors = function () {

            ConnectorService.adminConnector.get({id: $scope.connector.id}).$promise.then(function (connector) {
                $scope.connector = connector;
                // get properties (mandatory / secured) for connector parameters
                ConnectorsTypeService.connectors.all().$promise.then(function (connectorsTypes) {
                    $scope.connectorParams = [];
                    for (var i = 0; i < connectorsTypes.length; i++) {
                        if ($scope.connector.type.localeCompare(connectorsTypes[i].type) == 0) {
                            $scope.selectedType = connectorsTypes[i];
                            for (var j = 0; j < connectorsTypes[i].requiredParameters.length; j++) {
                                var value = $scope.connector.configurationParams[connectorsTypes[i].requiredParameters[j].name];
                                var key = {
                                    name: connectorsTypes[i].requiredParameters[j].name,
                                    mandatory: connectorsTypes[i].requiredParameters[j].mandatory,
                                    secured: connectorsTypes[i].requiredParameters[j].secured
                                };
                                $scope.connectorParams.push({
                                    objKey: key,
                                    objValue: value
                                });
                            }
                        }
                    }
                });
            })
        };

        $scope.isFormValid = function () {
            for (var i = 0; i < $scope.connectorParams.length; i++) {
                if ($scope.connectorParams[i].objKey.mandatory && !$scope.connectorParams[i].objValue) {
                    return false;
                }
            }
            return true;
        };


        $scope.initConnectors();

        $scope.ok = function () {
            var configParams = {};
            for (var i = 0; i < $scope.connectorParams.length; i++) {
                configParams[$scope.connectorParams[i].objKey.name] = $scope.connectorParams[i].objValue;
            }
            $scope.connector.configurationParams = configParams;


            ConnectorService.connector.update({id: $scope.connector.id}, $scope.connector).$promise.then(function () {
                ConnectorService.fetch.post({id: $scope.connector.id}, null).$promise.then(function () {

                });
                $uibModalInstance.close({'project': $scope.project, 'connector': $scope.connector});
            });

        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss({'project': $scope.project});
        };

        $scope.testConnector = function () {
            $scope.showSpinner = true;
            var configParams = {};
            for (var i = 0; i < $scope.connectorParams.length; i++) {
                configParams[$scope.connectorParams[i].objKey.name] = $scope.connectorParams[i].objValue;
            }
            $scope.connector.configurationParams = configParams;

            ProjectService.connector.test({id: $scope.project.id}, $scope.connector).$promise.then(function (connector) {
                $scope.isConnectorValid = true;
                $scope.connectionError = false;
                $scope.showSpinner = false;
            }, function (failure) {
                $scope.isConnectorValid = false;
                $scope.connectionError = true;
                $scope.showSpinner = false;
            })
        }


    });
