'use strict';

angular.module('spectreApp')
    .controller('ProjectsMyController', function ($rootScope, $scope, $state, ProjectModel, $uibModal, Principal, UserService, ProjectService) {

        $scope.myaccount = Principal.identity();

        // set scope tab to my projects
        $scope.initMyProjects = function () {
            $scope.refreshFavouritesProjects();
            $scope.refreshMyProjects();
        };

        $scope.saveProject = function (project, callback) {
            if (project.id == null) {
                UserService.projects.create({login: Principal.identity().login}, project).$promise
                    .then(function (_project) {
                        ProjectModel.setCurrentProject(_project);

                        for (var i = 0; i < project.members.length; i++) {
                            if (project.members[i].login !== $scope.myaccount.login) {
                                ProjectService.membersProjects.add({
                                    login: project.members[i].login,
                                    id: _project.id,
                                    admin: project.members[i].admin
                                }, null).$promise.then(function () {
                                });
                            }
                        }
                        $scope.initProject($scope.initMyProjects);
                        if (callback) {
                            callback();
                        }


                    });
            } else {
                ProjectService.project.update({
                    id: project.id
                }, project).$promise.then(function () {
                    ProjectModel.setCurrentProject(project);
                    $scope.initProject($scope.initMyProjects);
                    if (callback) {
                        callback();
                    }
                });
            }
        }

        $scope.createProject = function () {
            var newProject = {
                isNew: true,
                name: null,
                reachable: false,
                members: [{
                    admin: true,
                    login: $scope.myaccount.login
                }]
            };
            $scope.openUpdateProjectModal(newProject);
        };

        $scope.editProject = function (project) {
            ProjectModel.refreshProjectMembers(project).then(function (members) {
                project.members = members;
                ProjectModel.refreshProjectConnectors(project).then(function (connectors) {
                    project.connectors = connectors;
                    $scope.openUpdateProjectModal(project);
                })

            })

        };

        $scope.deleteProject = function (project) {
            var title = "Projet " + project.name + " - Suppression";
            var info = "Etes vous sûr de vouloir supprimer ce projet ?";
            $scope.openDeleteModal(title, info, function () {
                $scope.deleteCurrentProject(project)
            });
        }


        $scope.deleteCurrentProject = function (project) {
            ProjectService.project.delete({login: Principal.identity().login, id: project.id}).$promise
                .then(function (response) {

                    $scope.initProject($scope.initMyProjects);
                }, function (error) {
                    console.log("TODO : gestion des erreurs");
                    console.log(error);
                });
        };

        //================================
        // PAGE INITIALIZATION
        //================================

        // clear current session project, clear scope public projects
        // set scope favourite projects to user favourite projects
        // set scope tab to my projects
        $scope.initProject($scope.initMyProjects);


        $scope.openUpdateProjectModal = function (project) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/projects/nav/my/modal/update/update.html',
                controller: 'ModalUpdateProjectController',
                size: 'lg',
                resolve: {
                    project: function () {
                        return project;
                    }
                }
            });
            modalInstance.result.then(function (_updateProjectData) {
                    if (_updateProjectData.next == 'add-connector') {
                        $scope.saveProject(_updateProjectData.project, function () {
                            var newConnector = {
                                periodicity: '86400'
                            };
                            $scope.openAddConnectorProjectModal(ProjectModel.getCurrentProject(), newConnector)
                        });
                    } else {
                        if (_updateProjectData.next == 'edit-connector') {
                            $scope.saveProject(_updateProjectData.project, function () {
                                $scope.openEditConnectorProjectModal(ProjectModel.getCurrentProject(), _updateProjectData.connector);
                            });
                        } else if (_updateProjectData.next == 'clone-connector') {
                            $scope.saveProject(_updateProjectData.project, function () {
                                $scope.openCloneConnectorProjectModal(ProjectModel.getCurrentProject(), _updateProjectData.connector);
                            });
                        } else {
                            $scope.saveProject(_updateProjectData.project);
                        }
                    }
                }, function () {
                }
            );
        }

        $scope.openAddConnectorProjectModal = function (project, connector) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/projects/nav/my/modal/add-connector/add-connector.html',
                controller: 'ModalAddConnectorProjectController',
                size: 'lg',
                resolve: {
                    project: function () {
                        return project;
                    },
                    connector: function () {
                        return connector;
                    }
                }
            });
            modalInstance.result.then(function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            }, function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            });
        }

        $scope.openEditConnectorProjectModal = function (project, connector) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/projects/nav/my/modal/edit-connector/edit-connector.html',
                controller: 'ModalEditConnectorProjectController',
                size: 'lg',
                resolve: {
                    project: function () {
                        return project;
                    },
                    connector: function () {
                        return connector;
                    }
                }
            });
            modalInstance.result.then(function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            }, function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            });
        }

        $scope.openCloneConnectorProjectModal = function (project, connector) {
            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/projects/nav/my/modal/clone-connector/clone-connector.html',
                controller: 'ModalCloneConnectorProjectController',
                size: 'lg',
                resolve: {
                    project: function () {
                        return project;
                    },
                    connector: function () {
                        return connector;
                    }
                }
            });
            modalInstance.result.then(function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            }, function (_updateConnectorData) {
                $scope.editProject(_updateConnectorData.project);
            });
        }

        $scope.openDeleteModal = function (title, info, deleteFunction) {

            var modalInstance = $uibModal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'scripts/app/modal/delete/delete.html',
                controller: 'ModalDeleteController',
                size: 'lg',
                resolve: {
                    title: function () {
                        return title;
                    },
                    info: function () {
                        return info;
                    }
                }
            });

            modalInstance.result.then(function () {
                deleteFunction();
            }, function () {

            });
        };
    })
;
