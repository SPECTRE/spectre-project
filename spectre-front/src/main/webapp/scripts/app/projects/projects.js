'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('projects-public', {
                parent: 'site',
                data: {
                    pageTitle: 'Projets publics'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/projects/projects.html',
                          controller: 'ProjectsController'
                    },
                    'nav@projects-public': {
                        templateUrl: 'scripts/app/projects/nav/public/public.html',
                        controller: 'ProjectsPublicController'
                    }
                }
            })
            .state('projects-my', {
                parent: 'site',
                data: {
                    pageTitle: 'Mes projets'
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/app/projects/projects.html',
                          controller: 'ProjectsController'
                    },
                    'nav@projects-my': {
                        templateUrl: 'scripts/app/projects/nav/my/my.html',
                        controller: 'ProjectsMyController'
                    }
                }
            });
    });
