'use strict';

angular.module('spectreApp')
    .factory('ApplicationService', function Application($resource) {
        return {
        	all: $resource('api/applications', {}, {
        		'get': { method: 'GET', isArray: true},
            }),
            
			application: $resource('api/applications/:id', {id:'@id'}, {
				'get': { method: 'GET'},
				 'update': { method:'PUT' }
            })
            
        
              
         };      
    });