'use strict';

angular.module('spectreApp')
    .factory('ProjectModel', function ProjectModel($rootScope, $q, ProjectService){


        var currentProject = null;

        return {
            // get current session project
            getCurrentProject: function(){
                return currentProject;
            },

            // set current session project
            setCurrentProject: function(_project){
                currentProject = _project;
                this.notifyProjectEvent();
            },

            // reset current session project to null
            clearCurrentProject: function(){
                currentProject = null;
            },

            // call Spectre Core and reset current session project connectors to all connectors of the current session project
            refreshProjectConnectors: function(project){
                var deferred = $q.defer();
                ProjectService.connectors.get({id: project.id})
                    .$promise
                    .then(function (_connectors) {
                        var connectors = new Array();
                        for (var i=0; i < _connectors.length; i++)  {
                            var _connector = _connectors[i];
                            connectors.push(_connector);
                        }

                        deferred.resolve(connectors);

                    }, function (error) {
                        deferred.reject(error);

                    });
                return deferred.promise;
            },


            refreshProjectMembers: function(project){
                var deferred = $q.defer();
                if(project != null && project.id != null){
                    ProjectService.projectMembers.get({id: project.id})
                        .$promise
                        .then(function (_projectMembers) {
                            var members = new Array();
                            for (var i=0; i < _projectMembers.length; i++)  {
                                var _projectMember = _projectMembers[i];
                                members.push(_projectMember);
                            }
                            deferred.resolve(members);

                        }, function (error) {
                            deferred.reject(error);

                        });
                    }else{
                        deferred.reject("empty project");
                    }
                return deferred.promise;
            },


            subscribeProjectEvent: function(scope, callback) {
                var handler = $rootScope.$on('project-updated', callback);
                scope.$on('$destroy', handler);
            },
            notifyProjectEvent: function() {
                $rootScope.$emit('project-updated');
            }


        };
    });

