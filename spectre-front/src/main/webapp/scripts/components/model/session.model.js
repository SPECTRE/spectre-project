'use strict';

angular.module('spectreApp')
    .factory('SessionModel', function SessionModel($rootScope){

        var isSessionExpired = false;

        return {

            getIsSessionExpired: function() {
                return isSessionExpired;
            },

            setIsSessionExpired: function(sessionExpired) {
                isSessionExpired = sessionExpired;
            }


        };
    });

