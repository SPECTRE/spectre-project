'use strict';

angular.module('spectreApp')
    .factory('RefreshModel', function RefreshModel($rootScope, $interval, DashboardModel){

        $interval(function() {
            refreshDashboard();
        }, 300000, 0);

        var refreshDashboard = function(){
            if(DashboardModel.getCurrentDashboard() != null){
                DashboardModel.refreshWidgets();
            }

        }

        return {

        };
    });

