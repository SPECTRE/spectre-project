'use strict';

angular.module('spectreApp')
    .factory('DashboardModel', function DashboardModel($rootScope, $q, DashboardService, VisualisationService,IndicatorValueService,VisDataSet, Principal){


        var currentDashboard = null;
        var currentWidget = null;
        var widgetsPerPage = 8;
        var tupleRefreshDelay = 1500;
        var addedGroupTuple = [];

        var graph_groups = new vis.DataSet();
        graph_groups.add({
            id: 0,
            content: '',
            className: 'custom-points',
            options: {
                drawPoints: {
                    style: 'circle' // square, circle
                },
                shaded: {
                    orientation: 'bottom' // top, bottom
                }
            }});

        var graph_options = {
             // Set start date as first kpi date minus 1 day in ms
//                        start: $scope.currentDashboard.widgets[i].kpis[0].date - 86400000,
             // Set end date as last kpi date plus 1 day in ms
//                        end: $scope.currentDashboard.widgets[i].kpis[kpisCount - 1].date + 86400000,
             width:  '100%',
             height: '100%',
             // When clicked, a blue shadow border is displayed around the graph.
             clickToUse: true,
             // Set the minimal width of the yAxis. The axis will resize to accomodate the labels of the Y values.
             // Set the left axis (with the widget name)
             dataAxis: {
                visible: true
                //showMajorLabels: true
//                width:'5px',
//                left:{
//                    title:{
//                        text:'test'
//                    }
//                }
             },
             legend: {
                left : {
                    visible: false
                }
             },
             // Hide the vertical bar at the current time
             showCurrentTime: true,
             showMajorLabels : true
         }

         var graph_groups_tuple = new vis.DataSet();
         graph_groups_tuple.add({

         });

         var graph_options_tuple = {
            width:  '100%',
            height: '100%',
            clickToUse: true,
            drawPoints: true,
            dataAxis: {
                visible: true,
            },
            legend: {
                left : {
                    position: 'bottom-left',
                    visible: true
                }
            }
          }


        var parseNumeric = function(val){
            var valBis = parseFloat(val);

            if(valBis.toString().indexOf('.') > -1){
                return valBis.toFixed(1);
            }else{
                return valBis;
            }

        };

        var getTrend = function(kpis){
            var trend = null;
            if(kpis !== null && kpis !== undefined && kpis.length > 1){
                if(kpis[0].value > kpis[1].value){
                    trend = {
                        evolution : 'glyphicon glyphicon-arrow-up',
                        diff : '( + '.concat(parseNumeric(kpis[0].value - kpis[1].value)).concat(' )')
                    };

                }else if(kpis[0].value === kpis[1].value){
                    trend = {
                        evolution : 'glyphicon glyphicon-arrow-right',
                        diff : '( = )'
                    };
                }else{
                    trend = {
                        evolution : 'glyphicon glyphicon-arrow-down',
                        diff : '( - '.concat(parseNumeric(kpis[1].value - kpis[0].value)).concat(' )')
                    };
                }
            }
            return trend;
        };

        var computeTupleKpi = function(indicatorData){
            var value = [];

            var kpi = null;


            if(indicatorData.value != null && indicatorData.value.data != null && indicatorData.value.label != null){

                for(var i = 0; i < indicatorData.value.data.length; i++){
                    value.push({value:indicatorData.value.data[i],label:indicatorData.value.label[i]});
                }

                kpi = {
                    measure : indicatorData.value.measure,
                    date : indicatorData.date,
                    value : value
                }

            }else{
                value.push({value:"N/A",label:""});

                kpi = {
                    measure : "N/A",
                    date : indicatorData.date,
                    value : value
                }

            }

            return kpi;
        };

        var computeNumericKpi = function(indicatorData){
            var kpi = {'date' : indicatorData.date}
            if(indicatorData.value != null && indicatorData.value.data != null){
                kpi.value = parseNumeric(indicatorData.value.data);
            }else{
                kpi.value = "N/A";
            }
            kpi.group = 0;
            return kpi;
        };

        var computeTextKpi = function(indicatorData){
            var kpi = {'date' : indicatorData.date}
            if(indicatorData.value != null && indicatorData.value.data != null){
                kpi.value = indicatorData.value.data;
            }else{
                kpi.value = "N/A";
            }
            return kpi;
        };

        var computeMoodKpi = function(indicatorData){
            var kpi = {'date' : indicatorData.date}
            if(indicatorData.value != null && indicatorData.value.data != null){
                kpi.value = indicatorData.value.data;
                if(kpi.value == 'HAPPY'){
                     kpi.icon = "fa fa-smile-o";
                }else if(kpi.value == 'NORMAL'){
                     kpi.icon = "fa fa-meh-o";
                 }else{
                    kpi.icon = "fa fa-frown-o";
                 }

            }else{
                kpi.value = "N/A";
            }
            return kpi;
        };



        return {
              // reset current session dashboard to null
            clearCurrentDashboard: function(){
                currentDashboard = null;
                this.notifyDashboardEvent();
            },

            // get current session dashboard
            getCurrentDashboard: function(){
                return currentDashboard;
            },

            // set current session dashboard
            setCurrentDashboard: function(_dashboard){
                currentDashboard = _dashboard;
                this.notifyDashboardEvent();
            },

            // set current session widget
            setCurrentWidget: function(_widget){
                currentWidget = _widget;

            },
            getCurrentWidget: function(){
                return Widget;
            },




          getTupleRefreshDelay: function(){
                return tupleRefreshDelay;
            },

            // Return the number of widgets displayed on a page
            getWidgetsPerPage: function(){
                return widgetsPerPage;
            },


            // get current session dashboard indicators
            getCurrentDashboardIndicators: function(){
                if(currentDashboard && currentDashboard.indicators){
                    return currentDashboard.indicators;
                }else{
                    return null;
                }
            },

            // get current session dashboard connectors
            getCurrentDashboardConnectors: function(){
                if(currentDashboard && currentDashboard.connectors){
                    return currentDashboard.connectors;
                }else{
                    return null;
                }
            },



            // call Spectre Core and reset current session dashboard widgets to all widgets of the current session dashboard
            refreshDashboardMembers: function(){
                var deferred = $q.defer();
                var that = this;
                if(currentDashboard != null && currentDashboard.id != null){
                    DashboardService.members.get({id: currentDashboard.id})
                    .$promise
                    .then(function (_members) {
                        currentDashboard.members = _members;
                        that.notifyDashboardMembersEvent();
                        deferred.resolve(_members);
                    }, function (error) {
                        deferred.reject(error);
                    });
                }else{
                    deferred.reject("currentDashboard is empty");
                }
                return deferred.promise;

            },


            // call Spectre Core and reset current session dashboard widgets to all widgets of the current session dashboard
            refreshWidgets: function(){
                Principal.checkAuthentication();
                var deferred = $q.defer();
                var that = this;
                if (currentDashboard !== null) {

                    DashboardService.widgets.get({id: currentDashboard.id})
                        .$promise
                        .then(function (_widgets) {

                            currentDashboard.widgets = _widgets;

                            that.notifyDashboardWidgetsEvent();

                            deferred.resolve(_widgets);

                        }, function (error) {
                            deferred.reject(error);
                        });
                }
                return deferred.promise;
            },


            // call Spectre Kapi and get data for widget
            refreshWidgetData: function(index, widget){
                var deferred = $q.defer();
                var that = this;

                IndicatorValueService.kpis.get({
                    id: widget.indicator.id,
                    connectorType: widget.indicator.connectorType,
                    indicatorType: widget.indicator.type,
                    indicatorMeasure: widget.indicator.measure})
                        .$promise
                        .then(function (indicatorData) {
                                var kpis = [];
                                var graph_items = [];
                                for (var k = 0 ; k < indicatorData.length ; k++) {
                                    if(widget.indicator.measure == 'TUPLE'){
                                        widget.slides = {
                                            transition : true,
                                            active : 0,
                                            isAuto : false
                                        }
                                        var num = computeTupleKpi(indicatorData[k]);
                                        kpis.unshift(num);
                                        if(num.measure === 'NUMERIC' || num.measure === 'PERCENT'){
                                            for (var m = 0; m < num.value.length; m++) {
                                                var item = {};
                                                if(num.value[m].value != "N/A"){
                                                    item.x = indicatorData[k].date;
                                                    item.y = num.value[m].value;
                                                    item.group = num.value[m].label;
                                                    graph_items.unshift(item);
                                                }
                                            }
                                            var group = null;
                                            for (var h = 0; h < graph_items.length; h++) {
                                                if ($.inArray(graph_items[h].group, addedGroupTuple) == -1) {
                                                    graph_groups_tuple.add({
                                                        id: graph_items[h].group,
                                                        content: graph_items[h].group,
                                                    });
                                                }
                                                addedGroupTuple.push(graph_items[h].group);
                                                group = graph_items[h].group;
                                            }
                                        }
                                    }else if(widget.indicator.measure == 'NUMERIC' || widget.indicator.measure === 'PERCENT'){
                                        var num = computeNumericKpi(indicatorData[k]);
                                        kpis.unshift(num);
                                        var item = {};
                                        if(num.value != "N/A"){
                                            item.x = indicatorData[k].date;
                                            item.y = num.value;
                                            item.group = 0;
                                            graph_items.unshift(item);
                                        }

                                    }else if(widget.indicator.measure == 'MOOD'){
                                        kpis.unshift(computeMoodKpi(indicatorData[k]));
                                    }else{
                                        kpis.unshift(computeTextKpi(indicatorData[k]));
                                    }
                                 }

                                 if(graph_items.length > 0){
                                    widget.graph = {
                                        data : {
                                            items: new vis.DataSet(graph_items),
                                            groups: graph_groups
                                        },
                                        options: graph_options
                                    }
                                    var options = {};
                                 }else{
                                    widget.graph = null;
                                 }
                                 if (widget.indicator.measure == 'TUPLE') {
                                   widget.graph = {
                                       data : {
                                           items: new vis.DataSet(graph_items),
                                           groups: graph_groups_tuple
                                       },
                                       options: graph_options_tuple
                                   }
                                 }
                                 widget.kpis = kpis;

                                if( widget.indicator.measure == 'NUMERIC' || widget.indicator.measure == 'PERCENT' ) {
                                    widget.trend = getTrend(kpis);
                                }

                                widget.loaded = true;
                                if(currentDashboard != null) currentDashboard.widgets[index] = widget;
                                that.notifyDashboardWidgetDataEvent(index, widget);
                            deferred.resolve(widget);
                        }, function (error) {
                            if(widget.indicator.measure == 'TUPLE'){
                                var kpis = [{
                                   value: [{'value':"N/A",'label':""}],
                                   measure:"TEXT",
                                   date: null
                                }]
                            }else{
                                var kpis = [{
                                   value: "N/A",
                                   measure:"TEXT",
                                   date: null
                                }]
                            }

                            widget.kpis = kpis;
                            widget.loaded = true;
                            that.notifyDashboardWidgetDataEvent(index, widget);
                            deferred.resolve(widget);
                     });
                 return deferred.promise;

             },




            subscribeDashboardEvent: function(scope, callback) {
                var handler = $rootScope.$on('dashboard-updated', callback);
                scope.$on('$destroy', handler);
            },
            notifyDashboardEvent: function() {
                $rootScope.$emit('dashboard-updated');
            },
            subscribeDashboardMembersEvent: function(scope, callback) {
                var handler = $rootScope.$on('dashboard-members-updated', callback);
                scope.$on('$destroy', handler);
            },
            notifyDashboardMembersEvent: function() {
                $rootScope.$emit('dashboard-members-updated');
            },
            subscribeDashboardWidgetsEvent: function(scope, callback) {
                var handler = $rootScope.$on('dashboard-widgets-updated', callback);
                scope.$on('$destroy', handler);
            },
            notifyDashboardWidgetsEvent: function() {
                $rootScope.$emit('dashboard-widgets-updated');
            },
            subscribeDashboardWidgetDataEvent: function(scope, callback) {
                var handler = $rootScope.$on('dashboard-widget-data-updated', callback);
                scope.$on('$destroy', handler);
            },
            notifyDashboardWidgetDataEvent: function(index, widget) {
                $rootScope.$emit('dashboard-widget-data-updated', {'index':index, 'widget':widget});
            }


        };
    });

