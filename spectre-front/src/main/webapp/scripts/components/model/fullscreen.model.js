'use strict';

angular.module('spectreApp')
    .factory('FullScreenModel', function FullScreenModel($rootScope, $q,Fullscreen){


        var isFullScreen = false;


        return {
            fullScreen : function(){
                this.isFullScreen = !this.isFullScreen;

                this.notifyFullScreenEvent();

                if(!this.isFullScreen){
                    Fullscreen.cancel();
                }else{
                    Fullscreen.all();
                }
            },

            getFullScreen : function(){
                return this.isFullScreen;
            },

            subscribeFullScreenEvent: function(scope, callback) {
                var handler = $rootScope.$on('full-screen', callback);
                scope.$on('$destroy', handler);
            },

            notifyFullScreenEvent: function() {
                $rootScope.$emit('full-screen');
            }

        };
    });

