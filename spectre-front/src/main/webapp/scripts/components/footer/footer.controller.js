'use strict';

angular.module('spectreApp')
    .controller('FooterController', function ($scope, $state, ReleaseService) {
        $scope.year = new Date().getFullYear();

        ReleaseService.release.get().$promise.then(function(data){
            $scope.release = data.Releases[0].Version;
            }, function(error){

            } );

            ReleaseService.env.get().$promise.then(function(data){
            $scope.env = data.env;
            }, function(error){

            } );

        $scope.goToDocumentation = function () {
            window.open('http://dvxx2asc83.rouen.francetelecom.fr:82/#introduction');
        };

        $scope.goToPlazza = function () {
            window.open('https://plazza.orange.com/groups/spectre');
        };

        $scope.goToChangelog = function () {
            $state.go("release");
        };


    });
