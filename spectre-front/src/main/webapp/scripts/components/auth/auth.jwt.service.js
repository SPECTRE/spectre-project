'use strict';

angular.module('spectreApp')
    .factory('AuthServerProvider', function loginService($http, $rootScope, $state, localStorageService, $window, jwtHelper, ENV, Register) {
        return {
            login: function(credentials) {
                return $http.post('http://' + ENV.coreEndpoint + '/token/basic', null, {
                    headers: {
                        "Content-Type": "application/x-www-form-urlencoded",
                        "Accept": "application/json",
                        "Authorization": "Basic " + $window.btoa(credentials.username + ":" + credentials.password)
                    }

                }).success(function (response) {
                    localStorageService.set("login", credentials.username);
                    $rootScope.$broadcast("login",credentials);
                    localStorageService.set('spectre_token', response.accessToken);
                    return response;
                })
            },
            createAccount: function (account, callback) {
                var cb = callback || angular.noop;

                return Register.save(account,
                    function () {
                        return cb({'success':true, 'data':account});
                    },
                    function (err) {
                        return cb({'success':false, 'data': err});
                    }.bind(this)).$promise;
            },
            logout: function() {
                localStorageService.clearAll();
                var identity = {};
                identity.login = null;
                $rootScope.$broadcast("login", identity);
                $state.go('login');
            },
            getToken: function () {
                return localStorageService.get('spectre_token');
            },
            hasValidToken: function () {
                var token = this.getToken();
                return (token != null) && !jwtHelper.isTokenExpired(token);
            }
        };
    });
