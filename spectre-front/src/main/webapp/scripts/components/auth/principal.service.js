'use strict';

angular.module('spectreApp')
    .factory('Principal', function Principal($q, $state, $rootScope, localStorageService, Account, AuthServerProvider){
        var  _identity,
            _authenticated = false;

        var _authenticate = function(_login){
            _identity = {
                login: _login,
            };
            localStorageService.set('login', _login);
            $rootScope.$broadcast("login",_identity);
        }

        if(localStorageService.get('login') != null){
            _identity = {
                login: localStorageService.get('login'),
            };

            $rootScope.$broadcast("login",_identity);
        }


        return {
            isIdentityResolved: function () {
                return angular.isDefined(_identity);
            },
            authorized: function(){
                return AuthServerProvider.hasValidToken();
            },
            authenticate: function (identity) {
                _identity = identity;
                _authenticated = identity !== null;
            },
            checkAuthentication: function (force) {
                var deferred = $q.defer();
                if (force === true) {
                    _identity = undefined;
                }

                // check and see if we have retrieved the identity data from the server.
                // if we have, reuse it by immediately resolving
                if (angular.isDefined(_identity) && AuthServerProvider.hasValidToken()) {
                    deferred.resolve(_identity);
                    $rootScope.$broadcast("login", _identity);
                    return deferred.promise;
                }

                if (!AuthServerProvider.hasValidToken()) {
                    AuthServerProvider.logout();
                    $rootScope.$broadcast("sessionExpired", true);
                }

                // retrieve the identity data from the server, update the identity object, and then resolve.
                var login = localStorageService.get("login");
                if (login != null) {
                    Account.get({login:login}).$promise
                        .then(function (account) {
                            _identity = account;
                            _authenticated = true;
                            deferred.resolve(_identity);
                            $rootScope.$broadcast("login", _identity);

                        })
                        .catch(function() {
                            _identity = null;
                            _authenticated = false;
                           deferred.resolve(_identity);
                        });
                }
                return deferred.promise;
            },
            identity: function () {
                return _identity;
            }
        };
    });
