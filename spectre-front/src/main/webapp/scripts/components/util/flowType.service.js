'use strict';

angular.module('spectreApp')
    .directive('responsiveValueType', function () {
        return {
            restrict: 'A',
            link: function (scope, element,attrs) {

                element.flowtype({
                     minFont : 20,
//                     maxFont : 100,
                     fontRatio : 5

                });
            }
        };
    })
    .directive('responsiveLegendType', function () {
        return {
            restrict: 'A',
            link: function (scope, element,attrs) {

                element.flowtype({
                     minFont : 10,
//                     maxFont : 100,
                     fontRatio : 12
                });
            }
        };
    })
    .directive('responsiveTitleType', function () {
        return {
            restrict: 'A',
            link: function (scope, element,attrs) {

                element.flowtype({
                        minFont : 10,
//                       maxFont : 100,
                       fontRatio : 12
                });
            }
        };
    })
    .directive('responsiveNavType', function () {
        return {
            restrict: 'A',
            link: function (scope, element,attrs) {

                element.flowtype({
                     minFont : 10,
//                     maxFont : 20,
                     fontRatio : 5
                });
            }
        };
    })
    .directive('responsiveTupleType', function () {
        return {
            restrict: 'A',
            link: function (scope, element,attrs) {

                element.flowtype({
                     minFont : 20,
//                     maxFont : 20,
                     fontRatio : 3
                });
            }
        };
    });

