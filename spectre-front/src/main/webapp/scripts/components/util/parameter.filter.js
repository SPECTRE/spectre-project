'use strict';


angular.module('connectorParameterFilters', []).filter('params', function() {
  return function(input) {
    // pretty text formatting for connector parameters display
    if (input != null){
            input = input.toLowerCase();
            input = input.replace(/-/g, " ");
            input = input.replace(/_/g, " ");
            input = input.substring(0, 1).toUpperCase() + input.substring(1)
        }
        return input;
  };
});
