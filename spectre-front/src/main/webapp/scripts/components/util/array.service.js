'use strict';

angular.module('spectreApp')
    .factory('ArrayService', function ArrayService() {
        return {
            // convert an object into an array
            // return an array of objects {objKey: key, objValue: value}
            // for example, if obj = {key_1: value_1, key_2: value_2, ... key_n: value_n}
            // return [{objKey: key_1, objValue: value_1}, {objKey: key_2, objValue: value_2}, ... {objKey: key_n, objValue: value_n}]
            objToArray: function (obj) {
                if (!angular.isObject(obj)) {
                    return obj;
                }
                return Object.keys(obj).map(function (key) {
                    return {objKey: key, objValue: obj[key]};
                });
            },

            // convert an object into an array
            // return an array of objects {objKey: value, objValue: ""}
            // for example, if obj = {key_1: value_1, key_2: value_2, ... key_n: value_n}
            // return [{objKey: value_1, objValue: ""}, {objKey: value_2, objValue: ""}, ... {objKey: value_n, objValue: ""}]
            paramToArray: function (obj) {
                if (!angular.isObject(obj)) {
                    return obj;
                }
                var array = new Array();
                for (var i = 0; i < obj.length; i++) {
                    var current = obj[i];
                    array.push({objKey: current, objValue: ""});
                }
                return array;
            },

            // convert an array of objects {objKey: key, objValue: value} into a map
            // return a map {key: value}
            // for example, if obj = [{objKey: key_1, objValue: value_1}, {objKey: key_2, objValue: value_2}, ... {objKey: key_n, objValue: value_n}]
            // return {key_1: value_1, key_2: value_2, ... key_n: value_n}
            arrayToMap: function (obj) {
                if (!angular.isObject(obj)) {
                    return obj;
                }
                var map = new Map();
                for (var i = 0; i < obj.length; i++) {
                    var current = obj[i];
                    map.set(current.objKey, current.objValue);
                }
                return map;
            },

            // convert a map {key: value} into an array
            // return an array of objects {objKey: key, objValue: value}
            // for example, if map = {key_1: value_1, key_2: value_2, ... key_n: value_n}
            // return [{objKey: key_1, objValue: value_1}, {objKey: key_2, objValue: value_2}, ... {objKey: key_n, objValue: value_n}]
            mapToArray: function (map) {
                if (!Map.prototype.isPrototypeOf(map)) {
                    return map;
                }
                var tab = [];
                for (var i = 0; i < map.keys().length; i++) {
                    var key = map.keys()[i];
                    tab.push({objKey: key, objValue: map.get(key)});
                }
                return tab;
            },

            // convert an array of objects {objKey: key, objValue: value} into an object
            // return an object {key: value}
            // for example, if array = [{objKey: key_1, objValue: value_1}, {objKey: key_2, objValue: value_2}, ... {objKey: key_n, objValue: value_n}]
            // return {key_1: value_1, key_2: value_2, ... key_n: value_n}
            arrayToObj: function (array) {
                if (!angular.isArray(array)) {
                    return array;
                }
                var obj = {};
                for (var i = 0; i < array.length; i++) {
                    obj[array[i].objKey] = array[i].objValue;
                }
                return obj;
            }
        };
    });
