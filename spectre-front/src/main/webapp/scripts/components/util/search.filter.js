'use strict';

angular.module('spectreApp')
    // filter an array of connectors to exclude those which do not fulfill the criteria
    // the filter is not case sensitive
    // return an array of all connectors which contain every word of the criteria either in name or in type
    // return the full array of connectors when no criteria are provided
    .filter('searchConnectors', function () {
        return function (connectors, criteria) {
            // split the criteria into a word array
            var words = null;

            if(criteria != null) {
                words = criteria.match(/\S+/g);
            }

            // if no criteria provided, return all connectors
            if (words === null || words.length === 0) {
                return connectors;
            }

            // if criteria provided, compare them to connector name and type
            var found = [];
            var isMatching;
            var connectorCharacs;
            for (var i=0 ; i<connectors.length ; i++) {
                isMatching = true;
                connectorCharacs = connectors[i].objValue.name.toUpperCase() + " " + connectors[i].objValue.type.toUpperCase();
                for (var k=0 ; k<words.length ; k++) {
                    if (connectorCharacs.indexOf(words[k].toUpperCase()) < 0) {
                        isMatching = false;
                    }
                }
                if (isMatching) {
                    found.push(connectors[i]);
                }
            }
            return found;
        };
    });

angular.module('spectreApp')
    // filter an array of dashboards to exclude those which do not fulfill the criteria
    // the filter is not case sensitive
    // return an array of all dashboards which contain every word of the criteria either in name or in description
    // return the full array of dashboards when no criteria are provided
    .filter('searchDashboards', function () {
        return function (dashboards, criteria) {
            // split the criteria into a word array
            var words = null;

            if(criteria != null) {
                words = criteria.match(/\S+/g);
            }

            // if no criteria provided, return all dashboards
            if (words === null || words.length === 0) {
                return dashboards;
            }

            // if criteria provided, compare them to dashboard name and description
            var found = [];
            var isMatching;
            var dashboardCharacs;
            for (var i=0 ; i<dashboards.length ; i++) {
                isMatching = true;
                dashboardCharacs = dashboards[i].name.toUpperCase();
                if (dashboards[i].description) {
                    dashboardCharacs += " " + dashboards[i].description.toUpperCase();
                }
                for (var k=0 ; k<words.length ; k++) {
                    if (dashboardCharacs.indexOf(words[k].toUpperCase()) < 0) {
                        isMatching = false;
                    }
                }
                if (isMatching) {
                    found.push(dashboards[i]);
                }
            }
            return found;
        };
    });

angular.module('spectreApp')
    // filter an array of dashboards to exclude those which do not fulfill the criteria
    // the filter is not case sensitive
    // return an array of all dashboards which contain every word of the criteria either in name or in description
    // return the full array of dashboards when no criteria are provided
    .filter('searchUsers', function () {
        return function (users, criteria) {
            // split the criteria into a word array
            var words = null;

            if(criteria != null) {
                words = criteria.match(/\S+/g);
            }

            // if no criteria provided, return all dashboards
            if (words === null || words.length === 0) {
                return users;
            }

            // if criteria provided, compare them to dashboard name and description
            var found = [];
            var isMatching;
            var userCharacs;
            if(users != null){
                for (var i=0 ; i<users.length ; i++) {
                    isMatching = true;
                    userCharacs = users[i].login.toUpperCase();
                    if(users[i].firstname != null){
                        userCharacs += users[i].firstname.toUpperCase();
                    }
                    if(users[i].lastname != null){
                        userCharacs += users[i].lastname.toUpperCase();
                    }
                    for (var k=0 ; k<words.length ; k++) {
                        if (userCharacs.indexOf(words[k].toUpperCase()) < 0) {
                            isMatching = false;
                        }
                    }
                    if (isMatching) {
                        found.push(users[i]);
                    }
                }
            }
            return found;
        };
    });

angular.module('spectreApp')
    // filter an array of dashboards to exclude those which do not fulfill the criteria
    // the filter is not case sensitive
    // return an array of all dashboards which contain every word of the criteria either in name or in description
    // return the full array of dashboards when no criteria are provided
    .filter('membersIn', function () {
        return function (users, members) {
            // split the criteria into a word array
            var words = 0;
            if(members != null) {
                words = members.length;
            }

            // if no criteria provided, return all dashboards
            if (members === null || words == 0) {
                return users;
            }

            // if criteria provided, compare them to dashboard name and description
            var found = [];
            var isMatching;
            if(users != null){
                for (var i=0 ; i<users.length ; i++) {
                    isMatching = false;
                    for (var j =0; j<members.length;j++) {
                        if(members[j].login == users[i].login){
                            isMatching = true;
                            break;
                        }
                    }
                    if (isMatching == false) {
                        found.push(users[i]);
                    }
                }
            }
            return found;
        };
    });

    angular.module('spectreApp')
        // filter an array of dashboards to exclude those which do not fulfill the criteria
        // the filter is not case sensitive
        // return an array of all dashboards which contain every word of the criteria either in name or in description
        // return the full array of dashboards when no criteria are provided
        .filter('excludeById', function () {
            return function (input, objectsToExclude) {
                // split the criteria into a word array
                var words = 0;
                if(objectsToExclude != null) {
                    words = objectsToExclude.length;
                }

                // if no criteria provided, return all dashboards
                if (objectsToExclude === null || words == 0) {
                    return input;
                }

                // if criteria provided, compare them to dashboard name and description
                var found = [];
                var isMatching;
                for (var i=0 ; i < input.length ; i++) {
                    isMatching = false;
                    for (var j =0; j<objectsToExclude.length;j++) {
                        if(objectsToExclude[j].id == input[i].id){
                            isMatching = true;
                            break;
                        }
                    }
                    if (isMatching == false) {
                        found.push(input[i]);
                    }
                }
                return found;
            };
        });

    angular.module('spectreApp')
        // filter an array of dashboards to exclude those which do not fulfill the criteria
        // the filter is not case sensitive
        // return an array of all dashboards which contain every word of the criteria either in name or in description
        // return the full array of dashboards when no criteria are provided
        .filter('isInById', function () {
            return function (input, completeList) {


                if (completeList === null || completeList.length == 0) {
                    return [];
                }

                if(input === null || input.length == 0){
                    return [];
                }

                // if criteria provided, compare them to dashboard name and description
                var found = [];
                var isMatching;
                for (var i=0 ; i < input.length ; i++) {
                    isMatching = false;
                    for (var j =0; j<completeList.length;j++) {
                        if(completeList[j].id == input[i].id){
                            isMatching = true;
                            break;
                        }
                    }
                    if (isMatching == true) {
                        found.push(input[i]);
                    }
                }
                return found;
            };
        });

angular.module('spectreApp')
    // filter an array of projects to exclude those which do not fulfill the criteria
    // the filter is not case sensitive
    // return an array of all projects which contain every word of the criteria either in name or in description
    // return the full array of projects when no criteria are provided
    .filter('searchProjects', function () {
        return function (projects, criteria) {
            // split the criteria into a word array
            var words = null;

            if(criteria != null) {
                words = criteria.match(/\S+/g);
            }

            // if no criteria provided, return all projects
            if (words === null || words.length === 0) {
                return projects;
            }

            // if criteria provided, compare them to project name and description
            var found = [];
            var isMatching;
            var projectCharacs;
            for (var i=0 ; i<projects.length ; i++) {
                isMatching = true;
                projectCharacs = projects[i].name.toUpperCase();
                if (projects[i].description) {
                    projectCharacs += " " + projects[i].description.toUpperCase();
                }
                for (var k=0 ; k<words.length ; k++) {
                    if (projectCharacs.indexOf(words[k].toUpperCase()) < 0) {
                        isMatching = false;
                    }
                }
                if (isMatching) {
                    found.push(projects[i]);
                }
            }
            return found;
        };
    });
