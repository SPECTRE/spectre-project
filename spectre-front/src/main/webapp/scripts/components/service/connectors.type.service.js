'use strict';

angular.module('spectreApp')
    .factory('ConnectorsTypeService', function ConnectorsTypeService($resource,ENV){

        var currentConnectorType = null;


        return {
            connectors: $resource('http://' + ENV.coreEndpoint + '/connectors/types', {}, {
                'all': {method: 'GET', isArray: true}       // get available connectors
            })

        };
    });
