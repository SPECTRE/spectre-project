'use strict';

angular.module('spectreApp')
    .factory('UserService', function UserService($resource,ENV){




        return {
            accounts: $resource('http://' + ENV.coreEndpoint + '/accounts', {}, {
                'all': {method: 'GET', params:{}, isArray: true}            // get all users logins
            }),
            profilUpdate: $resource('http://' + ENV.coreEndpoint + '/accounts/:login', {}, {
                'get':{method: 'GET', isArray: false},
                'update': {method: 'PUT'}                                   // update profil user
            }),
            passwordUpdate: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/changePassword', {}, {
                'get':{method: 'GET', isArray: false},
                'update': {method: 'PUT'}                                   // update password user
            }),
            dashboards: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/dashboards/:id', {}, {
                'get':{method: 'GET', params: {}, isArray: false},          // get a user (identified by his login) dashboard (identified by its id)
                'create': {method: 'POST', params: {}, isArray: false}     // create a dashboard owned by a user (identified by his login)
            }),
            favouritesDashboards: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/dashboards/:id/favourites', {}, {
                'all':{method: 'GET', params: {}, isArray: true}            // get a user (identified by his login) fabourite dashboards list
            }),
             projects: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/projects/:id', {}, {
                'get':{method: 'GET', params: {}, isArray: false},          // get a user (identified by his login) project (identified by its id)
                'create': {method: 'POST', params: {}, isArray: false}     // create a project owned by a user (identified by his login)
            }),
            membersProjects: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/projects/members', {}, {
                'all':{method: 'GET', params: {}, isArray: true}            // get a user (identified by his login) members projects list
            }),
            membersDashboards: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/dashboards/:id/members', {}, {
                'all':{method: 'GET', params: {}, isArray: true}            // get a user (identified by his login) members projects list
            }),
            favouritesProjects: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/projects/:id/favourites', {}, {
                'all':{method: 'GET', params: {}, isArray: true}            // get a user (identified by his login) fabourite projects list
            }),
            availableIndicators: $resource('http://' + ENV.coreEndpoint + '/accounts/:login/indicators/types', {}, {
                'all':{method: 'GET', params: {}, isArray: false}           // get all available indicators depending on the user (identified by his login) projects
            })

        };
    });
