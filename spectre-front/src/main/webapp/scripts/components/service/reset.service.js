'use strict';

angular.module('spectreApp')
    .factory('Reset', function Account($resource, ENV) {
        return {
            init: $resource('http://' + ENV.coreEndpoint + '/accounts/reset_password/init', null, {
                'resetPassword': {method: 'POST'}
            }),
            finish: $resource('http://' + ENV.coreEndpoint + '/accounts/reset_password/finish', null, {
                'resetPassword': {method: 'POST'}
            })
        }
    });
