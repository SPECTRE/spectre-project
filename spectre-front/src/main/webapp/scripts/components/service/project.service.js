'use strict';

angular.module('spectreApp')
    .factory('ProjectService', function ConnectorService($rootScope, $resource, ENV) {

        return {
            project: $resource('http://' + ENV.coreEndpoint + '/projects/:id', null, {
                'allpublic': {method: 'GET', isArray: true},     // get all public projects
                'delete': {method: 'DELETE'},                    // delete a project identified by its id
                'update': {
                    method: 'PUT', interceptor: {         // update a project identified by its id
                        response: function (response) {
                            // expose response
                            $rootScope.$broadcast("projectUpdated", null);
                            return response;
                        }
                    }
                }
            }),
            connectors: $resource('http://' + ENV.coreEndpoint + '/projects/:id/connectors/:idConnector', null, {
                'get': {method: 'GET', isArray: true},          // get all connectors of a project identified by its project id (id param)
                'delete': {method: 'DELETE'},                    // delete a connector in a project
                                                                 // identified by its project id (id param) and connector id (idConnector param)
                'create': {method: 'POST'},                       // create a connector in a project identified by its project id (id param)
                'clone': {method: 'POST'}
                                                                // clone/create a connector in a project identified by its project id (id param)
            }),
            connector: $resource('http://' + ENV.coreEndpoint + '/projects/:id/connector/test', null, {
                'test': {method: 'PUT'}
            }),

            membersProjects: $resource('http://' + ENV.coreEndpoint + '/projects/:id/members/:login?admin=:admin', {}, {
                'remove': {method: 'DELETE', params: {}},                               // remove a public project (identified by its id)
                // from a user (identified by his login) members projects list
                'add': {method: 'PUT'}                                     // add a public project (identified by its id)
                // to a user (identified by his login) members projects list
            }),
            projectMembers: $resource('http://' + ENV.coreEndpoint + '/projects/:id/members', {}, {
                'get': {method: 'GET', isArray: true}                                     // add a public project (identified by its id)
                // to a user (identified by his login) members projects list
            }),
            favouritesProjects: $resource('http://' + ENV.coreEndpoint + '/projects/:id/favourites/:login', {}, {
                'get': {method: 'GET', isArray: true}, // get all accounts which have the dashboard (identified by its id) in there favourites
                'remove': {method: 'DELETE', params: {}},                               // remove a public project (identified by its id)
                // from a user (identified by his login) favourite projects list
                'add': {method: 'PUT', params: {}}                                     // add a public project (identified by its id)
                // to a user (identified by his login) favourite projects lis
            })
        };
    });
