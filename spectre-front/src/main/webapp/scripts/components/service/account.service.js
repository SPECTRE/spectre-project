'use strict';

angular.module('spectreApp')
    .factory('Account', function Account($resource, ENV) {
        return $resource('http://' + ENV.coreEndpoint + '/accounts/:login', {}, {
            'login': { method: 'GET', params: {login:'@login'}, isArray: false},
        });
    });
