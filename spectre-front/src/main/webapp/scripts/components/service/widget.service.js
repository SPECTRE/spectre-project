'use strict';

angular.module('spectreApp')
    .factory('WidgetService', function WidgetService($resource,ENV){


        return {
            widgets: $resource('http://' + ENV.coreEndpoint + '/widgets/:id', null, {
                'update': {method: 'PUT'},              // update a widget (identified by its id)
                'delete': {method: 'DELETE'}            // delete a widget (identified by its id)
                                                        // and its association with its parent dashboard
            }),
            customWidgets: $resource('http://' + ENV.coreEndpoint + '/widgets/:id/custom', null, {
                'update': {method: 'PUT'}              // update a custom widget (identified by its id)
            }),
          customTupleWidgets: $resource('http://' + ENV.coreEndpoint + '/widgets/:id/custom-tuple', null, {
              'update': {method: 'PUT'}              // update a custom widget (identified by its id)
          })
        };
    });
