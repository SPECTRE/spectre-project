'use strict';

angular.module('spectreApp')
    .factory('IndicatorService', function IndicatorService($resource,ENV){


        return {
            properties: $resource('http://' + ENV.coreEndpoint + '/indicators/properties', null, {
                'get': {method: 'GET'}          // get indicator properties (available measures and labels)
            })
        };
    });
