'use strict';

angular.module('spectreApp')
    .factory('IndicatorValueService', function IndicatorValueService($resource,ENV){


        return {
            kpis: $resource('http://' + ENV.kapiEndpoint + '/kpis/indicators/:id?connectorType=:connectorType&indicatorType=:indicatorType&indicatorMeasure=:indicatorMeasure', {}, {
                'get': { method: 'GET', isArray: true},
                'delete': { method: 'DELETE'}// retrieve all kpis stored by Spectre for a specific indicator, identified by:
                                                            // indicator id (id param)
                                                            // connector source (connectorType param)
                                                            // indicator source (indicatorType param)
                                                            // and indicator unity (indicatorMeasure param)

            }),
            kpi:$resource('http://' + ENV.coreEndpoint + '/indicators/:id/inject', {}, {
                'inject': { method: 'POST'}                 // inject custom data into Spectre without fetching data from a connector
                                                            // id: the indicator id
            })
        };
    });
