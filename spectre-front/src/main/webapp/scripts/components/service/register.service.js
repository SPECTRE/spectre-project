'use strict';

angular.module('spectreApp')
    .factory('Register', function ($resource, ENV) {
        return $resource('http://' + ENV.coreEndpoint + '/accounts/register', {}, {
        });
    });
