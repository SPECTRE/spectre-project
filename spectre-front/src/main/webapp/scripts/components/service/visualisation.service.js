'use strict';

angular.module('spectreApp')
    .factory('VisualisationService', function VisualisationService($q, $filter) {

        // convert a timestamp into a formatted date (DD/MM/YYYY)
        var getDateString = function(timestamp) {
            var date = new Date(timestamp);
            return date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear();
        };




        return {


            // set chart data from values for widget, respecting chartist.js data format
            // return an object containing lastData and historyData
            getRepresentationDatas: function (widget, kpiValues) {
                var lastData = {};
                var historyData = {};
                var date = null;
                var values = [];

                // format number values with 1 decimal
                for (var i = 0 ; i < kpiValues.length ; i++) {
                    if ('number' === typeof kpiValues[i].value.data) {
                        var formattedValue = kpiValues[i];
                        formattedValue.value.data = $filter('number')(kpiValues[i].value.data, 1)
                        values.push(formattedValue);
                    } else {
                        values.push(kpiValues[i]);
                    }
                }

                if (widget.indicator.measure == "NUMERIC") {
                    lastData = {
                        series: [],
                        labels: [values[values.length - 1].value.data]
                    };
                    lastData.series.push([]);
                    lastData.series[0] = [values[values.length - 1].value.data];

                    historyData = {
                        labels: [],
                        series: []
                    };
                    historyData.series.push([]);
                    for (var i = 0; i < values.length; i++) {
                        historyData.labels.push(getDateString(values[i].date));
                        historyData.series[0].push(values[i].value.data);
                    }


                } else if (widget.indicator.measure == "PERCENT") {
                    lastData = {
                        series: [],
                        labels: [values[values.length - 1].value.data + '%']
                    };
                    lastData.series.push([]);
                    lastData.series[0] = [values[values.length - 1].value.data];

                    historyData = {
                        labels: [],
                        series: []
                    };
                    historyData.series.push([]);
                    for (var i = 0; i < values.length; i++) {
                        historyData.labels.push(getDateString(values[i].date));
                        historyData.series[0].push(values[i].value.data);
                    }

                } else if (widget.indicator.measure == "TUPLE") {
                    var lastData = {
                        series: [],
                        labels: []
                    };
                    var historyData = {
                        labels: [],
                        series: []
                    };

                    for (var i = 0; i < values[values.length - 1].value.data.length; i++) {
                        lastData.labels.push(values[values.length - 1].value.data[i].label);
                        lastData.series.push(values[values.length - 1].value.data[i].value);
                    }
                    if (values.length > 0) {
                        for (var j = 0; j < values[0].value.data.length; j++) {
                            historyData.series.push([]);
                        }
                    }
                    for (var i = 0; i < values.length; i++) {
                        historyData.labels.push(getDateString(values[i].date));
                        for (var j = 0; j < values[i].value.data.length; j++) {
                            historyData.series[j].push({
                                meta: values[i].value.data[j].label,
                                value: values[i].value.data[j].value
                            });
                        }
                    }

                } else if (widget.indicator.measure == "MOOD") {
                    var lastData = {
                        series: [],
                        labels: [values[values.length - 1].value.data]
                    };
                    lastData.series.push([]);
                    lastData.series[0] = [values[values.length - 1].value.data];

                    var historyData = {
                        labels: [],
                        series: []
                    };
                    historyData.series.push([]);
                    for (var i = 0; i < values.length; i++) {
                        historyData.labels.push(getDateString(values[i].date));
                        var mood;
                        switch (values[i].value.data) {
                            case "SAD":
                                mood = 1;
                                break;
                            case "NORMAL":
                                mood = 2;
                                break;
                            case "HAPPY":
                                mood = 3;
                                break;
                            default:
                                mood = 0;
                                break;
                        }
                        historyData.series[0].push({
                            meta: values[i].value,
                            value: mood
                        });
                    }

                }

                return {lastData: lastData, historyData: historyData};
            }



        };
    })
;
