'use strict';

angular.module('spectreApp')
    .factory('DashboardService', function DashboardService($resource,ENV,  $rootScope){


        return {
            dashboard: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id', null, {
                'allpublic':{method: 'GET', isArray: true},                 // get all public dashboards: DO NOT PROVIDE "id" PARAM
                'get': {method: 'GET', isArray: false},                     // get a dashboard identified by its id
                'update': {method: 'PUT',interceptor: {                     // update a dashboard identified by its id
                    response: function(response) {
                        // expose response
                        $rootScope.$broadcast("dashboardUpdated",null);
                        return response;
                    }
                }},
                'delete':{method: 'DELETE'}                                 // delete a dashboard identified by its id
            }),
            sharedDashboard:  $resource('http://' + ENV.coreEndpoint + '/dashboards/public/shared', null, {
                'get': {method: 'GET', params: {}}                      // get a shared dashbord identified by its id
            }),
            widgets: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id/widgets', null, {
                'get': {method: 'GET', isArray: true}                      // get all widgets associated to the dashboard identified by its id
            }),
            sharedWidgets: $resource('http://' + ENV.coreEndpoint + '/dashboards/public/:id/widgets', null, {
                'get': {method: 'GET', isArray: true}                      // get all widgets associated to a shared dashboard identified by its id
            }),
            fetch: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id/fetch', null, {
                'all': {method: 'POST'}
            }),
            members: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id/members/:login?admin=:admin', {}, {
                'get': {method: 'GET', isArray: true},                      // get all dashboards's members
                'remove': {method: 'DELETE', params: {}},                   // remove a member (identified by his login) of a dashboard (identified by its id)
                'add': {method: 'PUT'},                                     // add a member (identified by his login) to a dashboard (identified by its id)
            }),
            favourites: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id/favourites/:login', {}, {
                'get': {method: 'GET', isArray: true},                      // get all accounts which have the dashboard (identified by its id) in there favourites
                'remove': {method: 'DELETE', params: {}},                   // remove a public dashboard (identified by its id)
                                                                            // from a user (identified by his login) favourite dashboards list
                'add': {method: 'PUT', params: {}},                         // add a public dashboard (identified by its id)
                                                                            // to a user (identified by his login) favourite dashboards lis
            }),
            manageWidgetSet: $resource('http://' + ENV.coreEndpoint + '/dashboards/:id/manageWidgetSet', null, {
                'addPage': {method: 'POST'},
                'deletePage': {method: 'PUT'}
            })
        }
    });
