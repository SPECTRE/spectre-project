'use strict';

angular.module('spectreApp')
    .factory('ReleaseService', function ReleaseService($resource, ENV) {
        return {
            release: $resource('http://' + ENV.coreEndpoint + '/release', {}, {
                'get': { method: 'GET'},
            }),
            env: $resource('http://' + ENV.coreEndpoint + '/release/env', {}, {
                'get': { method: 'GET'},
            })
    }
    
    });
