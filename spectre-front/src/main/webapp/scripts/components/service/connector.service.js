'use strict';

angular.module('spectreApp')
    .factory('ConnectorService', function ConnectorService($resource, ENV) {


        return {
            connector: $resource('http://' + ENV.coreEndpoint + '/connectors/:id', {}, {
                'get': {method: 'GET', isArray: false}, // get a connector identified y its id WITHOUT its configuration
                'update': {method: 'PUT'},               // update a connector identified by its id
                'delete': {method: 'DELETE'}             // delete a connector identified by its id
            }),
            adminConnector: $resource('http://' + ENV.coreEndpoint + '/connectors/:id?owner=true', {}, {
                'get': {method: 'GET', isArray: false}  // get a connector identified by its id WITH its configuration
            }),
            fetch: $resource('http://' + ENV.coreEndpoint + '/connectors/:id/indicators/fetch', {}, {
                'post': {method: 'POST'}
            })
        };
    });
