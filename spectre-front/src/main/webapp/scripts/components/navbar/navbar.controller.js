'use strict';

angular.module('spectreApp')
    .controller('NavbarController', function ($scope,$rootScope, $location, $state, $uibModal, Principal, AuthServerProvider, FullScreenModel) {

        $scope.$state = $state;
        $scope.isAuthenticated = Principal.authorized;
        $scope.isFullScreen = FullScreenModel.getFullScreen();

        FullScreenModel.subscribeFullScreenEvent($scope, function(){
            $scope.isFullScreen = FullScreenModel.getFullScreen();
        });
        // Flush listener on broadcast
        $rootScope.$$listeners['login'] = [];

        $rootScope.$on('login',function(event, account){
            $scope.login = account.login;
        });


        $scope.logout = function () {
            AuthServerProvider.logout();
        };

        $scope.goHome = function(){
            $state.go('dashboards-my-show');
        };

         $scope.logout = function () {
            AuthServerProvider.logout();
        };

        $scope.changeProfile = function () {
            
          var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'scripts/components/navbar/modal/profile/profile.html',
                controller: 'ModalProfilWidgetController',
                size: 'lg',
            });

            modalInstance.result.then(function () {
              }, function () {
              });

        };

         $scope.changePassword = function () {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'scripts/components/navbar/modal/password/password.html',
                controller: 'ModalPasswordWidgetController',
                size: 'lg',
            });

         }
    });
