'use strict';

angular.module('spectreApp')
    .controller('ModalPasswordWidgetController', function ($scope, $timeout, $uibModalInstance, Principal, UserService, AuthServerProvider, SessionModel) {
        $scope.success = null;
         $scope.error = null;
        $scope.doNotMatch = null;

        $scope.newPass = false;
        $scope.errorPassword = null;
        $scope.login = null;
        $scope.password = {
            current : null,
            new : null,
            confirm : null
        };

        var passwordData = {};

        $timeout(function (){angular.element('[ng-model="password.current"]').focus();});

        Principal.checkAuthentication(true).then(function(){
            $scope.login = Principal.identity().login;
        });

        

        $scope.update = function () {
            if ($scope.password.new !== $scope.password.confirm) {
                $scope.doNotMatch = true;
            } else {

                $scope.doNotMatch = false;
                passwordData.newPassword= $scope.password.new;

                UserService.passwordUpdate.update({login:$scope.login}, passwordData).$promise.then(function(){
                $scope.success = true;
                }, function(){
                 $scope.error = true;
                } );

                }

        };

        $scope.checkPassword = function () {
            var credentials = {
                username: $scope.login,
                password : $scope.password.current
            };
            AuthServerProvider.login(credentials)
                .then(function(account){
                    SessionModel.setIsSessionExpired(false);
                    $scope.newPass = true;
                    $scope.errorPassword = false;
                    $timeout(function (){angular.element('[ng-model="password.new"]').focus();});
                }, function(error) {
                   $scope.errorPassword = true;
                });
        };


       $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        };

    });