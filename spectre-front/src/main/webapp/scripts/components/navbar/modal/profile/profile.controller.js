'use strict';

angular.module('spectreApp')
    .controller('ModalProfilWidgetController', function ($scope, $timeout, $uibModalInstance, Principal, UserService) {
        $scope.success = null;
        $scope.error = null;

        $scope.updateAccount = {};

        var login = null;

        
        Principal.checkAuthentication(true).then(function(){

            login =  Principal.identity().login;
            $scope.updateAccount.firstname = Principal.identity().firstname;
            $scope.updateAccount.lastname = Principal.identity().lastname;
            $scope.updateAccount.email = Principal.identity().email;

        });

        $timeout(function (){angular.element('[ng-model="updateAccount.lastname"]').focus();});

        $scope.update = function () {

            UserService.profilUpdate.update({login:login}, $scope.updateAccount).$promise.then(function(){
            $scope.success = true;
            }, function(){
             $scope.error = true;
            } );

        };


       $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
        };

    });
