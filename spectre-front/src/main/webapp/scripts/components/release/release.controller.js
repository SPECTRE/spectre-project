'use strict';

angular.module('spectreApp')
    .controller('ReleaseController', function ($scope, ReleaseService) {

        ReleaseService.release.get().$promise.then(function(data){
            $scope.releases = data;
            }, function(error){
             $scope.error = error;
            } );

    });