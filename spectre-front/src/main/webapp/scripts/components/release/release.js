'use strict';

angular.module('spectreApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('release', {
                parent: 'site',
                url: '/release',
                data: {
                    pageTitle: "Release notes"
                },
                views: {
                    'content@': {
                          templateUrl: 'scripts/components/release/release.html',
                          controller: 'ReleaseController'
                    }
                }
            });
    });