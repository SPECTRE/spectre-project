How to contribute
-------------

- **Fork** project:

![fork project screenshot - step 1](img/contributing_fork_spectre_front_project_step1.png)

![fork project screenshot - step 2](img/contributing_fork_spectre_front_project_step2.png)

![fork project screenshot - step 3](img/contributing_fork_spectre_front_project_step3.png)

![fork project screenshot - step 4](img/contributing_fork_spectre_front_project_step4.png)

---

- **Clone** the git repository of your forked project
(the url of the git repository is available on the welcome page of your forked project on GitLab):

```
git clone git@gitlab.forge.orange-labs.fr:<your_alliance_code>/spectre-front.git
```

![forked project screenshot: url of the git repository](img/contributing_clone_forked_spectre_front_project.png)

---

- **Work** on your forked project.
You can also share it with other people if you are part of a team working together.

- When you are done, **fetch the original project, and merge it into your forked project**
(this way, your contribution will be up to date with the last version of the original project,
which may have evolved simultanously):

>```
git fetch git@gitlab.forge.orange-labs.fr:spectre/spectre-front.git develop
git checkout -b spectre/spectre-front-develop FETCH_HEAD
```

> Review the original project changes locally

> Merge the branch and fix any conflicts that come up:

>```
git checkout <your_branch_name>
git merge --no-ff spectre/spectre-front-develop
```

> Push the result of the merge to GitLab:

>```
git push origin <your_branch_name>
```


- Submit a **merge request** into the branch _develop_ of the original project:

![merge request screenshot: step 1](img/contributing_merge_request_step1.png)

![merge request screenshot: step 2](img/contributing_merge_request_step2.png)

![merge request screenshot: step 3](img/contributing_merge_request_step3.png)

![merge request screenshot: step 4](img/contributing_merge_request_step4.png)

---
That's it:
if everything is OK with your work, **SPECTRE** core-team will now merge it into the original project
so that it becomes available for everyone!
