Welcome on Spectre Front!
===================

Spectre-Front is the Spectre GUI project  .

Based on AngularJS Framework, it allows users to create their own **dashboards** and share them with their teammates.
With a user-friendly interface, they can compose their **dashboard** using out-of-the-box **connectors** on the most popular
tools to follow the key performance indicators of a project, such as:

 - Sonar
 - Jenkins
 - TeamColony
 - Mingle
 - Trello
 - Symbioz
 - Actuator
 - ... It's modular design so you can create your own as needed (see "How to contribute")

Easy to use, simple to configure, with an url and credentials you will be able to collect various **KPI** in **real-time** and follow them in a single page.

The project just started, short demo to discover the workflow will be available soon.


Installation
-------------

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
    Depending on your system, you can install Node either from source or as a pre-packaged bundle.

    After installing Node, you should be able to run the following command to install development tools (like
    [Bower][] ). You will only need to run this command when dependencies change in _package.json_:

    ```
    npm install
    ````

2. We use [Grunt][] as our build system. Install the grunt command-line tool globally with:

    ```
    npm install -g grunt-cli
    ```

3. Install bower dependencies. You will only need to run this command when dependencies change in bower.json_:

    ```
    bower install
    ```

4. Then you need to put the webapp folder in a http server like apache or nginx.

    If you want the code to be updated automatically, you can use grunt serve command (you need to run your http server on port 8080).

    ```
    grunt serve
    ```

___

>**Note**:

>If you have any trouble running the previous commands, it is certainly linked to the Orange corporate proxy.
>You can solve the problems this way on a MS Windows machine.

>We use [Fiddler][] as a local debugging proxy to allow some protocols (used by node especially).

>Launch Fiddler when installed. It automatically updates your browsers settings.

>In Fiddler _Rules_ menu, check the _automatically authenticate_ option.

>Open a MSDOS window. Enter the following commands:

>```
npm config set proxy http://127.0.0.1:8888
npm config set https-proxy http://127.0.0.1:8888
>```

>Make sure you have a correct .bowerrc file when lauching bower install. Your .bowerrc should like this:
>```
{
	"proxy": "http://127.0.0.1:8888",
	"https-proxy": "http://127.0.0.1:8888",
	"registry": {
	    "search": [
	       "http://privatebower-frontfactory.kermit.rd.francetelecom.fr",
	       "https://bower.herokuapp.com"
	   ]
	},
	"directory": "src/main/webapp/bower_components"
}
>```

>The traffic will now go through http://127.0.0.1:8888, your fiddler proxy, and is compatible with the Orange proxy.
>That's all!

___


Using Docker
-------------

A Dockerfile is provided to build an image in order to run spectre-front inside a container.

Your Docker need to be able to connect to the public registry (dockerhub) because we are using the official node image.
After setting up your favorite proxy (fiddler, cntlm), you need to configure the docker daemon to use this proxy.

>**Note**:

>If you are using boot2docker, then just edit this file /var/lib/boot2docker/profile (you may need to sudo) and add http://ip:port to HTTP\_PROXY and HTTPS\_PROXY.

>Also, add to NO_PROXY everything that you need (ex : .orange.com, 192.*, 10.*, 127.0.0.1, localhost etc...)

Validate your setup using this command :

    ```
    docker search ubuntu
    ```
Then you need to pass your proxy information as env var to the docker build command by using :

    ```
    docker build -t <cuid>/spectre-front --build-arg http_proxy=<proxy> --build-arg https_proxy=<proxy> .
    ```

Building this project will take around 20 minutes.

Be aware, the image created expose port 81, so running a fresh container is as simple as running this command :

    ```
    docker run -d --name <container_name> -p <external_port>:81 -e "core_endpoint=<ip:port>" -e "kapi_endpoint=<ip:port>" <cuid>/spectre-front
    ```
    
Use your browser and open the url http://ip:external_port of your container, you should have the frontpage of the webapp.
If not, have a look to logs using this command :

    ```
    docker logs <container_name>
    ```
___


How to run SPECTRE FRONT
-------------

Here is a schema describing **SPECTRE** software architecture:

![SPECTRE software architecture](img/readme_spectre_software_architecture.png)

As is visible on the schema, **SPECTRE FRONT** depends on 2 other **SPECTRE** modules:

- K-API
- SPECTRE CORE.

If you want to run **SPECTRE FRONT** locally without installing all other modules of **SPECTRE** architecture,
you can just target the instances of _K-API_ and _SPECTRE CORE_ which are kept running on a server.
Otherwise, if you prefer, you can install and run locally all **SPECTRE** modules.

You can configure _K-API_ and _SPECTRE CORE_ endpoints for **SPECTRE FRONT** in **SPECTRE FRONT Gruntfile.js**:

- Locate the 2 lines containing _coreEndpoint_ and _kapiEndpoint_
- Edit these lines as follows:

```javascript
coreEndpoint: '10.198.134.162:8083',	//to target SPECTRE CORE on the server
kapiEndpoint: '10.198.134.162:8084',	//to target K-API on the server
```

or

```javascript
coreEndpoint: 'localhost:8083',		//to target SPECTRE CORE locally on your machine
kapiEndpoint: 'localhost:8084',		//to target K-API locally on your machine
```

- Rebuild your project with grunt:

```
grunt build
```

- Start your http server


That's it!
**SPECTRE FRONT** is now accessible in your browser: http://localhost _(add port if you are not using port 80)_.

___

Using Docker
-------------

If you don't want to build an image, you can configure your docker to use our private-repository to immediately run a container based on the official image available.


>**Note**:

>If you are using boot2docker, you can set the private registry quiet easily.
>Find the variable "EXTRA_ARGS" (if it does not exist, add it to the end of the file) in /var/lib/boot2docker/profile (you may need to use sudo), and add this argument :

    ```
    EXTRA_ARGS="--insecure-registry 10.203.4.29:5000"
    ```

> Then, restart docker daemon using

    ```
    sudo /etc/init.d/docker restart
    ```

Validate your setup by using :

    ```
    docker pull 10.203.4.29:5000/vxhv5309/spectre-core
    ```

Then, use this command to create a new container using this command :

    ```
    docker run -d --name <container_name> -p <external_port>:81 -e "core_endpoint=<ip:port>" -e "kapi_endpoint=<ip:port>" 10.203.4.29:5000/vxhv5309/spectre-front
    ```

___

How to contribute
-------------

Please consult the _contribution guide_:

![Link to the contribution guide screenshot](img/readme_contribution_guide_link.png)


[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
[Grunt]: http://gruntjs.com/
[Fiddler]: http://www.telerik.com/fiddler
