package com.orange.spectre.config

import akka.actor.{ActorSystem, ExtendedActorSystem, Extension, ExtensionId, ExtensionIdProvider}
import com.typesafe.config.Config

/**
  * Utility Class to handle application.conf property file
  *
  * Created by Hervé Darritchon on 31/05/2016.
  *
  */
class SettingsAppImpl(config: Config) extends Extension {
  // Spectre Core Config
  val SpectreCoreHost : String = config.getString("spectre-engine.spectre-core.host")
  val SpectreCorePort : Int = config.getInt("spectre-engine.spectre-core.port")

  // Security Config
  val JwtTokenHeader : String = config.getString("spectre-engine.security.jwt.tokenHeader")
  val BasicAuthHeader : String = config.getString("spectre-engine.security.basicAuth.header")
  val ApiLogin : String = config.getString("spectre-engine.security.api.login")
  val ApiPassword : String = config.getString("spectre-engine.security.api.password")

}

object Settings extends ExtensionId[SettingsAppImpl] with ExtensionIdProvider {

  override def lookup = Settings

  override def createExtension(system: ExtendedActorSystem) =
    new SettingsAppImpl(system.settings.config)

  /**
    * Java API: retrieve the Settings extension for the given system.
    */
  override def get(system: ActorSystem): SettingsAppImpl = super.get(system)
}