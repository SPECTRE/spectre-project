package com.orange.spectre.actors

import java.io.IOException
import java.security.InvalidParameterException

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpHeader, HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.orange.spectre.config.Settings
import com.orange.spectre.model.Connector
import spray.json._

import scala.collection.immutable.Seq
import scala.concurrent.Future
import scala.util.{Failure, Success}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val connectorFormat = jsonFormat4(Connector)
}


object Clerk {

  /**
    * Message to Start the Clerk
    *
    * Created by Hervé Darritchon on 11/04/2016.
    */
  case class StartClerkMessage()

  case class FetchConnectorInfo()

  /**
    * Message to Stop the Clerk
    */
  case class StopClerkMessage()

  def props(token: String): Props = Props(new Clerk(token))

}

/**
  * This Actor main goal is to consume ReST API from the Spectre Core and give Data to the Scheduler.
  * It's important in an asynchronous way to separate the computation from the data retrieving.
  * <p>
  * Messages :
  * StartClerkMessage : Start the Actor abd retrieve the Connector data from the Spectre-Core API
  * RefreshConfigurationMessage : Demande la récupération de configuration des Connecteurs à la base de données
  * StopClerkMessage : Stop gracefully the Clerk
  * <p>
  * Created by Hervé Darritchon on 10/05/2016.
  */
class Clerk(token: String) extends Actor with ActorLogging with JsonSupport {

  import Clerk._

  // Dispatcher is used as implicit for all the future call methods in this class
  import context.dispatcher

  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  val http = Http(context.system)
  val settings = Settings(context.system)

  lazy val spectreCoreConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    http.outgoingConnection(settings.SpectreCoreHost, settings.SpectreCorePort)

  override def preStart(): Unit = {
    // Initialize children here
    log.debug("Pre-Starting Clerk")
  }

  override def postStop(): Unit = {
    // Initialize children here
    log.debug("Post-Stop Clerk")
  }

  /**
    * Make the ReST call to the API Producer
    *
    * @param request : the request constructed with body, headers, path, ...
    * @return a Future to the response of the API Call
    */
  def connectorApiRequest(request: HttpRequest): Future[HttpResponse] = {
    Source.single(request).via(spectreCoreConnectionFlow).runWith(Sink.head)
  }

  /**
    * Retrieve the connector configuration from Sprectre Core API
    *
    * @param jwtToken : the security token
    * @return a Future of a response that can be either a list of connector with their configuration or either an error
    */
  def fetchConnectorInfo(jwtToken: String): Future[Either[String, List[Connector]]] = {
    val request: HttpRequest = RequestBuilding.Get("/connectors")
    val jwtAuthHeader: HttpHeader = RawHeader(settings.JwtTokenHeader, jwtToken)
    val headers: Seq[HttpHeader] = request.headers.seq ++ Seq(jwtAuthHeader)


    connectorApiRequest(request.withHeaders(headers)).flatMap { response =>
      response.status match {
        case StatusCodes.OK => Unmarshal(response.entity).to[List[Connector]].map(Right(_))
        case StatusCodes.BadRequest => Future.failed(new InvalidParameterException(s"bad parameter"))
        case StatusCodes.Unauthorized => Future.failed(new RuntimeException(s"token expired"))
        case _ => {
          val error = s"FAIL - ${response.status}"
          Future.failed(new IOException(error))
        }
      }
    }
  }

  /**
    * To handle the message of this Actor
    *
    * @return
    */
  override def receive: Receive = {

    case StartClerkMessage => {

      log.debug(s"Starting the clerk with id : $this")
    }

    case FetchConnectorInfo => {

      log.debug(s"Starting the clerk with id : $this")
      // Store the sender for a later use in a Future because in a Future the sender is lost !!
      val origSender = sender

      val response: Future[List[Connector]] = fetchConnectorInfo(token).map[List[Connector]] {
        case Right(c) => c
        case Left(errorMessage) => List()
      }

      response.onComplete {
        case Success(result) => {
          log.debug("Parsing is a Success")
          origSender ! Receptionist.ConnectorInformationMessage(result)
        }
        case Failure(throwable:Throwable) => {
          throwable match {
            case throwable:RuntimeException => {
              log.debug(s"Refresh Token")
              origSender ! Receptionist.RefreshSecurityTokenMessage
              context stop self
            }
            case throwable:IOException => {
              log.debug(s"Error 500 - ${throwable.getMessage}")
              log.error(throwable.getMessage)
            }
            case _ => {
              log.debug(s"Unknown error - ${throwable.getMessage}")
              log.error(throwable.getMessage)
            }
          }
        }
      }

    }

    case StopClerkMessage => log.debug(s"Stopping the clerk with id : $this")
    case _ => log.warning("Unknown message !!")
  }

}

