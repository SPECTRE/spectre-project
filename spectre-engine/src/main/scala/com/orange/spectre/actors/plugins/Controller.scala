package com.orange.spectre.actors.plugins

import java.security.InvalidParameterException
import java.util.concurrent.TimeoutException

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.orange.spectre.actors.KeyWarden
import com.orange.spectre.config.Settings
import com.orange.spectre.model.Connector
import spray.json.DefaultJsonProtocol

import scala.collection.immutable.Seq
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Failure, Success}

object Controller {

  /**
    * Message handled by the Supervisor
    *
    * Created by Hervé Darritchon on 13/05/2016.
    */
  case class StartControllerMessage()

  case class RefreshSecurityTokenMessage(token: String)

  case class RefreshKpiMessage()

  case class UpdateConnectorConfigurationMessage(connector: Connector, token: String)

  case class TimeOutMessage(message: String)

  case class StopControllerMessage()

  /**
    * Create Props for an actor of this type.
    *
    * @param connector The connector to be passed to this actor’s constructor.
    * @return a Props for creating this actor, which can then be further configured
    *         (e.g. calling `.withDispatcher()` on it)
    */
  def props(connector: Connector, jwtToken: String): Props = Props(new Controller(connector, jwtToken))

}

/**
  * Interface used to unmarshall a json response into an Object
  *
  */
trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val connectorFormat = jsonFormat4(Connector)
}

/**
  * Acteur Controller dont le rôle est d'ordonnacer le traitement d'un Plugin/Connector. C'est le cerveau du Plugin.
  * Il va orchestrer la récupération des données via l'acteur Getter puis stocker dans ES via l'acteur Scribe.
  * <p>
  * Remarque :
  * Actuellement, il n'y a qu'un seul Getter même si il y a plusieurs Indicateurs à récupérer.
  * <p>
  * Messages :
  * StartControllerMessage : Démarre le Controller qui orchestre la mise à jour des données du connecteur.
  * SonarInformationMessage : Demande la récupération des informations dans Sonar
  * StoredInESMessage : Demande la mise à jour de la base de données ES avec les données fraîches de Sonar.
  * Terminated : Message en provenance des acteurs qui sont surveillés par l'acteur (watch) lorsqu'il se sont terminés
  * StopGracefullyMessage : Demande l'arrêt propre de l'acteur
  * <p>
  * Created by Hervé Darritchon on 13/05/2016.
  */
class Controller(pConnector: Connector, jwtToken: String) extends Actor with ActorLogging with JsonSupport {

  import Controller._

  private var scheduler: ActorRef = context.parent

  private var token = jwtToken

  private var connector = pConnector

  //Use system's dispatcher as ExecutionContext
  import context.dispatcher

  import scala.concurrent.duration._

  //Use to unmarshall some data
  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  val http = Http(context.system)

  val settings = Settings(context.system)

  lazy val spectreCoreConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    http.outgoingConnection(settings.SpectreCoreHost, settings.SpectreCorePort)

  //This will schedule to send the Tick-message
  //to the tickActor after 0ms repeating every 50ms
  var cancellable =
  context.system.scheduler.schedule(0 milliseconds,
    connector.periodicity seconds,
    self,
    RefreshKpiMessage)

  def connectorApiRequest(request: HttpRequest): Future[HttpResponse] =
//    Source.single(request).via(spectreCoreConnectionFlow).idleTimeout(20 minutes).completionTimeout(4 seconds).initialTimeout(18 minutes).runWith(Sink.head)
    Source.single(request).via(spectreCoreConnectionFlow).runWith(Sink.head)

  def fetchConnectorInfo(): Future[Either[String, String]] = {
    val connectorId = connector.id

    /*
      jwt.payload.issuer: Spectre Auth Server
      jwt.payload.subject: Spectre Token
      jwt.signature.secretkey: c29jbGVfcmNkX2x1ZG9fbmljb192YWxvX2NhdGhfdmV2ZQ==
    */

    val path: String = s"/connectors/$connectorId/indicators/fetch"
    val request: HttpRequest = RequestBuilding.Post(path)
    val jwtAuthHeader: HttpHeader = RawHeader(settings.JwtTokenHeader, token)
    val headers: Seq[HttpHeader] = request.headers.seq ++ Seq(jwtAuthHeader)

    connectorApiRequest(request.withHeaders(headers)).flatMap { response =>
      response.status match {
        case StatusCodes.OK => Future.successful(Right(s"WS successfully return with status - ${StatusCodes.OK}"))
        case StatusCodes.Created => Future.successful(Right(s"WS successfully return with status - ${StatusCodes.Created}"))
        case StatusCodes.BadRequest => Future.failed(new InvalidParameterException(s"bad request"))
        case StatusCodes.Unauthorized => Future.failed(new RuntimeException(s"token expired"))
        case s => Future.failed(new Exception(s"FAIL ${response.status} - ${s.defaultMessage()}"))
      }
    }
  }

  override def postRestart(reason: Throwable): Unit = {
    //super.postRestart(reason)
    log.debug(s"Controller LifeCycle : Controller with id $this has been restarted with reason ${reason.getMessage} !!")
  }

  override def preStart() = {
    //This cancels further Ticks to be sent
    log.debug(s"Controller LifeCycle : Controller (${context.self.path}) with id $this is about to start")
  }

  override def postStop() = {
    //This cancels further Ticks to be sent
    log.debug(s"Controller LifeCycle : Controller with id $this is stopped")
    cancellable.cancel()
  }

  override def receive: Receive = {

    case StartControllerMessage => {
      log.debug("Controller " + this.toString + " is started with refresh periodicity - " + connector)
      this.scheduler = sender()
    }

    case RefreshKpiMessage => {

      log.debug(s"Refresh this connector Kpi with connector - ${connector.name}")

      val me = self

      val response: Future[String] = fetchConnectorInfo().map[String] {
        case Right(m) => m
        case Left(errorMessage) => errorMessage
      }
      response.onComplete {
        case Success(result) => {
          log.debug(s"Updating connector ${connector.id}/${connector.name} is a Success with message - ${result}")
        }
        case Failure(throwable) => {
          throwable match {
            case t: RuntimeException => {
              log.warning(s"Updating connector ${connector.id}/${connector.name} is a Failure because token has expired with throwable - ${throwable.getClass.toString}")
              //scheduler ! Scheduler.InvalidSecurityToken
              throw KeyWarden.TokenExpiredException()
              context stop self
            }
            case t: TimeoutException => {
              log.warning(s"Updating connector ${connector.id}/${connector.name} status is unknown with throwable - ${throwable.getClass.toString}")
              me ! TimeOutMessage(t.getMessage)
            }
            case _ => {
              log.debug(s"Updating connector ${connector.id}/${connector.name} is a Failure (message: ${throwable.getMessage}) because of an unknown error with throwable - ${throwable.getClass.toString}")
            }
          }
        }
      }
    }

    case UpdateConnectorConfigurationMessage(connector: Connector, token: String) => {
      log.debug(s"Update Connector Configuration")
      if (connector.periodicity != this.connector.periodicity) {
        cancellable.cancel()
        cancellable = context.system.scheduler.schedule(0 milliseconds,
          connector.periodicity seconds,
          self,
          RefreshKpiMessage)
      }
      this.connector = connector
      this.token = token
    }

    case RefreshSecurityTokenMessage(token: String) => {
      log.debug(s"Refreshing SecurityToken value")
      this.token = token
    }
    case StopControllerMessage => {
      log.debug(s"Stopping the controller with id : $this")
    }

    case TimeOutMessage(message) => {
      log.debug(s"Timeout error but data should be updated in spectre-core so we resume")
      throw new scala.concurrent.TimeoutException(s"connector ${connector.id}/${connector.name} is in Timeout during update !")
    }

    case _ => {
      log.warning("Unknown message !!")
    }

  }

}