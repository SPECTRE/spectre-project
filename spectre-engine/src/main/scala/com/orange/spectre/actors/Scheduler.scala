package com.orange.spectre.actors

import akka.actor.SupervisorStrategy.{Restart, Resume, Stop}
import akka.actor.{Actor, ActorLogging, ActorRef, OneForOneStrategy, Props}
import com.orange.spectre.actors.KeyWarden.TokenExpiredException
import com.orange.spectre.actors.plugins.Controller
import com.orange.spectre.model.{Connector, JwtToken}

import scala.concurrent.TimeoutException


object Scheduler {

  /**
    * Message handled by the Supervisor
    *
    * Created by Hervé Darritchon on 11/04/2016.
    */
  case class StartSchedulerMessage()

  case class ConnectorConfigurationMessage(connectors: List[Connector], jwtToken: String)

  case class TickSelectConnectorToBeRefreshedMessage()

  case class ReadyToFetchMessage(jwtToken: String)

  case class NewSecurityTokenMessage(securityToken: JwtToken)

  case class InvalidSecurityToken()

  case class StopSchedulerMessage()

  /**
    * Create Props for an actor of this type.
    *
    * @param receptionist The ref to the receptionist actor to be passed to this actor’s constructor.
    * @return a Props for creating this actor, which can then be further configured
    *         (e.g. calling `.withDispatcher()` on it)
    */
  def props(receptionist: ActorRef): Props = Props(new Scheduler(receptionist))

}

/**
  * This Actor main goal is to schedule the update of all the connector based upon their configuration.
  * Each connector has a refresh periodicity stored in the database.
  * The scheduler based on this periodicity will trigger the process of refreshing the kpi of each connector when the time is up
  * To be able to trigger the refresh process, the scheduler uses an autoinject pattern (<a href='http://doc.akka.io/docs/akka/snapshot/scala/scheduler.html'>Akka Documentation</a>)
  * One message is autoinjected to the scheduler :
  * - A tick to tell the Scheduler to search for all the connector to refresh (TickSelectConnectorToBeRefreshedMessage).
  * <p>
  * Messages :
  * StartSchedulerMessage : Start the scheduler and create the Clerk actor.
  * ReadyToFetchMessage : Tell the Scheduler that is ready to Fetch data from Spectre-Core. It means Authentication to the Spectre-Core API has been done.
  * ConnectorConfigurationMessage : Connector Data from the Spectre-Core API through the Clerk actor.
  * StopSchedulerMessage : Stop the scheduler gracefully
  * <p>
  * Created by Hervé Darritchon on 29/03/2016.
  */
class Scheduler(receptionist: ActorRef) extends Actor with ActorLogging {

  import Scheduler._

  var idx: Long = 0

  private var controllers: Map[String, (Connector, ActorRef)] = Map()

  override def supervisorStrategy = OneForOneStrategy() {
    case _: TokenExpiredException => {
      log.warning("restart actor because token is not valid anymore")
      Restart
    }
    case _: TimeoutException => {
      log.warning("resume actor because of Timeout")
      Resume
    }
    case _: Exception => {
      log.error("stop actor because of unspecified Exception")
      Stop
    }
  }

  /*  final val defaultStrategy: SupervisorStrategy = {
      val defaultDecider: Decider = {
        case e: KeyWarden.TokenExpiredException => {
          log.warning("stop actor because token is not valid anymore")
          Stop
        }
        case e: java.util.concurrent.TimeoutException => {
          log.warning("resume actor because of Timeout")
          Resume
        }
        case e: Exception => {
          log.error("restart actor because of unspecified Exception")
          Restart
        }
      }
      OneForOneStrategy()(defaultDecider)
    }*/

  override def preStart(): Unit = {
    // Initialize children here
    log.debug("Pre-Starting Scheduler")
  }

  override def postStop(): Unit = {
    // Initialize children here
    log.debug("Post-Stop Scheduler")
  }

  override def receive: Receive = {
    case StartSchedulerMessage => {
      log.debug(s"Starting the scheduler with id : $this")

    }
    case ConnectorConfigurationMessage(connectors, jwtToken) => {
      /* Clean Up the controllers */
      for (connector: Connector <- connectors) {
        if (controllers.contains(connector.name)) {
          val runningConnector = controllers.get(connector.name)
          runningConnector.get._2 ! Controller.UpdateConnectorConfigurationMessage(runningConnector.get._1, jwtToken)
        }
        else {
          log.debug("Starting controller for connector --> " + connector.name)
          val controllerProps = Props(classOf[Controller], connector, jwtToken)

          // Create the 'controller' actor
          val controller = context.actorOf(controllerProps, s"controller-$idx-${connector.id}")
          context.watch(controller)
          controller ! Controller.StartControllerMessage
          controllers = controllers + (connector.name -> (connector, controller))
        }
      }
      log.debug(s"Size of the controller list after update is ${controllers.size}")
    }
    case InvalidSecurityToken => {
      log.debug(s"Security token is invalid and need to be refreshed.")
    }

    case NewSecurityTokenMessage(token) => {
      log.debug(s"New Security Token is available - ${token.accessToken}.")

    }

    case ReadyToFetchMessage(jwtToken) => {
      log.debug(s"System is ready to fetch connector information from Spectre-Core.")
    }

    case StopSchedulerMessage => log.debug(s"Stopping the scheduler with id : $this")

    case message => log.warning(s"Unknown message ${message.getClass} !!")
  }
}
