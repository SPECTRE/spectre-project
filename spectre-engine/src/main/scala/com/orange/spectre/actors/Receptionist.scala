package com.orange.spectre.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.orange.spectre.model.{Connector, JwtToken, RawJwtToken}

object Receptionist {

  /**
    * Receptionist Start Message
    *
    */
  case class StartReceptionistMessage(scheduler: ActorRef)

  /**
    * Message to Stop gracefully the Receptionist actor.
    *
    */
  case class StopReceptionistMessage()

  /**
    *
    */
  case class RefreshSecurityTokenMessage();

  /**
    * Message from the KeyWarden actor.
    *
    * @param token the Jwt Security Token. This token is used to grant access to the Spectre Code API.
    */
  case class ReceiveRawSecurityToken(token: Option[RawJwtToken])

  /**
    * Message from the Clerk.
    *
    * @param connectors the list of connectors receive from the Spectre Core API
    */
  case class ConnectorInformationMessage(connectors: List[Connector])

  /**
    * Message that respond to the question message SendSecurityTokenMessage with the security token
    *
    * @param token a complex object composed with the raw token, the token validity duration and the header and payload extracted from the raw roken
    *
    */
  case class SecurityTokenTranslatedMessage(token: JwtToken)

}

/**
  * This actor role is to provide an actor to manage security and connection to the Spectre Core API.
  * This actor is the ancestor actor, it manages the creation of the actors and the supervision of these actors.
  *
  */
class Receptionist() extends Actor with ActorLogging {

  /**
    * a variable used to make actors name unique.
    */
  var idx: Long = 0

  import Receptionist._

  private var scheduler: ActorRef = _

  private var securityToken: Option[JwtToken] = None

  // Create the 'clerk' actor
  private var clerk: ActorRef = _

  // Create the 'keyWarden' actor
  private var keyWarden = context.actorOf(KeyWarden.props(0), "key-warden")

  // Create the 'keyWarden' actor
  private val interpreter = context.actorOf(Props[Interpreter], "interpreter")

  def tokenIsDifferent(oldToken: JwtToken, newToken: JwtToken): Boolean = oldToken.accessToken != newToken.accessToken

  /**
    * To handle the messages of this Actor
    *
    * @return
    */
  override def receive: Receive = {
    case StartReceptionistMessage(scheduler) => {
      this.scheduler = scheduler
      log.debug(s"Start the Receptionist with id : $this")
    }

    case StopReceptionistMessage => {
      log.debug(s"Stopping gracefully the Receptionist with id : $this")
    }

    case ReceiveRawSecurityToken(optionRawToken) => {
      log.debug(s"Ask the Interpreter to translate and create the complex object JwtToken based on the RawToken.")
      interpreter ! Interpreter.TranslateSecurityTokenMessage(optionRawToken.get)
    }

    case SecurityTokenTranslatedMessage(token) => {
      log.debug(s"Security token validity duration is ${token.duration}")
      if (securityToken.isEmpty || tokenIsDifferent(token, this.securityToken.get)) {
        log.debug(s"A new Token is available to the system.")
        context stop keyWarden
        //FIXME This is ugly. I chose to fix the duration to 90% of the token duration.
        keyWarden = context.actorOf(KeyWarden.props((token.duration * 0.9).toInt), s"key-warden-$idx")
      }
      securityToken = Some(token)
      this.idx = this.idx + 1
      if (clerk != null) context stop clerk
      clerk = context.actorOf(Clerk.props(securityToken.get.accessToken), s"clerk-$idx")
      clerk ! Clerk.FetchConnectorInfo
    }

    case ConnectorInformationMessage(connectors) => {
      log.debug(s"Receiving a list of connector configuration, size - ${connectors.size}")
      scheduler ! Scheduler.ConnectorConfigurationMessage(connectors, this.securityToken.get.accessToken)
    }

    case RefreshSecurityTokenMessage => {
      log.debug(s"Refresh security token")
    }

    case message => log.warning(s"Unknown message ${message.getClass} !!")
  }


}
