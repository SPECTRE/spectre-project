package com.orange.spectre.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}

object Supervisor {

  /**
    * Message to Begin the Supervisor to work properly doing all the stuff it has to do
    *
    * Created by Hervé Darritchon on 11/04/2016.
    */
  case class StartSupervisorMessage()

  /**
    * Message to Stop gracefully the Supervisor.
    *
    */
  case class StopSupervisorMessage()

}

/**
  * This actor is the main ancestor of the actor hierarchy.
  * It has no really functional role, in fact, it used as the first parent of all the other actors just to be able to supervise those actors.
  * It has to handle all the life cycle of the other actors, handle errors. It has to know how and what to do when there is a failure in the application.
  * <p>
  * Messages :
  * StartSystemMessage : Start the Supervisor actor which will start the Scheduler Actor and the Receptionist
  * StopGracefullyMessage : Stop the Supervisor in a safe manner. It will first stop the Scheduler.
  * <p>
  * Created by Hervé Darritchon on 21/03/2016.
  */
class Supervisor extends Actor with ActorLogging with TokenJsonSupport {

  import Supervisor._

  private var receptionist: ActorRef = null

  override def preStart(): Unit = {
    // Initialize children here
    log.debug("Pre-Starting Supervisor")
  }

  override def postStop(): Unit = {
    // Initialize children here
    log.debug("Post-Stop Supervisor")
  }

  override def receive: Receive = {
    case StartSupervisorMessage => {
      log.debug(s"Starting the supervisor with id - $this")

      // Create the 'receptionist' actor
      val receptionist: ActorRef = context.actorOf(Props[Receptionist], "receptionist")

      this.receptionist = receptionist

      // Create the 'scheduler' actor
      val scheduler: ActorRef = context.actorOf(Scheduler.props(receptionist), "scheduler")

      this.receptionist ! Receptionist.StartReceptionistMessage(scheduler)

      scheduler ! Scheduler.StartSchedulerMessage

    }
    case StopSupervisorMessage => log.debug(s"Stopping the supervisor gracefully with id - $this")
    case message => log.warning(s"Unknown message ${message.getClass} !!")
  }

}
