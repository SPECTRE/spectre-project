package com.orange.spectre.actors

import java.util.Base64

import akka.actor.{Actor, ActorLogging}
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.orange.spectre.model.{JwtToken, JwtTokenHeader, JwtTokenPayload, RawJwtToken}
import spray.json.DefaultJsonProtocol

trait TokenPartsJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val jwtTokenHeaderFormat = jsonFormat2(JwtTokenHeader)
  implicit val jwtTokenPayloadFormat = jsonFormat5(JwtTokenPayload)
}

object Interpreter {

  case class StartInterpreterMessage()

  /**
    * Question message that ask the Translater to decompose the raw token and create the complex object JwtToken
    */
  case class TranslateSecurityTokenMessage(rawToken: RawJwtToken)

  case class StopInterpreterMessage()

}

/**
  * Created by Hervé Darritchon on 17/08/2016.
  *
  */
class Interpreter() extends Actor with ActorLogging with TokenPartsJsonSupport {

  import Interpreter._

  // Dispatcher is used as implicit for all the future call methods in this class
  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  /*  private def decode[T](raw: String) : Future[Either[String,T]] = {
      val decoded = java.util.Base64.getDecoder.decode(raw).toString
      Unmarshal(decoded).to[T].map(Right(_))
    }*/


  def computeDuration(payload: JwtTokenPayload): Int = payload.exp.toInt - payload.iat.toInt

  def translate(rawToken: RawJwtToken): JwtToken = {
    val parts: Array[String] = rawToken.accessToken.split("""\.""")
    //val parts: Seq[String] = rawToken.accessToken.split(".").toSeq


    val decodedHeader: String = new String(Base64.getDecoder.decode(parts(0)))
    val decodedPayload: String = new String(Base64.getDecoder.decode(parts(1)))

    //val header: Future[Either[String, JwtTokenHeader]] = Unmarshal(decodedHeader).to[JwtTokenHeader].map(Right(_))
    //val payload: Future[Either[String, JwtTokenPayload]] = Unmarshal(decodedPayload).to[JwtTokenPayload].map(Right(_))

    import spray.json._
    val header: JwtTokenHeader = decodedHeader.parseJson.convertTo[JwtTokenHeader]
    val payload: JwtTokenPayload = decodedPayload.parseJson.convertTo[JwtTokenPayload]

    //val header: Future[JwtTokenHeader] = Unmarshal(decodedHeader).to[JwtTokenHeader]

    //val header = Unmarshal(decodedHeader).to[JwtTokenHeader]

    //TODO Stub waiting for the unmarshalling issue with implicit to be fixed
    //val payload: JwtTokenPayload = new JwtTokenPayload(1471609884, "Spectre Auth Server", "Spectre Token", 1471609944, "api-user")
    //val header: JwtTokenHeader = new JwtTokenHeader("JWT", "HS256")

    new JwtToken(rawToken.accessToken, computeDuration(payload), header, payload)
  }

  override def preStart(): Unit = {
    // Initialize children here
    log.debug("Pre-Starting Interpreter")
  }

  override def postStop(): Unit = {
    // Destruct children here
    log.debug("Post-Stop Interpreter")
  }

  /**
    * To handle the message of this Actor
    *
    * @return
    */
  override def receive: Receive = {

    case StartInterpreterMessage => {
      log.debug(s"Starting the Interpreter with id : $this")
    }

    case TranslateSecurityTokenMessage(rawToken) => {
      log.debug(s"Message asking to translate a token with value : $rawToken")
      val token: JwtToken = translate(rawToken)
      sender ! Receptionist.SecurityTokenTranslatedMessage(token)
    }

    case StopInterpreterMessage => {
      log.debug(s"Stopping the Interpreter with id : $this")
    }

    case _ => log.warning("Unknown message !!")

  }
}
