package com.orange.spectre.actors

import java.io.IOException
import java.security.InvalidParameterException

import akka.actor.{Actor, ActorLogging, Props}
import akka.http.scaladsl.Http
import akka.http.scaladsl.client.RequestBuilding
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.model.{HttpHeader, HttpRequest, HttpResponse, StatusCodes}
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.scaladsl.{Flow, Sink, Source}
import akka.stream.{ActorMaterializer, ActorMaterializerSettings}
import com.orange.spectre.config.Settings
import com.orange.spectre.model.RawJwtToken
import spray.json.DefaultJsonProtocol

import scala.collection.immutable.Seq
import scala.concurrent.Future
import scala.language.postfixOps
import scala.util.{Failure, Success}

trait TokenJsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val jwtTokenFormat = jsonFormat1(RawJwtToken)
}

object KeyWarden {

  /**
    * Exception when the token has expired
    *
    */
  case class TokenExpiredException() extends RuntimeException("Token expired")

  /**
    * Message to Start the Supervisor
    *
    * Created by Hervé Darritchon on 11/04/2016.
    */
  case class StartKeyWardenMessage()

  case class StopKeyWardenMessage()

  case class WhatIsTheSecurityToken()

  case class FetchSecurityToken()

  case class RefreshSecurityToken()

  def props(refreshPeriodicity: Int): Props = Props(new KeyWarden(refreshPeriodicity))
}

/**
  * 0 --> just 1 time
  * n --> token refresh periodicity
  *
  * @param refreshPeriodicity
  */
class KeyWarden(refreshPeriodicity: Int = 0) extends Actor with ActorLogging with TokenJsonSupport {

  import KeyWarden._

  val receptionist = context.parent

  // Dispatcher is used as implicit for all the future call methods in this class
  import context.dispatcher

  import scala.concurrent.duration._

  final implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(context.system))

  val http = Http(context.system)
  val settings = Settings(context.system)

  log.debug(s"KeyWarden created with refreshPeriodicity = $refreshPeriodicity")

  val cancellable = {
    if (refreshPeriodicity == 0) {
      log.debug("KeyWarden set to run once")
      context.system.scheduler.scheduleOnce(10 milliseconds,
        self,
        KeyWarden.FetchSecurityToken)
    }
    else {
      log.debug("KeyWarden set to run in a not ending loop")
      context.system.scheduler.schedule(0 milliseconds,
        refreshPeriodicity seconds,
        self,
        KeyWarden.FetchSecurityToken)
    }
  }

  override def preStart(): Unit = {
    // Initialize children here
    log.debug("Pre-Starting Key Warden")
  }

  override def postStop(): Unit = {
    // Initialize children here
    log.debug("Post-Stop Key warden")
    cancellable.cancel()
  }

  /**
    * Make the ReST call to the API Producer
    *
    * @param request : the request constructed with body, headers, path, ...
    * @return a Future to the response of the API Call
    */
  def connectorApiRequest(request: HttpRequest): Future[HttpResponse] = {
    Source.single(request).via(spectreCoreConnectionFlow).runWith(Sink.head)
  }

  lazy val spectreCoreConnectionFlow: Flow[HttpRequest, HttpResponse, Any] =
    http.outgoingConnection(settings.SpectreCoreHost, settings.SpectreCorePort)

  /**
    * Retrieve the security token using the Spectre Core security API using the login/credential of the API fake user.
    *
    * @return a Future of a response that can be either an error or either a JwtToken
    */
  def fetchJwtToken(): Future[RawJwtToken] = {
    val request: HttpRequest = RequestBuilding.Post("/token/basic")
    val credentials: String = s"${settings.ApiLogin}:${settings.ApiPassword}"

    val encodedCredentials: String = java.util.Base64.getEncoder.encodeToString(credentials.getBytes("UTF-8"))
    log.debug(s"request with credential encoded : $encodedCredentials")
    val authHeader: HttpHeader = RawHeader(settings.BasicAuthHeader, s"Basic $encodedCredentials")
    val headers: Seq[HttpHeader] = request.headers.seq ++ Seq(authHeader)

    connectorApiRequest(request.withHeaders(headers)).flatMap { response =>
      response.status match {
        case StatusCodes.OK => {
          Unmarshal(response.entity).to[RawJwtToken]
        }
        case StatusCodes.BadRequest => Future.failed(new InvalidParameterException(s"bad request"))
        case _ => Unmarshal(response.entity).to[String].flatMap { entity =>
          val error = s"FAIL - ${response.status}"
          Future.failed(new IOException(error))
        }
      }
    }
  }


  /**
    * To handle the message of this Actor
    *
    * @return
    */
  override def receive: Receive = {

    case StartKeyWardenMessage => {

      log.debug(s"Starting the Key Warden with id : $this")

    }
    case FetchSecurityToken => {

      log.debug(s"Fetching Security token Spectre-Core")

      fetchJwtToken().onComplete {
        case Success(result) => {
          val token: String = result.accessToken
          log.debug(s"Fetching Jwt Token is a Success with ${token}.")
          receptionist ! Receptionist.ReceiveRawSecurityToken(Some(RawJwtToken(token)))
        }
        case Failure(exception) => {
          log.debug("Parsing is a Failure")
          log.error(exception.getMessage)
          None
          receptionist ! Receptionist.ReceiveRawSecurityToken(None)
        }

      }

    }

    /*      case RefreshSecurityToken => {
            log.debug(s"Refresh security token")

            val securityToken = getSecurityToken()

            securityToken match {
              case Some(token) => {
                receptionist ! Receptionist.ReceiveRawSecurityToken(Some(token))
              }
              case None => {
                receptionist ! Receptionist.ReceiveRawSecurityToken(None)
              }
            }
          }*/

    case StopKeyWardenMessage => {
      log.debug(s"Stopping the Key Warden with id : $this")
    }

    case _ => log.warning("Unknown message !!")

  }
}
