package com.orange.spectre.model

/**
  * Created by Hervé Darritchon on 11/05/2016.
  *
  */
final case class Connector(
                            id: Long,
                            name: String,
                            `type`: String,
                            periodicity: Long
                            /*                      ,params: Map[String, String],
                                                  indicators: Set[AbstractIndicator]*/
                          ) {

  def isEqual(that: Connector) = (periodicity == that.periodicity)
}
