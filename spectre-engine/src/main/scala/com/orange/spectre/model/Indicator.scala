package com.orange.spectre.model

import java.time.LocalDateTime

/**
  * Created by Hervé Darritchon on 11/05/2016.
  *
  */
case class Indicator(
                              //connector: AbstractConnector,
                              exportDate: LocalDateTime,
                              //measure: IndicatorMeasure,
                              measureUnity: String
                            )
