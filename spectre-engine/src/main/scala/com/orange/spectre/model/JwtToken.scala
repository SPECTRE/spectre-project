package com.orange.spectre.model

/**
  * A case class to store the JwtToken object.
  * It's an object constructed from the Raw Token get from the Spectre-Core API.
  * The Header et and Payload of the token have been expanded in an object and the duration of the token validity has been computed.
  *
  * @param accessToken : the raw Token
  * @param duration : the computed validity duration of the token
  * @param header : the Header as an object
  * @param payload : the payload as an object.
  */
case class JwtToken(accessToken: String, duration: Int, header: JwtTokenHeader, payload: JwtTokenPayload)
