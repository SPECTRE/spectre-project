package com.orange.spectre.model

/**
  * Created by Hervé Darritchon on 30/05/2016.
  *
  */
case class RawJwtToken(accessToken: String)
