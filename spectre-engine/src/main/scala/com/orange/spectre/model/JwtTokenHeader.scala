package com.orange.spectre.model

/**
  * Created by Hervé Darritchon on 30/05/2016.
  *
  */
case class JwtTokenHeader(`type`: String, alg: String)
