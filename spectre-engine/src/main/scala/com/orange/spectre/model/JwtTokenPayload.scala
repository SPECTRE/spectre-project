package com.orange.spectre.model

/**
  * Created by Hervé Darritchon on 30/05/2016.
  *
  */
case class JwtTokenPayload(iat: Long, iss: String, sub: String, exp: Long, user_id: String)
