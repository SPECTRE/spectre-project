package com.orange.spectre

import akka.actor.{ActorRef, ActorSystem, Props}
import com.orange.spectre.actors.Supervisor

/**
  * In this modular architecture (microservice architecture ???), this app is used to handle and manage the scheduling of all the connector refreshing tasks.
  * To refresh the kpi data of Spectre, we use a Scheduler that will depending on a timer start a connector and refresh is data.
  *
  * Created by Hervé Darritchon on 04/05/2016.
  *
  */
object SpectreEngineApplication extends App {

  // Create the 'Spectre Engine' actor system
  val system: ActorSystem = ActorSystem("spectre-engine")

  // Create the 'greeter' actor
  val supervisor: ActorRef = system.actorOf(Props[Supervisor], "supervisor")

  supervisor ! Supervisor.StartSupervisorMessage

}
