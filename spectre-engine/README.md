## Synopsis

This module is part of Spectre project. It main goal is to schedule connector data updates.
Each connector update will be triggered based on the Spectre-Core Configuration stored in the MySql Database.

Spectre-Engine is based on Akka Framework, coded in Scala.

Akka framework is an Actor based framework.

In this module, we have several actors :

* Supervisor --> supervise the system
* Scheduler --> schedule all the connector update
* Clerk --> consume spectre-core API to retrieve the connector configuration
* Controller --> process the connector update, triggered by the scheduler. Use spectre-core API to update the data in the Elastic Search database.


## Configuration

Use the application.conf file to set up and tune the module depending on your environment.

You need to configure the host and port of the spectre-core application server and the spectre-core api login and password for security purpose.

**Remark :**
application.conf is a property file, if you need to add some new keys in this file, please remember you have to add those keys in the utility class SettingsAppImpl.
Otherwise you can't use the new keys.

## API Reference

Depending on the size of the project, if it is small and simple enough the reference docs can be added to the README. For medium size to larger projects it is important to at least provide a link to where the API reference docs live.

## Tests

Describe and show how to run the tests with code examples.

## Contributors

Author : Hervé Darritchon (herve.darritchon@orange.com)
Contributor : Achraf Bendada

## License

A short snippet describing the license (MIT, Apache, etc.)

