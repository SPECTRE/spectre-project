# Configuration de son environnement d'exécution

## Fonctionnement Spring-Boot

Spring-Boot permet une grande souplesse dans la configuration de son espace d'exécution d'une application en utilisation les fichiers de configuration, les profils et les variables.
Il est souvent intéressant d'avoir une variable d'environnement pour adapter l'exécution de son application à son environnement mais il arrive que ces variables ne soient pas valorisées.
Pour éviter une configuration compliquée de son poste de développement ou d'une serveur, il est possible de valoriser ses variables d'environnement avec des valeurs par défaut dans les fichiers properties ou yaml de configuration.

## Valeur par défaut

Grâce à Spring-Boot, il est possible d'avoir une valeur par défaut pour une variable d'environnement. Pour le faire, il faut simplement utiliser la syntaxe suivante : ${variable-d-environnement:valeur-par-défaut}

**Exemple :**

Nous souhaitons avoir une variable d'environement pour gérer un fichier de log (spectre-core-log), si cette variable d'environnement n'est pas valorisée alors nous utiliserons une valeur par défaut (./logs/spectre-core.log).
Pour faire cela, nous mettrons dans un fichier properties : ${spectre-core-log:./logs/spectre-core.log} comme valeur de la propriété qui nous intéresse.

## Documentation officielle :

Vous pouvez trouver la documentation officielle Spring-Boot : https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-external-config.html

**Attention :**
La partie sur la surcharge avec une valeur par défaut n'est pas écrite dans la documentation de Spring.

## Bonne pratique

C'est toujours une bonne pratique de surchager avec des valeurs par défauts les variables d'environnement, ce qui rend plus simple la vie du développeur.