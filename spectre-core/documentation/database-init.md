# Initialisation de la base de données de Spectre-Core au démarrage

## Introduction

Dans le cadre du projet Sprectre, pour la partie Sprectre-Core, il est intéressant de remplir la base de données H2 au démarrage de l'application et ainsi s'affranchir des problèmes de BD vide, de se connecter sur une BD distante ou bien avoir un MySQL en local.
Pour populer sa base H2, il est possible d'utiliser un fonctionnement natif de Spring-Boot.

## Fonctionnement Spring-Boot

Il est possible avec Spring-Boot de populer une base de données au démarrage d'une application de manière simple et facile, il suffit de mettre dans le répertoire ressource du projet un fichier data.sql ou schema.sql.
Ce fichier peut être configuré différemment en fonction du profil d'exécution (local, dev, prod, ...). Pour le faire, il suffit de suffixer le fichier avec le profil.

Dans le cadre de Sprectre-Core, il y a un fichier data-h2.sql qui sera exécuté et remplira la BD H2 au démarrage si c'est le profil H2 qui est utilisé.

## Documentation officielle :

Vous pouvez trouver la documentation officielle Spring-Boot : https://docs.spring.io/spring-boot/docs/current/reference/html/howto-database-initialization.html
Le chapitre 74.3 parle du chargement de la BD.