Welcome on Spectre Core!
===================

**Spectre** allows users to create their own **dashboards** and share them with their teammates.
With a user-friendly interface, they can compose their **dashboard** using out-of-the-box **connectors** on the most popular
tools to follow the key performance indicators of a project, such as:

 - Sonar
 - Jenkins
 - TeamColony
 - Mingle
 - Trello
 - Symbioz
 - Actuator
 - ... It's modular design so you can create your own as needed (see "How to contribute")

Easy to use, simple to configure, with an url and credentials you will be able to collect various **KPIs** in **real-time** and follow them in a single page.

The project just started, short demo to discover the workflow will be available soon.


**Spectre-Core** is the Spectre component which gathers KPIs retrieved by Spectre connector plugins,
and stores them into an Elastic Search database.

**Spectre-Core** also manages the storage and retrieval of the configuration data (for dashboards, connectors, KPIs...) into a MySQL database,
and exposes a RESTful API which can be used by front applications (like Spectre-Front) to create, read, or update these configuration data.


___

Installation
-------------

**Spectre-Core** is a Java Maven project based on Spring-Boot.

**Spectre-Core** depends on other Spectre components:

- Spectre-Common
- Spectre connector plugins

The last stable versions of **Spectre-Common** and **Spectre connector plugins**, compatible with the last stable
version of **Spectre-Core**, are available on Orange Nexus, so you don't need to install them on your development
environment if you intend to work only on Spectre-Core.

Otherwise, if you implement features which require some modifications of **Spectre-Common** or **Spectre connector plugins**,
you will need to install and build these components on your development environment.


**Spectre-Core** also needs to connect to 2 databases:

- a MySQL database, to store configuration data
- an Elastic Search database, to store the gathered KPIs.

On a development environment, the **MySQL database** can be replaced by a H2 database: in this case, the "localh2" profile needs to be used.
Alternatively, if a **MySQL database** is available on your development environment, you can use it instead of the H2 database, if you want to.
In this case, you will first need to update the _application-localmysql.yml_ file to provide
the correct parameters for your database:
```
url: jdbc:mysql://<mysql_machine>:<mysql_port>/<mysql_database>
username: <mysql_user>
password: <mysql_password>
```

> **Elastic Search database**: coming soon...


- **Build** Spectre-Core:
```
mvn clean install -U
```

- **Run** Spectre-Core:
with H2 database:
```
mvn spring-boot:run -U -Dspring.profiles.active=localh2
```
or with your MySQL database:
```
mvn spring-boot:run -U -Dspring.profiles.active=localmysql
```

___


Using Docker
-------------

A Dockerfile is provided to build an image in order to run spectre-core inside a container.

Your Docker need to be able to connect to the public registry (dockerhub) because we are using the official maven image.
After setting up your favorite proxy (fiddler, cntlm), you need to configure the docker daemon to use this proxy.

>**Note**:

>If you are using boot2docker, then just edit this file /var/lib/boot2docker/profile (you may need to sudo) and add http://ip:port to HTTP\_PROXY and HTTPS\_PROXY.

>Also, add to NO_PROXY everything that you need (ex : .orange.com, 192.\*, 10.\*, 127.0.0.1, localhost etc...)

Validate your setup using this command :

    docker search ubuntu
    
Then, you can build your image using this command :

    docker build -t <cuid>/spectre-core .

Make sure that you have access to the Corporate Nexus.

Beware, the image created expose port 8083 and you need to be able to connect to a elasticsearch instance named elasticsearch on port 9200. (using link functionnality of docker).

So running fresh containers is as simple as running this command :


    docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 elasticsearch -Des.cluster.name=elasticsearch_build


    docker run -d --name <container_name> -p <external_port>:8083 --link elasticsearch <cuid>/spectre-core


___

Run directly without building an image using Docker
-------------

If you don't want to build an image, you can configure your docker to use our private-repository to immediately run a container based on the official image available.

>**Note**:

>If you are using boot2docker, you can set the private registry quiet easily.
>Find the variable "EXTRA_ARGS" (if it does not exist, add it to the end of the file) in /var/lib/boot2docker/profile (you may need to use sudo), and add this argument :

    EXTRA_ARGS="--insecure-registry 10.203.4.29:5000"

> Then, restart docker daemon using

    sudo /etc/init.d/docker restart

Validate your setup by using :

    docker pull 10.203.4.29:5000/vxhv5309/spectre-core

Then, create new containers :

    docker run -d --name elasticsearch -p 9200:9200 -p 9300:9300 elasticsearch -Des.cluster.name=elasticsearch_build


    docker run -d --name <container_name> -p <external_port>:8083 --link elasticsearch vxhv5309/spectre-core

___

How to contribute
-------------

Please consult the _contribution guide_:

![Link to the contribution guide screenshot](img/readme_contribution_guide_link.png)
