package com.orange.spectre.plugins.teamcolony.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by WKNL8600 on 09/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class GlobalMoodIndicatorTest {
    @Test
    public void testGetLabels(){
        AbstractConnector connector = new TeamColonyConnector();
        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getLabel()).isNotNull();
        }
    }




    @Test
    public void testGetMeasures(){
        AbstractConnector connector = new TeamColonyConnector();
        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getMeasure()).isNotNull();
        }
    }

    @Test
    public void testGetTypes(){
        AbstractConnector connector = new TeamColonyConnector();
        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getType()).isNotNull();
        }
    }

    @Test
    public void testGetFamilies(){
        AbstractConnector connector = new TeamColonyConnector();
        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getFamily()).isNotNull();
        }
    }

}