package com.orange.spectre.plugins.teamcolony.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by WKNL8600 on 09/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class TeamColonyConnectorTest {
    @Test
    public void testGetAvailableIndicators(){
        AbstractConnector connector = new TeamColonyConnector();
        assertThat(connector.getAvailableIndicators()).isNotNull();
        assertThat(connector.getAvailableIndicators()).isNotEmpty();
    }

    @Test
    public void testGetConnectionParams(){
        AbstractConnector connector = new TeamColonyConnector();
        assertThat(connector.getRequiredParameters()).isNotNull();
    }

    @Test
    public void testGetType(){
        AbstractConnector connector = new TeamColonyConnector();
        assertThat(connector.getType()).isNotNull();
    }

    @Test
    public void testCreateIndicator(){
        AbstractConnector connector = new TeamColonyConnector();
        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            assertThat(connector.createIndicator(type.name())).isNotNull();
        }
    }

}