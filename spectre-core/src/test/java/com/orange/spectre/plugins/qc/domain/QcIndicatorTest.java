package com.orange.spectre.plugins.qc.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class QcIndicatorTest {

	@Test
	public void testGetLabels() {
		AbstractConnector connector = new QcConnector();
		for (QcIndicatorType type : QcIndicatorType.values()) {
			AbstractIndicator a = connector.createIndicator(type.name());
			assertThat(a.getLabel()).isNotNull();
		}
	}

	@Test
	public void testGetMeasures() {
		AbstractConnector connector = new QcConnector();
		for (QcIndicatorType type : QcIndicatorType.values()) {
			AbstractIndicator a = connector.createIndicator(type.name());
			assertThat(a.getMeasure()).isNotNull();
		}
	}

	@Test
	public void testGetTypes() {
		AbstractConnector connector = new QcConnector();
		for (QcIndicatorType type : QcIndicatorType.values()) {
			AbstractIndicator a = connector.createIndicator(type.name());
			assertThat(a.getType()).isNotNull();
		}
	}

	@Test
	public void testGetFamilies() {
		AbstractConnector connector = new QcConnector();
		for (QcIndicatorType type : QcIndicatorType.values()) {
			AbstractIndicator a = connector.createIndicator(type.name());
			assertThat(a.getFamily()).isNotNull();
		}
	}

}
