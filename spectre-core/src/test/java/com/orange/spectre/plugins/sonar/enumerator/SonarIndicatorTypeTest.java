package com.orange.spectre.plugins.sonar.enumerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 02/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class SonarIndicatorTypeTest {

    @Test
    public void testGetLabel(){

        for(SonarIndicatorType type : SonarIndicatorType.values()){
            assertThat(type.getLabel()).isNotNull();
        }
    }

    @Test
    public void testCreateIndicator(){

        for(SonarIndicatorType type : SonarIndicatorType.values()){
            assertThat(type.createIndicator()).isNotNull();
        }
    }

}
