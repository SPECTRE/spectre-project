package com.orange.spectre.plugins.sacre.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sonar.standard.domain.SonarConnector;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 02/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class SacreIndicatorTest {

    @Test
    public void testGetLabels(){
        AbstractConnector connector = new SacreConnector();
        for(SacreIndicatorType type : SacreIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getLabel()).isNotNull();
        }
    }

    @Test
    public void testGetMeasures(){
        AbstractConnector connector = new SacreConnector();
        for(SacreIndicatorType type : SacreIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getMeasure()).isNotNull();
        }
    }

    @Test
    public void testGetTypes(){
        AbstractConnector connector = new SacreConnector();
        for(SacreIndicatorType type : SacreIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getType()).isNotNull();
        }
    }

    @Test
    public void testGetFamilies(){
        AbstractConnector connector = new SacreConnector();
        for(SacreIndicatorType type : SacreIndicatorType.values()){
            AbstractIndicator a = connector.createIndicator(type.name());
            assertThat(a.getFamily()).isNotNull();
        }
    }

}
