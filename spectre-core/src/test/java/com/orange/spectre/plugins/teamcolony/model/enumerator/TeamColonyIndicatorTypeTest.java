package com.orange.spectre.plugins.teamcolony.model.enumerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;



/**
 * Created by WKNL8600 on 09/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class TeamColonyIndicatorTypeTest {

    @Test
    public void testGetLabel(){

        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            assertThat(type.getLabel()).isNotNull();
        }
    }

    @Test
    public void testCreateIndicator(){

        for(TeamColonyIndicatorType type : TeamColonyIndicatorType.values()){
            assertThat(type.createIndicator()).isNotNull();
        }
    }


}
