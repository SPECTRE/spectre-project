package com.orange.spectre.plugins.sacre.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sonar.standard.domain.SonarConnector;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 02/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class SacreConnectorTest {

    @Test
    public void testGetAvailableIndicators(){
        AbstractConnector connector = new SacreConnector();
        assertThat(connector.getAvailableIndicators()).isNotNull();
        assertThat(connector.getAvailableIndicators()).isNotEmpty();
    }

    @Test
    public void testGetConnectionParams(){
        AbstractConnector connector = new SacreConnector();
        assertThat(connector.getRequiredParameters()).isNotNull();
    }

    @Test
    public void testGetType(){
        AbstractConnector connector = new SacreConnector();
        assertThat(connector.getType()).isNotNull();
    }

    @Test
    public void testCreateIndicator(){
        AbstractConnector connector = new SacreConnector();
        for(SacreIndicatorType type : SacreIndicatorType.values()){
            assertThat(connector.createIndicator(type.name())).isNotNull();
        }

    }


}
