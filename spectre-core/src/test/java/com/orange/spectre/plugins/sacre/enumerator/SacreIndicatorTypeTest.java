package com.orange.spectre.plugins.sacre.enumerator;

import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 02/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class SacreIndicatorTypeTest {

    @Test
    public void testGetLabel(){

        for(SacreIndicatorType type : SacreIndicatorType.values()){
            assertThat(type.getLabel()).isNotNull();
        }
    }

    @Test
    public void testCreateIndicator(){

        for(SacreIndicatorType type : SacreIndicatorType.values()){
            assertThat(type.createIndicator()).isNotNull();
        }
    }

}
