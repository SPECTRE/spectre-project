package com.orange.spectre.plugins.qc.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class QcConnectorTest {

	@Test
	public void testGetAvailableIndicators() {
		AbstractConnector connector = new QcConnector();
		assertThat(connector.getAvailableIndicators()).isNotNull();
		assertThat(connector.getAvailableIndicators()).isNotEmpty();
	}

	@Test
	public void testGetConnectionParams() {
		AbstractConnector connector = new QcConnector();
		assertThat(connector.getRequiredParameters()).isNotNull();
	}

	@Test
	public void testGetType() {
		AbstractConnector connector = new QcConnector();
		assertThat(connector.getType()).isNotNull();
	}

	@Test
	public void testCreateIndicator() {
		AbstractConnector connector = new QcConnector();
		for (QcIndicatorType type : QcIndicatorType.values()) {
			assertThat(connector.createIndicator(type.name())).isNotNull();
		}
	}
}
