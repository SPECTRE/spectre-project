package com.orange.spectre.plugins.teamcolony;

import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.plugins.teamcolony.utils.TeamColonyResponseUtils;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by WKNL8600 on 10/05/2016.
 */
public class TeamColonyResponseUtilsTest {

    @Test
    public void getMoodGlobalHappy() throws Exception {
        List<String> moods = new ArrayList<String>();
        moods.add("#happy");
        moods.add("#happy");
        moods.add("#sad");
        StringData data =new StringData();
        data.setData("happy");
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);

    }

    @Test
    public void getMoodGlobalnormal() throws Exception {
        List<String> moods = new ArrayList<String>();
        moods.add("#normal");
        moods.add("#normal");
        moods.add("#happy");
        StringData data =new StringData("normal");
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);

    }

    @Test
    public void getMoodGlobalNothing() throws Exception {
        List<String> moods = new ArrayList<String>();
        moods.add("#Normaal");
        moods.add("#Normaal");
        moods.add("#saada");
        StringData data =new StringData(null);
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);
    }

    @Test
    public void getMoodGlobalEqual() throws Exception {
        List<String> moods = new ArrayList<String>();
        moods.add("#normal");
        moods.add("#normal");
        moods.add("#sad");
        moods.add("#sad");
        moods.add("#happy");
        moods.add("#happy");
        StringData data =new StringData("normal");
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);

    }

    @Test
    public void getMoodGlobalEmpty() throws Exception {
        List<String> moods = new ArrayList<String>();
        StringData data =new StringData(null);
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);

    }

    @Test
    public void getMoodGlobalSad() throws Exception {
        List<String> moods = new ArrayList<String>();
        moods.add("#sad");
        moods.add("#normal");
        moods.add("#normal");
        moods.add("#sad");
        moods.add("#sad");
        moods.add("#happy");
        StringData data =new StringData();
        data.setData("sad");
        assertThat(TeamColonyResponseUtils.getMoodGlobal(moods)).isEqualTo(data);}

}