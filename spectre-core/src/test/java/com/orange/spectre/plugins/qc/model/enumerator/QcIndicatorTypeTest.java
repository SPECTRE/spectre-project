package com.orange.spectre.plugins.qc.model.enumerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
@IntegrationTest
public class QcIndicatorTypeTest {

	@Test
	public void testGetLabel() {

		for (com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType type : com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType
				.values()) {
			assertThat(type.getLabel()).isNotNull();
		}
	}

	@Test
	public void testCreateIndicator() {

		for (com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType type : com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType
				.values()) {
			assertThat(type.createIndicator()).isNotNull();
		}
	}

}
