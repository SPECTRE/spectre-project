package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.Widget;
import com.orange.spectre.core.dummy.*;
import com.orange.spectre.core.repository.*;
import com.orange.spectre.core.service.DashboardService;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.KpiInjectionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.HashSet;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ludovic on 28/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class DashboardResourceTest {

    private MockMvc restWelcomeMockMvc;

    @InjectMocks
    private DashboardResource dashboardResource;

    @Mock
    DashboardRepository dashboardRepository;

    @Mock
    WidgetRepository widgetRepository;

    @Mock
    DeletionService deletionService;

    @Mock
    AbstractIndicatorRepository indicatorRepository;

    @Mock
    KpiInjectionService kpiInjectionService;

    @Mock
    DashboardService dashboardService;

    @Mock
    ProjectRepository projectRepository;

    @Mock
    DashboardMemberRepository dashboardMemberRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(dashboardResource).build();

        Mockito.when(dashboardRepository.findAllByReachableTrueOrderByIdAsc()).thenReturn(new HashSet<>());
        Mockito.when(dashboardRepository.exists(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(true);
        Mockito.when(dashboardRepository.findOne(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()));
        Mockito.when(dashboardRepository.save(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()))).thenReturn(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()));

        Mockito.when(dashboardRepository.exists(DashboardDummy.getNewDashboardWithWidget(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(true);
        Mockito.when(dashboardRepository.findOne(DashboardDummy.getNewDashboardWithWidget(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(DashboardDummy.getNewDashboardWithWidget(AccountDummy.getExistingAccount().getLogin()));


        Mockito.when(indicatorRepository.exists(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()).getId())).thenReturn(true);

        Mockito.when(indicatorRepository.findOne(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()).getId())).thenReturn(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()));
        Mockito.when(indicatorRepository.save(Mockito.any(AbstractIndicator.class))).thenReturn(IndicatorDummy.getNewCustomIndicator());

        Mockito.when(widgetRepository.save(Mockito.any(Widget.class))).thenReturn(WidgetDummy.getNewCustomWidget(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin())));
        Mockito.when(dashboardService.checkAdmin(Mockito.any(Long.class),Mockito.anyString())).thenReturn(true);


    }

    @Test
    public void testGetPublicDashboards() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/dashboards").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/dashboards/2").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/dashboards/" + DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId()).contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/dashboards/2").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/dashboards/" + DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateWidgetInUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        post("/dashboards/2/indicators/2/widgets").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(WidgetDummy.getNewWidgetDto()))
                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateWidgetInUnknownIndicator() throws Exception{
        restWelcomeMockMvc
                .perform(
                        post("/dashboards/"+ DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId() +"/indicators/2/widgets").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(WidgetDummy.getNewWidgetDto()))
                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllWidgetInUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/dashboards/2/widgets").contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetAllWidgetInDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/dashboards/" + DashboardDummy.getNewDashboardWithWidget(AccountDummy.getExistingAccount().getLogin()).getId() + "/widgets").contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/dashboards/2").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/dashboards/" + DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin()).getId()).contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(DashboardDummy.getNewDashboardDto()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
