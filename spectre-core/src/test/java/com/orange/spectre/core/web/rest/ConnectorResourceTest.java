package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.dummy.ConnectorDummy;
import com.orange.spectre.core.repository.AbstractConnectorRepository;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.service.KpiInjectionService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Created by ludovic on 28/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class ConnectorResourceTest {

	private MockMvc restWelcomeMockMvc;

	@InjectMocks
	private ConnectorResource connectorResource;

	@Mock
	private AbstractConnectorRepository connectorRepository;

	@Mock
	private AbstractIndicatorRepository indicatorRepository;

	@Mock
	private KpiInjectionService kpiInjectionService;

	@Mock
	private FetchIndicatorsService fetchIndicatorsService;
	@Mock
	private DeletionService deletionService;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(connectorResource).build();

		Mockito.when(connectorRepository.findOne(ConnectorDummy.getNewSonarConnector().getId()))
				.thenReturn(ConnectorDummy.getNewSonarConnector());
		Mockito.when(connectorRepository.exists(ConnectorDummy.getNewSonarConnector().getId())).thenReturn(true);
		Mockito.when(connectorRepository.exists(new Long(2))).thenReturn(false);
	}

	@Test
	public void testGetUnknownConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						get("/connectors/2").contentType("application/json").accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testGetConnectorByOwner() throws Exception {
		restWelcomeMockMvc
				.perform(
						get("/connectors/1?owner=true").contentType("application/json").accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						get("/connectors/1").contentType("application/json").accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testGetAvailableConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						get("/connectors/types").contentType("application/json").accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().contentType("application/json;charset=UTF-8"))
				.andExpect(jsonPath("$").value(Matchers.notNullValue()));
	}

	@Test
	public void testUpdateUnknownConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						put("/connectors/2").contentType("application/json")
								.content(TestUtil.convertObjectToJsonBytes(ConnectorDummy.getNewSonarConnector()))
								.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testUpdateConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						put("/connectors/" + ConnectorDummy.getNewSonarConnector().getId())
								.contentType("application/json")
								.content(TestUtil.convertObjectToJsonBytes(ConnectorDummy.getNewSonarConnector()))
								.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testUpdateNullConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						put("/connectors/" + ConnectorDummy.getNewSonarConnector().getId())
								.contentType("application/json")
								.content(TestUtil.convertObjectToJsonBytes(null))
								.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void testFetchIndicatorsForUnknownConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						post("/connectors/2/indicators/fetch")
								.contentType("application/json")
								.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound());
	}

	@Test
	public void testFetchIndicatorsForConnector() throws Exception {
		restWelcomeMockMvc
				.perform(
						post("/connectors/" + ConnectorDummy.getNewSonarConnector().getId() + "/indicators/fetch")
								.contentType("application/json")
								.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
