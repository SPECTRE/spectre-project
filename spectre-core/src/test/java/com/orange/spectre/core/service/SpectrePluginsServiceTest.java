package com.orange.spectre.core.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.Project;
import com.orange.spectre.core.dummy.AccountDummy;
import com.orange.spectre.core.dummy.ConnectorDummy;
import com.orange.spectre.core.dummy.DashboardDummy;
import com.orange.spectre.core.dummy.ProjectDummy;
import com.orange.spectre.core.repository.AbstractConnectorRepository;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.DashboardMemberRepository;
import com.orange.spectre.core.repository.DashboardRepository;
import com.orange.spectre.core.repository.ProjectMemberRepository;
import com.orange.spectre.core.repository.ProjectRepository;
import com.orange.spectre.core.repository.WidgetRepository;

/**
 * Created by epeg7421 on 04/10/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class SpectrePluginsServiceTest {

	@InjectMocks
	private SpectrePluginsService spectrePluginsService;

	@Mock
	AbstractConnectorRepository connectorRepository;

	@Mock
	AbstractIndicatorRepository indicatorRepository;

	@Mock
	ProjectRepository projectRepository;

	@Mock
	WidgetRepository widgetRepository;

	@Mock
	AccountRepository accountRepository;

	@Mock
	DashboardRepository dashboardRepository;

	@Mock
	DashboardMemberRepository dashboardMemberRepository;

	@Mock
	ProjectMemberRepository projectMemberRepository;

	@Before
	public void setUp() throws Exception {
		// Create a new account
		Account account = AccountDummy.getNewAccount();
		Project projectReachableTrue = ProjectDummy.getNewProject(account.getLogin());
		Project projectWithConnectorReachableTrue = ProjectDummy.getNewProjectWithConnector(account.getLogin(),
				ConnectorDummy.getNewSonarConnectorWithoutIndicator());
		Project projectBisReachableFalse = ProjectDummy.getNewProjectBis(account.getLogin());

		// Mock Lists ALl projects
		Set<Project> projectsAll = new HashSet<>();
		projectsAll.add(projectReachableTrue);
		projectsAll.add(projectWithConnectorReachableTrue);
		projectsAll.add(projectBisReachableFalse);

		// Mock Lists projects reachable True
		Set<Project> projectsReachableTrue = new HashSet<>();
		projectsReachableTrue.add(projectReachableTrue);
		projectsReachableTrue.add(projectWithConnectorReachableTrue);
		// Mock Lists projects reachable False
		Set<Project> projectsReachableFalse = new HashSet<>();
		projectsReachableFalse.add(projectBisReachableFalse);
		// Mock Lists All dashboards
		Dashboard dashboard = DashboardDummy.getNewDashboard(account.getLogin());
		Dashboard dashboardReachableFalse = DashboardDummy.getNewDashboardBis(account.getLogin());
		Dashboard dashboardWithoutWidget = DashboardDummy.getNewDashboardWithoutWidget(account.getLogin());
		// MOCK Dashboards ALL
		Set<Dashboard> dashboards = new HashSet<>();
		dashboards.add(dashboard);
		dashboards.add(dashboardReachableFalse);
		dashboards.add(dashboardWithoutWidget);
		// MOCK Dashboards Reachable TRUE ALL
		Set<Dashboard> dashboardsReachableTrue = new HashSet<>();
		dashboardsReachableTrue.add(dashboard);
		dashboardsReachableTrue.add(dashboardWithoutWidget);

		// MOCK Dashboards Reachable FALSE ALL
		Set<Dashboard> dashboardsReachableFalse = new HashSet<>();
		dashboardsReachableFalse.add(dashboardReachableFalse);

		//
		AbstractConnector sonarConnector = ConnectorDummy.getNewSonarConnector();
		AbstractConnector sonarConnectorBis = ConnectorDummy.getNewSonarConnectorWithoutIndicator();

		// MOCK Dashboards ALL
		Set<AbstractConnector> connectors = new HashSet<>();
		connectors.add(sonarConnector);
		connectors.add(sonarConnectorBis);
		//
		Mockito.when(accountRepository.exists(new Long(1))).thenReturn(true);

		Mockito.when(projectRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(projectRepository.findOne(new Long(1))).thenReturn(projectReachableTrue);
		Mockito.when(projectRepository.exists(new Long(4))).thenReturn(true);
		Mockito.when(projectRepository.findOne(new Long(4))).thenReturn(projectWithConnectorReachableTrue);
		Mockito.when(projectRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(projectRepository.findOne(new Long(3))).thenReturn(projectBisReachableFalse);
		Mockito.when(projectRepository.countAll()).thenReturn((long) projectsAll.size());
		Mockito.when(projectRepository.findAllByReachableTrue()).thenReturn(projectsReachableTrue);
		Mockito.when(projectRepository.findAllByReachableFalse()).thenReturn(projectsReachableFalse);

		Mockito.when(dashboardRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(dashboardRepository.findOne(new Long(1))).thenReturn(dashboard);
		Mockito.when(dashboardRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(dashboardRepository.findOne(new Long(3))).thenReturn(dashboardWithoutWidget);
		Mockito.when(dashboardRepository.countAll()).thenReturn((long) dashboards.size());
		Mockito.when(dashboardRepository.findAllByReachableTrueOrderByIdAsc()).thenReturn(dashboardsReachableTrue);
		Mockito.when(dashboardRepository.findAllByReachableFalseOrderByIdAsc()).thenReturn(dashboardsReachableFalse);

		Mockito.when(connectorRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(connectorRepository.findOne(new Long(1))).thenReturn(sonarConnector);
		Mockito.when(connectorRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(connectorRepository.findOne(new Long(3))).thenReturn(sonarConnectorBis);

	}

	@Test
	@Ignore
	public void countAllUsers() throws Exception {
		Long result = spectrePluginsService.countAllUsers();
		assertThat(result).isEqualTo(1);
	}

	@Test
	public void countAllProjects() throws Exception {
		Long result = spectrePluginsService.countAllProjects();
		assertThat(result).isEqualTo(3);

	}

	@Test
	public void countAllProjectsByReachableFalse() throws Exception {
		Long result = spectrePluginsService.countAllProjectsByReachableFalse();
		assertThat(result).isEqualTo(1);
	}

	@Test
	public void countAllProjectsByReachableTrue() throws Exception {
		Long result = spectrePluginsService.countAllProjectsByReachableTrue();
		assertThat(result).isEqualTo(2);
	}

	@Test
	public void countAllDashboards() throws Exception {
		Long result = spectrePluginsService.countAllDashboards();
		assertThat(result).isEqualTo(3);
	}

	@Test
	public void countAllDashboardsByReachableFalse() throws Exception {
		Long result = spectrePluginsService.countAllDashboardsByReachableFalse();
		assertThat(result).isEqualTo(1);
	}

	@Test
	public void countAllDashboardsByReachableTrue() throws Exception {
		Long result = spectrePluginsService.countAllDashboardsByReachableTrue();
		assertThat(result).isEqualTo(2);
	}

	@Test
	@Ignore
	public void countAllConnectors() throws Exception {
		Long result = spectrePluginsService.countAllConnectors();
		assertThat(result).isEqualTo(2);
	}

}