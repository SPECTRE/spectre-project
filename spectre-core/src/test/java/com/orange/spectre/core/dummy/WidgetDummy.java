package com.orange.spectre.core.dummy;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.Widget;
import com.orange.spectre.core.web.dto.CustomWidgetDto;
import com.orange.spectre.core.web.dto.WidgetDto;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class WidgetDummy {

    public static Widget getNewWidget(AbstractIndicator indicator, Dashboard dashboard){
        Widget widget = new Widget();
        widget.setId(new Long(1));
        widget.setLabel("widget");
        widget.setIndicator(indicator);
        widget.setDashboard(dashboard);

        return widget;
    }

    public static Widget getNewCustomWidget(Dashboard dashboard){
        Widget widget = new Widget();
        widget.setId(new Long(3));
        widget.setLabel("widget");
        widget.setIndicator(IndicatorDummy.getNewCustomIndicator());
        widget.setDashboard(dashboard);

        return widget;
    }

    public static CustomWidgetDto getNewCustomWidgetDto(Dashboard dashboard){
        CustomWidgetDto widget = new CustomWidgetDto();
        widget.setId(new Long(3));
        widget.setLabel("widget");
        widget.setMeasure("PERCENT");


        return widget;
    }


    public static WidgetDto getNewWidgetDto() {
        WidgetDto widget = new WidgetDto();
        widget.setLabel("widget");

        return widget;
    }

    public static WidgetDto getExistingWidgetDto() {
        WidgetDto widget = new WidgetDto();
        widget.setLabel("widget");
        widget.setId(new Long(5));
        return widget;
    }
}
