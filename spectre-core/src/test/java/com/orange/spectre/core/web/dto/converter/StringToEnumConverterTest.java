package com.orange.spectre.core.web.dto.converter;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.model.enumerator.ConnectorType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 27/04/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class StringToEnumConverterTest {

    @Test
    public void testEnumToStringConvertion(){
        StringToEnumConverter converter = new StringToEnumConverter();
        Enum source = ConnectorType.sonar;
        String destination = null;
        Object result = converter.convert(destination, source,String.class,ConnectorType.class);
        String resultS = (String)result;
        assertThat(resultS).isEqualTo("sonar");
    }

    @Test
    public void testStringToEnumConvertion(){
        StringToEnumConverter converter = new StringToEnumConverter();
        String source = "sonar";
        Enum destination = null;
        Object result = converter.convert(destination, source,ConnectorType.class,String.class);
        assertThat(result instanceof ConnectorType).isTrue();
        assertThat(result).isSameAs(ConnectorType.sonar);
    }

    @Test
    public void testSourceNullConvertion(){
        StringToEnumConverter converter = new StringToEnumConverter();
        Object source = null;
        Object destination = null;
        Object result = converter.convert(destination, source,ConnectorType.class,String.class);
        assertThat(result).isNull();
    }

    @Test
    public void testTypeErrorConvertion(){
        StringToEnumConverter converter = new StringToEnumConverter();
        Dashboard source = new Dashboard();
        AbstractIndicator destination = null;
        Object result = converter.convert(destination, source, AbstractIndicator.class, Dashboard.class);
        assertThat(result).isNull();
    }

    @Test
    public void testTypeErrorConvertionString(){
        StringToEnumConverter converter = new StringToEnumConverter();
        String source = "string";
        AbstractIndicator destination = null;
        Object result = converter.convert(destination, source, AbstractIndicator.class, String.class);
        assertThat(result).isNull();
    }

    @Test
    public void testTypeErrorConvertionEnum(){
        StringToEnumConverter converter = new StringToEnumConverter();
        Enum source = ConnectorType.sonar;
        AbstractIndicator destination = null;
        Object result = converter.convert(destination, source, AbstractIndicator.class, ConnectorType.class);
        assertThat(result).isNull();
    }
}
