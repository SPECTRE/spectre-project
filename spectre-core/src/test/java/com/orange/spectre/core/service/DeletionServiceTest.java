package com.orange.spectre.core.service;

import static org.mockito.Matchers.any;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.Project;
import com.orange.spectre.core.domain.Widget;
import com.orange.spectre.core.dummy.AccountDummy;
import com.orange.spectre.core.dummy.ConnectorDummy;
import com.orange.spectre.core.dummy.DashboardDummy;
import com.orange.spectre.core.dummy.IndicatorDummy;
import com.orange.spectre.core.dummy.ProjectDummy;
import com.orange.spectre.core.dummy.WidgetDummy;
import com.orange.spectre.core.exception.ConnectorNotFoundException;
import com.orange.spectre.core.exception.DashboardNotFoundException;
import com.orange.spectre.core.exception.IndicatorNotFoundException;
import com.orange.spectre.core.exception.ProjectNotFoundException;
import com.orange.spectre.core.exception.WidgetNotFoundException;
import com.orange.spectre.core.repository.AbstractConnectorRepository;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.DashboardMemberRepository;
import com.orange.spectre.core.repository.DashboardRepository;
import com.orange.spectre.core.repository.ProjectMemberRepository;
import com.orange.spectre.core.repository.ProjectRepository;
import com.orange.spectre.core.repository.WidgetRepository;

/**
 * Created by ludovic on 26/04/2016.
 */

@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class DeletionServiceTest {

	@InjectMocks
	private DeletionService deletionService;

	@Mock
	AbstractConnectorRepository connectorRepository;

	@Mock
	AbstractIndicatorRepository indicatorRepository;

	@Mock
	ProjectRepository projectRepository;

	@Mock
	WidgetRepository widgetRepository;

	@Mock
	AccountRepository accountRepository;

	@Mock
	DashboardRepository dashboardRepository;

	@Mock
	DashboardMemberRepository dashboardMemberRepository;

	@Mock
	ProjectMemberRepository projectMemberRepository;

	@Before
	public void setUp() throws Exception {
		// Create a new account
		Account account = AccountDummy.getNewAccount();
		Project project = ProjectDummy.getNewProject(account.getLogin());

		Dashboard dashboard = DashboardDummy.getNewDashboard(account.getLogin());
		Dashboard dashboardWithoutWidget = DashboardDummy.getNewDashboardWithoutWidget(account.getLogin());
		AbstractConnector sonarConnector = ConnectorDummy.getNewSonarConnector();

		AbstractConnector sonarConnectorWithoutIndicator = ConnectorDummy.getNewSonarConnectorWithoutIndicator();

		Project projectWithConnector = ProjectDummy.getNewProjectWithConnector(account.getLogin(), sonarConnector);

		AbstractIndicator sonarIndicator = IndicatorDummy.getNewSonarTestCoverageIndicator(sonarConnector);
		AbstractIndicator sonarIndicatorWithoutWidget = IndicatorDummy.getNewSonarTestCoverageIndicatorWithoutWidget(sonarConnector);
		Widget widget = WidgetDummy.getNewWidget(sonarIndicator, dashboard);
		dashboard.setWidgets(new HashSet<>());
		dashboard.getWidgets().add(widget);

		Set<AbstractIndicator> indicators = new HashSet<>();
		indicators.add(sonarIndicator);
		sonarConnector.setIndicators(indicators);

		Set<Widget> widgets = new HashSet<>();
		widgets.add(widget);

		Mockito.when(widgetRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(widgetRepository.findOne(new Long(1))).thenReturn(widget);
		Mockito.when(widgetRepository.findAllByIndicator(sonarIndicator)).thenReturn(widgets);

		Mockito.when(dashboardRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(dashboardRepository.findOne(new Long(1))).thenReturn(dashboard);

		Mockito.when(dashboardRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(dashboardRepository.findOne(new Long(3))).thenReturn(dashboardWithoutWidget);

		Mockito.when(connectorRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(connectorRepository.findOne(new Long(1))).thenReturn(sonarConnector);

		Mockito.when(connectorRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(connectorRepository.findOne(new Long(3))).thenReturn(sonarConnectorWithoutIndicator);

		Mockito.when(indicatorRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(indicatorRepository.findOne(new Long(1))).thenReturn(sonarIndicator);

		Mockito.when(indicatorRepository.exists(new Long(3))).thenReturn(true);
		Mockito.when(indicatorRepository.findOne(new Long(3))).thenReturn(sonarIndicatorWithoutWidget);

		Mockito.when(projectRepository.exists(new Long(1))).thenReturn(true);
		Mockito.when(projectRepository.findOne(new Long(1))).thenReturn(project);

		Mockito.when(projectRepository.exists(new Long(4))).thenReturn(true);
		Mockito.when(projectRepository.findOne(new Long(4))).thenReturn(projectWithConnector);

		Mockito.when(projectMemberRepository.findByProjectId(any(Long.class))).thenReturn(new HashSet<>());
		Mockito.when(dashboardMemberRepository.findByDashboardId(any(Long.class))).thenReturn(new HashSet<>());

	}

	@Test(expected = WidgetNotFoundException.class)
	public void testDeleteUnknownWidget() {
		deletionService.deleteWidget(new Long(2));
	}

	@Test
	public void testDeleteExistingWidget() {
		deletionService.deleteWidget(new Long(1));
		Mockito.verify(widgetRepository).delete(new Long(1));
	}

	@Test(expected = DashboardNotFoundException.class)
	public void testDeleteUnknownDashboard() {
		deletionService.deleteDashboard(new Long(2));
	}

	@Test
	public void testDeleteExistingDashboard() {
		deletionService.deleteDashboard(new Long(1));
		Mockito.verify(dashboardRepository).delete(new Long(1));
	}

	@Test
	public void testDeleteExistingDashboardWithoutWidget() {
		deletionService.deleteDashboard(new Long(3));
		Mockito.verify(dashboardRepository).delete(new Long(3));
	}

	@Test(expected = IndicatorNotFoundException.class)
	public void testDeleteUnknownIndicator() {
		deletionService.deleteIndicator(new Long(2));
	}

	@Test
	public void testDeleteExistingIndicator() {
		deletionService.deleteIndicator(new Long(1));
		Mockito.verify(indicatorRepository).delete(new Long(1));
	}

	@Test
	public void testDeleteExistingIndicatorWithoutWidget() {
		deletionService.deleteIndicator(new Long(3));
		Mockito.verify(indicatorRepository).delete(new Long(3));

	}

	@Test(expected = ConnectorNotFoundException.class)
	public void testDeleteUnknownConnector() {
		deletionService.deleteConnector(new Long(2));
	}

	@Test
	public void testDeleteExistingConnector() {
		deletionService.deleteConnector(new Long(1));
		Mockito.verify(connectorRepository).delete(new Long(1));
		Mockito.verify(indicatorRepository).delete(new Long(1));
	}

	@Test
	public void testDeleteExistingConnectorWithoutIndicator() {
		deletionService.deleteConnector(new Long(3));
	}

	@Test(expected = ProjectNotFoundException.class)
	public void testDeleteUnknownProject() {
		deletionService.deleteProject(new Long(2));
	}

	@Test
	public void testDeleteExistingProject() {
		deletionService.deleteProject(new Long(1));
		Mockito.verify(projectRepository).delete(new Long(1));
	}

	@Test
	public void testDeleteExistingProjectWithConnector() {
		deletionService.deleteProject(new Long(4));
		Mockito.verify(projectRepository).delete(new Long(4));
	}

}
