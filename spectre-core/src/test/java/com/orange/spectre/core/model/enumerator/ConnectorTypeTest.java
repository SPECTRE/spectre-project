package com.orange.spectre.core.model.enumerator;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.plugins.sonar.standard.domain.SonarConnector;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 27/04/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
@Transactional
public class ConnectorTypeTest {


    @Test
    public void testSonarConnectorCreation(){
        AbstractConnector connector = ConnectorType.sonar.createConnector();

        assertThat(connector).isInstanceOf(SonarConnector.class);
    }

    @Test(expected= RuntimeException.class)
    public void testUnknownConnectorCreation(){
        ConnectorType.valueOf("test").createConnector();
    }


}
