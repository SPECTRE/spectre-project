package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.dummy.AccountDummy;
import com.orange.spectre.core.dummy.ConnectorDummy;
import com.orange.spectre.core.dummy.ProjectDummy;
import com.orange.spectre.core.repository.*;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.service.ProjectService;
import com.orange.spectre.core.web.dto.EditConnectorDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ludovic on 28/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class ProjectResourceTest {

    private MockMvc restWelcomeMockMvc;

    @InjectMocks
    private ProjectResource projectResource;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    AbstractConnectorRepository connectorRepository;

    @Mock
    AbstractIndicatorRepository indicatorRepository;

    @Mock
    ProjectMemberRepository projectMemberRepository;

    @Mock
    DeletionService deletionService;

    @Mock
    FetchIndicatorsService fetchIndicatorsService;

    @Mock
    ProjectService projectService;

    @Mock
    AccountRepository accountRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(projectResource).build();

        Mockito.when(projectRepository.exists(ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(true);
        Mockito.when(projectRepository.exists(new Long(2))).thenReturn(false);
        Mockito.when(projectRepository.findOne(ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId())).thenReturn(ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()));

        Mockito.when(connectorRepository.save(Mockito.any(AbstractConnector.class))).thenReturn(ConnectorDummy.getNewSonarConnector());
        Mockito.when(connectorRepository.exists(ConnectorDummy.getNewSonarConnector().getId())).thenReturn(true);
        Mockito.when(connectorRepository.findOne(ConnectorDummy.getNewSonarConnector().getId())).thenReturn(ConnectorDummy.getNewSonarConnector());

        Mockito.when(projectService.checkAdmin(Mockito.any(Long.class),Mockito.anyString())).thenReturn(true);
    }

    @Test
    public void testGetPublicProjects() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/projects").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetConnectorsForUnknownProject() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/projects/2/connectors").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testGetConnectorsForProject() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId() + "/connectors").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testCreateConnectorIntoUnknownProject() throws Exception {
        EditConnectorDto connectorDto = new EditConnectorDto();
        connectorDto.setName("connector");

        restWelcomeMockMvc
                .perform(
                        post("/projects/2/connectors").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(connectorDto))
                                .accept(
                                        MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateConnectorIntoProject() throws Exception {
        EditConnectorDto connectorDto = new EditConnectorDto();
        connectorDto.setName("connector");
        connectorDto.setType("sonar");
        connectorDto.setPeriodicity(new Long(10));
        connectorDto.setOwner("aaaa1111");
        connectorDto.setPeriodicity(new Long(60));
        restWelcomeMockMvc
                .perform(
                        post("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId() + "/connectors").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(connectorDto))
                                .accept(
                                        MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testDeleteConnectorFromUnknownProject() throws Exception {

        restWelcomeMockMvc
                .perform(
                        delete("/projects/2/connectors/2").contentType("application/json")
                                .accept(
                                        MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteUnknownConnectorFromProject() throws Exception {

        restWelcomeMockMvc
                .perform(
                        delete("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId() + "/connectors/2").contentType("application/json")
                                .accept(
                                        MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testDeleteConnectorFromProject() throws Exception {

        restWelcomeMockMvc
                .perform(
                        delete("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId() + "/connectors/" + ConnectorDummy.getNewSonarConnector().getId()).contentType("application/json")
                                .accept(
                                        MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateUnknownProject() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/projects/2").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateProject() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId()).contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(ProjectDummy.getNewProjectDto()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUnknownProject() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/projects/2").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteProject() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/projects/" + ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin()).getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }


}
