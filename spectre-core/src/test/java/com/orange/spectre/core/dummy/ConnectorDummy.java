package com.orange.spectre.core.dummy;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.model.enumerator.ConnectorType;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class ConnectorDummy {


    public static AbstractConnector getNewSonarConnector(){
        AbstractConnector connector = ConnectorType.sonar.createConnector();
        connector.setId(new Long(1));
        connector.setName("sonar");
        connector.setDescription("sonar description");

        connector.getIndicators().add(IndicatorDummy.getNewSonarTestCoverageIndicator(connector));

        return connector;
    }

    public static AbstractConnector getNewSonarConnectorWithoutIndicator(){
        AbstractConnector connector = ConnectorType.sonar.createConnector();
        connector.setId(new Long(3));
        connector.setName("sonar");
        connector.setDescription("sonar description");

        return connector;
    }

}
