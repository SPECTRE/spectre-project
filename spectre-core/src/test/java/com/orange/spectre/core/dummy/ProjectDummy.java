package com.orange.spectre.core.dummy;

import java.util.HashSet;
import java.util.Set;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.Project;
import com.orange.spectre.core.web.dto.MemberDto;
import com.orange.spectre.core.web.dto.UpdateProjectDto;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class ProjectDummy {

	public static Project getCreationProject(String owner) {
		Project project = new Project();
		project.setName("projet");
		project.setOwner(owner);
		project.setReachable(true);
		return project;
	}

	public static Project getNewProject(String owner) {
		Project project = new Project();
		project.setId(new Long(1));
		project.setName("projet");
		project.setOwner(owner);
		project.setReachable(true);

		project.getFavouritesAccounts().add(AccountDummy.getExistingAccount());

		project.getConnectors().add(ConnectorDummy.getNewSonarConnector());

		return project;
	}

	public static UpdateProjectDto getNewProjectDto() {
		UpdateProjectDto project = new UpdateProjectDto();
		project.setName("projetDto");
		Set<MemberDto> members = new HashSet<>();
		project.setMembers(members);
		return project;
	}

	public static Project getNewProjectBis(String owner) {
		Project project = new Project();
		project.setId(new Long(3));
		project.setName("projetBis");
		project.setOwner(owner);
		project.setReachable(false);
		return project;
	}

	public static Project getNewProjectWithConnector(String owner, AbstractConnector connector) {
		Project project = new Project();
		project.setId(new Long(4));
		project.setName("projetWithConnector");
		project.setOwner(owner);
		project.setReachable(true);
		project.setConnectors(new HashSet<>());
		project.getConnectors().add(connector);

		return project;
	}

}
