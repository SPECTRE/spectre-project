package com.orange.spectre.core.dummy;


import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.CustomIndicator;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class IndicatorDummy {

    public static AbstractIndicator getNewSonarTestCoverageIndicator(AbstractConnector connector){
        AbstractIndicator indicator = connector.createIndicator(SonarIndicatorType.TEST_COVERAGE.name());
        indicator.setConnector(connector);
        indicator.setId(new Long(1));
        return indicator;
    }

    public static AbstractIndicator getNewCustomIndicator(){
        AbstractIndicator indicator = new CustomIndicator();
        indicator.setId(new Long(3));
        return indicator;
    }

    public static AbstractIndicator getNewSonarTestCoverageIndicatorWithoutWidget(AbstractConnector connector){
        AbstractIndicator indicator = connector.createIndicator(SonarIndicatorType.TEST_COVERAGE.name());
        indicator.setConnector(connector);
        indicator.setId(new Long(2));
        return indicator;
    }


}
