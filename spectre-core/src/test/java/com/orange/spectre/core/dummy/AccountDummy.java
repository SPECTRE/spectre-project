package com.orange.spectre.core.dummy;

import com.orange.spectre.core.domain.Account;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class AccountDummy {

    public static Account getExistingAccount(){
        Account account = new Account();
        account.setId(new Long(1));
        account.setLogin("aaaa1111");
        account.setEmail("account@orange.com");
        account.setPassword("azerty");
        return account;
    }

    public static Account getNewAccount(){
        Account account = new Account();
        account.setLogin("aaaa2222");
        account.setEmail("account2@orange.com");
        account.setPassword("azerty");
        return account;
    }

    public static Account getNewAccountCreated(){
        Account account = new Account();
        account.setId(new Long(2));
        account.setLogin("aaaa2222");
        account.setEmail("account2@orange.com");
        account.setPassword("azerty");
        return account;
    }

}
