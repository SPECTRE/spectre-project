package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.dummy.ConnectorDummy;
import com.orange.spectre.core.dummy.IndicatorDummy;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.service.KpiInjectionService;
import com.orange.spectre.core.web.dto.CustomDataDTO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ludovic on 28/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class IndicatorResourceTest {

    private MockMvc restWelcomeMockMvc;

    @InjectMocks
    private IndicatorResource indicatorResource;

    @Mock
    private AbstractIndicatorRepository indicatorRepository;

    @Mock
    private KpiInjectionService kpiInjectionService;

    private CustomDataDTO customData;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(indicatorResource).build();

        Mockito.when(indicatorRepository.findOne(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()).getId())).thenReturn(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()));
        Mockito.when(indicatorRepository.findOne(IndicatorDummy.getNewCustomIndicator().getId())).thenReturn(IndicatorDummy.getNewCustomIndicator());

        Mockito.when(indicatorRepository.exists(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()).getId())).thenReturn(true);
        Mockito.when(indicatorRepository.exists(IndicatorDummy.getNewCustomIndicator().getId())).thenReturn(true);


        customData = new CustomDataDTO();
        customData.setData("data");
    }

    @Test
    public void testInjectCustomDataInUnknownIndicator() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/indicators/2/inject").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(customData))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testInjectCustomDataInNotCustomIndicator() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/indicators/"+ IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()).getId() +"/inject").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(customData))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

    }

    @Test
    public void testInjectCustomDataInCustomIndicator() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/indicators/"+ IndicatorDummy.getNewCustomIndicator().getId() +"/inject").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(customData))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetIndicatorProperties() throws Exception{

        restWelcomeMockMvc
                .perform(
                        get("/indicators/properties")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }


}
