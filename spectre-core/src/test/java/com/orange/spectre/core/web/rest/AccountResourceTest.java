package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.Project;
import com.orange.spectre.core.dummy.AccountDummy;
import com.orange.spectre.core.dummy.DashboardDummy;
import com.orange.spectre.core.dummy.ProjectDummy;
import com.orange.spectre.core.repository.*;
import com.orange.spectre.core.service.DashboardService;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.service.MailService;
import com.orange.spectre.core.service.ProjectService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Optional;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
/**
 * Created by ludovic on 27/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class AccountResourceTest {

    private MockMvc restWelcomeMockMvc;

    @InjectMocks
    private AccountResource accountResource;

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private DashboardRepository dashboardRepository;

    @Mock
    AbstractIndicatorRepository indicatorRepository;

    @Mock
    private WidgetRepository widgetRepository;

    @Mock
    private ProjectRepository projectRepository;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private MailService mailService;


    @Mock
    private DashboardService dashboardService;

    @Mock
    private ProjectService projectService;

    @Mock
    FetchIndicatorsService fetchIndicatorsService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);


        Account account = AccountDummy.getExistingAccount();
        account.setFavouritesDashboard(new HashSet<>());
        account.setMembersProjects(new HashSet<>());
        account.setFavouritesProjects(new HashSet<>());
        account.getFavouritesDashboard().add(DashboardDummy.getNewDashboard(account.getLogin()));
        //account.getMembersProjects().add(ProjectDummy.getNewProject(account.getLogin()));
        account.getFavouritesProjects().add(ProjectDummy.getNewProject(account.getLogin()));



        Optional<Account> optionalAccount = Optional.of( account );
        Optional<Account> optionalAccountEmpty = Optional.empty();


        Stream<String> streamAccount = Stream.of(account.getLogin());

        Mockito.when(accountRepository.findAllLogins()).thenReturn(streamAccount);
        Mockito.when(accountRepository.findByLogin(AccountDummy.getExistingAccount().getLogin())).thenReturn(optionalAccount);
        Mockito.when(accountRepository.findByLogin("bbbb1111")).thenReturn(optionalAccountEmpty);
        Mockito.when(accountRepository.findByEmail("account2@orange.com")).thenReturn(optionalAccountEmpty);
        Mockito.when(accountRepository.findByLogin("dashboards")).thenReturn(optionalAccountEmpty);
        Mockito.when(accountRepository.findByLogin("projects")).thenReturn(optionalAccountEmpty);
        Mockito.when(accountRepository.findByLogin(AccountDummy.getNewAccount().getLogin())).thenReturn(optionalAccountEmpty);
        Mockito.when(accountRepository.save(AccountDummy.getNewAccount())).thenReturn(AccountDummy.getNewAccountCreated());
        Mockito.when(dashboardRepository.save(DashboardDummy.getSavedDashboard())).thenReturn(DashboardDummy.getNewDashboard("aaaa1111"));

        Mockito.when(dashboardRepository.findOne(DashboardDummy.getNewDashboard(account.getLogin()).getId())).thenReturn(DashboardDummy.getNewDashboard(account.getLogin()));
        Mockito.when(dashboardRepository.findOne(DashboardDummy.getNewDashboardBis(account.getLogin()).getId())).thenReturn(DashboardDummy.getNewDashboardBis(account.getLogin()));

        Mockito.when(projectRepository.findOne(ProjectDummy.getNewProject(account.getLogin()).getId())).thenReturn(ProjectDummy.getNewProject(account.getLogin()));
        Mockito.when(projectRepository.findOne(ProjectDummy.getNewProjectBis(account.getLogin()).getId())).thenReturn(ProjectDummy.getNewProjectBis(account.getLogin()));

        this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(accountResource).build();
    }

    @Test
    public void testGetAllLogins() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/accounts/").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetAccountByLogin() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/accounts/" + AccountDummy.getExistingAccount().getLogin()).contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$").value(Matchers.notNullValue()))
                .andExpect(jsonPath("$.email").value(Matchers.notNullValue()))
                .andExpect(jsonPath("$.login").value(AccountDummy.getExistingAccount().getLogin()));
    }

    @Test
    public void testGetUnknownAccountByLogin() throws Exception {
        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111").contentType("application/json").accept(
                                MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testCreateAlreadyExistingAccount() throws Exception {
        restWelcomeMockMvc
                .perform(
                        post("/accounts/register")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(AccountDummy.getExistingAccount()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isConflict());
    }

    @Test
    public void testCreateInvalidAccount() throws Exception {
        Account tempAccount = AccountDummy.getNewAccount();
        tempAccount.setLogin(null);
        restWelcomeMockMvc
                .perform(
                        post("/accounts/register")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(tempAccount))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        restWelcomeMockMvc
                .perform(
                        post("/accounts/register")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(""))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void testCreateAccount() throws Exception {
        restWelcomeMockMvc
                .perform(
                        post("/accounts/register")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(AccountDummy.getNewAccount()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void testGetAllDashboardsForUnknownUser() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/dashboards")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//dashboards")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetAllDashboards() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/dashboards")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetAllFavouritesDashboardsForUnknownUser() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/dashboards/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//dashboards/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetAllFavouritesDashboards() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/dashboards/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }



    @Test
    public void testGetAllProjectsForUnknownUser() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/projects")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//projects")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetAllProjects() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/projects")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetAllFavouritesProjectsForUnknownUser() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/projects/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//projects/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetAllFavouritesProjects() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/projects/favourites")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetAllMembersProjectsForUnknownUser() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/projects/members")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//projects/members")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetAllMembersProjects() throws Exception {

        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/projects/members")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    public void testGetDashboardForUnknownUser() throws Exception{
        Dashboard dashboard = DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin());
        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/dashboards/" + dashboard.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//dashboards/" + dashboard.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetDashboardForUnknownDashboard() throws Exception{
        Dashboard dashboard = DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin());
        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+" /dashboards/3")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetDashboardForNotOwnedDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/accounts/aaaa1111/dashboards/2")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetDashboardForAUser() throws Exception{
        Dashboard dashboard = DashboardDummy.getNewDashboard(AccountDummy.getExistingAccount().getLogin());
        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/dashboards/" + dashboard.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Matchers.notNullValue()))
                .andExpect(jsonPath("$.id").value(dashboard.getId().intValue()));

    }

    @Test
    public void testGetProjectForUnknownUser() throws Exception{
        Project project = ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin());
        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/projects/" + project.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

        restWelcomeMockMvc
                .perform(
                        get("/accounts//projects/" + project.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetProjectForUnknownDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+" /projects/3")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetProjectForNotOwnedDashboard() throws Exception{
        restWelcomeMockMvc
                .perform(
                        get("/accounts/aaaa1111/projects/2")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    public void testGetProjectForAUser() throws Exception{
        Project project = ProjectDummy.getNewProject(AccountDummy.getExistingAccount().getLogin());
        restWelcomeMockMvc
                .perform(
                        get("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/projects/" + project.getId())
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").value(Matchers.notNullValue()))
                .andExpect(jsonPath("$.id").value(project.getId().intValue()));

    }

    @Test
    public void testCreateDashboard() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/dashboards/")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(DashboardDummy.getCreationDashboardDto(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                                .andExpect(status().isCreated());


    }

    @Test
    public void testCreateDashboardWithUnknownUser() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/accounts/bbbb1111/dashboards/")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(DashboardDummy.getCreationDashboard(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());


    }

    @Test
    public void testCreateProject() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/accounts/"+ AccountDummy.getExistingAccount().getLogin()+ "/projects/")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(ProjectDummy.getCreationProject(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());


    }

    @Test
    public void testCreateProjectWithUnknownUser() throws Exception{

        restWelcomeMockMvc
                .perform(
                        post("/accounts/bbbb1111/projects/")
                                .contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(ProjectDummy.getCreationProject(AccountDummy.getExistingAccount().getLogin())))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }




    @Test
    public void testGetAvailabledIndicatorsForUnknownUser() throws  Exception{
        restWelcomeMockMvc
                .perform(
                        get("/accounts/bbbb1111/indicators/types")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());

    }

    @Test
    @Transactional
    public void testGetAvailabledIndicators() throws  Exception{
        restWelcomeMockMvc
                .perform(
                        get("/accounts/" + AccountDummy.getExistingAccount().getLogin() + "/indicators/types")
                                .contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}
