package com.orange.spectre.core.dummy;

import java.util.HashSet;
import java.util.Set;

import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.web.dto.CreateDashboardDto;
import com.orange.spectre.core.web.dto.MemberDto;
import com.orange.spectre.core.web.dto.UpdateDashboardDto;

/**
 * Created by ludovic on 26/04/2016.
 */
public final class DashboardDummy {

	public static Dashboard getCreationDashboard(String owner) {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setDescription("description");
		dashboard.setOwner(owner);

		return dashboard;
	}

	public static CreateDashboardDto getCreationDashboardDto(String owner) {
		CreateDashboardDto dashboard = new CreateDashboardDto();
		dashboard.setName("dashboard");
		dashboard.setDescription("Description de mon dashboard");
		dashboard.setReachable(true);
		dashboard.setOwner(owner);
		return dashboard;
	}

	public static Dashboard getNewDashboard(String owner) {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setId(new Long(1));
		dashboard.setDescription("description");
		dashboard.setReachable(true);
		dashboard.setOwner(owner);

		dashboard.getFavouritesAccounts().add(AccountDummy.getExistingAccount());

		return dashboard;
	}

	public static UpdateDashboardDto getNewDashboardDto() {
		UpdateDashboardDto dashboard = new UpdateDashboardDto();
		dashboard.setName("dashboard");
		Set<MemberDto> members = new HashSet<>();
		dashboard.setMembers(members);

		return dashboard;
	}

	public static Dashboard getSavedDashboard() {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setDescription(null);
		dashboard.setReachable(true);
		dashboard.setOwner("aaaa1111");
		dashboard.setMembersAccounts(null);
		dashboard.setFavouritesAccounts(null);
		dashboard.setWidgets(null);
		return dashboard;
	}

	public static Dashboard getNewDashboardWithWidget(String owner) {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setId(new Long(4));
		dashboard.setDescription("description");
		dashboard.setOwner(owner);

		dashboard.getFavouritesAccounts().add(AccountDummy.getExistingAccount());
		dashboard.getWidgets().add(WidgetDummy
				.getNewWidget(IndicatorDummy.getNewSonarTestCoverageIndicator(ConnectorDummy.getNewSonarConnector()), dashboard));
		dashboard.getWidgets().add(WidgetDummy.getNewCustomWidget(dashboard));

		return dashboard;
	}

	public static Dashboard getNewDashboardBis(String owner) {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setId(new Long(3));
		dashboard.setDescription("description");
		dashboard.setReachable(false);
		dashboard.setOwner(owner);

		return dashboard;
	}

	public static Dashboard getNewDashboardWithoutWidget(String owner) {
		Dashboard dashboard = new Dashboard();
		dashboard.setName("dashboard");
		dashboard.setId(new Long(3));
		dashboard.setDescription("description");
		dashboard.setReachable(true);
		dashboard.setOwner(owner);

		return dashboard;
	}

}
