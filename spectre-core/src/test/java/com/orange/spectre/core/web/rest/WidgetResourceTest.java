package com.orange.spectre.core.web.rest;

import com.orange.spectre.SpectreCoreApplication;
import com.orange.spectre.core.dummy.WidgetDummy;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.repository.WidgetRepository;
import com.orange.spectre.core.service.DeletionService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static com.orange.spectre.core.dummy.WidgetDummy.getExistingWidgetDto;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ludovic on 28/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("tests")
@SpringApplicationConfiguration(classes = SpectreCoreApplication.class)
@WebAppConfiguration
@IntegrationTest
public class WidgetResourceTest {

    private MockMvc restWelcomeMockMvc;

    @InjectMocks
    private WidgetResource widgetResource;

    @Mock
    DeletionService deletionService;

    @Mock
    WidgetRepository widgetRepository;

    @Mock
    AbstractIndicatorRepository indicatorRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(widgetResource).build();

        Mockito.when(widgetRepository.exists(WidgetDummy.getNewWidget(null,null).getId())).thenReturn(true);
        Mockito.when(widgetRepository.findOne(WidgetDummy.getNewWidget(null,null).getId())).thenReturn(WidgetDummy.getNewWidget(null,null));


    }

    @Test
    public void testUpdateUnknownWidget() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/widgets/2").contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(getExistingWidgetDto()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testUpdateWidget() throws Exception{
        restWelcomeMockMvc
                .perform(
                        put("/widgets/" + WidgetDummy.getNewWidget(null,null).getId()).contentType("application/json")
                                .content(TestUtil.convertObjectToJsonBytes(WidgetDummy.getExistingWidgetDto()))
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteWidget() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/widgets/" + WidgetDummy.getNewWidget(null,null).getId()).contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteUnknownWidget() throws Exception{
        restWelcomeMockMvc
                .perform(
                        delete("/widgets/1").contentType("application/json")
                                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

}
