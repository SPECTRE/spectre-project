package com.orange.spectre;

import com.orange.spectre.core.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PreDestroy;

/**
 * Application main class
 */
@SpringBootApplication
@EnableScheduling
@ComponentScan
@EnableSpringConfigured
@EnableConfigurationProperties({ LiquibaseProperties.class })
public class SpectreCoreApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(SpectreCoreApplication.class);

	private static ApplicationContext context;

	/**
	 * Main method
	 *
	 * @param args
	 *            Execution params
	 */
	public static void main(String[] args) {
		System.setProperty("spring.profiles.default", Constants.SPRING_PROFILE_LOCAL_H2);

		context = SpringApplication.run(SpectreCoreApplication.class, args);
		LOGGER.info("Application Spectre is starting ...");

	}

	/**
	 * Shutdown application method Used to cleanup akka actors
	 */
	@PreDestroy
	public void terminatingApplicationStep() {
		LOGGER.info("Shutting down");

		LOGGER.info("TERMINATE SPECTRE-CORE APPLICATION");
	}

}
