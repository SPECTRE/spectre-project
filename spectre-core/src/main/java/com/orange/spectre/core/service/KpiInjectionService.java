package com.orange.spectre.core.service;

import com.orange.spectre.core.model.enumerator.InjectedData;
import com.orange.spectre.core.model.enumerator.InjectedKpi;
import org.apache.commons.lang.StringUtils;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

/**
 * Created by ludovic on 12/04/2016.
 * <p>
 * Service to push kpi into elasticsearch
 */
@Service
public class KpiInjectionService {

    private final Logger LOGGER = LoggerFactory.getLogger(KpiInjectionService.class);

    private Client client;

    @Value("${elastic-server}")
    private String url;

    public KpiInjectionService() {
        client = ClientBuilder.newClient().register(JacksonFeature.class);
    }

    /**
     * Inject KPI into ElasticSearch
     *
     * @param injectedKpi
     */
    public void injectKpi(InjectedKpi injectedKpi) {

        if (injectedKpi.getInjectedData().getValue() == null) {
            LOGGER.debug("data was null...", injectedKpi.toString());
            return;
        }

        LOGGER.info("inject kpi {}", injectedKpi.toString());
        WebTarget target = null;
        target = client.target(url + StringUtils.remove((injectedKpi.getConnectorType() +
                    injectedKpi.getIndicatorType()).toLowerCase(), "_") + "/" +
                    StringUtils.remove(injectedKpi.getIndicatorMeasure().toLowerCase(), "_"));

        if (target == null) {
            LOGGER.error("Handler du serveur ES - url: {} - est null !!",url);
        }else {
            Entity<InjectedData> kpiEntity = Entity.entity(injectedKpi.getInjectedData(), MediaType.APPLICATION_JSON);
            target.request().post(kpiEntity);
        }

    }

}
