package com.orange.spectre.core.model.data;

import java.util.Objects;

public class FloatData implements AbstractData {

    private Float data;

    public FloatData() {
        // Empty
    }

    public FloatData(Float data) {
        this.data = data;
    }

    public Float getData() {
        return data;
    }

    public void setData(Float data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                FloatData that = (FloatData) o;
                return Objects.equals(data, that.data);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "FloatData{" +
                "data='" + data + '\'' +
                '}';
    }
}
