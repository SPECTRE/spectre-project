package com.orange.spectre.core.service;

import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.DashboardRepository;
import com.orange.spectre.core.repository.ProjectRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

/**
 * Created by epeg7421 on 04/10/2016.
 * <p>
 * Service to handle Spectre "plugins" operations Access to all JPA Repository
 */
@Service
public class SpectrePluginsService {
	@Inject
	ProjectRepository projectRepository;

	@Inject
	AccountRepository accountRepository;

	@Inject
	DashboardRepository dashboardRepository;

	/**
	 * count all users in Spectre
	 *
	 * @return All users
	 */
	public Long countAllUsers() {
		return accountRepository.findAllLogins().count();
	}

	/**
	 * count all projects in Spectre
	 *
	 * @return All active projects
	 */
	public Long countAllProjects() {
		return projectRepository.countAll();
	}

	/**
	 * count all private projects in Spectre
	 *
	 * @return All private projects
	 */
	public Long countAllProjectsByReachableFalse() {
		return (long) projectRepository.findAllByReachableFalse().size();
	}

	/**
	 * count all public projects in Spectre
	 *
	 * @return All public projects
	 */
	public Long countAllProjectsByReachableTrue() {
		return (long) projectRepository.findAllByReachableTrue().size();
	}

	/**
	 * count all projects in Spectre
	 *
	 * @return All active projects
	 */
	public Long countAllDashboards() {
		return dashboardRepository.countAll();
	}

	/**
	 * count all private dashboards in Spectre
	 *
	 * @return All private dashboards
	 */
	public Long countAllDashboardsByReachableFalse() {
		return (long) dashboardRepository.findAllByReachableFalseOrderByIdAsc().size();
	}

	/**
	 * count all public dashboards in Spectre
	 *
	 * @return All public dashboards
	 */
	public Long countAllDashboardsByReachableTrue() {
		return (long) dashboardRepository.findAllByReachableTrueOrderByIdAsc().size();
	}

	/**
	 * count all connectors in Spectre
	 *
	 * @return All public connectors attached to a project
	 */
	public Long countAllConnectors() {
		return projectRepository.findAllConnectors();
	}
}
