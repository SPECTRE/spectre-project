package com.orange.spectre.core.service;

import com.orange.spectre.core.domain.*;
import com.orange.spectre.core.exception.*;
import com.orange.spectre.core.repository.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Set;

/**
 * Created by ludovic on 12/04/2016.
 * <p>
 * Service to handler dashboard, project, connector, indicator, widget and the links between them
 */
@Service
public class DeletionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeletionService.class);

    @Inject
    private AbstractConnectorRepository connectorRepository;

    @Inject
    private AbstractIndicatorRepository indicatorRepository;

    @Inject
    private ProjectRepository projectRepository;

    @Inject
    private WidgetRepository widgetRepository;

    @Inject
    private DashboardRepository dashboardRepository;

    @Inject
    private DashboardMemberRepository dashboardMemberRepository;

    @Inject
    private ProjectMemberRepository projectMemberRepository;

    /**
     * Service to delete a project
     *
     * @param projectId Project ID
     */
    public void deleteProject(Long projectId) {

        LOGGER.debug("delete project {}", projectId);
        if (!projectRepository.exists(projectId)) {
            LOGGER.error("project unknown");
            throw new ProjectNotFoundException(projectId);
        }

        Project project = projectRepository.findOne(projectId);
        Set<AbstractConnector> connectors = project.getConnectors();

        Set<ProjectMember> members = projectMemberRepository.findByProjectId(projectId);
        for(ProjectMember member : members){
            projectMemberRepository.delete(member);
        }

        projectRepository.delete(projectId);

        if (connectors != null) {
            for (AbstractConnector connector : connectors) {
                deleteConnector(connector.getId());
            }
        }


    }

    /**
     * Service to delete a connector
     *
     * @param connectorId Connector ID
     */
    public void deleteConnector(Long connectorId) {

        LOGGER.debug("delete connector {}", connectorId);
        if (!connectorRepository.exists(connectorId)) {
            LOGGER.error("connector unknown");
            throw new ConnectorNotFoundException(connectorId);
        }


        AbstractConnector connector = connectorRepository.findOne(connectorId);
        Set<AbstractIndicator> indicators = connector.getIndicators();

        for (AbstractIndicator indicator : indicators) {
            deleteIndicator(indicator.getId());
        }

        connectorRepository.delete(connectorId);

    }

    /**
     * Service to delete an indicator
     *
     * @param indicatorId Indicator ID
     */
    public void deleteIndicator(Long indicatorId) {
        LOGGER.debug("delete indicator {}", indicatorId);

        if (!indicatorRepository.exists(indicatorId)) {
            LOGGER.error("indicator unknown");
            throw new IndicatorNotFoundException(indicatorId);
        }

        AbstractIndicator indicator = indicatorRepository.findOne(indicatorId);
        Set<Widget> widgets = widgetRepository.findAllByIndicator(indicator);

        for (Widget widget : widgets) {
            widget.setIndicator(null);
            widget.setLabel(null);
            widgetRepository.save(widget);
        }

        indicatorRepository.delete(indicatorId);

    }

    /**
     * Service to delete a widget
     *
     * @param widgetId Widget ID
     */
    public void deleteWidget(Long widgetId) {

        LOGGER.debug("delete widget {}", widgetId);

        if (!widgetRepository.exists(widgetId)) {
            LOGGER.error("widget unknown");
            throw new WidgetNotFoundException(widgetId);
        }

        Widget widget = widgetRepository.findOne(widgetId);
        Dashboard dashboard = widget.getDashboard();

        dashboard.getWidgets().remove(widget);
        dashboardRepository.save(dashboard);
        widgetRepository.delete(widgetId);


    }

    /**
     * Service to delete a dashboard
     *
     * @param dashboardId Dashboard ID
     */
    public void deleteDashboard(Long dashboardId) {

        LOGGER.debug("delete dashboard {}", dashboardId);

        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error("dashboard unknown");
            throw new DashboardNotFoundException(dashboardId);
        }

        Set<DashboardMember> members = dashboardMemberRepository.findByDashboardId(dashboardId);
        for(DashboardMember member : members){
            dashboardMemberRepository.delete(member);
        }

        Dashboard dash = dashboardRepository.findOne(dashboardId);
        Set<Widget> widgets = dash.getWidgets();

        if (widgets != null) {
            for (Widget widget : widgets) {
                deleteWidget(widget.getId());
            }
        }
        dashboardRepository.delete(dashboardId);

    }


}
