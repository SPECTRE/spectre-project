package com.orange.spectre.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

import javax.validation.constraints.NotNull;

/**
 * Created by ludovic on 15/03/2016.
 * <p>
 *     Spring profile variables
 * </p>
 */
@Configuration
@ConfigurationProperties(prefix = "spectre", ignoreUnknownFields = false)
public class SpectreProperties {

    private final CorsConfiguration cors = new CorsConfiguration();

    private final MailProperties mail = new MailProperties();

    private final  Environment environment = new Environment();

    private final Async async = new Async();


    /**
     * Get CORS configuration parameters
     * @return CORS configuration
     */
    public CorsConfiguration getCors() {
        return cors;
    }

    /**
     * Get Mailing configuration parameters
     * @return Mailing configuration parameters
     */
    public MailProperties getMail() {
        return mail;
    }
    public Async getAsync() {
        return async;
    }

    public Environment getEnvironment() {
        return environment;
    }


    public static class Async {

        private int corePoolSize = 2;

        private int maxPoolSize = 50;

        private int queueCapacity = 10000;

        public int getCorePoolSize() {
            return corePoolSize;
        }

        public void setCorePoolSize(int corePoolSize) {
            this.corePoolSize = corePoolSize;
        }

        public int getMaxPoolSize() {
            return maxPoolSize;
        }

        public void setMaxPoolSize(int maxPoolSize) {
            this.maxPoolSize = maxPoolSize;
        }

        public int getQueueCapacity() {
            return queueCapacity;
        }

        public void setQueueCapacity(int queueCapacity) {
            this.queueCapacity = queueCapacity;
        }
    }


}
