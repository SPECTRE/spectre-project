/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.core.repository;

import com.orange.spectre.core.domain.DashboardMember;
import com.orange.spectre.core.domain.DashboardMemberId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by ludovic on 06/04/2016.
 *
 * JPA Repository for database Dashboard Member association entity operations
 */
public interface DashboardMemberRepository extends JpaRepository<DashboardMember, DashboardMemberId> {

    /**
     * Get all dashboard member association for a dashboard id
     * @param dashboardId Dashboard ID
     * @return All association for a dashboard
     */
    Set<DashboardMember> findByDashboardId(Long dashboardId);
    
}
