package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Custom widget rest DTO
 * </p>
 */
public class CustomWidgetDto {

    private Long id;
    private IndicatorDto indicator;
    private Long order;
    private String measure;
    private String unity;
    private String data;
    private String label;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public IndicatorDto getIndicator() {
        return indicator;
    }

    public void setIndicator(IndicatorDto indicator) {
        this.indicator = indicator;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getOrder() {
        return order;
    }

    public void setOrder(Long order) {
        this.order = order;
    }
}
