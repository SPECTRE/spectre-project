package com.orange.spectre.core.domain;


import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.model.data.AbstractData;

import javax.persistence.*;
import java.util.Map;

/**
 * Jpa custom indicator entity
 */
@Entity
@DiscriminatorValue("CUSTOM")
public class CustomIndicator extends AbstractIndicator {

    public CustomIndicator() {
        super();
    }

    public CustomIndicator(IndicatorMeasure measure) {
        this.measure = measure;
    }

    public CustomIndicator(IndicatorMeasure measure, String unity) {

        this.measure = measure;
        this.measureUnity = unity;

    }

    @Override
    public String getLabel() {
        return "custom";
    }

    @Override
    public AbstractData getData(Map<String, String> map) {
        return null;
    }

    @Override
    public IndicatorMeasure getMeasure() {
        if (this.measure == null) {
            return IndicatorMeasure.NUMERIC;
        } else {
            return measure;
        }
    }

    @Override
    public String getMeasureUnity() {
        return measureUnity;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }


    public void setMeasure(String measure) {
        // Used by dozer mapper
        this.measure = IndicatorMeasure.valueOf(measure);
    }

    @SuppressWarnings( "findbugs:Used by dozer")
    public void setMeasureUnity(String unity) {
        // Used by dozer mapper
        this.measureUnity = unity;
    }

    @Override
    public IndicatorFamily getFamily() {
        return null;
    }

    @Override
    public Enum getType() {
        return CustomEnumType.CUSTOM;
    }

    public enum CustomEnumType {
        CUSTOM
    }
}
