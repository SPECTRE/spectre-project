package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for unknown dashboard
 *
 * Return http 404 when exception raised
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class DashboardNotFoundException extends RuntimeException {

    public DashboardNotFoundException(Long dashboardId) {
        super("Could not find dashboard '" + Long.toString(dashboardId) + "'.");
    }

    public DashboardNotFoundException(String shareId) {
        super("Could not find dashboard '" + shareId + "'.");
    }
}
