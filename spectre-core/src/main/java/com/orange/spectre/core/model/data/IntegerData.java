package com.orange.spectre.core.model.data;

import java.util.Objects;

public class IntegerData implements AbstractData {

    private Integer data;

    public IntegerData() {
        // Empty
    }

    public IntegerData(Integer data) {
        this.data = data;
    }

    public Integer getData() {
        return data;
    }

    public void setData(Integer data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                IntegerData that = (IntegerData) o;
                return Objects.equals(data, that.data);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "IntegerData{" +
                "data='" + data + '\'' +
                '}';
    }
}
