package com.orange.spectre.core.model.data;

/**
 * Used to generify return type of indicator's 'getData' method.
 */
public interface AbstractData {
	abstract public Object getData();
}