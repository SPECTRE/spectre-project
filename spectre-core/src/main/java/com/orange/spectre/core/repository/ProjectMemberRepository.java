/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.core.repository;

import com.orange.spectre.core.domain.ProjectMember;
import com.orange.spectre.core.domain.ProjectMemberId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

/**
 * Created by ludovic on 06/04/2016.
 *
 * JPA Repository for database Project Member association entity operations
 */
public interface ProjectMemberRepository extends JpaRepository<ProjectMember, ProjectMemberId>{

	/**
	 * Get all project member association for a project id
	 * @param projectId Project ID
	 * @return All association for a project
     */
	Set<ProjectMember> findByProjectId(Long projectId);
	
}
