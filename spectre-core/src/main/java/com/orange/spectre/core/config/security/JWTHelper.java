package com.orange.spectre.core.config.security;
/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


import com.orange.spectre.core.config.security.exception.Unauthorized;
import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component("jwtHelper")
public class JWTHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(JWTHelper.class);
	private static final String JWT_TYPE_FIELD_NAME = "type";
	private static final String JWT_USER_ID_FIELD_NAME = "user_id";

	@Value("${jwt.payload.issuer}")
	private String jwtIssuer;

	@Value("${jwt.payload.subject}")
	private String jwtSubject;

	@Value("${jwt.signature.secretkey}")
	private String jwtSecretKey;

	private Claims getClaims(final String jwt) {
		Claims claims;
		try {
			claims = Jwts.parser().setSigningKey(this.jwtSecretKey).parseClaimsJws(jwt)
					.getBody();
		}
		catch (final ExpiredJwtException e) {
			LOGGER.info("Token expired");
			throw new UnauthorizedException(Unauthorized.GENERIC, "Token expired");
		}
		catch (final UnsupportedJwtException | MalformedJwtException e) {
			LOGGER.error("Invalid token format");
			throw new UnauthorizedException(Unauthorized.GENERIC, "Invalid token format");
		}
		catch (final SignatureException | IllegalArgumentException e) {
			LOGGER.error("Invalid token");
			throw new UnauthorizedException(Unauthorized.GENERIC, "Invalid token");
		}
		return claims;
	}

	/**
	 * Get UserId from JWT and ensuring that the token is valid, wellformed and verify
	 *
	 * @param jwt
	 * @return
	 */
	public String getUserIdFromJWT(final String jwt) {
		final Claims claims = this.getClaims(jwt);
		return claims.get(JWT_USER_ID_FIELD_NAME, String.class);
	}

	public String getJWT(final String userId, final int expiresInMinutes) {

		// The JWT signature algorithm we will be using to sign the token
		final SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		final long nowMillis = System.currentTimeMillis();
		final Date now = new Date(nowMillis);

		// Expiration date
		final long expMillis = nowMillis + (expiresInMinutes * 60000);
		final Date exp = new Date(expMillis);

		// Let's set the JWT Claims
		final JwtBuilder builder = Jwts.builder()
				.setHeaderParam(JWT_TYPE_FIELD_NAME, "JWT").setIssuedAt(now)
				.setIssuer(this.jwtIssuer).setSubject(this.jwtSubject).setExpiration(exp)
				.claim(JWT_USER_ID_FIELD_NAME, userId)
				.signWith(signatureAlgorithm, this.jwtSecretKey);

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

}
