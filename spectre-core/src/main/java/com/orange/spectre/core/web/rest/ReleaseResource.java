package com.orange.spectre.core.web.rest;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.config.Environment;
import com.orange.spectre.core.config.SpectreProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.File;


/**
 * Created by DFQR0168 on 14/04/2017.
 */

@RestController
@RequestMapping("/release")
public class ReleaseResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReleaseResource.class);

    @Inject
    private SpectreProperties spectreProperties;

    /**
     * Get list of releases
     *
     * @return HttpStatus OK with list of releases.
     */
    @RequestMapping(method = RequestMethod.GET)
    @Produces({ MediaType.APPLICATION_JSON })
    ResponseEntity<Object> getRelease(){
        Object obj;
        try {
            ObjectMapper mapper = new ObjectMapper();
            obj = mapper.readValue(new File(spectreProperties.getEnvironment().getReleaseFilePath()), Object.class);
            LOGGER.debug(obj.toString());

        }catch (Exception e){
            LOGGER.error(e.toString());
            return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(obj, null, HttpStatus.OK);
    }

    /**
     * Get environment
     *
     * @return HttpStatus OK with environment.
     */
    @RequestMapping(value = "/env", method = RequestMethod.GET)
    @Produces({ MediaType.APPLICATION_JSON })
    ResponseEntity<Environment> getEnv(){
        LOGGER.info(spectreProperties.getEnvironment().getEnv());
        return new ResponseEntity<>(spectreProperties.getEnvironment(), null, HttpStatus.OK);
    }
}
