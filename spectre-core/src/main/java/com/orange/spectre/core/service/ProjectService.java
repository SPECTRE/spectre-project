/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.core.service;

import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.domain.Project;
import com.orange.spectre.core.domain.ProjectMember;
import com.orange.spectre.core.domain.ProjectMemberId;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.ProjectMemberRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

/**
 * @author NBLG6551
 *         <p>
 *         Service to handler project operations
 */
@Service
public class ProjectService {

    @Inject
    ProjectMemberRepository projectMemberRepository;

    @Inject
    AccountRepository accountRepository;

    /**
     * Add a member to a project
     *
     * @param project Project
     * @param account Member account
     * @param isAdmin True if member is admin of the project
     */
    public void addMember(Project project, Account account, boolean isAdmin) {
        ProjectMemberId id = new ProjectMemberId();
        id.setProjectId(project.getId());
        id.setAccountId(account.getId());
        if (projectMemberRepository.exists(id)) {
            ProjectMember association = projectMemberRepository.findOne(id);
            association.setAdmin(isAdmin);
            projectMemberRepository.save(association);
        } else {
            ProjectMember association = new ProjectMember();
            association.setMember(account);
            association.setProject(project);
            association.setAccountId(account.getId());
            association.setProjectId(project.getId());
            association.setAdmin(isAdmin);
            projectMemberRepository.save(association);
        }
    }

    /**
     * Remove a member of a project
     *
     * @param project Project
     * @param account Member account
     */
    public void removeMember(Project project, Account account) {
        ProjectMember association = new ProjectMember();
        association.setAccountId(account.getId());
        association.setProjectId(project.getId());
        projectMemberRepository.delete(association);
    }

    /**
     * Check if the user is admin of the project
     * @param projectId Project ID
     * @param login User login
     * @return True if user is admin
     */
    public Boolean checkAdmin(Long projectId,String login) {
        Optional<Account> account = accountRepository.findByLogin(login);
        ProjectMemberId id = new ProjectMemberId();
        id.setProjectId(projectId);
        id.setAccountId(account.get().getId());
        ProjectMember myPM = projectMemberRepository.findOne(id);
        return myPM.isAdmin();
    }


}
