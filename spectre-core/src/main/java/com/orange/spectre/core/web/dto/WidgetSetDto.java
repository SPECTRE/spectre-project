package com.orange.spectre.core.web.dto;

import java.util.List;
import java.util.Objects;

public class WidgetSetDto {

    private List<WidgetDto> widgets;

    public List<WidgetDto> getWidgets() {
        return widgets;
    }

    public void setWidgets(List<WidgetDto> widgets) {
        this.widgets = widgets;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WidgetSetDto that = (WidgetSetDto) o;
        return Objects.equals(widgets, that.widgets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(widgets);
    }

    @Override
    public String toString() {
        return "WidgetSetDto{" +
                "widgets=" + widgets +
                '}';
    }
}
