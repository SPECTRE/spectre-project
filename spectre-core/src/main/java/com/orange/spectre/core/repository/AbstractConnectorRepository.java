package com.orange.spectre.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.orange.spectre.core.domain.AbstractConnector;

@Repository
public interface AbstractConnectorRepository extends JpaRepository<AbstractConnector, Long> {
}
