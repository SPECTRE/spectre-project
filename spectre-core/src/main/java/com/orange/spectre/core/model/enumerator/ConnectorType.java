
package com.orange.spectre.core.model.enumerator;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.plugins.adv.domain.MingleAdvConnector;
import com.orange.spectre.plugins.jenkins.faas.domain.JenkinsFaasConnector;
import com.orange.spectre.plugins.jenkins.standard.domain.JenkinsConnector;
import com.orange.spectre.plugins.jira.custom.domain.CustomJiraConnector;
import com.orange.spectre.plugins.jira.domain.JiraConnector;
import com.orange.spectre.plugins.qc.custom.domain.CustomQcConnector;
import com.orange.spectre.plugins.qc.domain.QcConnector;
import com.orange.spectre.plugins.sacre.domain.SacreConnector;
import com.orange.spectre.plugins.sonar.faas.domain.SonarFaasConnector;
import com.orange.spectre.plugins.sonar.standard.domain.SonarConnector;
import com.orange.spectre.plugins.spectre.domain.SpectreConnector;
import com.orange.spectre.plugins.teamcolony.domain.TeamColonyConnector;
import com.orange.spectre.plugins.trello.domain.TrelloConnector;
import com.orange.spectre.plugins.tuleap.bug.domain.TuleapBugsConnector;
import com.orange.spectre.plugins.tuleap.scrum.domain.TuleapScrumConnector;
import com.orange.spectre.plugins.weather.domain.WeatherConnector;
import com.orange.spectre.plugins.devopsic.domain.DevopsicConnector;

/**
 * Enum of all available connector types
 */
public enum ConnectorType {

    sonar(SonarConnector.class),
    sonar_faas(SonarFaasConnector.class),
    qc(QcConnector.class),
    qc_custom(CustomQcConnector.class),
    teamcolony(TeamColonyConnector.class),
    jenkins(JenkinsConnector.class),
    jenkins_faas(JenkinsFaasConnector.class),
    weather(WeatherConnector.class),
    spectre(SpectreConnector.class),
    forge_bugs(TuleapBugsConnector.class),
    forge_scrum(TuleapScrumConnector.class),
    jira(JiraConnector.class),
    jira_custom(CustomJiraConnector.class),
    sacre(SacreConnector.class),
    trello(TrelloConnector.class),
    mingle_adv(MingleAdvConnector.class),
    /* cbs(CbsConnector.class), */
    devopsic(DevopsicConnector.class);


    private Class<? extends AbstractConnector> clazz;

    ConnectorType(Class<? extends AbstractConnector> clazz) {
        this.clazz = clazz;
    }

    /**
     * Connector instantiation depending on it's type
     *
     * @return A connector instance
     */
    public AbstractConnector createConnector() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {

            throw new RuntimeException(clazz + " has no default constructor");
        }
    }
}
