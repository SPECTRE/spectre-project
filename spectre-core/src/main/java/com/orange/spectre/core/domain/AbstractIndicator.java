package com.orange.spectre.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.model.data.AbstractData;

import javax.persistence.*;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "INDICATOR")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class AbstractIndicator extends AbstractEntity {

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "CONNECTOR_ID", referencedColumnName = "ID")
    private AbstractConnector connector;

    protected IndicatorMeasure measure;

    protected String measureUnity;

    public AbstractConnector getConnector() {
        return connector;
    }

    public void setConnector(AbstractConnector connector) {
        this.connector = connector;
    }

    /**
     * Fetch data.
     *
     * @param connectorParams Contains all the params such as URL, token, ... to connect to third providers.
     * @return The fetched data.
     * @throws FetchDataException indicators must throw their exceptions in this custom core consumed exception.
     */
    public abstract AbstractData getData(Map<String, String> connectorParams) throws FetchDataException;
    public abstract IndicatorFamily getFamily();
    public abstract Enum getType();
    public abstract String getLabel();
    public abstract IndicatorMeasure getMeasure() ;
    public abstract String getMeasureUnity();
    public abstract String getDomain();
    public abstract String getPurpose();
    public abstract String getDefinition();
    public abstract String getOrigin();


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                AbstractIndicator that = (AbstractIndicator) o;
                return Objects.equals(connector, that.connector) &&
                        measure == that.measure &&
                        Objects.equals(measureUnity, that.measureUnity);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), connector, measure, measureUnity);
    }
}
