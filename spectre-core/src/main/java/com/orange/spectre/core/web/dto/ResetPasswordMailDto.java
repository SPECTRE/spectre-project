package com.orange.spectre.core.web.dto;

public class ResetPasswordMailDto {
    private String mail;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}
