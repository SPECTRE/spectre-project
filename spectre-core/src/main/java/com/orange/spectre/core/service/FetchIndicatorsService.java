package com.orange.spectre.core.service;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.enumerator.InjectedData;
import com.orange.spectre.core.model.enumerator.InjectedKpi;
import com.orange.spectre.core.repository.AbstractConnectorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class FetchIndicatorsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FetchIndicatorsService.class);

    @Inject
    AbstractConnectorRepository connectorRepository;

    @Inject
    KpiInjectionService kpiInjectionService;

    /**
     * This method is executed at 01:01am each day
     * It will fetch data for every connectors available in Spectre database
     */
    @Scheduled(cron = "0 1 1 * * ?")
    public void fetchAll() {
        List<AbstractConnector> connectors = connectorRepository.findAll();
        connectors.forEach(this::fetchByConnector);
    }

    public void fetchByConnector(AbstractConnector connector) {
        for (AbstractIndicator indicator : connector.getIndicators()) {
            this.fetchByIndicator(connector.getParams(), connector.getType(), indicator);
        }
    }

    public void fetchByIndicator(Map<String, String> params, String connectorType, AbstractIndicator indicator) {
        InjectedKpi injectedKpi = new InjectedKpi();
        InjectedData injectedData = new InjectedData();
        try {
            injectedData.setValue(indicator.getData(params));
        } catch (FetchDataException e) {
            LOGGER.debug(e.getMessage() + " - caused by : " + e.getCause(), e.getStackTrace());
        }
        injectedData.setExportDate(Timestamp.valueOf(LocalDateTime.now()));
        injectedData.setIndicatorId(indicator.getId());
        injectedKpi.setIndicatorMeasure(indicator.getMeasure().name());
        injectedKpi.setIndicatorUnity(indicator.getMeasureUnity());
        injectedKpi.setInjectedData(injectedData);
        injectedKpi.setConnectorType(connectorType);
        injectedKpi.setIndicatorType(indicator.getType().name());
        kpiInjectionService.injectKpi(injectedKpi);
    }
}
