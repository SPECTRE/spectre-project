package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for unknown indicator
 *
 * Return http 404 when exception raised
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class IndicatorNotFoundException extends RuntimeException {

    public IndicatorNotFoundException(Long indicatorId) {
        super("Could not find indicator '" + Long.toString(indicatorId) + "'.");
    }
}
