package com.orange.spectre.core.config.security.exception;

import java.io.Serializable;

public abstract class AbstractApiException extends RuntimeException
		implements Serializable, ApiException {

	private static final long serialVersionUID = 6383516104802857951L;

	private ErrorBean code;

	protected AbstractApiException(ErrorBean code) {
		super();
		this.code = code;
	}

	protected AbstractApiException(ErrorBean code, String message) {
		super(message);
		this.code = code;
	}

	@Override
	public ErrorBean getCode() {
		return this.code;
	}

}
