package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Custom data rest DTO
 * </p>
 */
public class CustomDataDTO {

    private String data;


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

}
