package com.orange.spectre.core.web.dto;

public class ConnectorRequiredParamsDto {
	
	private String name;
	private boolean mandatory;
	private boolean secured;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public boolean isMandatory() {
		return mandatory;
	}
	
	public void setMandatory(boolean mandatory) {
		this.mandatory = mandatory;
	}
	
	public boolean isSecured() {
		return secured;
	}
	
	public void setSecured(boolean secured) {
		this.secured = secured;
	}

}
