package com.orange.spectre.core.web.dto;


import com.orange.spectre.core.model.enumerator.IndicatorFamily;

/**
 * <p>
 *  Indicator family rest DTO
 * </p>
 */
public class IndicatorFamilyDto {

    private IndicatorFamily family;
    private String label;

    public IndicatorFamilyDto(IndicatorFamily pFamily){
        family = pFamily;
        label = family.toString();
    }

    public IndicatorFamily getFamily() {
        return family;
    }

    public void setFamily(IndicatorFamily family) {
        this.family = family;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
