package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Widget rest DTO
 * </p>
 */
public abstract class AbstractUpdateWidgetDto {

    private Long id;
    private String label;
    private String measure;
    private String unity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

}
