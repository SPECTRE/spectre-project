package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.IndicatorNotFoundException;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.model.enumerator.InjectedData;
import com.orange.spectre.core.model.enumerator.InjectedKpi;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.service.KpiInjectionService;
import com.orange.spectre.core.web.dto.CustomDataDTO;
import com.orange.spectre.core.web.dto.IndicatorAvailablePropertiesDto;
import com.orange.spectre.core.web.dto.IndicatorMeasureDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * Indicator API
 */
@RestController
@RequestMapping("/indicators")
public class IndicatorResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndicatorResource.class);

    private static final String INDICATOR_UNKNOWN_ERROR = "indicator unknown";
    @Inject
    AbstractIndicatorRepository indicatorRepository;

    @Inject
    KpiInjectionService kpiInjectionService;

    /**
     * Inject custom data into elastic search without fetching data.
     *
     * @param indicatorId The indicator id
     * @return HttpStatus OK if the indicator is found, FORBIDDEN if its type is not CUSTOM, else NOT_FOUND.
     */
    @RequestMapping(value = "/{indicatorId}/inject", method = RequestMethod.POST)
    ResponseEntity<InjectedKpi> injectCustomData(@PathVariable Long indicatorId, @RequestBody CustomDataDTO customDataDTO) {

        LOGGER.debug("inject data for custom indicator {}", indicatorId);
        if (!indicatorRepository.exists(indicatorId)) {
            LOGGER.error(INDICATOR_UNKNOWN_ERROR);
            throw new IndicatorNotFoundException(indicatorId);
        }
        AbstractIndicator indicator = indicatorRepository.findOne(indicatorId);
        if (!"CUSTOM".equals(indicator.getType().name())) {
            LOGGER.error("not a custom indicator");
            return new ResponseEntity<>(null, null, HttpStatus.FORBIDDEN);
        }

        InjectedKpi injectedKpi = new InjectedKpi();
        InjectedData injectedData = new InjectedData();
        injectedData.setValue(new StringData(customDataDTO.getData()));
        injectedData.setIndicatorId(indicatorId);
        injectedData.setExportDate(Timestamp.valueOf(LocalDateTime.now()));
        injectedKpi.setInjectedData(injectedData);
        injectedKpi.setConnectorType("custom");
        injectedKpi.setIndicatorType(indicator.getType().name());
        injectedKpi.setIndicatorMeasure(indicator.getMeasure().name());
        kpiInjectionService.injectKpi(injectedKpi);

        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }

    /**
     * Get indicator properties
     * @return HttpStatus OK with indicator properties
     */
    @RequestMapping(value = "/properties", method = RequestMethod.GET)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    IndicatorAvailablePropertiesDto getIndicatorsProperties(){
        LOGGER.debug("get indicator properties");
        IndicatorAvailablePropertiesDto properties = new IndicatorAvailablePropertiesDto();

        IndicatorMeasure[] measures = IndicatorMeasure.values();
        for(IndicatorMeasure measure : measures){
            properties.getMeasures().add(new IndicatorMeasureDto(measure));
        }

        return properties;
    }
}
