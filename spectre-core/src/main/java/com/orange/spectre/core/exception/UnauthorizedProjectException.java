package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for unknown project
 *
 * Return http 403 when exception raised
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class UnauthorizedProjectException extends RuntimeException {

    public UnauthorizedProjectException(String login, Long projectId) {
        super("Project " + Long.toString(projectId) + " not authorized  for user " +login + "'.");
    }
}
