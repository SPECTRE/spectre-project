package com.orange.spectre.core.web.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  Indicator properties rest DTO
 * </p>
 */
public class IndicatorAvailablePropertiesDto {

    private List<IndicatorMeasureDto> measures;

    public List<IndicatorMeasureDto> getMeasures() {
        if(measures == null){
            measures = new ArrayList<>();
        }
        return measures;
    }

    public void setMeasures(List<IndicatorMeasureDto> measures) {
        this.measures = measures;
    }
}
