package com.orange.spectre.core.config;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Created by pkvt9666 on 15/03/2016.
 * <p>
 *     Swagger configuration class
 * </p>
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Swagger instantiation bean
     * @return Swagger instance
     */
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("spectre-core")
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(Predicates.not(PathSelectors.regex("/error")))
                .build();
    }

    /**
     * Get API signature informations
     * @return Api informations
     */
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spectre Core API Documentation")
                .description("Spectre Core a REST API to compose your own dashboard configuration with various indicators provided. ")
                .termsOfServiceUrl("http://www-03.ibm.com/software/sla/sladb.nsf/sla/bm?Open")
                .license("Apache License Version 2.0")
                .licenseUrl("http://www.apache.org/licenses/LICENSE-2.0")
                .version("2.0")
                .contact(new Contact("Spectre Core Team", "", "spectre.coreteam@orange.com"))
                .build();
    }
}
