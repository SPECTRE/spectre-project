package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.CustomIndicator;
import com.orange.spectre.core.domain.Widget;
import com.orange.spectre.core.exception.WidgetNotFoundException;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.model.enumerator.InjectedData;
import com.orange.spectre.core.model.enumerator.InjectedKpi;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.repository.WidgetRepository;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.KpiInjectionService;
import com.orange.spectre.core.web.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Widget API
 */
@RestController
@RequestMapping("/widgets")
public class WidgetResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(WidgetResource.class);

    @Inject
    DeletionService deletionService;

    @Inject
    WidgetRepository widgetRepository;

    @Inject
    AbstractIndicatorRepository indicatorRepository;

    @Inject
    KpiInjectionService kpiInjectionService;

    /**
     * Update a widget by its id.
     *
     * @param widgetId The widget id
     * @param widgetDto The widget DTO
     * @return HttpStatus OK if the widget is found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{widgetId}", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<WidgetDto> updateWidget(@PathVariable Long widgetId, @RequestBody UpdateWidgetDto widgetDto) {
        LOGGER.debug("update widget {}", widgetId);
        if (!widgetRepository.exists(widgetId)) {
            LOGGER.error("widget unknown");
            throw new WidgetNotFoundException(widgetId);
        }
        Widget widget = widgetRepository.findOne(widgetId);
        widget.setLabel(widgetDto.getLabel());
        AbstractIndicator indicator = indicatorRepository.findOne(widgetDto.getIndicatorId());
        widget.setIndicator(indicator);
        widgetRepository.save(widget);


        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }


    /**
     * Update a custom widget by its id.
     *
     * @param widgetId The widget id
     * @param widgetDto The widget DTO
     * @return HttpStatus OK if the widget is found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{widgetId}/custom", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<WidgetDto> updateCustomWidget(@PathVariable Long widgetId, @RequestBody UpdateCustomWidgetDto widgetDto) {
        LOGGER.debug("update custom widget {}", widgetId);
        if (!widgetRepository.exists(widgetId)) {
            LOGGER.error("widget unknown");
            throw new WidgetNotFoundException(widgetId);
        }
        Widget widget = widgetRepository.findOne(widgetId);
        widget.setLabel(widgetDto.getLabel());

        CustomIndicator customIndicator = null;
        if(widget.getIndicator() !=null && widget.getIndicator().getConnector() == null && widget.getIndicator().getMeasure().name().equals(widgetDto.getMeasure())){
            customIndicator = (CustomIndicator) widget.getIndicator();
        }else{
            customIndicator = indicatorRepository.save(new CustomIndicator(IndicatorMeasure.valueOf(widgetDto.getMeasure()), widgetDto.getUnity()));
            widget.setIndicator(customIndicator);
        }


        widgetRepository.save(widget);
        InjectedKpi injectedKpi = new InjectedKpi();
        InjectedData injectedData = new InjectedData();

        injectedData.setValue(new StringData(widgetDto.getData()));
        injectedData.setIndicatorId(customIndicator.getId());
        injectedData.setExportDate(Timestamp.valueOf(LocalDateTime.now()));
        injectedKpi.setInjectedData(injectedData);
        injectedKpi.setConnectorType("custom");
        injectedKpi.setIndicatorType(customIndicator.getType().name());
        injectedKpi.setIndicatorMeasure(customIndicator.getMeasure().name());
        kpiInjectionService.injectKpi(injectedKpi);


        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }

    /**
     * Update a custom widget by its id.
     *
     * @param widgetId The widget id
     * @param widgetDto The widget DTO
     * @return HttpStatus OK if the widget is found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{widgetId}/custom-tuple", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<WidgetDto> updateCustomTupleWidget(@PathVariable Long widgetId, @RequestBody UpdateCustomTupleWidgetDto widgetDto) {
        LOGGER.debug("update custom widget {}", widgetId);
        if (!widgetRepository.exists(widgetId)) {
            LOGGER.error("widget unknown");
            throw new WidgetNotFoundException(widgetId);
        }
        Widget widget = widgetRepository.findOne(widgetId);
        widget.setLabel(widgetDto.getLabel());

        if (widget.getIndicator() == null) {
            CustomIndicator customIndicator = indicatorRepository.save(new CustomIndicator(IndicatorMeasure.valueOf(widgetDto.getMeasure()), widgetDto.getUnity()));
            widget.setIndicator(customIndicator);
        }

        widgetRepository.save(widget);

        InjectedKpi injectedKpi = new InjectedKpi();
        InjectedData injectedData = new InjectedData();

        List<String> datas = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        for(WidgetDataDto data : widgetDto.getData()){
            datas.add(data.getValue());
            labels.add(data.getLabel());
        }

        TupleData tuple = new TupleData();
        tuple.setData(datas);
        tuple.setLabel(labels);
        tuple.setMeasure(widgetDto.getDataMeasure());

        injectedData.setValue(tuple);
        injectedData.setIndicatorId(widget.getIndicator().getId());
        injectedData.setExportDate(Timestamp.valueOf(LocalDateTime.now()));
        injectedKpi.setInjectedData(injectedData);
        injectedKpi.setConnectorType("custom");
        injectedKpi.setIndicatorType(widget.getIndicator().getType().name());
        injectedKpi.setIndicatorMeasure(widget.getIndicator().getMeasure().name());
        kpiInjectionService.injectKpi(injectedKpi);


        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }

    /**
     * Delete the given widget and its association with the dashboard.
     *
     * @param widgetId The widget id
     * @return HttpStatus OK if both dashboard and widget are found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{widgetId}", method = RequestMethod.DELETE)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<Widget> deleteWidget(@PathVariable Long widgetId) {
        LOGGER.debug("delete widget {}", widgetId);

        if (!widgetRepository.exists(widgetId)) {
            LOGGER.error("widget unknown");
            throw new WidgetNotFoundException(widgetId);
        }
        Widget widget = widgetRepository.findOne(widgetId);
        AbstractIndicator indicator = widget.getIndicator();


        widget.setIndicator(null);
        widget.setLabel(null);
        widgetRepository.save(widget);

        // Delete custom indicator
        if(indicator != null && indicator.getConnector() == null){
            deletionService.deleteIndicator(indicator.getId());
        }

        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }


}
