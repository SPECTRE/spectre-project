package com.orange.spectre.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Jpa account entity
 */
@Entity
@Table(name = "ACCOUNT")
public class Account extends AbstractEntity {

    private static final long serialVersionUID = -5313782712900809659L;

    @ApiModelProperty(required = true)
    private String login;

    @JsonProperty(access = Access.WRITE_ONLY)
    private String password;

    private String email;

    private String firstname;

    private String lastname;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "favouritesProjectsAccounts")
    private Set<Project> favouritesProjects;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "projectMember")
    private Set<ProjectMember> membersProjects;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "favouritesDashboardsAccounts")
    @OrderBy("id ASC")
    private Set<Dashboard> favouritesDashboard;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "dashboardMember")
    @OrderBy("dashboardId ASC")
    private Set<DashboardMember> membersDashboards;

    @Size(max = 20)
    @Column(name = "resetKey", length = 20)
    private String resetKey;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Dashboard> getFavouritesDashboard() {
        if (favouritesDashboard==null) {
            favouritesDashboard = new HashSet<>();
        }
        return favouritesDashboard;
    }

    public void setFavouritesDashboard(Set<Dashboard> favouritesDashboard) {
        this.favouritesDashboard = favouritesDashboard;
    }

    public Set<DashboardMember> getMembersDashboards() {
        if (membersDashboards==null) {
            membersDashboards = new HashSet<>();
        }
        return membersDashboards;
    }

    public void setMembersDashboards(Set<DashboardMember> membersDashboards) {
        this.membersDashboards = membersDashboards;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Set<Project> getFavouritesProjects() {
        if (favouritesProjects==null) {
            favouritesProjects = new HashSet<>();
        }
        return favouritesProjects;
    }

    public void setFavouritesProjects(Set<Project> favouritesProjects) {
        this.favouritesProjects = favouritesProjects;
    }

    public Set<ProjectMember> getMembersProjects() {
        if (membersProjects==null) {
            membersProjects = new HashSet<>();
        }
        return membersProjects;
    }

    public void setMembersProjects(Set<ProjectMember> membersProjects) {
        this.membersProjects = membersProjects;
    }

    public String getResetKey() {
        return resetKey;
    }

    public void setResetKey(String resetKey) {
        this.resetKey = resetKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Account account = (Account) o;
        return Objects.equals(login, account.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), login, password, email);
    }

    @Override
    public String toString() {
        return "Account{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", favouritesProjects=" + favouritesProjects +
                ", membersProjects=" + membersProjects +
                '}';
    }
}
