package com.orange.spectre.core.config.security.exception;

public class FunctionalException extends AbstractApiException {

	private static final long serialVersionUID = -1286318486967465054L;

	public FunctionalException(Functional error) {
		super(error.getValue());
	}

	public FunctionalException(Functional error, String message) {
		super(error.getValue(), message);
	}

	@Override
	public int getCategory() {
		return 422;
	}

}
