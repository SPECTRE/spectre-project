package com.orange.spectre.core.web.dto;

import java.io.Serializable;

public class TokenDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String accessToken;

	public TokenDTO() {}

	public TokenDTO(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@Override
	public String toString() {
		return "TokenDTO [accessToken=" + accessToken + "]";
	}
}
