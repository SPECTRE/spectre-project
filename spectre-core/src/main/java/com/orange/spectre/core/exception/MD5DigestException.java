package com.orange.spectre.core.exception;

/**
 * Created by kctl7743 on 17/07/2017.
 *
 * Exception for an unknown MD5 algorithm
 */
public class MD5DigestException extends RuntimeException  {

    public MD5DigestException() {
        super("Could not find MD5 Algorithm");
    }

}
