package com.orange.spectre.core.web.dto;

import java.util.Set;

/**
 * Created by ludovic on 19/08/2016.
 */
public class UpdateProjectDto {

    private String name;
    private String description;
    private boolean reachable;

    private Set<MemberDto> members;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public Set<MemberDto> getMembers() {
        return members;
    }

    public void setMembers(Set<MemberDto> members) {
        this.members = members;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
