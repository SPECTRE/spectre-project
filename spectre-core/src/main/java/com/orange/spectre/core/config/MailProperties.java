package com.orange.spectre.core.config;

/**
 * Created by ludovic on 11/05/2016.
 */
public class MailProperties {

    private String from;

    private boolean active;

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
