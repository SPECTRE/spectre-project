package com.orange.spectre.core.model.enumerator;

import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * Elasticsearch kpi bean
 */
public class InjectedKpi implements Serializable {

    private static final long serialVersionUID = 3104843157740982472L;

    private String connectorType;
    private String indicatorType;
    private InjectedData injectedData;
    private String indicatorMeasure;
    private String indicatorUnity;

    public String getIndicatorUnity() {
        if (StringUtils.isNotBlank(indicatorUnity)) {
            return StringUtils.remove(indicatorUnity.toLowerCase(), "_");
        }
        return indicatorUnity;
    }

    public String getIndicatorMeasure() {
        if (StringUtils.isNotBlank(indicatorMeasure)) {
            return StringUtils.remove(indicatorMeasure.toLowerCase(), "_");
        }
        return indicatorMeasure;
    }

    public void setIndicatorMeasure(String indicatorMeasure) {
        this.indicatorMeasure = indicatorMeasure;
    }

    public void setIndicatorUnity(String indicatorUnity) {
        this.indicatorUnity = indicatorUnity;
    }

    public String getConnectorType() {
        if (StringUtils.isNotBlank(connectorType)) {
            return StringUtils.remove(connectorType.toLowerCase(), "_");
        }
        return connectorType;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getIndicatorType() {
        if (StringUtils.isNotBlank(indicatorType)) {
            return StringUtils.remove(indicatorType.toLowerCase(), "_");
        }
        return indicatorType;
    }

    public void setIndicatorType(String indicatorType) {
        this.indicatorType = indicatorType;
    }

    public InjectedData getInjectedData() {
        return injectedData;
    }

    public void setInjectedData(InjectedData injectedData) {
        this.injectedData = injectedData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            }else{
                InjectedKpi that = (InjectedKpi) o;
                return Objects.equals(connectorType, that.connectorType) &&
                        Objects.equals(indicatorType, that.indicatorType) &&
                        Objects.equals(injectedData, that.injectedData) &&
                        Objects.equals(indicatorUnity, that.indicatorUnity);
            }
        }


    }

    @Override
    public int hashCode() {
        return Objects.hash(connectorType, indicatorType, injectedData, indicatorUnity);
    }

    @Override
    public String toString() {
        return "InjectedKpi{" +
                "connectorType='" + connectorType + '\'' +
                ", indicatorType='" + indicatorType + '\'' +
                ", injectedData=" + injectedData +
                ", indicatorUnity='" + indicatorUnity + '\'' +
                '}';
    }
}
