package com.orange.spectre.core.web.dto;

import java.util.Map;

/**
 * <p>
 *  Admin connector rest DTO
 * </p>
 */
public class EditConnectorDto extends ConnectorDto {

    private String owner;
    private Map<String, String> configurationParams;

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Map<String, String> getConfigurationParams() {
        return configurationParams;
    }

    public void setConfigurationParams(Map<String, String> configurationParams) {
        this.configurationParams = configurationParams;
    }

    @Override
    public String toString() {
        return "EditConnectorDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", owner='" + owner + '\'' +
                ", type='" + type + '\'' +
                ", configurationParams=" + configurationParams +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            } else {
                EditConnectorDto that = (EditConnectorDto) o;

                return !id.equals(that.id) || !name.equals(that.name) || !description.equals(that.description)|| !owner.equals(that.owner) || !type.equals(that.type);
            }
        }


    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
