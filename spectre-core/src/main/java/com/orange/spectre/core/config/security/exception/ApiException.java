package com.orange.spectre.core.config.security.exception;

public interface ApiException {

	int getCategory();

	ErrorBean getCode();

	String getMessage();

}
