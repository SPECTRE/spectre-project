package com.orange.spectre.core.model.enumerator;

/**
 * Created by WKNL8600 on 10/05/2016.
 */
public enum MoodEnum {
    HAPPY,
    NORMAL,
    SAD;

}
