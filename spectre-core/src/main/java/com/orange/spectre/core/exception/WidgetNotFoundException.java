package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for unknown widget
 *
 * Return http 404 when exception raised
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class WidgetNotFoundException extends RuntimeException {

    public WidgetNotFoundException(Long widgetId) {
        super("Could not find widget '" + Long.toString(widgetId) + "'.");
    }
}
