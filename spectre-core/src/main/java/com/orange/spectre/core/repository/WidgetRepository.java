package com.orange.spectre.core.repository;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.Widget;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

/**
 * Created by pkvt9666 on 08/04/2016.
 *
 * JPA Repository for database Widget entity operations
 */
public interface WidgetRepository extends JpaRepository<Widget, Long> {

    /**
     * Get all widgets linked to a indicator
     * @param indicator An indicator
     * @return Indicator's widgets
     */
    Set<Widget> findAllByIndicator(AbstractIndicator indicator);

    List<Widget> findAllByDashboard(Dashboard dashboard);
}
