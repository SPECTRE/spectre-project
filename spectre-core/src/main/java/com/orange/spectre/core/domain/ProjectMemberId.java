package com.orange.spectre.core.domain;

import java.io.Serializable;

/**
 * Jpa project member association id entity
 */
public class ProjectMemberId implements Serializable {

    private static final long serialVersionUID = 4264772177174134767L;

    private Long accountId;
    private Long projectId;

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public int hashCode() {
        return (int) (accountId + projectId);
    }

    public boolean equals(Object object) {
        if (object instanceof ProjectMemberId) {
            ProjectMemberId otherId = (ProjectMemberId) object;
            return (otherId.accountId == this.accountId) && (otherId.projectId == this.projectId);
        }
        return false;
    }
}
