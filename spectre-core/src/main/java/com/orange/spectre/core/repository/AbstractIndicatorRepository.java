package com.orange.spectre.core.repository;

import com.orange.spectre.core.domain.AbstractIndicator;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AbstractIndicatorRepository extends JpaRepository<AbstractIndicator, Long> {}