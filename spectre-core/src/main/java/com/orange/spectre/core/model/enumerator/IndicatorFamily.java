package com.orange.spectre.core.model.enumerator;


public enum IndicatorFamily {
    PERFORMANCE,
    QUALITY,
    MANAGEMENT,
    DEVOPS,
    AUTRE;

    /**
     * Returns a more human reading String.
     *
     * @param family The enum
     * @return The enum value
     */
    public String getValue(IndicatorFamily family) {
        switch (family) {
            case PERFORMANCE: return "Performance";
            case QUALITY: return "Qualité";
            case MANAGEMENT: return "Management";
            case DEVOPS: return "Devops";
            case AUTRE: return "Autre";
            default: return null;
        }
    }
}
