package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.config.DozerMapper;
import com.orange.spectre.core.config.SecurityUtils;
import com.orange.spectre.core.domain.*;
import com.orange.spectre.core.exception.ConnectorNotFoundException;
import com.orange.spectre.core.exception.ProjectNotFoundException;
import com.orange.spectre.core.exception.UnauthorizedAdminException;
import com.orange.spectre.core.exception.UserNotFoundException;
import com.orange.spectre.core.model.enumerator.ConnectorType;
import com.orange.spectre.core.repository.*;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.service.ProjectService;
import com.orange.spectre.core.web.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;

import static com.orange.spectre.core.config.Constants.LOGIN_UNKNOWN_ERROR;
import static com.orange.spectre.core.config.Constants.NOT_AN_ADMIN;

/**
 * Project API
 */
@RestController
@RequestMapping("/projects")
public class ProjectResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProjectResource.class);

	private static final String PROJECT_UNKNOWN_ERROR = "project unknown";

	private static final String CONNECTOR_UNKNOWN_ERROR = "connector unknown";
	@Inject
	AbstractConnectorRepository connectorRepository;
	@Inject
	AbstractIndicatorRepository indicatorRepository;
	@Inject
	DeletionService deletionService;
	@Inject
	FetchIndicatorsService fetchIndicatorsService;
	@Inject
	AccountRepository accountRepository;
	@Inject
	ProjectService projectService;
	@Inject
	ProjectMemberRepository projectMemberRepository;
	@Inject
	private ProjectRepository projectRepository;

	/**
	 * Get public projects.
	 *
	 * @return A set of projects which boolean Reachable is true.
	 */
	@RequestMapping(method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Set<Project> getPublicProjects() {
		LOGGER.debug("get all public projects");
		return projectRepository.findAllByReachableTrue();

	}

	/**
	 * Gets all connectors by project.
	 *
	 * @param projectId the project id
	 * @return A list of ConnectorDto if the project is found, else HttpStatus NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}/connectors", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	List<ConnectorDto> getAllConnectorsByProject(@PathVariable Long projectId) {
		LOGGER.debug("get all connectors for project {}", projectId);

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		Project proj = projectRepository.findOne(projectId);
		List<ConnectorDto> connectorDtos = new ArrayList<>();
		for (AbstractConnector abstractConnector : proj.getConnectors()) {
			ConnectorDto dto = DozerMapper.mapper.map(abstractConnector, ConnectorDto.class);
			connectorDtos.add(dto);
		}
		return connectorDtos;
	}

	/**
	 * Gets all M by project.
	 *
	 * @param projectId the project id
	 * @return A list of ConnectorDto if the project is found, else HttpStatus NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}/members", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	List<MembersDto> getAllMembersByProject(@PathVariable Long projectId) {
		LOGGER.debug("get all members for project {}", projectId);

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		Set<ProjectMember> proj = projectMemberRepository.findByProjectId(projectId);
		List<MembersDto> membersDtos = new ArrayList<>();
		for (ProjectMember projectMembers : proj) {
			MembersDto dto = DozerMapper.mapper.map(projectMembers, MembersDto.class);
			membersDtos.add(dto);
		}
		return membersDtos;
	}

	/**
	 * Create connector associated with a project.
	 *
	 * @param projectId the project id
	 * @param input     the connector DTO
	 * @return HttpStatus CREATED with the created connector, else NOT_FOUND if the dashboard is not found.
	 */
	@RequestMapping(value = "/{projectId}/connectors", method = RequestMethod.POST)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<AbstractConnector> createConnector(@PathVariable Long projectId, @RequestBody EditConnectorDto input) {

		LOGGER.debug("create a connector for project {}", projectId);

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		Project proj = projectRepository.findOne(projectId);
		AbstractConnector connector = ConnectorType.valueOf(input.getType()).createConnector();
		connector.setName(input.getName());
		connector.setDescription(input.getDescription());
		connector.setParams(input.getConfigurationParams());
		connector.setLastRun(new Date());
		connector.setPeriodicity(Math.max(600, input.getPeriodicity()));
		AbstractConnector savedConnector = connectorRepository.save(connector);

		// indicators creation
		List<String> indicatorList = savedConnector.getAvailableIndicators();
		Set<AbstractIndicator> indicators = new HashSet<>();
		for (String indicator : indicatorList) {
			AbstractIndicator indic = savedConnector.createIndicator(indicator);
			if (indic != null) {
				indic.setConnector(savedConnector);
				AbstractIndicator savedIndicator = indicatorRepository.save(indic);
				indicators.add(savedIndicator);
			}
		}
		savedConnector.setIndicators(indicators);
		savedConnector = connectorRepository.save(savedConnector);

		Set<AbstractConnector> connectors = proj.getConnectors();
		connectors.add(savedConnector);
		proj.setConnectors(connectors);
		projectRepository.save(proj);

		fetchIndicatorsService.fetchByConnector(savedConnector);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(connector.getId()).toUri());

		return new ResponseEntity<>(savedConnector, httpHeaders, HttpStatus.CREATED);
	}

	/**
	 * Clone/Duplicate connector associated with a project.
	 *
	 * @param projectId   the project id
	 * @param connectorId the connector id to clone
	 * @return HttpStatus CREATED with the created cloned connector, else NOT_FOUND if the project or clonable connector is not found.
	 */
	@RequestMapping(value = "/{projectId}/connectors/{connectorId}", method = RequestMethod.POST)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<AbstractConnector> duplicateConnector(@PathVariable Long projectId, @PathVariable Long connectorId) {

		LOGGER.debug("duplicate the {} connector for project {}", connectorId, projectId);

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}
		if (!connectorRepository.exists(connectorId)) {
			LOGGER.error(CONNECTOR_UNKNOWN_ERROR);
			throw new ConnectorNotFoundException(connectorId);
		}

		Project proj = projectRepository.findOne(projectId);
		AbstractConnector clonableConnector = connectorRepository.findOne(connectorId);

		AbstractConnector clonedConnector = ConnectorType.valueOf(clonableConnector.getType()).createConnector();
		clonedConnector.setName(String.format("%s_copie", clonableConnector.getName()));
		clonedConnector.setDescription(String.format("%s_copie", clonableConnector.getDescription()));
		clonedConnector.setParams(clonableConnector.getParams());
		clonedConnector.setLastRun(new Date());
		clonedConnector.setPeriodicity(Math.max(600, clonableConnector.getPeriodicity()));
		AbstractConnector savedClonedConnector = connectorRepository.save(clonedConnector);

		// indicators creation
		List<String> indicatorList = savedClonedConnector.getAvailableIndicators();
		Set<AbstractIndicator> indicators = new HashSet<>();
		for (String indicator : indicatorList) {
			AbstractIndicator indic = savedClonedConnector.createIndicator(indicator);
			if (indic != null) {
				indic.setConnector(savedClonedConnector);
				AbstractIndicator savedIndicator = indicatorRepository.save(indic);
				indicators.add(savedIndicator);
			}
		}
		savedClonedConnector.setIndicators(indicators);
		savedClonedConnector = connectorRepository.save(savedClonedConnector);

		Set<AbstractConnector> connectors = proj.getConnectors();
		connectors.add(savedClonedConnector);
		proj.setConnectors(connectors);
		projectRepository.save(proj);

		fetchIndicatorsService.fetchByConnector(savedClonedConnector);

		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setLocation(ServletUriComponentsBuilder
				.fromCurrentRequest().path("/{id}")
				.buildAndExpand(clonedConnector.getId()).toUri());

		return new ResponseEntity<>(savedClonedConnector, httpHeaders, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{projectId}/connector/test", method = RequestMethod.PUT)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<Boolean> testConnector(@RequestBody EditConnectorDto input) {
		AbstractConnector connector = ConnectorType.valueOf(input.getType()).createConnector();
		connector.setName(input.getName());
		connector.setDescription(input.getDescription());
		connector.setParams(input.getConfigurationParams());
		connector.setLastRun(new Date());
		boolean isConnectorValid = connector.testConnector(connector.getParams());
		if (isConnectorValid) {
			return new ResponseEntity<>(new Boolean(true), null, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new Boolean(false), null, HttpStatus.BAD_REQUEST);
		}

	}

	/**
	 * Delete connector association with the given project.
	 *
	 * @param projectId   the project id
	 * @param connectorId the connector id
	 * @return HttpStatus OK if the project and the connector are found, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}/connectors/{connectorId}", method = RequestMethod.DELETE)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<AbstractConnector> deleteConnector(@PathVariable Long projectId, @PathVariable Long connectorId) {

		LOGGER.debug("delete connector {} for project {}", connectorId, projectId);

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}
		if (!connectorRepository.exists(connectorId)) {
			LOGGER.error(CONNECTOR_UNKNOWN_ERROR);
			throw new ConnectorNotFoundException(connectorId);
		}

		AbstractConnector connector = connectorRepository.findOne(connectorId);
		Project proj = projectRepository.findOne(projectId);

		proj.getConnectors().remove(connector);
		projectRepository.save(proj);

		deletionService.deleteConnector(connectorId);

		return new ResponseEntity<>(null, null, HttpStatus.OK);
	}

	/**
	 * Update the given project.
	 *
	 * @param projectId the project id
	 * @param project   the project
	 * @return HttpStatus OK with the updated project if it's found, else NOT_FOUND
	 */
	@RequestMapping(value = "/{projectId}", method = RequestMethod.PUT)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> updateProject(@PathVariable Long projectId, @RequestBody UpdateProjectDto project) {
		LOGGER.debug("update project {}", projectId);

		if (!projectService.checkAdmin(projectId, SecurityUtils.getCurrentUserLogin())) {
			LOGGER.error(NOT_AN_ADMIN);
			throw new UnauthorizedAdminException();
		}

		if (!projectRepository.exists(projectId)) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		Project savedProject = projectRepository.findOne(projectId);
		savedProject.setReachable(project.isReachable());
		savedProject.setName(project.getName());
		//        savedProject.setDescription(project.getDescription());
		projectRepository.save(savedProject);

		Set<ProjectMember> projMembers = projectMemberRepository.findByProjectId(savedProject.getId());

		for (ProjectMember member : projMembers) {
			boolean found = false;
			for (MemberDto memberDto : project.getMembers()) {
				if (member.getMember().getLogin().equals(memberDto.getLogin())) {
					found = true;
				}
			}

			if (!found) {
				projectService.removeMember(savedProject, member.getMember());
			}
		}

		for (MemberDto memberDto : project.getMembers()) {
			Optional<Account> account = this.accountRepository.findByLogin(memberDto.getLogin());

			if (account.isPresent()) {
				projectService.addMember(savedProject, account.get(), memberDto.isAdmin());
			}

		}

		return new ResponseEntity<>(DozerMapper.mapper.map(savedProject, EditProjectDto.class), null, HttpStatus.OK);
	}

	/**
	 * Delete project by its id.
	 *
	 * @param projectId the project id
	 * @return HttpStatus OK if the project is found, else HttpStatus NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}", method = RequestMethod.DELETE)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> deleteProject(@PathVariable Long projectId) {
		LOGGER.debug("delete project {}", projectId);
		deletionService.deleteProject(projectId);
		return new ResponseEntity<>(null, null, HttpStatus.OK);
	}

	/**
	 * Add a public project to user favourites projects
	 *
	 * @param login     User login
	 * @param projectId Project id
	 * @return HttpStatus OK if project added, else NOT_FOUND is user or project not found,
	 * else FORBIDDEN if user is not member of the project.
	 */
	@RequestMapping(value = "/{projectId}/favourites/{login}", method = RequestMethod.PUT)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> addProjectToFavourites(@PathVariable String login, @PathVariable Long projectId) {
		LOGGER.debug("add project {} to {} user's favourites", projectId, login);

		Optional<Account> account = this.accountRepository.findByLogin(login);

		if (!account.isPresent()) {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}

		Project project = projectRepository.findOne(projectId);
		if (project == null) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		Set<Account> accounts = project.getFavouritesAccounts();
		accounts.add(account.get());

		project = projectRepository.save(project);

		return new ResponseEntity<>(DozerMapper.mapper.map(project, EditProjectDto.class), null, HttpStatus.OK);
	}

	/**
	 * Remove a public project of user favourites projects
	 *
	 * @param login     User login
	 * @param projectId Project id
	 * @return HttpStatus OK if project removed, else NOT_FOUND is user or project not found,
	 * else FORBIDDEN if user is not member of the project.
	 */
	@RequestMapping(value = "/{projectId}/favourites/{login}", method = RequestMethod.DELETE)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> removeProjectFromFavourites(@PathVariable String login, @PathVariable Long projectId) {
		LOGGER.debug("remove project {} to {} user's favourites", projectId, login);

		Optional<Account> account = this.accountRepository.findByLogin(login);

		if (!account.isPresent()) {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}

		Project project = projectRepository.findOne(projectId);
		if (project == null) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		if (project.getFavouritesAccounts().contains(account.get())) {
			project.getFavouritesAccounts().remove(account.get());
			project = projectRepository.save(project);
		}

		HttpHeaders httpHeaders = new HttpHeaders();
		return new ResponseEntity<>(DozerMapper.mapper.map(project, EditProjectDto.class), httpHeaders, HttpStatus.OK);
	}

	/**
	 * Add a public project to user favourites projects
	 *
	 * @param login     User login
	 * @param projectId Project id
	 * @return HttpStatus OK if project added, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}/members/{login}", method = RequestMethod.PUT)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> addMemberToProject(@PathVariable String login, @PathVariable Long projectId,
			@RequestParam("admin") Optional<Boolean> isAdmin) {
		LOGGER.debug("add project {} to {} user's member", projectId, login);

		if (!projectService.checkAdmin(projectId, SecurityUtils.getCurrentUserLogin())) {
			LOGGER.error(NOT_AN_ADMIN);
			throw new UnauthorizedAdminException();
		}

		Optional<Account> account = this.accountRepository.findByLogin(login);

		if (!account.isPresent()) {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}

		Project project = projectRepository.findOne(projectId);
		if (project == null) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}
		if (isAdmin.isPresent()) {
			projectService.addMember(project, account.get(), isAdmin.get());
		} else {
			projectService.addMember(project, account.get(), false);
		}
		return new ResponseEntity<>(DozerMapper.mapper.map(project, EditProjectDto.class), null, HttpStatus.OK);
	}

	/**
	 * Remove a public project of user favourites projects
	 *
	 * @param login     User login
	 * @param projectId Project id
	 * @return HttpStatus OK if project removed, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{projectId}/members/{login}", method = RequestMethod.DELETE)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> removeMemberOfProject(@PathVariable String login, @PathVariable Long projectId) {
		LOGGER.debug("remove project {} to {} user's members", projectId, login);

		if (!projectService.checkAdmin(projectId, SecurityUtils.getCurrentUserLogin())) {
			LOGGER.error(NOT_AN_ADMIN);
			throw new UnauthorizedAdminException();
		}

		Optional<Account> account = this.accountRepository.findByLogin(login);

		if (!account.isPresent()) {
			LOGGER.error("login unknow");
			throw new UserNotFoundException(login);
		}

		Project project = projectRepository.findOne(projectId);
		if (project == null) {
			LOGGER.error(PROJECT_UNKNOWN_ERROR);
			throw new ProjectNotFoundException(projectId);
		}

		projectService.removeMember(project, account.get());

		HttpHeaders httpHeaders = new HttpHeaders();
		return new ResponseEntity<>(DozerMapper.mapper.map(project, EditProjectDto.class), httpHeaders, HttpStatus.OK);
	}

}
