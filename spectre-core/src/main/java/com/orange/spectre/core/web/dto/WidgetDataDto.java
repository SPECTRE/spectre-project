package com.orange.spectre.core.web.dto;

/**
 * Created by ludovic on 05/08/2016.
 */
public class WidgetDataDto {

    private String label;
    private String value;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
