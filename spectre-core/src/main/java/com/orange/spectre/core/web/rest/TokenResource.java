/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.service.TokenService;
import com.orange.spectre.core.web.dto.TokenDTO;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.internal.util.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;

/*******************************************************
 *
 * REST controller to managing token
 *
 *******************************************************/
@RestController
@RequestMapping("/token")
public class TokenResource {

	@Inject
	private TokenService tokenService;

	@Inject
	private AccountRepository accountRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(TokenResource.class);

	/**
	 * REST request to proceed with basic authentication
	 *
	 * @param authorization
	 *            authorization
	 * @return response Object
	 *
	 */
	@RequestMapping(value = "/basic", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public TokenDTO basicAuth(final @RequestHeader(value = "Authorization") String authorization,
							  HttpServletResponse response) {

		TokenDTO token = null;
		try {
			if (StringUtils.isEmpty(authorization)) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}

			String[] splitted = authorization.split(" ");
			String decoded = Base64.decodeAsString(splitted[1]);
			String[] loginPassword = decoded.split(":");

			if (loginPassword.length < 2) {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
			String login = loginPassword[0];
			String password = loginPassword[1];
			token = this.tokenService.getForBasicAuth(login, password);
		} catch (RestClientException ex) {
			LOGGER.error("Api versionning call has failed", ex);
			response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} catch (UnauthorizedException ue) {
			LOGGER.error("Backend Version is not allowed for the current user", ue);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		}

		return token;
	}
}
