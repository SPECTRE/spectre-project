package com.orange.spectre.core.config;

/**
 * Created by DFQR0168 on 19/04/2017.
 */
public class Environment {

    private String env;
    private String releaseFilePath;

    public String getReleaseFilePath() {
        return releaseFilePath;
    }
    public void setReleaseFilePath(String releaseFilePath) {
        this.releaseFilePath = releaseFilePath;
    }
    public void setEnv(String env) {
        this.env = env;
    }

    public String getEnv() {
        return env;
    }
}
