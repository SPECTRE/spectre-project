/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.orange.spectre.core.config.security;

import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

	@Inject
	private JWTHelper jwtHelper;

	public TokenAuthenticationProvider(final JWTHelper jwtHelper) {
		this.jwtHelper = jwtHelper;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Authentication authenticate(final Authentication authentication) throws AuthenticationException,UnauthorizedException {

		// Get token from authentication principal
		final Optional<String> principal = (Optional<String>) authentication.getPrincipal();

		// Check presence
		if (!principal.isPresent() || principal.get().isEmpty()) {
			throw new BadCredentialsException("Token required");
		}

		final String jwt = principal.get();

		/*
		 * Decode JWT to handle UserId and AuthorizedProfile and
		 * authorizedApiVersions These two lines while also ensure the token
		 * validity or throwing a UnauthorizedException
		 */
		final String userId = this.jwtHelper.getUserIdFromJWT(jwt);

		// Extract GrantedAuthorities from the list of profiles
		final List<GrantedAuthority> authorities = new ArrayList<>();

		return new PreAuthenticatedAuthenticationToken(userId, null, authorities);
	}

	@Override
	public boolean supports(final Class<?> authentication) {
		return authentication.equals(PreAuthenticatedAuthenticationToken.class);
	}

}
