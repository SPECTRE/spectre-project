package com.orange.spectre.core.web.dto;

/**
 * Created by KCTL7743 on 16/03/2017.
 */
public class NewPasswordDto {

    public NewPasswordDto() {

    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    private String newPassword;
}
