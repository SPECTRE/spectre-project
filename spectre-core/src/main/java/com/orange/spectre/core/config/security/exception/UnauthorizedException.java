package com.orange.spectre.core.config.security.exception;

public class UnauthorizedException extends AbstractApiException {

	private static final long serialVersionUID = 8594425596566770442L;

	public UnauthorizedException(Unauthorized error) {
		super(error.getValue());
	}

	public UnauthorizedException(Unauthorized error, String message) {
		super(error.getValue(), message);
	}

	@Override
	public int getCategory() {
		return 401;
	}

}
