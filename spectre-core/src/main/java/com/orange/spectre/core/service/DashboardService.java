/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.core.service;

import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.domain.Dashboard;
import com.orange.spectre.core.domain.DashboardMember;
import com.orange.spectre.core.domain.DashboardMemberId;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.DashboardMemberRepository;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Optional;

/**
 *
 * @author NBLG6551
 * <p>
 * Service to handler dashboard operations
 */
@Service
public class DashboardService {
    
    @Inject
    DashboardMemberRepository dashboardMemberRepository;

    @Inject
    AccountRepository accountRepository;

    /**
     * Add a member to a dashboard
     * @param dashboard Dashboard
     * @param account Member account
     * @param isAdmin True if member is admin of the dashboard
     */
    public void addMember(Dashboard dashboard, Account account, boolean isAdmin) {

        DashboardMemberId id = new DashboardMemberId();
        id.setDashboardId(dashboard.getId());
        id.setAccountId(account.getId());
        if(dashboardMemberRepository.exists(id)){
            DashboardMember association =dashboardMemberRepository.findOne(id);
            association.setAdmin(isAdmin);
            dashboardMemberRepository.save(association);
        }else{
            DashboardMember association = new DashboardMember();
            association.setMember(account);
            association.setDashboard(dashboard);
            association.setAccountId(account.getId());
            association.setDashboardId(dashboard.getId());
            association.setAdmin(isAdmin);
            dashboardMemberRepository.save(association);
        }


    }

    /**
     * Remove a member of a dashboard
     * @param dashboard Dashboard
     * @param account Member account
     */
    public void removeMember(Dashboard dashboard, Account account) {
        DashboardMemberId id = new DashboardMemberId();
        id.setDashboardId(dashboard.getId());
        id.setAccountId(account.getId());
        if(dashboardMemberRepository.exists(id)){
            dashboardMemberRepository.delete(id);
        }
    }

    /**
     * Check if the user is admin of the dashboard
     * @param dashboardId Dashboard ID
     * @param login User login
     * @return True if user is admin
     */
    public Boolean checkAdmin(Long dashboardId,String login) {
        Optional<Account> account = accountRepository.findByLogin(login);
    	DashboardMemberId id = new DashboardMemberId();
        id.setDashboardId(dashboardId);
        id.setAccountId(account.get().getId());
		DashboardMember myDB = dashboardMemberRepository.findOne(id);
		return myDB.isAdmin();
    }
    

}
