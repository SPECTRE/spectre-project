package com.orange.spectre.core.service;

import com.orange.spectre.core.config.SpectreProperties;
import com.orange.spectre.core.domain.Account;
import org.apache.commons.lang.CharEncoding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

import javax.inject.Inject;
import javax.mail.internet.MimeMessage;
import java.util.Locale;

/**
 * Service for sending e-mails.
 * <p/>
 * <p>
 * We use the @Async annotation to send e-mails asynchronously.
 * </p>
 */
@Service
public class MailService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MailService.class);


    @Inject
    private SpectreProperties spectreProperties;

    @Inject
    private JavaMailSenderImpl javaMailSender;

    @Inject
    private MessageSource messageSource;

    @Inject
    private SpringTemplateEngine templateEngine;

    /**
     * System default email address that sends the e-mails.
     */

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        LOGGER.info("spectreProperties.getMail().isActive()");
        if(spectreProperties.getMail().isActive()) {
            LOGGER.debug("Send e-mail[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
                    isMultipart, isHtml, to, subject, content);

            // Prepare message using a Spring helper
            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            try {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, CharEncoding.UTF_8);
                message.setTo(to);
                message.setFrom(spectreProperties.getMail().getFrom());
                message.setSubject(subject);
                message.setText(content, isHtml);
                javaMailSender.send(mimeMessage);
                LOGGER.debug("Sent e-mail to User '{}'", to);
            } catch (Exception e) {
                LOGGER.error("E-mail could not be sent to user '{}', exception is: {}", to, e.getMessage());
            }
        }
    }




    @Async
    public void sendActivationEmail(Account user, String baseUrl) {
        LOGGER.debug("Sending activation e-mail to '{}'", user.getEmail());
        Locale locale = Locale.FRENCH;
        Context context = new Context(locale);
        context.setVariable("user", user);
        String content = templateEngine.process("activationEmail", context);
        sendEmail(user.getEmail(), "Welcome on Spectre!", content, false, true);
    }

    @Async
    public void sendPasswordResetMail(Account user, String baseUrl, String resetKey) {
        LOGGER.debug("Sending password reset e-mail to '{}'", user.getEmail());
        Locale locale = Locale.FRANCE;
        Context context = new Context(locale);
        context.setVariable("user", user);
        context.setVariable("resetKey", resetKey);
        context.setVariable("baseUrl", baseUrl);
        String content = templateEngine.process("passwordResetEmail", context);
        sendEmail(user.getEmail(), "Réinitialisation du mot de passe", content, false, true);
    }

}
