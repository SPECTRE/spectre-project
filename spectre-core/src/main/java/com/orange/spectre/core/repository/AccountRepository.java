package com.orange.spectre.core.repository;

import com.orange.spectre.core.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.stream.Stream;

/**
 *  JPA Repository for database User entity operations
 */
public interface AccountRepository extends JpaRepository<Account, Long> {

    /**
     * Search of a account by user login
     * @param login User login
     * @return User if exist
     */
    Optional<Account> findByLogin(String login);

    Optional<Account> findByEmail(String email);

    /**
     * Search all accounts login
     * @return List of all accounts login
     */
    @Query("select u.login from Account u")
    Stream<String> findAllLogins();

    Account findOneByEmail(String mail);

    Account findOneByResetKey(String key);
}
