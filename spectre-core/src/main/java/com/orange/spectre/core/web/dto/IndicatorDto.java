package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Indicator rest DTO
 * </p>
 */
public class IndicatorDto {

    private Long id;
    private String type;
    private String connectorType;
    private String label;
    private String measure;
    private String unity;
    private String family;
    private String project;
    private String projectName;
    private String connectorDescription;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getUnity() {
        return unity;
    }

    public void setUnity(String unity) {
        this.unity = unity;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getConnectorType() {
        return connectorType;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public void setConnectorType(String connectorType) {
        this.connectorType = connectorType;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getConnectorDescription() {
        return connectorDescription;
    }

    public void setConnectorDescription(String connectorDescription) {
        this.connectorDescription = connectorDescription;
    }
}
