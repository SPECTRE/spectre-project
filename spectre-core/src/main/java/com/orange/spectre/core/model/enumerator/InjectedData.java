package com.orange.spectre.core.model.enumerator;

import com.orange.spectre.core.model.data.AbstractData;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * Elasticsearch data bean
 */
public class InjectedData implements Serializable {

    private static final long serialVersionUID = 565826119738900985L;

    private Long indicatorId;
    private Timestamp exportDate;
    private AbstractData value;

    public Timestamp getExportDate() {
        return exportDate;
    }

    public void setExportDate(Timestamp exportDate) {
        this.exportDate = exportDate;
    }

    public AbstractData getValue() {
        return value;
    }

    public void setValue(AbstractData value) {
        this.value = value;
    }

    public Long getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }else{
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            else{
                InjectedData that = (InjectedData) o;
                return Objects.equals(indicatorId, that.indicatorId) &&
                        Objects.equals(exportDate, that.exportDate) &&
                        Objects.equals(value, that.value);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(indicatorId, exportDate, value);
    }

    @Override
    public String toString() {
        return "InjectedData{" +
                "indicatorId=" + indicatorId +
                ", exportDate=" + exportDate +
                ", value='" + value + '\'' +
                '}';
    }
}
