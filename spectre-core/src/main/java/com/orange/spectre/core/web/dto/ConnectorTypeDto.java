package com.orange.spectre.core.web.dto;

import java.util.List;

/**
 * <p>
 *  Connector type rest DTO
 * </p>
 */
public class ConnectorTypeDto {

    private String type;
    private List<ConnectorRequiredParamsDto> requiredParameters;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<ConnectorRequiredParamsDto> getRequiredParameters() {
        return requiredParameters;
    }

    public void setRequiredParameters(List<ConnectorRequiredParamsDto> requiredParameters) {
        this.requiredParameters = requiredParameters;
    }
}
