package com.orange.spectre.core.model.data;

import java.util.List;
import java.util.Objects;

public class TupleData implements AbstractData {

    List<String> data;
    List<String> label;
    String measure;

    public TupleData(){

    }

    public List<String> getData() {
        return data;
    }

    public void setData(List<String> data) {
        this.data = data;
    }

    public List<String> getLabel() {
        return label;
    }

    public void setLabel(List<String> label) {
        this.label = label;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TupleData tupleData = (TupleData) o;
        return Objects.equals(data, tupleData.data) &&
                Objects.equals(label, tupleData.label) &&
                Objects.equals(measure, tupleData.measure);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, label, measure);
    }

    @Override
    public String toString() {
        return "TupleData{" +
                "data=" + data +
                ", label=" + label +
                ", measure='" + measure + '\'' +
                '}';
    }
}
