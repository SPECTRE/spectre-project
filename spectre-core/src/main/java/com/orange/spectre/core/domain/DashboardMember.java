package com.orange.spectre.core.domain;

import javax.persistence.*;
import java.util.Objects;

/**
 * Created by ludovic on 02/06/2016.
 * Jpa dashboard member association entity
 */
@Entity
@Table(name = "DASHBOARD_MEMBER")
@IdClass(DashboardMemberId.class)
public class DashboardMember {

    @Id
    @Column(name="DASHBOARD_ID")
    private Long dashboardId;

    @Id
    @Column(name="ACCOUNT_ID")
    private Long accountId;

    @Column(name="IS_ADMIN")
    private boolean isAdmin;

    @ManyToOne
    @JoinColumn(name="ACCOUNT_ID", updatable = false, insertable = false, referencedColumnName = "ID")
    private Account dashboardMember;

    @ManyToOne
    @JoinColumn(name="DASHBOARD_ID", updatable = false, insertable = false, referencedColumnName = "ID")
    private Dashboard memberDashboard;

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public Account getMember() {
        return dashboardMember;
    }

    public void setMember(Account account) {
        this.dashboardMember = account;
    }

    public Dashboard getDashboard() {
        return memberDashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.memberDashboard = dashboard;
    }

    public Long getDashboardId() {
        return dashboardId;
    }

    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardMember that = (DashboardMember) o;
        return Objects.equals(dashboardId, that.dashboardId) &&
                Objects.equals(accountId, that.accountId) ;
    }

    @Override
    public int hashCode() {
        return Objects.hash(dashboardId, accountId);
    }
}
