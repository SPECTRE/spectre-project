/*
 * Copyright 2012-2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.orange.spectre.core.config.security;

import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class AuthenticationFilter extends GenericFilterBean {

	private final static Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);
	private AuthenticationManager authenticationManager;

	public AuthenticationFilter(final AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		Optional<String> token = Optional.ofNullable(httpRequest.getHeader("X-Auth-Token"));

		try {

			if (token.isPresent()) {
				AuthenticationFilter.LOGGER.debug("Trying to authenticate user by X-Auth-Token method. Token: {}",
						token);
				this.processTokenAuthentication(token);
			}

			AuthenticationFilter.LOGGER.debug("AuthenticationFilter is passing request down the filter chain");
			chain.doFilter(request, response);

		} catch (InternalAuthenticationServiceException internalAuthenticationServiceException) {
			SecurityContextHolder.clearContext();
			AuthenticationFilter.LOGGER.error("Internal authentication service exception",
					internalAuthenticationServiceException);
			httpResponse.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		} catch (AuthenticationException authenticationException) {
			SecurityContextHolder.clearContext();
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, authenticationException.getMessage());
		} catch (UnauthorizedException unauthorizedException) {
			SecurityContextHolder.clearContext();
			LOGGER.error("Unauthorized version Exception",unauthorizedException);
			//Send INTERNAL_SERVER_ERROR  to front-end  in order to prevent authentication of user
			httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, unauthorizedException.getMessage());
		}
	}

	private void processTokenAuthentication(final Optional<String> token) {
		final Authentication resultOfAuthentication = this.tryToAuthenticateWithToken(token);
		SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
	}

	private Authentication tryToAuthenticateWithToken(final Optional<String> token) {
		final PreAuthenticatedAuthenticationToken requestAuthentication = new PreAuthenticatedAuthenticationToken(token,
				null);
		return this.tryToAuthenticate(requestAuthentication);
	}

	private Authentication tryToAuthenticate(final Authentication requestAuthentication) {
		final Authentication responseAuthentication = this.authenticationManager.authenticate(requestAuthentication);
		if (responseAuthentication == null || !responseAuthentication.isAuthenticated()) {
			throw new InternalAuthenticationServiceException(
					"Unable to authenticate Domain User for provided credentials");
		}
		AuthenticationFilter.LOGGER.debug("User successfully authenticated");
		return responseAuthentication;
	}
}
