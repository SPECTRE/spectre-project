package com.orange.spectre.core.model.data;

import java.util.Objects;

public class StringData implements AbstractData {

    private String data;

    public StringData() {
        // Empty
    }

    public StringData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            } else {
                StringData that = (StringData) o;
                return Objects.equals(data, that.data);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "StringData{" +
                "data='" + data + '\'' +
                '}';
    }
}
