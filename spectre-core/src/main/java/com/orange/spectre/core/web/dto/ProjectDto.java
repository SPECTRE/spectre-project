package com.orange.spectre.core.web.dto;

import java.util.Objects;

/**
 * <p>
 *  Project rest DTO
 * </p>
 */
public class ProjectDto {

    private Long id;
    private String name;
    private String description;
    private boolean reachable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }



    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            }else{
                ProjectDto that = (ProjectDto) o;
                return
                        Objects.equals(id, that.id) &&
                        Objects.equals(name, that.name) &&
                        Objects.equals(description, that.description);
            }
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, reachable);
    }
}
