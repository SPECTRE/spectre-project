package com.orange.spectre.core.config;

import com.orange.spectre.plugins.common.util.ProxyConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by epeg7421 on 09/01/17.
 * Spring instantiate Component with yaml properties values
 */
@Component
public class ProxyConfigurator {
	private final Logger LOGGER = LoggerFactory.getLogger(ProxyConfigurator.class);

	@Value("${proxy.useProxy:false}")
	private boolean useProxy;
	@Value("${proxy.host}")
	private String host;
	@Value("${proxy.port}")
	private String port;

	@PostConstruct
	public void configure() {
		LOGGER.debug(this.toString());
		if (useProxy) {
			ProxyConfig.getInstance().use(host, port);
		}
	}

	@Override public String toString() {
		return String.format("ProxyConfigurator{useProxy='%s', host='%s', port='%s'}", useProxy,
				host, port);
	}

}
