package com.orange.spectre.core.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SPRING_PROFILE_LOCAL_MYSQL = "localmysql";
    public static final String SPRING_PROFILE_LOCAL_H2 = "localh2";
    public static final String SPRING_PROFILE_SERVER = "server";
    public static final String SYSTEM_ACCOUNT = "system";

    public static final String ENV_SPRING_MAIL = "spring.mail.";
    public static final String ENV_SPRING_MAIL_DEFAULT_HOST = "127.0.0.1";
    public static final String ENV_SPRING_MAIL_HOST = "host";
    public static final String ENV_SPRING_MAIL_PORT = "port";
    public static final String ENV_SPRING_MAIL_USER = "user";
    public static final String ENV_SPRING_MAIL_RESET_PASSWORD_BASE_URL = "resetPasswordBaseUrl";
    public static final String ENV_SPRING_MAIL_PASSWORD = "password";
    public static final String ENV_SPRING_MAIL_PROTO = "protocol";
    public static final String ENV_SPRING_MAIL_TLS = "tls";
    public static final String ENV_SPRING_MAIL_AUTH = "auth";

    public static final String MAIL_SMTP_AUTH = "mail.smtp.auth";
    public static final String MAIL_STARTTLS = "mail.smtp.starttls.enable";
    public static final String MAIL_TRANSPORT_PROTO = "mail.transport.protocol";

    public static final String LOGIN_UNKNOWN_ERROR = "login unknown";
    public static final String PROJECT_UNKNOWN_ERROR = "project unknown";
    public static final String DASHBOARD_UNKNOWN_ERROR = "dashboard unknown";
    public static final String NOT_A_PROJECT_MEMBER = "user is no member of the project";
    public static final String NOT_AN_ADMIN = "User is not an admin of the ressource";





    private Constants() {
    }
}
