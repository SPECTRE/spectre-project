package com.orange.spectre.core.domain;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "CONNECTOR")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE", discriminatorType = DiscriminatorType.STRING)
public abstract class AbstractConnector extends AbstractEntity {

	private static final long serialVersionUID = 1273495467314664113L;

	private String name;

    private String description;


    @Column(name = "LAST_RUN", unique = false, nullable = false)
    private Date lastRun;

    @Column(name = "PERIODICITY", unique = false, nullable = false)
    private long periodicity;

    /**
     * Required connection parameters
     */
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "CONNECTOR_PARAMS")
    @MapKeyColumn(name = "PARAM_KEY")
    @Column(name = "PARAM_VALUE")
    private Map<String, String> params;

    @OneToMany(mappedBy = "connector", fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    private Set<AbstractIndicator> indicators;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Map<String, String> getParams() {
    	// decode secured parameters from base64
        if (params != null) {
            Map<String, String> uncryptedParams = new HashMap<String, String>(params);
            List<ConnectorRequiredParams> paramsList = this.getRequiredParameters();
            for (ConnectorRequiredParams paramsItem : paramsList) {
                if (paramsItem.isSecured() && Base64.isBase64(uncryptedParams.get(paramsItem.getName()))) {
                    String value = uncryptedParams.get(paramsItem.getName());
                    uncryptedParams.replace(paramsItem.getName(), StringUtils.newStringUtf8(Base64.decodeBase64(value)));
                }
            }
            return uncryptedParams;
        }else{
            return params;
        }


    }

    public void setParams(Map<String, String> params) {
    	// encode secured parameters into base64
    	List<ConnectorRequiredParams> paramsList = this.getRequiredParameters();
    	for (ConnectorRequiredParams paramsItem : paramsList) {
    		if (paramsItem.isSecured()) {
    			String value = params.get(paramsItem.getName());
    			params.replace(paramsItem.getName(), StringUtils.newStringUtf8(Base64.encodeBase64(value.getBytes())));
    		}
    	}

        this.params = params;
    }

    public Set<AbstractIndicator> getIndicators() {
        if (indicators == null) {
            indicators = new HashSet<>();
        }
        return indicators;
    }

    public void setIndicators(Set<AbstractIndicator> indicators) {
        this.indicators = indicators;
    }

    public Date getLastRun() {
        return lastRun;
    }

    public void setLastRun(Date lastRun) {
        this.lastRun = lastRun;
    }

    public long getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(long periodicity) {
        this.periodicity = periodicity;
    }

    public abstract List<String> getAvailableIndicators();

    public abstract String getType();

    public abstract List<ConnectorRequiredParams> getRequiredParameters();


    public abstract AbstractIndicator createIndicator(String indicatorType);

    public abstract boolean testConnector(Map<String,String> connectorParams);

    @Override
    public boolean equals(Object o) {
        if (this == o) {
        	return true;
        }
        if (o == null || getClass() != o.getClass()) {
        	return false;
        }
        if (!super.equals(o)) {
        	return false;
        }

        AbstractConnector connector = (AbstractConnector) o;
        return Objects.equals(name, connector.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name);
    }
}
