/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.core.web.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author NBLG6551
 */
public class EditProjectDto {
    
    private Long id;
    private List<String> members;
    private String name;
    private String description;

    private String owner;

    private boolean reachable;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public List<String> getMembers() {
        if(members == null){
            members = new ArrayList<>();
        }
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            }else{
                EditProjectDto that = (EditProjectDto) o;
                return
                        Objects.equals(id, that.id) &&
                        Objects.equals(name, that.name) &&
                        Objects.equals(description, that.description) &&
                        Objects.equals(members, that.members);
            }
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description, members, owner, reachable);
    }
}
