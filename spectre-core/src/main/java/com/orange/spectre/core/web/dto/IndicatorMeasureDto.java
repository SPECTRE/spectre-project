package com.orange.spectre.core.web.dto;

import com.orange.spectre.core.model.enumerator.IndicatorMeasure;

/**
 * <p>
 *  Indicator measure rest DTO
 * </p>
 */
public class IndicatorMeasureDto {

    private IndicatorMeasure measure;
    private String label;

    public IndicatorMeasureDto(){
        measure = null;
        label = null;
    }

    public IndicatorMeasureDto(IndicatorMeasure pMeasure){
        measure = pMeasure;
        label = pMeasure.getValue(pMeasure);
    }



    public IndicatorMeasure getMeasure() {
        return measure;
    }

    public void setMeasure(IndicatorMeasure measure) {
        this.measure = measure;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
