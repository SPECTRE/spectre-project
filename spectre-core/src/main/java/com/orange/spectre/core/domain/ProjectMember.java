package com.orange.spectre.core.domain;



import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;


/**
 * Jpa project member association entity
 */
@Entity
@Table(name = "PROJECT_MEMBER")
@IdClass(ProjectMemberId.class)
public class ProjectMember
{

	@Id
	@Column(name="ACCOUNT_ID")
	private Long accountId;
	
	@Id
	@Column(name="PROJECT_ID")
	private Long projectId;
	
	@Column(name="IS_ADMIN")
	private boolean admin;
	
    @JsonIgnore 
	@ManyToOne
	@JoinColumn(name="ACCOUNT_ID", updatable = false, insertable = false,referencedColumnName="ID")
	private Account projectMember;
	
	@JsonIgnore 
	@ManyToOne
	@JoinColumn(name="PROJECT_ID", updatable = false, insertable = false,referencedColumnName="ID")
	private Project memberProject;
	
	
	/**
	 * @return the accountId
	 */
	public Long getAccountId() {
		return accountId;
	}

	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}

	/**
	 * @return the projectId
	 */
	public Long getProjectId() {
		return projectId;
	}

	/**
	 * @param projectId the projectId to set
	 */
	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	/**
	 * @return the memberAccount
	 */
	public Account getMember() {
		return projectMember;
	}

	/**
	 * @param memberAccount the memberAccount to set
	 */
	public void setMember(Account memberAccount) {
		this.projectMember = memberAccount;
	}

	/**
	 * @return the memberProject
	 */
	public Project getProject() {
		return memberProject;
	}

	/**
	 * @param memberProject the memberProject to set
	 */
	public void setProject(Project memberProject) {
		this.memberProject = memberProject;
	}

	public int hashCode() {
		return (int) (accountId+projectId);
	}
	
	public boolean equals(Object object) {
	    if (object instanceof ProjectMember) {
	    	ProjectMember otherId = (ProjectMember) object;
	      return (otherId.accountId == this.accountId) && (otherId.projectId == this.projectId);
	    }
	    return false;
	  }
	
	

	public boolean isAdmin() {
		return admin;
	}

	public void setAdmin(boolean admin) {
		this.admin = admin;
	}
}
