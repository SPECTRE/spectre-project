package com.orange.spectre.core.web.dto;

import java.util.List;

/**
 * <p>
 *  Widget rest DTO
 * </p>
 */
public class UpdateCustomTupleWidgetDto extends AbstractUpdateWidgetDto{

    private List<WidgetDataDto> data;

    private String dataMeasure;

    public List<WidgetDataDto> getData() {
        return data;
    }

    public void setData(List<WidgetDataDto> data) {
        this.data = data;
    }

    public String getDataMeasure() {
        return dataMeasure;
    }

    public void setDataMeasure(String dataMeasure) {
        this.dataMeasure = dataMeasure;
    }
}
