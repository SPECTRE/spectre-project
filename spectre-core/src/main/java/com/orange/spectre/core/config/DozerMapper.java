package com.orange.spectre.core.config;

import org.dozer.DozerBeanMapper;

import java.util.Collections;

/**
 * Created by ludovic on 26/04/2016.
 *
 * Dozer bean mapper instanciation class
 */
public final class DozerMapper {

    /**
     * Mapper from the dozer-mapping.xml file
     */
    public static final DozerBeanMapper mapper = new DozerBeanMapper(Collections.singletonList("META-INF/dozer-mapping.xml"));


    private DozerMapper(){

    }


}
