package com.orange.spectre.core.domain;


import javax.persistence.*;
import java.util.Objects;

/**
 * Created by pkvt9666 on 05/04/2016.
 * <p>
 *     Jpa widget entity
 * </p>
 */
@Entity
@Table(name = "WIDGET")
public class Widget extends AbstractEntity {

    private String label;
    private Long gridIndex;


    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "INDICATOR_ID")
    private AbstractIndicator indicator;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DASHBOARD_ID")
    private Dashboard dashboard;


    public AbstractIndicator getIndicator() {
        return indicator;
    }

    public void setIndicator(AbstractIndicator indicator) {
        this.indicator = indicator;
    }



    public Dashboard getDashboard() {
        return dashboard;
    }

    public void setDashboard(Dashboard dashboard) {
        this.dashboard = dashboard;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getGridIndex() {
        return gridIndex;
    }

    public void setGridIndex(Long gridIndex) {
        this.gridIndex = gridIndex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                Widget widget = (Widget) o;
                return Objects.equals(getId(), widget.getId());
            }
        }


    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode());//, type, subtype, min, max, sup, active);
    }
}
