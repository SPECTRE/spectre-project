package com.orange.spectre.core.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * Composite key for dashboard member association
 * Created by ludovic on 02/06/2016.
 */
public class DashboardMemberId implements Serializable {

    private Long dashboardId;

    private Long accountId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DashboardMemberId that = (DashboardMemberId) o;
        return Objects.equals(dashboardId, that.dashboardId) &&
                Objects.equals(accountId, that.accountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dashboardId, accountId);
    }

    public Long getDashboardId() {

        return dashboardId;
    }

    public void setDashboardId(Long dashboardId) {
        this.dashboardId = dashboardId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }
}
