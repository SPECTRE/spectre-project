package com.orange.spectre.core.domain;

public interface ConnectorParamsInterface {
	public boolean isMandatory();
	public boolean isSecured();
}
