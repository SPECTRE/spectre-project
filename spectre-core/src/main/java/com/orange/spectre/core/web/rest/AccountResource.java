package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.config.Constants;
import com.orange.spectre.core.config.DozerMapper;
import com.orange.spectre.core.config.ResetPasswordUrl;
import com.orange.spectre.core.domain.*;
import com.orange.spectre.core.exception.DashboardNotFoundException;
import com.orange.spectre.core.exception.ProjectNotFoundException;
import com.orange.spectre.core.exception.UserNotFoundException;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.repository.DashboardRepository;
import com.orange.spectre.core.repository.ProjectRepository;
import com.orange.spectre.core.repository.WidgetRepository;
import com.orange.spectre.core.service.DashboardService;
import com.orange.spectre.core.service.MailService;
import com.orange.spectre.core.service.ProjectService;
import com.orange.spectre.core.technical.RandomUtil;
import com.orange.spectre.core.web.dto.AvailableIndicatorDto;
import com.orange.spectre.core.web.dto.CreateDashboardDto;
import com.orange.spectre.core.web.dto.DashboardDto;
import com.orange.spectre.core.web.dto.EditProjectDto;
import com.orange.spectre.core.web.dto.KeyAndPasswordDto;
import com.orange.spectre.core.web.dto.PresentationDashboardDto;
import com.orange.spectre.core.web.dto.PresentationProjectDto;
import com.orange.spectre.core.web.dto.ResetPasswordMailDto;
import com.orange.spectre.core.web.dto.UserDto;
import com.orange.spectre.core.web.dto.ProfileDto;
import com.orange.spectre.core.web.dto.NewPasswordDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;

import static com.orange.spectre.core.config.Constants.*;
import static com.orange.spectre.plugins.qc.model.enumerator.QcConnectorParams.login;


/**
 * Created by pkvt9666 on 09/03/2016.
 * <p>
 * Account API
 * </p>
 */
@RestController
@RequestMapping("/accounts")
public class AccountResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(AccountResource.class);

	@Inject
	AccountRepository accountRepository;
	@Inject
	DashboardRepository dashboardRepository;
	@Inject
	ProjectRepository projectRepository;
	@Inject
	WidgetRepository widgetRepository;
	@Inject
	MailService mailService;
	@Inject
	ProjectService projectService;
	@Inject
	DashboardService dashboardService;
	@Inject
	private PasswordEncoder passwordEncoder;
	@Inject
	private ResetPasswordUrl resetPasswordUrl;

	/**
	 * Get list of spectre users login
	 *
	 * @return HttpStatus OK with list of Users login.
	 */
	@RequestMapping(method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	List<UserDto> getAccounts() {
		LOGGER.debug("get user account by login {}", login);
		List<UserDto> personsArray = new ArrayList<>();
		List<Account> accounts = accountRepository.findAll();
		for (Account account : accounts) {
			personsArray.add(DozerMapper.mapper.map(account, UserDto.class));

		}

		return personsArray;
	}

	/**
	 * Get user account by login
	 *
	 * @param login User login
	 * @return HttpStatus OK with User account, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Account getAccountByLogin(@PathVariable String login) {
		LOGGER.debug("get user account by login {}", login);
		Optional<Account> optionalAccount = accountRepository.findByLogin(login);
		if (optionalAccount.isPresent()) {
			return optionalAccount.get();
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Registration resource
	 *
	 * @param input Registration account
	 * @return HttpStatus CREATED with Registered account, else NOT_FOUND if user not found, else CONFLICT.
	 */
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<Account> createAccount(@RequestBody Account input, HttpServletRequest request) {
		LOGGER.debug("create user account");
		if (input == null || input.getLogin() == null) {
			LOGGER.error("wrong account parameter");
			return new ResponseEntity(HttpStatus.BAD_REQUEST);
		}

		Optional<Account> optionalAccount = this.accountRepository.findByLogin(input.getLogin());
		if (!optionalAccount.isPresent()) {

			Optional<Account> optionalAccountEmail = this.accountRepository.findByEmail(input.getEmail());
			if (!optionalAccountEmail.isPresent()) {

				String encryptedPassword = passwordEncoder.encode(input.getPassword());
				input.setPassword(encryptedPassword);
				Account account = accountRepository.save(input);
				HttpHeaders httpHeaders = new HttpHeaders();
				httpHeaders.setLocation(ServletUriComponentsBuilder
						.fromCurrentRequest().path("/{login}")
						.buildAndExpand(account.getLogin()).toUri());
				String baseUrl = request.getScheme() + // "http"
						"://" +                                // "://"
						request.getServerName() +              // "myhost"
						":" +                                  // ":"
						request.getServerPort() +              // "80"
						request.getContextPath();              // "/myContextPath" or "" if deployed in root context

				mailService.sendActivationEmail(input, baseUrl);

				return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
			} else {
				LOGGER.error("email already exist");
				return new ResponseEntity<>(null, null, HttpStatus.NOT_ACCEPTABLE);
			}
		}
		LOGGER.error("account already exist");
		return new ResponseEntity<>(null, null, HttpStatus.CONFLICT);
	}

	/**
	 * Get all dashboards owned by a user
	 *
	 * @param login User login
	 * @return HttpStatus OK with user's dashboard, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/dashboards", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<Dashboard> getAllDashboards(@PathVariable String login) {
		LOGGER.debug("get all dashboards owned by {} user", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			return dashboardRepository.findAllByOwner(login);
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get all user's favourites dashboards
	 *
	 * @param login User login
	 * @return HttpStatus OK with User's favourites dashboards, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/dashboards/favourites", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<PresentationDashboardDto> getAllFavouritesDashboards(@PathVariable String login) {
		LOGGER.debug("get all {} user's favourites dashboards", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			Set<PresentationDashboardDto> dashboards = new LinkedHashSet<>();
			for (Dashboard dashboard : account.get().getFavouritesDashboard()) {
				PresentationDashboardDto dash = DozerMapper.mapper.map(dashboard, PresentationDashboardDto.class);
				dashboards.add(dash);
			}
			return dashboards;
		} else {
			LOGGER.error(Constants.LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get all user's dashboards
	 *
	 * @param login User login
	 * @return HttpStatus OK with User's dashboards, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/dashboards/members", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<PresentationDashboardDto> getAllMembersDashboards(@PathVariable String login) {
		LOGGER.debug("get all {} user's dashboards", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			Set<DashboardMember> dashboardsMembers = account.get().getMembersDashboards();
			Set<PresentationDashboardDto> dashboards = new LinkedHashSet<>();

			for (DashboardMember dashboardMember : dashboardsMembers) {
				PresentationDashboardDto dash = DozerMapper.mapper.map(dashboardMember.getDashboard(), PresentationDashboardDto.class);
				dash.setAdmin(dashboardMember.isAdmin());

				dashboards.add(dash);
			}

			return dashboards;
		} else {
			LOGGER.error(Constants.LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get all projects owned by a user
	 *
	 * @param login User login
	 * @return HttpStatus OK with User's projects, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/projects", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<Project> getAllOwnedProjects(@PathVariable String login) {
		LOGGER.debug("get all projects owned by {} user", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			return projectRepository.findAllByOwner(login);
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get all user's favourites projects
	 *
	 * @param login User login
	 * @return HttpStatus OK with User's favourites projects, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/projects/favourites", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<Project> getAllFavouritesProjects(@PathVariable String login) {
		LOGGER.debug("get all {} user's favourites projects", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			return account.get().getFavouritesProjects();
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get all user's projects
	 *
	 * @param login User login
	 * @return HttpStatus OK with User's projects, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/projects/members", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Collection<PresentationProjectDto> getAllMembersProjects(@PathVariable String login) {
		LOGGER.debug("get all {} user's projects", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			Set<ProjectMember> projectsMembers = account.get().getMembersProjects();
			Set<PresentationProjectDto> projects = new HashSet<>();

			for (ProjectMember projectMember : projectsMembers) {
				PresentationProjectDto proj = DozerMapper.mapper.map(projectMember.getProject(), PresentationProjectDto.class);
				proj.setAdmin(projectMember.isAdmin());
				projects.add(proj);
			}

			return projects;

		} else {
			LOGGER.error(Constants.LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get a user dashboard
	 *
	 * @param login User login
	 * @param id    Dashboard id
	 * @return HttpStatus OK with User's dashboard, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/dashboards/{id}", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<DashboardDto> getDashboard(@PathVariable String login, @PathVariable Long id) {
		LOGGER.debug("get {} user's dashboard {}", login, id);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			Dashboard dash = dashboardRepository.findOne(id);
			if (dash == null) {
				LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
				throw new DashboardNotFoundException(id);
			}

			Set<DashboardMember> dashboardMembers = account.get().getMembersDashboards();
			DashboardMember dashboardMember = new DashboardMember();
			dashboardMember.setAccountId(account.get().getId());
			dashboardMember.setDashboardId(id);
			DashboardDto dto = DozerMapper.mapper.map(dash, DashboardDto.class);
			if (dashboardMembers.contains(dashboardMember)) {
				return new ResponseEntity<>(dto, null, HttpStatus.OK);
			} else {
				Set<Dashboard> favouritesDashboards = account.get().getFavouritesDashboard();
				if (favouritesDashboards.contains(dash)) {
					return new ResponseEntity<>(dto, null, HttpStatus.OK);
				} else {
					LOGGER.error("user don't have this dashboard");
					throw new DashboardNotFoundException(id);
				}
			}

		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Get a user project
	 *
	 * @param login User login
	 * @param id    Project id
	 * @return HttpStatus OK with User project, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/projects/{id}", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<EditProjectDto> getProject(@PathVariable String login, @PathVariable Long id) {
		LOGGER.debug("get {} user's project {}", login, id);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			Project proj = projectRepository.findOne(id);
			if (proj == null) {
				LOGGER.error(PROJECT_UNKNOWN_ERROR);
				throw new ProjectNotFoundException(id);
			}

			Set<ProjectMember> projects = account.get().getMembersProjects();
			ProjectMember projectMember = new ProjectMember();
			projectMember.setAccountId(account.get().getId());
			projectMember.setProjectId(id);
			if (projects.contains(projectMember)) {
				return new ResponseEntity<>(convertProject(proj), null, HttpStatus.OK);
			} else {
				Set<Project> favouritesProjects = account.get().getFavouritesProjects();
				if (favouritesProjects.contains(proj)) {
					return new ResponseEntity<>(convertProject(proj), null, HttpStatus.OK);

				} else {
					LOGGER.error("user don't have this project");
					return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
				}

			}

		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	private EditProjectDto convertProject(Project project) {
		EditProjectDto projectDto = DozerMapper.mapper.map(project, EditProjectDto.class);
		for (ProjectMember accounts : project.getMembersAccounts()) {
			projectDto.getMembers().add(accounts.getMember().getLogin());
		}
		return projectDto;
	}

	/**
	 * Create a dashboard by a user
	 *
	 * @param login     User login
	 * @param dashboard New dashboard
	 * @return HttpStatus CREATED with new dashboard, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/dashboards", method = RequestMethod.POST)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<DashboardDto> createDashboard(@PathVariable String login, @RequestBody CreateDashboardDto dashboard) {
		LOGGER.debug("create a dashboard for {} user", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			dashboard.setOwner(login);

			Dashboard dash = DozerMapper.mapper.map(dashboard, Dashboard.class);
			Dashboard newDashboard = dashboardRepository.save(dash);

			dashboardService.addMember(newDashboard, account.get(), true);

			for (int i = 0; i < 8; i++) {
				Widget widget = new Widget();
				widget.setDashboard(newDashboard);
				widget.setGridIndex(new Long(i));
				Widget savedWidget = widgetRepository.save(widget);
				newDashboard.getWidgets().add(savedWidget);
				dashboardRepository.save(newDashboard);
			}

			DashboardDto dashDto = DozerMapper.mapper.map(newDashboard, DashboardDto.class);

			return new ResponseEntity<>(dashDto, null, HttpStatus.CREATED);
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	/**
	 * Create a project by a user
	 *
	 * @param login   User login
	 * @param project New project
	 * @return HttpStatus CREATED with new project, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/projects", method = RequestMethod.POST)
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<Project> createProject(@PathVariable String login, @RequestBody Project project) {
		LOGGER.debug("create a project for {} user", login);
		Optional<Account> account = this.accountRepository.findByLogin(login);
		if (account.isPresent()) {
			project.setOwner(login);

			Project newProject = projectRepository.save(project);
			projectService.addMember(newProject, account.get(), true);

			return new ResponseEntity<>(newProject, null, HttpStatus.CREATED);
		} else {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}
	}

	

	/**
	 * Get all available indicators depending on the user projects
	 *
	 * @param login User login
	 * @return HttpStatus OK with Available indicators filtered by category, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{login}/indicators/types", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	Map<String, Map<String, List<AvailableIndicatorDto>>> getAvailableIndicators(@PathVariable String login) {
		LOGGER.debug("get all {} user's indicators", login);

		Optional<Account> account = this.accountRepository.findByLogin(login);

		if (!account.isPresent()) {
			LOGGER.error(LOGIN_UNKNOWN_ERROR);
			throw new UserNotFoundException(login);
		}

		Set<Project> favouritesProjects = account.get().getFavouritesProjects();
		Set<ProjectMember> projectsMembers = account.get().getMembersProjects();
		Set<Project> membersProjects = new HashSet<>();
		for (ProjectMember projectMember : projectsMembers) {
			membersProjects.add(projectMember.getProject());
		}

		membersProjects.addAll(favouritesProjects);

		Map<String, Map<String, List<AvailableIndicatorDto>>> response = new HashMap<>();

		for (Project project : membersProjects) {
			response.put(project.getName(), getFamilyResponse(project));
		}
		return response;
	}

	/**
	 * Get all available indicators of a project
	 *
	 * @param project Project
	 * @return All indicators of the project
	 */
	private Map<String, List<AvailableIndicatorDto>> getFamilyResponse(Project project) {
		Map<String, List<AvailableIndicatorDto>> familyResponse = new HashMap<>();
		for (AbstractConnector connector : project.getConnectors()) {
			for (AbstractIndicator indicator : connector.getIndicators()) {
				AvailableIndicatorDto dto = new AvailableIndicatorDto();
				dto.setId(indicator.getId());
				dto.setLabel(indicator.getLabel());
				dto.setMeasure(indicator.getMeasure().name());
				dto.setUnity(indicator.getMeasureUnity());
				dto.setConnector(indicator.getConnector().getName());
				dto.setDomain(indicator.getDomain());
				dto.setPurpose(indicator.getPurpose());
				dto.setDefinition(indicator.getDefinition());
				dto.setOrigin(indicator.getOrigin());
				if (familyResponse.containsKey(indicator.getFamily().name())) {
					familyResponse.get(indicator.getFamily().name()).add(dto);
				} else {
					List<AvailableIndicatorDto> indicatorsResponse = new ArrayList<>();
					indicatorsResponse.add(dto);
					familyResponse.put(indicator.getFamily().name(), indicatorsResponse);
				}
			}
		}
		return familyResponse;
	}

	@RequestMapping(value = "/reset_password/init",
			method = RequestMethod.POST,
			produces = org.springframework.http.MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<?> requestPasswordReset(@RequestBody ResetPasswordMailDto resetPasswordMailDto, HttpServletRequest request) {
		Account account = accountRepository.findOneByEmail(resetPasswordMailDto.getMail());
		if (account == null) {
			return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
		}
		String resetKey = RandomUtil.generateResetKey();
		String baseUrl = resetPasswordUrl.getBaseUrl();
		account.setResetKey(resetKey);
		accountRepository.save(account);
		mailService.sendPasswordResetMail(account, baseUrl, resetKey);
		return new ResponseEntity<>(null, null, HttpStatus.OK);
	}

	@RequestMapping(value = "/reset_password/finish",
			method = RequestMethod.POST,
			produces = org.springframework.http.MediaType.TEXT_PLAIN_VALUE)
	public ResponseEntity<String> finishPasswordReset(@RequestBody KeyAndPasswordDto keyAndPassword) {
		String key = keyAndPassword.getKey();
		String password = keyAndPassword.getNewPassword();
		if (key == null) {
			return new ResponseEntity<>(null, null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (accountRepository.findOneByResetKey(key) == null) {
			return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
		}
		Account account = accountRepository.findOneByResetKey(key);
		account.setPassword(passwordEncoder.encode(password));
		account.setResetKey(null);
		accountRepository.save(account);

        return new ResponseEntity<>(account.getLogin(), null, HttpStatus.OK);
    }


    /**
     * Change user password account
     *
     * @param login User login
     * @return HttpStatus OK, else NOT_FOUND.
     */
    @RequestMapping(value = "/{login}/changePassword", method = RequestMethod.PUT)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<String> changePassword(@PathVariable String login, @RequestBody NewPasswordDto newPassword ) {
        LOGGER.debug("change password account of {}", login);
        Optional<Account> optionalAccount = accountRepository.findByLogin(login);

        if (optionalAccount.isPresent()) {
			Account account = optionalAccount.get();
            account.setPassword(passwordEncoder.encode(newPassword.getNewPassword()));
            accountRepository.save(account);
            return new ResponseEntity<>(null, null, HttpStatus.OK);
        } else {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

    }

    /**
     * Change user profile account
     *
     * @param login User login
     * @return HttpStatus OK, else NOT_FOUND.
     */
    @RequestMapping(value = "/{login}", method = RequestMethod.PUT)
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<String> changeProfile(@PathVariable String login, @RequestBody ProfileDto profile ) {
        LOGGER.debug("change user account profile of {}", login);
        Optional<Account> optionalAccount = accountRepository.findByLogin(login);
        if (optionalAccount.isPresent()) {
			Account account = optionalAccount.get();
            account.setLastname(profile.getLastname());
            account.setFirstname(profile.getFirstname());
            account.setEmail(profile.getEmail());
            accountRepository.save(account);
            return new ResponseEntity<>(null, null, HttpStatus.OK);
        } else {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

    }
}
