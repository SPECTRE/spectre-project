package com.orange.spectre.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.bind.RelaxedPropertyResolver;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.inject.Inject;
import java.util.Properties;

@Configuration
public class MailConfiguration implements EnvironmentAware {

    @Inject
    private SpectreProperties spectreProperties;

    private static final Logger LOGGER = LoggerFactory.getLogger(MailConfiguration.class);

    private RelaxedPropertyResolver propertyResolver;

    @Override
    public void setEnvironment(Environment environment) {
        this.propertyResolver = new RelaxedPropertyResolver(environment, Constants.ENV_SPRING_MAIL);
    }

    @Bean
    public JavaMailSenderImpl javaMailSender() {
        if(spectreProperties.getMail().isActive()) {
            LOGGER.debug("Configuring mail server");
            String host = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_HOST, Constants.ENV_SPRING_MAIL_HOST);
            int port = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_PORT, Integer.class, 0);
            String user = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_USER);
            String password = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_PASSWORD);
            String protocol = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_PROTO);
            Boolean tls = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_TLS, Boolean.class, false);
            Boolean auth = propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_AUTH, Boolean.class, false);

            JavaMailSenderImpl sender = new JavaMailSenderImpl();
            if (host != null && !host.isEmpty()) {
                sender.setHost(host);
            } else {
                LOGGER.warn("Warning! Your SMTP server is not configured. We will try to use one on localhost.");
                LOGGER.debug("Did you configure your SMTP settings in your application.yml?");
                sender.setHost(Constants.ENV_SPRING_MAIL_DEFAULT_HOST);
            }
            sender.setPort(port);
            sender.setUsername(user);
            sender.setPassword(password);

            Properties sendProperties = new Properties();
            sendProperties.setProperty(Constants.MAIL_SMTP_AUTH, auth.toString());
            sendProperties.setProperty(Constants.MAIL_STARTTLS, tls.toString());
            sendProperties.setProperty(Constants.MAIL_TRANSPORT_PROTO, protocol);
            sender.setJavaMailProperties(sendProperties);
            return sender;
        }
        return null;
    }

    @Bean
    public ResetPasswordUrl getBaseUrl() {
        ResetPasswordUrl resetPasswordUrl = new ResetPasswordUrl();
        resetPasswordUrl.setBaseUrl(propertyResolver.getProperty(Constants.ENV_SPRING_MAIL_RESET_PASSWORD_BASE_URL));
        return resetPasswordUrl;
    }
}
