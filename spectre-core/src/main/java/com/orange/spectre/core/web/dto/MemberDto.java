package com.orange.spectre.core.web.dto;

/**
 * Created by ludovic on 07/06/2016.
 */
public class MemberDto {

    private String login;
    private boolean isAdmin;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
