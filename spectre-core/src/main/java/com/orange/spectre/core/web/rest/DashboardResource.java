package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.config.DozerMapper;
import com.orange.spectre.core.config.SecurityUtils;
import com.orange.spectre.core.config.security.exception.Unauthorized;
import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import com.orange.spectre.core.domain.*;
import com.orange.spectre.core.exception.DashboardNotFoundException;
import com.orange.spectre.core.exception.UnauthorizedAdminException;
import com.orange.spectre.core.exception.UserNotFoundException;
import com.orange.spectre.core.repository.*;
import com.orange.spectre.core.service.DashboardService;
import com.orange.spectre.core.service.DeletionService;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.technical.CustomComparator;
import com.orange.spectre.core.web.dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.*;

import static com.orange.spectre.core.config.Constants.LOGIN_UNKNOWN_ERROR;
import static com.orange.spectre.core.config.Constants.NOT_AN_ADMIN;



/**
 * Dashboard API
 */
@RestController
@RequestMapping("/dashboards")
public class DashboardResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(DashboardResource.class);
    private static final String DASHBOARD_UNKNOWN_ERROR = "dashboard unknown";
    private static final String INDICATOR_UNKNOWN_ERROR = "indicator unknown";

    @Inject
    DashboardRepository dashboardRepository;

    @Inject
    WidgetRepository widgetRepository;

    @Inject
    DeletionService deletionService;

    @Inject
    AbstractIndicatorRepository indicatorRepository;

    @Inject
    FetchIndicatorsService fetchIndicatorsService;

    @Inject
    AccountRepository accountRepository;

    @Inject
    DashboardService dashboardService;

    @Inject
    ProjectRepository projectRepository;

    @Inject
    DashboardMemberRepository dashboardMemberRepository;

    /**
     * Get public dashboards.
     *
     * @return HttpStatus OK with a set of dashboards which boolean Reachable is true.
     */
    @RequestMapping(method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    Set<PresentationDashboardDto> getPublicDashboards() {
        LOGGER.debug("get all public dashboards");
        Set<PresentationDashboardDto> dashboards = new HashSet<>();
        for (Dashboard dashboard : dashboardRepository.findAllByReachableTrueOrderByIdAsc()) {
            PresentationDashboardDto dash = new PresentationDashboardDto();
            dash.setId(dashboard.getId());
            dash.setDescription(dashboard.getDescription());
            dash.setName(dashboard.getName());
            dashboards.add(dash);
        }
        return dashboards;

    }


    /**
     * Get dashboard by its id.
     *
     * @param dashboardId The dashboard id
     * @return HttpStatus OK with dashboard, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}", method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    DashboardDto getDashboard(@PathVariable Long dashboardId) {
        LOGGER.debug("get dashboard {}", dashboardId);
        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }
        return DozerMapper.mapper.map(dashboardRepository.findOne(dashboardId), DashboardDto.class);
    }

    /**
     * Get a shared dashboard by its shareid.
     *
     * @param shareId The dashboard shareId
     * @return HttpStatus OK with dashboard, else NOT_FOUND.
     */
    @RequestMapping(value = "/public/shared", method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    DashboardDto getSharedDashboard(@RequestParam String shareId) {

        if (dashboardRepository.findByShareid(shareId) == null) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(shareId);
        }

        DashboardDto dashboardDto = DozerMapper.mapper.map(dashboardRepository.findByShareid(shareId), DashboardDto.class);

        if(dashboardDto.isReachable()) {
            return dashboardDto;
        }else {
            LOGGER.error("Not a Public Dashboard");
            throw new UnauthorizedException(Unauthorized.GENERIC);
        }
    }

    /**
     * Delete dashboard by its id.
     *
     * @param dashboardId The dashboard id
     * @return HttpStatus OK if the dashboard is found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}", method = RequestMethod.DELETE)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> deleteDashboard(@PathVariable Long dashboardId) {
        LOGGER.debug("delete dashboard {}", dashboardId);
        deletionService.deleteDashboard(dashboardId);

        return new ResponseEntity<>(null, null, HttpStatus.OK);
    }

    /**
     * Get all widgets by dashboard.
     *
     * @param dashboardId The dashboard id
     * @return A list of Widget DTOs by dashboard if the dashboard is found, else HttpStatus NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}/widgets", method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public List<WidgetDto> getAllWidgetsByDashboard(@PathVariable Long dashboardId) {

        LOGGER.debug("get all widgets into the dashboard {}", dashboardId);
        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        List<WidgetDto> widgetsDto = new ArrayList<>();
        Set<Widget> widgets = dashboard.getWidgets();

        for (Widget widget : widgets) {
            WidgetDto widgetDto = DozerMapper.mapper.map(widget, WidgetDto.class);
            if(widget.getIndicator() != null){
                IndicatorDto indicatorDto = DozerMapper.mapper.map(widget.getIndicator(), IndicatorDto.class);
                if (widget.getIndicator().getConnector() != null) {
                    indicatorDto.setConnectorType(widget.getIndicator().getConnector().getType());
                    indicatorDto.setConnectorDescription(widget.getIndicator().getConnector().getDescription());
                    Set<Long> connectorId = new HashSet<>();
                    connectorId.add(widget.getIndicator().getConnector().getId());
                    Set<Project> projects = projectRepository.findAllByConnectorsId(connectorId);
                    if(!projects.isEmpty()) {
                        indicatorDto.setProjectName(projects.iterator().next().getName());
                    }
                }
                if ("CUSTOM".equals(widget.getIndicator().getType().name())) {
                    indicatorDto.setConnectorType("custom");
                }
                widgetDto.setIndicator(indicatorDto);
            }

            widgetsDto.add(widgetDto);
        }

        return widgetsDto;
    }

    /**
     * Get all widgets by  a shared dashboard.
     *
     * @param  dashboardId The dashboard id
     * @return A list of Widget DTOs by dashboard if the dashboard is found, else HttpStatus NOT_FOUND.
     */
    @RequestMapping(value = "public/{dashboardId}/widgets", method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public List<WidgetDto> getAllWidgetsBySharedDashboard(@PathVariable Long dashboardId) {

        LOGGER.debug("get all widgets into the dashboard {}", dashboardId);

        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        Dashboard dashboard = dashboardRepository.findOne(dashboardId);

        if(!dashboard.isReachable()) {
            LOGGER.error("Not a Public Dashboard");
            throw new UnauthorizedException(Unauthorized.GENERIC);
        }

        List<WidgetDto> widgetsDto = new ArrayList<>();
        Set<Widget> widgets = dashboard.getWidgets();

        for (Widget widget : widgets) {
            WidgetDto widgetDto = DozerMapper.mapper.map(widget, WidgetDto.class);
            if(widget.getIndicator() != null){
                IndicatorDto indicatorDto = DozerMapper.mapper.map(widget.getIndicator(), IndicatorDto.class);
                if (widget.getIndicator().getConnector() != null) {
                    indicatorDto.setConnectorType(widget.getIndicator().getConnector().getType());
                    indicatorDto.setConnectorDescription(widget.getIndicator().getConnector().getDescription());
                    Set<Long> connectorId = new HashSet<>();
                    connectorId.add(widget.getIndicator().getConnector().getId());
                    Set<Project> projects = projectRepository.findAllByConnectorsId(connectorId);
                    if(!projects.isEmpty()) {
                        indicatorDto.setProjectName(projects.iterator().next().getName());
                    }
                }
                if ("CUSTOM".equals(widget.getIndicator().getType().name())) {
                    indicatorDto.setConnectorType("custom");
                }
                widgetDto.setIndicator(indicatorDto);
            }

            widgetsDto.add(widgetDto);
        }

        return widgetsDto;
    }

    /**
     * Update the given dashboard.
     *
     * @param dashboardId The dashboard id
     * @param dashboard   The dashboard
     * @return HttpStatus OK with the updated dashboard if it's found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> updateDashboard(@PathVariable Long dashboardId, @RequestBody UpdateDashboardDto dashboard) {

        LOGGER.debug("update dashboard {}", dashboardId);

        if (!dashboardService.checkAdmin(dashboardId,SecurityUtils.getCurrentUserLogin())) {
            LOGGER.error(NOT_AN_ADMIN);
            throw new UnauthorizedAdminException();
        }

        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        Dashboard savedDashboard = dashboardRepository.findOne(dashboardId);
        savedDashboard.setReachable(dashboard.isReachable());
        savedDashboard.setName(dashboard.getName());

        savedDashboard = dashboardRepository.save(savedDashboard);


        Set<DashboardMember> dashMembers = dashboardMemberRepository.findByDashboardId(savedDashboard.getId());

        for(DashboardMember member : dashMembers){
            boolean found = false;
            for(MemberDto memberDto : dashboard.getMembers()){
                if(member.getMember().getLogin().equals(memberDto.getLogin())){
                    found = true;
                }
            }

            if(!found){
                dashboardService.removeMember(savedDashboard, member.getMember());
            }
        }

        for(MemberDto memberDto : dashboard.getMembers()){
            Optional<Account> account = this.accountRepository.findByLogin(memberDto.getLogin());

            if (account.isPresent()) {
                dashboardService.addMember(savedDashboard, account.get(),memberDto.isAdmin());
            }

        }

        return new ResponseEntity<>(DozerMapper.mapper.map(savedDashboard, DashboardDto.class), null, HttpStatus.OK);
    }


    /**
     * Inject all indicators by dashboard in ElasticSearch.
     *
     * @param dashboardId the dashbaord id
     * @return HttpStatus OK if the dashboard is found, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}/fetch", method = RequestMethod.POST)
    @Produces({ MediaType.APPLICATION_JSON })
    ResponseEntity<Object> fetchIndicators(@PathVariable Long dashboardId) {
        LOGGER.debug("call dashboard {} to get all its kpis and send them to elastic", dashboardId);

        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }
        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        Set<Widget> widgets = dashboard.getWidgets();
        for (Widget widget : widgets) {
            AbstractIndicator indicator = widget.getIndicator();
            AbstractConnector connector = indicator.getConnector();
            if (connector != null) {
            	fetchIndicatorsService.fetchByIndicator(connector.getParams(), connector.getType(), indicator);
            }
        }

        return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
    }


    /**
     * Add a public project to user favourites dashboards
     * @param login User login
     * @param dashboardId Dashboard id
     * @return HttpStatus OK if dashboard added, else NOT_FOUND is user or dashboard not found,
     * else FORBIDDEN if user is not member of the dashboard.
     */
    @RequestMapping(value = "/{dashboardId}/favourites/{login}", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> addDashboardToFavourites(@PathVariable String login, @PathVariable Long dashboardId) {
        LOGGER.debug("add dashboard {} to {} user's favourites", dashboardId, login);
        Optional<Account> account = this.accountRepository.findByLogin(login);

        if (!account.isPresent()) {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

        Dashboard dash = dashboardRepository.findOne(dashboardId);
        if (dash == null) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        Set<Account> accounts = dash.getFavouritesAccounts();
        accounts.add(account.get());

        dash = dashboardRepository.save(dash);


        return new ResponseEntity<>(DozerMapper.mapper.map(dash, DashboardDto.class), null, HttpStatus.OK);
    }

    /**
     * Remove a public dashboard of user favourites dashboards
     * @param login User login
     * @param dashboardId Dashboard id
     * @return HttpStatus OK if dashboard removed, else NOT_FOUND is user or dashboard not found,
     * else FORBIDDEN if user is not member of the dashboard.
     */
    @RequestMapping(value = "/{dashboardId}/favourites/{login}", method = RequestMethod.DELETE)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> removeDashboardFromFavourites(@PathVariable String login, @PathVariable Long dashboardId) {
        LOGGER.debug("remove dashboard {} to {} user's favourites", dashboardId, login);
        Optional<Account> account = this.accountRepository.findByLogin(login);

        if (!account.isPresent()) {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

        Dashboard dash = dashboardRepository.findOne(dashboardId);
        if (dash == null) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }


        if (dash.getFavouritesAccounts().contains(account.get())) {
            dash.getFavouritesAccounts().remove(account.get());
            dash = dashboardRepository.save(dash);
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        return new ResponseEntity<>(DozerMapper.mapper.map(dash, DashboardDto.class), httpHeaders, HttpStatus.OK);
    }

    /**
     * Add a user to dashboard's member list
     * @param login User login
     * @param dashboardId Dashboard id
     * @return HttpStatus OK if user added, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}/members/{login}", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> addMemberToDashboard(@PathVariable String login, @PathVariable Long dashboardId, @RequestParam("admin") Optional<Boolean> isAdmin) {
        LOGGER.debug("add dashboard {} to {} user's member", dashboardId, login);

        if (!dashboardService.checkAdmin(dashboardId,SecurityUtils.getCurrentUserLogin())) {
			LOGGER.error(NOT_AN_ADMIN);
        	throw new UnauthorizedAdminException();
		}

        Optional<Account> account = this.accountRepository.findByLogin(login);

        if (!account.isPresent()) {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        if (dashboard == null) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        if(isAdmin.isPresent()){
            dashboardService.addMember(dashboard, account.get(), isAdmin.get());
        }else{
            dashboardService.addMember(dashboard, account.get(), false);
        }


        return new ResponseEntity<>(DozerMapper.mapper.map(dashboard, DashboardDto.class), null, HttpStatus.OK);
    }

    /**
     * Remove user from dashboard members
     * @param login User login
     * @param dashboardId Dashboard id
     * @return HttpStatus OK if user removed, else NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}/members/{login}", method = RequestMethod.DELETE)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> removeMemberOfDashboard(@PathVariable String login, @PathVariable Long dashboardId) {
        LOGGER.debug("remove dashboard {} to {} user's members", dashboardId, login);


        if (!dashboardService.checkAdmin(dashboardId,SecurityUtils.getCurrentUserLogin())) {
			LOGGER.error(NOT_AN_ADMIN);
        	throw new UnauthorizedAdminException();
		}


        Optional<Account> account = this.accountRepository.findByLogin(login);

        if (!account.isPresent()) {
            LOGGER.error(LOGIN_UNKNOWN_ERROR);
            throw new UserNotFoundException(login);
        }

        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        if (dashboard == null) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        dashboardService.removeMember(dashboard, account.get());

        return new ResponseEntity<>(DozerMapper.mapper.map(dashboard, DashboardDto.class), new HttpHeaders(), HttpStatus.OK);
    }

    /**
     * Get all dashboard's members.
     *
     * @param dashboardId The dashboard id
     * @return A list of Members DTOs by dashboard if the dashboard is found, else HttpStatus NOT_FOUND.
     */
    @RequestMapping(value = "/{dashboardId}/members", method = RequestMethod.GET)
    @Produces({MediaType.APPLICATION_JSON})
    @Transactional
    public List<MemberDto> getAllMembers(@PathVariable Long dashboardId) {

        LOGGER.debug("get all widgets into the dashboard {}", dashboardId);
        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }

        List<MemberDto> members = new ArrayList<>();

        Set<DashboardMember> dashMembers = dashboardMemberRepository.findByDashboardId(dashboardId);
        for(DashboardMember member : dashMembers){
            members.add(DozerMapper.mapper.map(member,MemberDto.class));
        }

        return members;


    }

    @RequestMapping(value = "/{dashboardId}/manageWidgetSet", method = RequestMethod.POST)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> addWidgetSet(@PathVariable Long dashboardId) {
        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }
        LOGGER.debug("Create a new set of widgets for dashboard {}", dashboardId);
        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        List<Widget> widgets = widgetRepository.findAllByDashboard(dashboard);
        Long currentIndex = 0L;
        if (!widgets.isEmpty()) {
            Collections.sort(widgets, new CustomComparator());
            currentIndex = widgets.get(widgets.size()-1).getGridIndex();
        }
        for(int i = 0; i < 8; i++){
            currentIndex++;
            Widget widget = new Widget();
            widget.setDashboard(dashboard);
            widget.setGridIndex(currentIndex);
            Widget savedWidget = widgetRepository.save(widget);
            dashboard.getWidgets().add(savedWidget);
        }
        dashboardRepository.save(dashboard);
        DashboardDto dashboardDto = DozerMapper.mapper.map(dashboard,DashboardDto.class );
        return new ResponseEntity<>(dashboardDto, null, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/{dashboardId}/manageWidgetSet", method = RequestMethod.PUT)
    @Produces({MediaType.APPLICATION_JSON})
    ResponseEntity<DashboardDto> deleteWidgetSet(@PathVariable Long dashboardId, @RequestBody WidgetSetDto widgetSetDto) {
        if (!dashboardRepository.exists(dashboardId)) {
            LOGGER.error(DASHBOARD_UNKNOWN_ERROR);
            throw new DashboardNotFoundException(dashboardId);
        }
        LOGGER.debug("Delete a set of widgets for dashboard {}", dashboardId);
        for (WidgetDto widget : widgetSetDto.getWidgets()) {
            deletionService.deleteWidget(widget.getId());
        }
        Dashboard dashboard = dashboardRepository.findOne(dashboardId);
        DashboardDto dashboardDto = DozerMapper.mapper.map(dashboard,DashboardDto.class );
        return new ResponseEntity<>(dashboardDto, null, HttpStatus.CREATED);
    }

}

