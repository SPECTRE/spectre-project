package com.orange.spectre.core.technical;

import com.orange.spectre.core.domain.Widget;

import java.util.Comparator;

/**
 * Comparator for Widget to filter by grid index
 */
public class CustomComparator implements Comparator<Widget> {
    @Override
    public int compare(Widget o1, Widget o2) {
        return o1.getGridIndex().compareTo(o2.getGridIndex());
    }
}
