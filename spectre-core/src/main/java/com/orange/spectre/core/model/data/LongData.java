package com.orange.spectre.core.model.data;

import java.util.Objects;

/**
 * Created by epeg7421 on 04/10/2016.
 */
public class LongData implements AbstractData {
	private Long data;

	public LongData() {
		// Empty
	}

	public LongData(Long data) {
		this.data = data;
	}

	public Long getData() {
		return data;
	}

	public void setData(Long data) {
		this.data = data;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		} else {
			if (o == null || getClass() != o.getClass() || !super.equals(o)) {
				return false;
			} else {
				LongData that = (LongData) o;
				return Objects.equals(data, that.data);
			}
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(data);
	}

	@Override
	public String toString() {
		return "LongData{" +
				"data='" + data + '\'' +
				'}';
	}

}
