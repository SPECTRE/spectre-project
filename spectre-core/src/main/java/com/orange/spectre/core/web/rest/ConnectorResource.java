package com.orange.spectre.core.web.rest;

import com.orange.spectre.core.config.DozerMapper;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.exception.ConnectorNotFoundException;
import com.orange.spectre.core.model.enumerator.ConnectorType;
import com.orange.spectre.core.repository.AbstractConnectorRepository;
import com.orange.spectre.core.repository.AbstractIndicatorRepository;
import com.orange.spectre.core.service.FetchIndicatorsService;
import com.orange.spectre.core.service.KpiInjectionService;
import com.orange.spectre.core.web.dto.ConnectorDto;
import com.orange.spectre.core.web.dto.ConnectorTypeDto;
import com.orange.spectre.core.web.dto.EditConnectorDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Connector API
 */
@RestController
@RequestMapping("/connectors")
public class ConnectorResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectorResource.class);

	private static final String CONNECTOR_UNKNOWN_ERROR = "connector unknown";
	@Inject
	AbstractConnectorRepository connectorRepository;

	@Inject
	AbstractIndicatorRepository indicatorRepository;

	@Inject
	KpiInjectionService kpiInjectionService;

	@Inject
	FetchIndicatorsService fetchIndicatorsService;

	/**
	 * Get all connectors.
	 * If isOwner is true, add configuration to the returned DTO.
	 *
	 * @return HttpStatus OK with the connectors or an empty list.
	 */
	@RequestMapping(value = "", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<List<ConnectorDto>> getConnector() {
		LOGGER.debug("get all connectors");
		List<AbstractConnector> abstractConnectors = this.connectorRepository.findAll();
		List<ConnectorDto> connectorDtos = new ArrayList<>();
		for (AbstractConnector connector : abstractConnectors) {
			connectorDtos.add(DozerMapper.mapper.map(connector, ConnectorDto.class));
		}
		return new ResponseEntity<>(connectorDtos, null, HttpStatus.OK);
	}

	/**
	 * Get connector by its id.
	 * If isOwner is true, add configuration to the returned DTO.
	 *
	 * @param connectorId The connector id
	 * @param isOwner     Defines if the given connector is owned by the request sender
	 * @return HttpStatus OK with the found connector, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{connectorId}", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<ConnectorDto> getConnector(@PathVariable Long connectorId, @RequestParam("owner") Optional<Boolean> isOwner) {
		LOGGER.debug("get connector {} ", connectorId);
		AbstractConnector abstractConnector = this.connectorRepository.findOne(connectorId);
		if (abstractConnector != null) {
			if (isOwner.isPresent() && isOwner.get()) {
				LOGGER.debug("get extended connector {}", connectorId);
				EditConnectorDto connectorDto = DozerMapper.mapper.map(abstractConnector, EditConnectorDto.class);
				Map<String, String> params = abstractConnector.getParams();
				connectorDto.setConfigurationParams(params);
				return new ResponseEntity<>(connectorDto, null, HttpStatus.OK);
			} else {
				ConnectorDto connectorDto = DozerMapper.mapper.map(abstractConnector, ConnectorDto.class);
				return new ResponseEntity<>(connectorDto, null, HttpStatus.OK);
			}
		} else {
			LOGGER.error(CONNECTOR_UNKNOWN_ERROR);
			return new ResponseEntity<>(null, null, HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Get available connectors
	 *
	 * @return HttpStatus OK with connectors.
	 */
	@RequestMapping(value = "/types", method = RequestMethod.GET)
	@Produces({ MediaType.APPLICATION_JSON })
	List<ConnectorTypeDto> getAvailableConnectors() {
		LOGGER.debug("get all connectors types available");
		List<ConnectorTypeDto> connectorsDto = new ArrayList<>();
		for (ConnectorType type : ConnectorType.values()) {
			connectorsDto.add(DozerMapper.mapper.map(type.createConnector(), ConnectorTypeDto.class));
		}
		return connectorsDto;
	}

	/**
	 * Update connector by its id.
	 *
	 * @param connectorId  The connector id
	 * @param connectorDto The connector DTO
	 * @return HttpStatus OK with the updated connector if it's found, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{connectorId}", method = RequestMethod.PUT)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<AbstractConnector> updateConnector(@PathVariable Long connectorId, @RequestBody EditConnectorDto connectorDto) {
		LOGGER.debug("update connector {}", connectorId);
		if (!connectorRepository.exists(connectorId)) {
			LOGGER.error(CONNECTOR_UNKNOWN_ERROR);
			throw new ConnectorNotFoundException(connectorId);
		}
		AbstractConnector connector = connectorRepository.findOne(connectorId);
		connector.setName(connectorDto.getName());
		connector.setDescription(connectorDto.getDescription());
		connector.setPeriodicity(Math.max(600, connectorDto.getPeriodicity()));
		connector.setParams(connectorDto.getConfigurationParams());
		AbstractConnector savedConnector = connectorRepository.save(connector);
		return new ResponseEntity<>(savedConnector, null, HttpStatus.OK);
	}

	/**
	 * Inject all indicators by connector in ElasticSearch.
	 *
	 * @param connectorId the connector id
	 * @return HttpStatus OK if the connector is found, else NOT_FOUND.
	 */
	@RequestMapping(value = "/{connectorId}/indicators/fetch", method = RequestMethod.POST)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<Object> fetchIndicators(@PathVariable Long connectorId) {
		LOGGER.debug("call connector {} to get all its kpis and send them to elastic", connectorId);

		if (!connectorRepository.exists(connectorId)) {
			LOGGER.error(CONNECTOR_UNKNOWN_ERROR);
			throw new ConnectorNotFoundException(connectorId);
		}
		AbstractConnector connector = connectorRepository.findOne(connectorId);
		fetchIndicatorsService.fetchByConnector(connector);
		return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
	}

	@RequestMapping(value = "/fetchAll", method = RequestMethod.POST)
	@Produces({ MediaType.APPLICATION_JSON })
	ResponseEntity<Object> fetchAllIndicators() {
		LOGGER.debug("Fetch kpis for all connectors...");
		fetchIndicatorsService.fetchAll();
		return new ResponseEntity<>(null, new HttpHeaders(), HttpStatus.OK);
	}
}
