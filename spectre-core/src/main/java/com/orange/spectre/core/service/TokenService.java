package com.orange.spectre.core.service;

import com.orange.spectre.core.config.security.JWTHelper;
import com.orange.spectre.core.config.security.exception.Functional;
import com.orange.spectre.core.config.security.exception.FunctionalException;
import com.orange.spectre.core.config.security.exception.UnauthorizedException;
import com.orange.spectre.core.domain.Account;
import com.orange.spectre.core.repository.AccountRepository;
import com.orange.spectre.core.web.dto.TokenDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/*******************************************************
 *
 * Service class to manage tokens
 *
 *******************************************************/
@Service("tokenService")
public class TokenService {
	private static final Logger LOGGER = LoggerFactory.getLogger(TokenService.class);

	@Inject
	private JWTHelper jwtHelper;

	@Inject
	private AccountRepository accountRepository;

	@Inject
	private PasswordEncoder passwordEncoder;

	/**
	 * Create access token
	 *
	 * @return token entity
	 *
	 */
	public TokenDTO createToken(final Account user)throws RestClientException, UnauthorizedException {
		final TokenDTO token = new TokenDTO();
		Map<String, String> authorizedApiVersions = new HashMap<>();


		token.setAccessToken(this.jwtHelper.getJWT(user.getLogin(), 720));
		return token;
	}

	/**
	 * Check authentication and get valid user token
	 *
	 * @param login
	 * @param password
	 * @return token
	 *
	 */
	public TokenDTO getForBasicAuth(final String login, final String password) throws RestClientException, UnauthorizedException{
		Optional<Account> user = accountRepository.findByLogin(login);

		if (!user.isPresent()) {
			throw new FunctionalException(Functional.AUTHENTICATION,
					"Unknown user: " + login);
		}

		boolean passwordIsValid = passwordEncoder.matches(password, user.get().getPassword());
		if (!passwordIsValid) {
			throw new FunctionalException(Functional.AUTHENTICATION,
					"Wrong password");
		}
		return createToken(user.get());
	}
}
