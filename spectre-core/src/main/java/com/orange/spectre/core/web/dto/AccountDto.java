package com.orange.spectre.core.web.dto;

/**
 * Created by ludovic on 05/04/2016.
 * <p>
 *  Account rest DTO
 * </p>
 */
public class AccountDto {

    private String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
