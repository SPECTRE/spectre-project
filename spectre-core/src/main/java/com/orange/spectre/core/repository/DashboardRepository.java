package com.orange.spectre.core.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.orange.spectre.core.domain.Dashboard;

/**
 * JPA Repository for database Dashboard entity operations
 */
public interface DashboardRepository extends JpaRepository<Dashboard, Long> {

	/**
	 * Get all public dashboards
	 *
	 * @return All public dashboards
	 */
	Set<Dashboard> findAllByReachableTrueOrderByIdAsc();

	/**
	 * Get all private dashboards
	 *
	 * @return All private dashboards
	 */
	Set<Dashboard> findAllByReachableFalseOrderByIdAsc();

	/**
	 * Get all dashboards owned by an user login
	 *
	 * @param login
	 *            User login
	 * @return All user's dashboards
	 */
	Set<Dashboard> findAllByOwner(String login);

	/**
	 * Count all dashboards
	 *
	 * @return all dashboards
	 */
	@Query("select count(d.id) from Dashboard d")
	Long countAll();

	/**
	 * Find a shared dashboard
	 *
	 * @return dashboard
	 */
	Dashboard findByShareid(String shareid);

}
