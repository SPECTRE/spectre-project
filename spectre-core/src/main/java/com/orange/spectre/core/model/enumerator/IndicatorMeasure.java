package com.orange.spectre.core.model.enumerator;

public enum IndicatorMeasure {
    NUMERIC,
    PERCENT,
    MOOD,
    TUPLE,
    TEXT;

    public String getValue(IndicatorMeasure mesure) {
        switch (mesure) {
            case NUMERIC: return "Valeur numérique";
            case PERCENT: return "Pourcentage";
            case MOOD: return "Humeur";
            case TUPLE: return "Tableau";
            case TEXT: return "Texte";
            default: return null;
        }
    }

}
