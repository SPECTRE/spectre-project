package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Widget rest DTO
 * </p>
 */
public class UpdateCustomWidgetDto extends AbstractUpdateWidgetDto{

    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
