package com.orange.spectre.core.technical;

import com.orange.spectre.core.exception.MD5DigestException;
import org.apache.commons.codec.binary.Hex;

import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by kctl7743 on 22/06/2017.
 *
 * hash a string with a MD5 Algorithm
 */
public class MD5Digest {

    /**
     *
     * @param text the string to hash with MD5 Algorithm
     * @return a MD5 hash String
     */
    public static String generate (String text) {

        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(text.getBytes(Charset.forName("UTF8")));
            final byte[] resultByte = messageDigest.digest();
            final String md5 = new String(Hex.encodeHex(resultByte));

            return md5;

        } catch (NoSuchAlgorithmException e) {
            throw new MD5DigestException();
        }
    }

}
