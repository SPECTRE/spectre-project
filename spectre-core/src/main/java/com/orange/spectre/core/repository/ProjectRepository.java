package com.orange.spectre.core.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.orange.spectre.core.domain.Project;

/**
 * Created by ludovic on 06/04/2016.
 * <p>
 * JPA Repository for database Project entity operations
 */
public interface ProjectRepository extends JpaRepository<Project, Long> {

	/**
	 * Get all projects owned by an user login
	 *
	 * @param login
	 *            User login
	 * @return All user's projects
	 */
	Set<Project> findAllByOwner(String login);

	/**
	 * Get all public projects
	 *
	 * @return All public projects
	 */
	Set<Project> findAllByReachableTrue();

	/**
	 * Get all private projects
	 *
	 * @return All private projects
	 */
	Set<Project> findAllByReachableFalse();

	/**
	 * Get all projects by connectors
	 *
	 * @return All projects by connectors
	 */
	Set<Project> findAllByConnectorsId(Set<Long> ids);

	/**
	 * Count all projects
	 *
	 * @return all projects
	 */
	@Query("select count(p.id) from Project p ")
	Long countAll();

	/**
	 * Count all connectors join to project
	 *
	 * @return all connectors
	 */
	@Query("select count(c.id) from Project p left join p.connectors c")
	Long findAllConnectors();
}
