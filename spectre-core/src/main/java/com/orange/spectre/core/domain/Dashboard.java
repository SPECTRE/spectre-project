package com.orange.spectre.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.orange.spectre.core.technical.MD5Digest;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

/**
 * Jpa dashboard entity
 */
@Entity
@Table(name = "DASHBOARD")
public class Dashboard extends AbstractEntity {

    private static final long serialVersionUID = 1707370267966893047L;

    private String name;

    private String description;

    private boolean reachable;

    private String owner;


    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(unique = true, nullable = false)
    private String shareid = MD5Digest.generate(UUID.randomUUID().toString());

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "dashboardMember")
    private Set<DashboardMember> membersAccounts;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "DASHBOARD_FAVOURITE", joinColumns = @JoinColumn(name = "DASHBOARD_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID"))
    private Set<Account> favouritesDashboardsAccounts;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @JoinTable(name = "DASHBOARD_WIDGETS", joinColumns = @JoinColumn(name = "DASHBOARD_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "WIDGET_ID", referencedColumnName = "ID"))
    private Set<Widget> widgets;

    /**
     * Default constructor
     *
     * @param name
     * @param description
     * @param owner
     */
    public Dashboard(String name, String description, String owner) {
        this.name = name;
        this.description = description;
        this.owner = owner;
    }

    public Dashboard() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Set<Account> getFavouritesAccounts() {
        if(favouritesDashboardsAccounts == null){
            favouritesDashboardsAccounts = new HashSet<>();
        }
        return favouritesDashboardsAccounts;
    }

    public void setFavouritesAccounts(Set<Account> favouritesAccounts) {
        this.favouritesDashboardsAccounts = favouritesAccounts;
    }


    public Set<DashboardMember> getMembersAccounts() {
        if (membersAccounts==null) {
            membersAccounts = new HashSet<>();
        }
        return membersAccounts;
    }

    public void setMembersAccounts(Set<DashboardMember> membersAccounts) {
        this.membersAccounts = membersAccounts;
    }

    public Set<Widget> getWidgets() {
        if(widgets == null){
            widgets = new HashSet<>();
        }
        return widgets;
    }

    public void setWidgets(Set<Widget> widgets) {
        this.widgets = widgets;
    }



    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public String getShareid() {
        return shareid;
    }

    public void setShareid(String shareid) {
        this.shareid = shareid;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                Dashboard dashboard = (Dashboard) o;
                return
                        Objects.equals(name, dashboard.name) &&
                                Objects.equals(description, dashboard.description) &&
                                Objects.equals(owner, dashboard.owner);
            }
        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description, reachable, owner);
    }

    @Override
    public String toString() {
        return "Dashboard{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", reachable=" + reachable +
                ", owner='" + owner + '\'' +
                ", shareid='" + shareid + '\'' +
                ", widgets=" + widgets +
                '}';
    }
}
