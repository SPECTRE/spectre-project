package com.orange.spectre.core.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by pkvt9666 on 05/04/2016.
 * <p>
 * Jpa project entity
 * </p>
 */
@Entity
@Table(name = "PROJECT")
public class Project extends AbstractEntity {

    private static final long serialVersionUID = 8760960390938343501L;

    private String name;

    private String description;

    private boolean reachable;

    private String owner;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "PROJECT_FAVOURITE", joinColumns = @JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ACCOUNT_ID", referencedColumnName = "ID"))
    private Set<Account> favouritesProjectsAccounts;

    @JsonIgnore
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "memberProject")
    private Set<ProjectMember> membersAccounts;


    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
    @JoinTable(name = "PROJECT_CONNECTORS", joinColumns = {@JoinColumn(name = "PROJECT_ID", referencedColumnName = "ID")}, inverseJoinColumns = {@JoinColumn(name = "CONNECTOR_ID", referencedColumnName = "ID")})
    private Set<AbstractConnector> connectors;

    public Project() {
        super();
    }

    public Project(String name, String description, String owner) {
        this.name = name;
        this.description = description;
        this.owner = owner;
    }

    public void addMember(Account account, boolean isadmin) {
        ProjectMember association = new ProjectMember();
        association.setMember(account);
        association.setProject(this);
        association.setAccountId(account.getId());
        association.setProjectId(this.getId());
        association.setAdmin(isadmin);

        getMembersAccounts().add(association);
        account.getMembersProjects().add(association);
    }

    public void removeMember(Account account) {
        ProjectMember association = new ProjectMember();
        association.setMember(account);
        association.setProject(this);
        association.setAccountId(account.getId());
        association.setProjectId(this.getId());

        getMembersAccounts().remove(association);
        account.getMembersProjects().remove(association);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isReachable() {
        return reachable;
    }

    public void setReachable(boolean reachable) {
        this.reachable = reachable;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Set<Account> getFavouritesAccounts() {
        if (favouritesProjectsAccounts == null) {
            favouritesProjectsAccounts = new HashSet<>();
        }
        return favouritesProjectsAccounts;
    }

    public void setFavouritesAccounts(Set<Account> favouritesAccounts) {
        this.favouritesProjectsAccounts = favouritesAccounts;
    }

    public Set<ProjectMember> getMembersAccounts() {
        if (membersAccounts == null) {
            membersAccounts = new HashSet<>();
        }
        return membersAccounts;
    }

    public void setMembersAccounts(Set<ProjectMember> membersAccounts) {
        this.membersAccounts = membersAccounts;
    }

    public Set<AbstractConnector> getConnectors() {

        if (connectors == null) {
            connectors = new HashSet<>();
        }
        return connectors;
    }

    public void setConnectors(Set<AbstractConnector> connectors) {
        this.connectors = connectors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass() || !super.equals(o)) {
                return false;
            } else {
                Project project = (Project) o;
                return getId().equals(project.getId());
            }

        }

    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, description, reachable, owner);
    }

    @Override
    public String toString() {
        return "Project{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", reachable=" + reachable +
                ", owner='" + owner + '\'' +
                ", favouritesAccounts=" + favouritesProjectsAccounts +
                ", membersAccounts=" + membersAccounts +
                ", connectors=" + connectors +
                '}';
    }
}
