package com.orange.spectre.core.config;

import org.h2.server.web.WebServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Application configuration file
 */
@Configuration
public class WebConfiguration implements ServletContextInitializer, EmbeddedServletContainerCustomizer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebConfiguration.class);

    @Inject
    private SpectreProperties spectreProperties;

    /**
     * H2 database instantiation for localh2 profile
     * @return H2 console servlet
     */
    @Bean
    @Profile(value = "localh2")
    ServletRegistrationBean h2ServletRegistration() {
        LOGGER.debug("h2 servlet registration");
        ServletRegistrationBean registrationBean = new ServletRegistrationBean(new WebServlet());
        registrationBean.addUrlMappings("/console/*");
        registrationBean.addInitParameter("webAllowOthers", "true");
        return registrationBean;
    }

    /**
     * CORS web filter instantiation
     * @return CORS filter
     */
    @Bean
    public CorsFilter corsFilter() {
        LOGGER.debug("CORS Filter registration");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = spectreProperties.getCors();
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            source.registerCorsConfiguration("/accounts/**", config);
            source.registerCorsConfiguration("/dashboards/**", config);
            source.registerCorsConfiguration("/connectors/**", config);
            source.registerCorsConfiguration("/indicators/**", config);
            source.registerCorsConfiguration("/projects/**", config);
            source.registerCorsConfiguration("/widgets/**", config);
        }
        return new CorsFilter(source);
    }

    /**
     * Servlet customization
     * @param configurableEmbeddedServletContainer Servlet container
     */
    @Override
    public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
        // No customization needed
    }

    /**
     *  Servlet startup actions
     * @param servletContext Servlet Context
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // No startup specification
    }
}
