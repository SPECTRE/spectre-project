package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Widget rest DTO
 * </p>
 */
public class UpdateWidgetDto extends  AbstractUpdateWidgetDto{

    private Long indicatorId;

    public Long getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }



}
