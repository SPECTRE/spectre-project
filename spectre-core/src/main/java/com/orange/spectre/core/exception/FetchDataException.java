package com.orange.spectre.core.exception;

public class FetchDataException extends Exception {

    public FetchDataException(final String message, final Throwable exception) {
        super(message, exception);
    }

    public FetchDataException(final String message) {
        super(message);
    }

    public FetchDataException(final Exception exception) {
        super(exception);
    }
}
