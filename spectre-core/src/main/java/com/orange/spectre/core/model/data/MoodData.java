package com.orange.spectre.core.model.data;


import com.orange.spectre.core.model.enumerator.MoodEnum;

import java.util.Objects;

public class MoodData implements AbstractData {

    private MoodEnum data;

    public MoodData() {
        // Empty
    }

    public MoodData(MoodEnum data) {
        this.data = data;
    }

    public MoodEnum getData() {
        return data;
    }

    public void setData(MoodEnum data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else {
            if (o == null || getClass() != o.getClass()) {
                return false;
            } else {
                MoodData that = (MoodData) o;
                return Objects.equals(data, that.data);
            }
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(data);
    }

    @Override
    public String toString() {
        return "MoodData{" +
                "data='" + data + '\'' +
                '}';
    }
}
