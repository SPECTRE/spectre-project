


package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.orange.spectre.core.config.SecurityUtils;

/**
 * Exception for unknown project
 *
 * Return http 403 when exception raised
 */
@ResponseStatus(HttpStatus.FORBIDDEN)
public class UnauthorizedAdminException extends RuntimeException {

    public UnauthorizedAdminException() {
        super("User " + SecurityUtils.getCurrentUserLogin() + " not authorized on this ressource");
    }
}
