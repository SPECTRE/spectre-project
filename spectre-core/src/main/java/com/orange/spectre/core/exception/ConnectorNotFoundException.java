package com.orange.spectre.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception for unknown connector
 *
 * Return http 404 when exception raised
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ConnectorNotFoundException extends RuntimeException {
    public ConnectorNotFoundException(Long connectorId) {
        super("Could not find connector '" + Long.toString(connectorId) + "'.");
    }
}
