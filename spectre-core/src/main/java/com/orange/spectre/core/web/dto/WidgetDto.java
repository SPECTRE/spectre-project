package com.orange.spectre.core.web.dto;

/**
 * <p>
 *  Widget rest DTO
 * </p>
 */
public class WidgetDto {

    private Long id;
    private String label;
    private IndicatorDto indicator;
    private Long gridIndex;

    public IndicatorDto getIndicator() {
        return indicator;
    }

    public void setIndicator(IndicatorDto indicator) {
        this.indicator = indicator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Long getGridIndex() {
        return gridIndex;
    }

    public void setGridIndex(Long gridIndex) {
        this.gridIndex = gridIndex;
    }
}
