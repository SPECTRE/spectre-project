package com.orange.spectre.plugins.cbs.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.cbs.model.enumerator.CbsIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;

@Entity
@DiscriminatorValue("CBS_TX_INDISPO")
public class TxIndispoIndicator extends AbstractIndicator {

	private static final long	serialVersionUID	= -7225464104303937879L;
	@Transient
	private final Logger		LOGGER				= LoggerFactory
															.getLogger(TxIndispoIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {
		try {
			Map<String, Object> params = new HashMap<>();

			params.put("project", connectorParams.get("project"));
			params.put("indicator", CbsIndicatorType.TX_INDISPO.getKey());

			return new FloatData(Float.valueOf(CbsConnector.DATA.get(
					params.get("project")).get(params.get("indicator"))) * 100F);

		} catch (Exception e) {
			LOGGER.error("Exception: problème lors de la recuperation du kpi : "
					+ e);
		}

		return null;
	}

	@Override
	public String getLabel() {
		return CbsIndicatorType.TX_INDISPO.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		// SELECT THE RIGHT MEASURE VALUE
		return IndicatorMeasure.PERCENT;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		// SELECT THE RIGHT FAMILY VALUE
		return IndicatorFamily.DEVOPS;
	}

	@Override
	public Enum<CbsIndicatorType> getType() {
		return CbsIndicatorType.TX_INDISPO;
	}

}
