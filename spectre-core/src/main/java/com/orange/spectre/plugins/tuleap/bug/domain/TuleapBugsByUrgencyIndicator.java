package com.orange.spectre.plugins.tuleap.bug.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.tuleap.bug.enumerator.TuleapBugsConnectorParams;
import com.orange.spectre.plugins.tuleap.bug.enumerator.TuleapBugsIndicatorType;
import com.orange.spectre.plugins.tuleap.bug.utils.TuleapBugUtils;
import com.orange.spectre.plugins.tuleap.common.model.TuleapFilledBug;
import com.orange.spectre.plugins.tuleap.common.model.TuleapTokenResponse;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Jpa entity for global task state
 */
@Entity
@DiscriminatorValue("TULEAP_BUGS_BY_URGENCY")
public class TuleapBugsByUrgencyIndicator extends AbstractIndicator {


    private static final Logger LOGGER = LoggerFactory.getLogger(TuleapBugsByUrgencyIndicator.class);


    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {

            String login = connectorParams.get(TuleapBugsConnectorParams.login.name());
            String password = connectorParams.get(TuleapBugsConnectorParams.mot_de_passe.name());
            String project = connectorParams.get(TuleapBugsConnectorParams.nom_projet.name());
            String rapport = connectorParams.get(TuleapBugsConnectorParams.nom_rapport.name());

            TuleapTokenResponse tuleapResponse = TuleapUtils.getTuleapConnectionData(login, password);
            String tuleapProjectId = TuleapUtils.getTuleapProjectIdByName(tuleapResponse.getToken(), tuleapResponse.getUserId(), project);
            String tuleapTrackerId = TuleapBugUtils.getTuleapBugTrackerId(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapProjectId);
            String tuleapTrackerReportId = TuleapBugUtils.getTuleapBugTrackerReportId(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapTrackerId, rapport);
            TuleapFilledBug[] bugs = TuleapBugUtils.getTuleapFilledBugs(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapTrackerReportId);

            List<String> criterias = new ArrayList<>();
            criterias.add("Urgency");
            criterias.add("Urgence");

            return TuleapBugUtils.getTuleDataFromCriteria(bugs, criterias);


        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return TuleapBugsIndicatorType.TULEAP_BUGS_BY_URGENCY.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return TuleapBugsIndicatorType.TULEAP_BUGS_BY_URGENCY;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}

