package com.orange.spectre.plugins.teamcolony.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by WKNL8600 on 27/04/2016.
 */

/**
 * TeamColony replies bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Replies {

    private String author;

    private String body;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "ClassPojo [author = " + author + ", body = " + body + "]";
    }

}
