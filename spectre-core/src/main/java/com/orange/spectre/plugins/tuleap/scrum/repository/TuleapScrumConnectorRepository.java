package com.orange.spectre.plugins.tuleap.scrum.repository;

import com.orange.spectre.plugins.tuleap.scrum.domain.TuleapScrumConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Tuleap connector JPA enumerator
 */
public interface TuleapScrumConnectorRepository extends JpaRepository<TuleapScrumConnector, Long> {
}
