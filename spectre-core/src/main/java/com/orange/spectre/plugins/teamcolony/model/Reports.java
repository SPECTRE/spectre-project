package com.orange.spectre.plugins.teamcolony.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WKNL8600 on 27/04/2016.
 */

/**
 * TeamColony reports bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Reports {
    @JsonProperty("replies")
    private List<Replies> replies = new ArrayList<Replies>();
   // private Replies[] replies;

    private String created_at;

    public List<Replies> getReplies() {
        return replies;
    }

    public void setReplies(List<Replies> replies) {
        this.replies = replies;
    }

    public String getCreated_at ()
    {
        return created_at;
    }

    public void setCreated_at (String created_at)
    {
        this.created_at = created_at;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [replies = "+replies+", created_at = "+created_at+"]";
    }
}
