package com.orange.spectre.plugins.sacre.util;

import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreConnectorParams;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

public final class SacreFluxUtil {

    private static CloseableHttpClient httpClient;

    private static HttpClientContext localContext;

    // Création du JSON

    /**
     * @param params
     * @return
     * @throws IOException
     */
    public static String getFluxJSON(Map<String, String> params) throws IOException, Exception {

        String sacreIP = (String) params.get(SacreConnectorParams.url.name());
        String sacreURL = "http://";
        sacreURL = sacreURL.concat(sacreIP);
        sacreURL = sacreURL.concat("/g7/adminnatsessid/webservices/tsi");


        HttpResponse response;

        URI uri = URI.create(sacreURL);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());

        httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(uri);

        response = httpClient.execute(host, httpGet);
        if (response.getStatusLine().getStatusCode() != 200) {
            return null;
        }
        return EntityUtils.toString(response.getEntity());

    }

}
