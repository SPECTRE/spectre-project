package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 03/04/2017.
 */
public class TuleapBacklogItem {

    private String id;
    private String status;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
