package com.orange.spectre.plugins.tuleap.common.utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ludovic on 24/03/2017.
 */
public final class TuleapConstants {

    public static final String FORGE_URL = "https://www.forge.orange-labs.fr:443/";
    public static final String FORGE_API_TOKENS_URI = "api/tokens";
    public static final String FORGE_API_PROJECTS_URI = "api/projects/";
    public static final String FORGE_API_MILESTONE_URI = "api/milestones/";
    public static final String FORGE_API_TRACKERS_URI = "api/trackers/";
    public static final String FORGE_API_PLANNINGS_URI = "api/plannings/";
    public static final String FORGE_API_TRACKER_REPORTS_URI = "api/tracker_reports/";
    public static final String FORGE_API_ARTIFACTS_URI = "api/artifacts/";
    public static final String FORGE_ARTIFACTS_URI = "/artifacts";
    public static final String FORGE_TRACKERS_URI = "/trackers";
    public static final String FORGE_PLANNING_URI = "/plannings";
    public static final String FORGE_TRACKER_REPORTS_URI = "/tracker_reports";
    public static final String FORGE_MILESTONES_URI = "/milestones";
    public static final String FORGE_BACKLOG_URI = "/backlog";
    public static final String FORGE_CONTENT_URI = "/content";

    public static final String FORGE_PROJECT_BUGS_ITEM_NAME = "Bugs";
    public static final String FORGE_USER_STORY_TYPE = "User Stories";

	public static final List<String> SEVERITY = Collections.unmodifiableList(
			Arrays.asList("Sévérité", "sévérité", "Severity", "severity"));

	public static final List<String> URGENCY = Collections.unmodifiableList(
			Arrays.asList("Urgence", "urgence","Urgency", "urgency"));

	public static final List<String> STATUS = Collections.unmodifiableList(
			Arrays.asList("Statut", "statut", "Status", "status"));;
}
