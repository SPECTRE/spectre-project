package com.orange.spectre.plugins.mingle.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.mingle.model.MingleSprintPojo;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;
import com.orange.spectre.plugins.mingle.util.MingleMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

import static com.orange.spectre.plugins.mingle.model.enumerator.MingleIndicatorType.SPRINT_VELOCITY;

/**
 * Jpa entity for percentage jobs fails
 *
 * @author nmcf5735
 */
@Entity
@DiscriminatorValue("SPRINT_VELOCITY")
public class SprintVelocityIndicator extends AbstractIndicator {

    @Override
    public String getLabel() {
        return SPRINT_VELOCITY.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {


        String sprint = connectorParams.get(MingleConnectorParams.sprint_courant.name());

        if (sprint != null) {

            String mqlRequest = "SELECT name, 'vélocité journalière' WHERE type='sprint'";

            JSONArray jsonArray = MingleMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {
                try {
                    Float velocity = getVelocity(jsonArray, sprint);
                    if(velocity != null){
                        return new FloatData(velocity);
                    }

                } catch (IOException e) {
                    return null;
                }
            }
        }
        return null;
    }

    private Float getVelocity(JSONArray jsonArray, String sprint) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject story = (JSONObject) jsonArray.get(i);
            MingleSprintPojo pojo = mapper.readValue(story.toString(), MingleSprintPojo.class);

            if (pojo.getName().contains(sprint) && pojo.getVelocity() != null){
                return Float.valueOf(pojo.getVelocity());
            }
        }

        return null;
    }


    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return SPRINT_VELOCITY;
    }


    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}

