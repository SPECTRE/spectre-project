package com.orange.spectre.plugins.mingle.util;

import com.google.common.net.UrlEscapers;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.Map;

/**
 * Created by ludovic on 12/10/2016.
 */
public final class MingleMqlResponseUtils<T> {


    public static JSONArray getResponseArray(Map<String, String> connectorParams, String mqlRequest){

        String login = connectorParams.get(MingleConnectorParams.login.name());

        String secret = connectorParams.get(MingleConnectorParams.token_hmac.name());

        String projet = connectorParams.get(MingleConnectorParams.projet.name());


        HmacAuth hmacAuth = new HmacAuth(login, secret);

        StringBuilder fullUrl = new StringBuilder();

        fullUrl.append("http://mingle.sso.francetelecom.fr/api/v2/projects/");
        fullUrl.append(projet);
        fullUrl.append("/cards/execute_mql.json?mql=");

        String escapedMqlRequest = UrlEscapers.urlFragmentEscaper().escape(mqlRequest);
        fullUrl.append(escapedMqlRequest);

        HttpGet httpGet = new HttpGet(fullUrl.toString());

        HmacRequestInterceptor authorizationInterceptor = new HmacRequestInterceptor(hmacAuth);

        CloseableHttpClient httpClient =  HttpClientBuilder.create().addInterceptorLast( authorizationInterceptor ).build();

        try {
            HttpResponse response =  httpClient.execute(httpGet);


            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                InputStream is = response.getEntity().getContent();
                StringWriter writer = new StringWriter();
                String encoding = "UTF-8";
                IOUtils.copy(is, writer, encoding);
                String decodedJsonResponse = StringEscapeUtils.unescapeJava(writer.toString());

                JSONArray jsonArray = new JSONArray(decodedJsonResponse);

                return jsonArray;

            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }
}
