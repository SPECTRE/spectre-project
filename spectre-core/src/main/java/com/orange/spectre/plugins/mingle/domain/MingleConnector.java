package com.orange.spectre.plugins.mingle.domain;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.UrlEscapers;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.mingle.model.MingleStoryPojo;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleIndicatorType;
import com.orange.spectre.plugins.mingle.util.HmacAuth;
import com.orange.spectre.plugins.mingle.util.HmacRequestInterceptor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of mingle connector
 */
@Entity
@DiscriminatorValue("MINGLE")
public class MingleConnector extends AbstractConnector {

    /**
     * All connector connection params
     */
    @Transient
    List<ConnectorRequiredParams> requiredParams;

    /**
     * Get list of all mingle indicators
     *
     * @return All mingle indicators
     */
    public List<String> getAvailableIndicators() {
        return Arrays.stream(MingleIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

    }

    /**
     * Get list of all mingle connection parameters
     *
     * @return mingle connection parameters
     */
    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (MingleConnectorParams param : MingleConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);

            }
        }
        return requiredParams;
    }

    /**
     * Get mingle string type
     *
     * @return String type
     */
    @Override
    public String getType() {
        return "mingle";
    }

    /**
     * Create a mingle indicator from an indicator type
     *
     * @param s indicator type
     * @return A new mingle indicator
     */
    @Override
    public AbstractIndicator createIndicator(String s) {
        return MingleIndicatorType.valueOf(s).createIndicator();
    }

    /**
     * Test acces to the connector
     *
     * @param connectorParams Mingle connection parameters
     * @return True if connection ok
     */
    @Override
    public boolean testConnector(Map<String, String> connectorParams) {

        String login = connectorParams.get(MingleConnectorParams.login.name());

        String secret = connectorParams.get(MingleConnectorParams.token_hmac.name());

        String projet = connectorParams.get(MingleConnectorParams.projet.name());


        HmacAuth hmacAuth = new HmacAuth(login, secret);

        StringBuilder fullUrl = new StringBuilder();

        fullUrl.append("http://mingle.sso.francetelecom.fr/api/v2/projects/");
        fullUrl.append(projet);
        fullUrl.append(".xml");


        HttpGet httpGet = new HttpGet(fullUrl.toString());

        HmacRequestInterceptor authorizationInterceptor = new HmacRequestInterceptor(hmacAuth);

        CloseableHttpClient httpClient =  HttpClientBuilder.create().addInterceptorLast( authorizationInterceptor ).build();

        try {
            HttpResponse response =  httpClient.execute(httpGet);


            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return true;

            }
        } catch (IOException e) {
            return false;
        }

        return false;
    }

}
