package com.orange.spectre.plugins.qc.custom.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.qc.domain.ClosedStatusNbIndicator;
import com.orange.spectre.plugins.qc.domain.CriticalSeverityNbIndicator;
import com.orange.spectre.plugins.qc.custom.domain.CustomDefectsNbIndicator;
import com.orange.spectre.plugins.qc.domain.MajorSeverityNbIndicator;
import com.orange.spectre.plugins.qc.domain.MinorSeverityNbIndicator;
import com.orange.spectre.plugins.qc.domain.OpenedClosedStatusNbIndicator;
import com.orange.spectre.plugins.qc.domain.OpenedStatusNbIndicator;

/**
 * Enumerator of all available qc indicators
 */
public enum CustomQcIndicatorType {

	DEFECTS_CUSTOM(CustomDefectsNbIndicator.class);
	/**
	 * Current qc indicator class
	 */
	private Class<? extends AbstractIndicator> clazz;

	/**
	 * Indicator type constructor
	 *
	 * @param clazz Current indicator class
	 */
	CustomQcIndicatorType(Class<? extends AbstractIndicator> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Indicator instantiation from it's type
	 *
	 * @return An qc indicator
	 */
	public AbstractIndicator createIndicator() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(clazz + " has no default constructor");
		}
	}

	/**
	 * Get indicator label from it's type
	 *
	 * @return Indicator label
	 */
	public String getLabel() {
		switch (this.name()) {
		case "DEFECTS_CUSTOM":
			return "Customize Query";
		default:
			return null;
		}
	}
}


