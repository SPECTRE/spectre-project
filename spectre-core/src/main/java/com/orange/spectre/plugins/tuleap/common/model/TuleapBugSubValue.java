package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 28/03/2017.
 */
public class TuleapBugSubValue {

    private String label;
    private String id;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
