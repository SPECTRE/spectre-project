package com.orange.spectre.plugins.sacre.common.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.DossierFIXE;
import com.orange.spectre.plugins.sacre.common.model.ISacreSubIndicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.IOException;
import java.util.Map;


public class DosClosFixeIndicator implements ISacreSubIndicator {
    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        IntegerData data = new IntegerData();
        ObjectMapper mapper = new ObjectMapper();
        int nbDosClos = 0;
        Data myData = mapper.readValue(flux, Data.class);
        for (DossierFIXE trai : myData.getData().getDossierFIXE()) {
            if (trai.getClos() != null && trai.getClos() != "") {
                nbDosClos += Integer.parseInt(trai.getClos());
            }

        }

        data.setData(nbDosClos);

        return data;
    }

    @Override
    public String getLabel() {
        return "Dossiers clos";
    }


}

