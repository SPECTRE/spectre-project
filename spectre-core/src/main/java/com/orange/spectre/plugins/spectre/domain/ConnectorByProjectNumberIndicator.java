package com.orange.spectre.plugins.spectre.domain;

import java.text.DecimalFormat;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.service.SpectrePluginsService;
import com.orange.spectre.plugins.spectre.model.enumerator.SpectreIndicatorType;

/**
 * Created by epeg7421 on 14/10/2016.
 */
@Entity
@Configurable
@DiscriminatorValue("SPECTRE_CONNECTOR_BY_PROJECT")
public class ConnectorByProjectNumberIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConnectorByProjectNumberIndicator.class);

	@Transient
	@Inject
	private SpectrePluginsService spectrePluginsService;

	/**
	 * Fetch data.
	 *
	 * @param connectorParams
	 *            Contains all the params such as URL, token, ... to connect to third providers.
	 * @return The fetched data.
	 * @throws FetchDataException
	 *             indicators must throw their exceptions in this custom core consumed exception.
	 */
	@Override
	public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {
		FloatData data = new FloatData();

		// Calcul la fréquence des connectors par projects f = Nb Connectors / Nb Projects
		Long dataAllConnectors = spectrePluginsService.countAllConnectors();
		Long dataAllProjects = spectrePluginsService.countAllProjects();

		data.setData(calculate(dataAllConnectors, dataAllProjects));
		return data;
	}

	Float calculate(Long nombre, Long nombre2) {
		Float value = 0f;
//		DecimalFormat df = new DecimalFormat("#.0");
		if (nombre2 > 0L) {
			value = Float.valueOf(nombre / nombre2);
		}

//		return new Float(df.format(value));
		// si on veut garder la partie entière uniquement
		return (value - value % 1);
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.MANAGEMENT;
	}

	@Override
	public Enum getType() {
		return SpectreIndicatorType.CONNECTOR_BY_PROJECT_NB;
	}

	@Override
	public String getLabel() {
		return SpectreIndicatorType.CONNECTOR_BY_PROJECT_NB.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}
}
