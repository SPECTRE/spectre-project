package com.orange.spectre.plugins.sonar.common.util;

import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.sonar.common.exception.SonarException;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;

public class SonarFaasUtil {
	private static final Logger LOGGER = LoggerFactory.getLogger(SonarFaasUtil.class);

	public static SonarResponse[] request(String url) throws Exception, IOException{

		CloseableHttpClient httpClient;

		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sc);

		// Complete url with Proxy Settings
		if (ProxyConfig.getInstance().isUseProxy()) {
			HttpHost proxy = new HttpHost(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort()));
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setProxy(proxy).build();
		} else {
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		}

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

    	RestTemplate restTemplate = new RestTemplate(requestFactory);
		SonarResponse[] sonarResponse = restTemplate.getForObject(url, SonarResponse[].class);

		try {
			httpClient.close();
		} catch (IOException e) {
			LOGGER.error("Exception: could not close Sonar Faas http client", e);
			throw new SonarException("Sonar Faas http client close error");
		}
		return sonarResponse;
	}

}

