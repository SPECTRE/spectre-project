package com.orange.spectre.plugins.tuleap.common.utils;

/**
 * Created by ludovic on 24/03/2017.
 */
public class TuleapApiException extends Exception {

    public TuleapApiException(final String message, final Throwable exception) {
        super(message, exception);
    }

    public TuleapApiException(final String message) {
        super(message);
    }

    public TuleapApiException(final Exception exception) {
        super(exception);
    }
}
