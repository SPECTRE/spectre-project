package com.orange.spectre.plugins.adv.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.adv.model.MingleSprintEncoursPojo;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvIndicatorType;
import com.orange.spectre.plugins.adv.util.MingleAdvMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by htxs7368 on 6/6/2017.
 */
@Entity
@DiscriminatorValue("ADV_SPRINT_ENCOURS")
public class SprintEncour extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SprintEncour.class);

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        String mqlRequest = "SELECT name WHERE 'statut du sprint' ='en cours'";
        try {
            JSONArray jsonArray = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {

                JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                ObjectMapper mapper = new ObjectMapper();
                MingleSprintEncoursPojo mingleSprintEncoursPojo = mapper.readValue(jsonObject.toString(), MingleSprintEncoursPojo.class);
                return new StringData(mingleSprintEncoursPojo.getName());
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.AUTRE;
    }

    @Override
    public Enum getType() {
        return MingleAdvIndicatorType.ADV_SPRINT_ENCOURS;
    }

    @Override
    public String getLabel() {
        return MingleAdvIndicatorType.ADV_SPRINT_ENCOURS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TEXT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}
