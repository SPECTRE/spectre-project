package com.orange.spectre.plugins.mingle.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all mingle connection parameters
 */
public enum MingleConnectorParams implements ConnectorParamsInterface {

    login (true, false),
	token_hmac (true, true),
	projet (true, false),
	sprint_courant(false, false);

	private final boolean mandatory;
	private final boolean secured;

	private MingleConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
