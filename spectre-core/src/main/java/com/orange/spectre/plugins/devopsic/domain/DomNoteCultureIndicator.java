package com.orange.spectre.plugins.devopsic.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.devopsic.enumerator.DevopsicIndicatorType;
import com.orange.spectre.plugins.devopsic.util.ComputeDataUtils;
import com.orange.spectre.plugins.devopsic.util.DevopsIcResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for global grade indicator
 */
@Entity
@DiscriminatorValue("DOM_NOTE_CULTURE_INDICATOR")
public class DomNoteCultureIndicator extends AbstractIndicator {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 3108793270529496569L;
	private static final Logger	LOGGER				= LoggerFactory
															.getLogger(DomNoteCultureIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {

		try {
			String devopsicUrl = connectorParams.get("url");
			DevopsIcResponse response = ComputeDataUtils.extract(devopsicUrl);

			if (response != null)
				return new StringData(response.getDom_note_C());

		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval : " + e);
		}

		return null;
	}

	@Override
	public String getLabel() {
		return DevopsicIndicatorType.DOM_NOTE_C.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.PERCENT;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public Enum<DevopsicIndicatorType> getType() {
		return DevopsicIndicatorType.DOM_NOTE_C;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.DEVOPS;
	}
}
