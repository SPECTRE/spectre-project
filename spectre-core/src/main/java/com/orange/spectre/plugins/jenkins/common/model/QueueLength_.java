
package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "hour",
    "min",
    "sec10"
})
public class QueueLength_ {

    @JsonProperty("hour")
    private Hour hour;
    @JsonProperty("min")
    private Min min;
    @JsonProperty("sec10")
    private Sec10 sec10;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The hour
     */
    @JsonProperty("hour")
    public Hour getHour() {
        return hour;
    }

    /**
     *
     * @param hour
     *     The hour
     */
    @JsonProperty("hour")
    public void setHour(Hour hour) {
        this.hour = hour;
    }

    /**
     *
     * @return
     *     The min
     */
    @JsonProperty("min")
    public Min getMin() {
        return min;
    }

    /**
     *
     * @param min
     *     The min
     */
    @JsonProperty("min")
    public void setMin(Min min) {
        this.min = min;
    }

    /**
     *
     * @return
     *     The sec10
     */
    @JsonProperty("sec10")
    public Sec10 getSec10() {
        return sec10;
    }

    /**
     *
     * @param sec10
     *     The sec10
     */
    @JsonProperty("sec10")
    public void setSec10(Sec10 sec10) {
        this.sec10 = sec10;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
