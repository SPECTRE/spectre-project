/**
 * 
 */
package com.orange.spectre.plugins.cbs.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.xssf.model.SharedStringsTable;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * @author vxhv5309
 *
 */
/**
 * See org.xml.sax.helpers.DefaultHandler javadocs
 */
public class SheetHandler extends DefaultHandler {
	private SharedStringsTable					sst;
	private String								lastContents;
	private String								type	= "";
	private boolean								nextIsString;
	private Map<String, String>					currentAppMap;
	private String								currentIndicator;

	private Map<String, Map<String, String>>	data	= new HashMap<String, Map<String, String>>();

	public SheetHandler(SharedStringsTable sst) {
		this.sst = sst;
	}

	public void startElement(String uri, String localName, String name,
			Attributes attributes) throws SAXException {
		// c => cell
		if (name.equals("c")) {
			if (isApplication(attributes.getValue("r")))
				type = "APP";
			else if (isIndicator(attributes.getValue("r")))
				type = "IND";
			else if (isValueOfIndicator(attributes.getValue("r")))
				type = "VAL";
			// Figure out if the value is an index in the SST
			String cellType = attributes.getValue("t");
			if (cellType != null && cellType.equals("s")) {
				nextIsString = true;
			} else {
				nextIsString = false;
			}
		}
		// Clear contents cache
		lastContents = "";
	}

	public void endElement(String uri, String localName, String name)
			throws SAXException {
		// Process the last contents as required.
		// Do now, as characters() may be called more than once
		if (nextIsString) {
			int idx = Integer.parseInt(lastContents);
			lastContents = new XSSFRichTextString(sst.getEntryAt(idx))
					.toString();
			nextIsString = false;
		}

		// v => contents of a cell
		// Output after we've seen the string contents
		if (name.equals("v")) {
			handleContent(lastContents);
		}
	}

	public void characters(char[] ch, int start, int length)
			throws SAXException {
		lastContents += new String(ch, start, length);
	}

	private void handleContent(String content) {
		switch (type) {
		case "APP":
			data.put(content, new HashMap<String, String>());
			currentAppMap = data.get(content);
			break;
		case "IND":
			currentIndicator = content;
			break;
		case "VAL":
			currentAppMap.put(currentIndicator, content);
			break;
		default:
			break;
		}
		type = "";
	}

	private boolean isApplication(String cellId) {
		if (cellId != null) {
			String numeric = cellId.replaceAll("\\D+", "");
			String alpha = cellId.replaceAll("[^A-Z]", "");
			int row = Integer.parseInt(numeric);
			if ("B".equals(alpha) && row >= 14)
				return true;
		}
		return false;
	}

	private boolean isIndicator(String cellId) {
		if (cellId != null) {
			String numeric = cellId.replaceAll("\\D+", "");
			String alpha = cellId.replaceAll("[^A-Z]", "");
			int row = Integer.parseInt(numeric);
			if ("D".equals(alpha) && row >= 14)
				return true;
		}
		return false;
	}

	private boolean isValueOfIndicator(String cellId) {
		if (cellId != null) {
			String numeric = cellId.replaceAll("\\D+", "");
			String alpha = cellId.replaceAll("[^A-Z]", "");
			int row = Integer.parseInt(numeric);
			if ("E".equals(alpha) && row >= 14)
				return true;
		}
		return false;
	}

	/**
	 * @return the data
	 */
	public Map<String, Map<String, String>> getData() {
		return data;
	}
}