package com.orange.spectre.plugins.sacre.common.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.sacre.domain.DosStateFixeIndicator;
import com.orange.spectre.plugins.sacre.domain.DosStateMobileIndicator;
import com.orange.spectre.plugins.sacre.domain.FluxAmontStatutIndicator;
import com.orange.spectre.plugins.sacre.domain.FluxAvalStatutIndicator;
import com.orange.spectre.plugins.sacre.domain.FluxRCEASTIIndicator;
import com.orange.spectre.plugins.sacre.domain.RestrictiveRetablissementIndicator;

public enum SacreIndicatorType {

	DOS_STATE_FIXE(DosStateFixeIndicator.class),
	DOS_STATE_MOBILE(DosStateMobileIndicator.class),
	FLUX_AMONT_STATUT(FluxAmontStatutIndicator.class),
	FLUX_AVAL_STATUT(FluxAvalStatutIndicator.class),
	FLUX_RCE_ASTI_STATUT(FluxRCEASTIIndicator.class),
	NOMBRE_MESURE_DISE(RestrictiveRetablissementIndicator.class);

	private Class<? extends AbstractIndicator> clazz;

	SacreIndicatorType(Class<? extends AbstractIndicator> clazz) {
		this.clazz = clazz;
	}

	/**
	 * @return
	 */
	public AbstractIndicator createIndicator() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(clazz + " has no default constructor");
		}
	}

	public String getLabel() {
		switch (this.name()) {
		case "DOS_STATE_FIXE":
			return "Dossiers fixes";
		case "DOS_STATE_MOBILE":
			return "Dossiers mobiles";
		case "FLUX_AMONT_STATUT":
			return "Flux amont";
		case "FLUX_AVAL_STATUT":
			return "Flux avals";
		case "FLUX_RCE_ASTI_STATUT":
			return "Flux RCE et ASTI";
		case "NOMBRE_MESURE_DISE":
			return "Mesures sur DISE";
		default:
			return null;
		}
	}

}
