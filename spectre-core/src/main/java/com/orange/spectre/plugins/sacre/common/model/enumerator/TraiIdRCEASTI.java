package com.orange.spectre.plugins.sacre.common.model.enumerator;

public enum TraiIdRCEASTI {
	
	ASTI(52),
	GROUPE_RCE(77);
	
	
	private int trai_id;
	
	TraiIdRCEASTI(int trai) {
		trai_id = trai;
	}
	
	public int getTrai() {
		return trai_id;
	}
	
	public static boolean contains(int trai){
		for(TraiIdRCEASTI mytrai : TraiIdRCEASTI.values()) {
			if (mytrai.getTrai() == trai) {
				return true;
			}
		}
		return false;
	}
	
}