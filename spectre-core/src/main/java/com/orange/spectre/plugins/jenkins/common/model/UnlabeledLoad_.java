
package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "busyExecutors",
    "queueLength",
    "totalExecutors"
})
public class UnlabeledLoad_ {

    @JsonProperty("busyExecutors")
    private BusyExecutors__ busyExecutors;
    @JsonProperty("queueLength")
    private QueueLength queueLength;
    @JsonProperty("totalExecutors")
    private TotalExecutors totalExecutors;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public BusyExecutors__ getBusyExecutors() {
        return busyExecutors;
    }

    /**
     *
     * @param busyExecutors
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public void setBusyExecutors(BusyExecutors__ busyExecutors) {
        this.busyExecutors = busyExecutors;
    }

    /**
     *
     * @return
     *     The queueLength
     */
    @JsonProperty("queueLength")
    public QueueLength getQueueLength() {
        return queueLength;
    }

    /**
     *
     * @param queueLength
     *     The queueLength
     */
    @JsonProperty("queueLength")
    public void setQueueLength(QueueLength queueLength) {
        this.queueLength = queueLength;
    }

    /**
     *
     * @return
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public TotalExecutors getTotalExecutors() {
        return totalExecutors;
    }

    /**
     *
     * @param totalExecutors
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public void setTotalExecutors(TotalExecutors totalExecutors) {
        this.totalExecutors = totalExecutors;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
