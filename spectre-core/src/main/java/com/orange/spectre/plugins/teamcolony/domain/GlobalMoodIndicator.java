package com.orange.spectre.plugins.teamcolony.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.teamcolony.model.Replies;
import com.orange.spectre.plugins.teamcolony.model.Reports;
import com.orange.spectre.plugins.teamcolony.model.TeamColonyResponse;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyConnectorParams;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyIndicatorType;
import com.orange.spectre.plugins.teamcolony.utils.TeamColonyResponseUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;

/**
 * Jpa entity for global mood
 */
@Entity
@DiscriminatorValue("TEAMCOLONY_GLOBAL_MOOD")
public class GlobalMoodIndicator extends AbstractIndicator {


    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalMoodIndicator.class);
    private static final String FORMAT = "yyyy-MM-dd";
    private static final String TEAMCOLONY_URL = "http://app.teamcolony.com/teams/";


    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {

            String token = connectorParams.get(TeamColonyConnectorParams.token.name());
            String teamName = connectorParams.get(TeamColonyConnectorParams.team_name.name());
            SimpleDateFormat formater = new SimpleDateFormat(FORMAT);
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -1);

            // url TEAMCOLONY
            String teamcolony = TEAMCOLONY_URL + teamName + "/reports/from/" + formater.format(cal.getTime()) + "/to/" + formater.format(cal.getTime()) + "/export.json?token=" + token + "";

            // call TEAMCOLONY
            RestTemplate restTemplate = null;
            if (ProxyConfig.getInstance().isUseProxy()) {
                SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
                Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort())));
                requestFactory.setProxy(proxy);
                restTemplate = new RestTemplate(requestFactory);
            } else {
                restTemplate = new RestTemplate();
            }

            TeamColonyResponse teamcolonyResponse = restTemplate.getForObject(teamcolony, TeamColonyResponse.class);

            List<String> moods = new ArrayList<String>();
            if (teamcolonyResponse != null) {
                for (Reports reports : teamcolonyResponse.getTeam().getReports()) {
                    for (Replies replies : reports.getReplies()) {
                        moods.add(replies.getBody());
                    }
                }

                return TeamColonyResponseUtils.getMoodGlobal(moods);
            }
            return null;

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return TeamColonyIndicatorType.GLOBAL_MOOD_TYPE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.MOOD;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Equipe";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre l'humeur moyenne de l'équipe";
    }

    @Override
    public String getDefinition() {
        return "Tous les jours, les membres de l'équipe soumettent leur humeur à Teamcolony qui réalise un moyenne afin de connaitre l'humeur de l'équipe au jours le jour.";
    }

    @Override
    public String getOrigin() {
        return "Teamcolony";
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return TeamColonyIndicatorType.GLOBAL_MOOD_TYPE;
    }


}

