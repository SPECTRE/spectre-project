package com.orange.spectre.plugins.jenkins.faas.domain;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.common.exception.JenkinsException;
import com.orange.spectre.plugins.jenkins.common.model.HealthReport;
import com.orange.spectre.plugins.jenkins.common.model.Job_;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasConnectorParams;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasIndicatorType;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for health report build
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_FAAS_HEALTH_REPORT")
public class HealthReportBuildIndicatorFaas extends AbstractIndicator {

    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getLabel(){
        return JenkinsFaasIndicatorType.FAAS_HEALTH_REPORT_BUILD.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier très rapidement si un job est instable";
    }

    @Override
    public String getDefinition() {
        return "Il s'agit de connaitre le pourcentage de succès d'un projet lors de ses cinq derniers lancements";
    }

    @Override
    public String getOrigin() {
        return "Jenkins FAAS";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return JenkinsFaasIndicatorType.FAAS_HEALTH_REPORT_BUILD;
    }

    /**
     *
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {
            String outputFlux = JenkinsFaasFluxUtil.getFluxJson(JenkinsFaasFluxUtil.buildUrlHealthReport(connectorParams));

            return this.computeData(connectorParams, outputFlux);

        } catch (Exception e) {

            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
        }
        return null;
    }

    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws JenkinsException {
        String jobName = (String) connectorParams.get(JenkinsFaasConnectorParams.nom_job.name());

        ObjectMapper mapper = new ObjectMapper();
        IntegerData data = new IntegerData();
        Integer score = null;

        if (jobName != null && jobName.length() > 0) {

            Job_ responseJob = null;

            try {
                responseJob = mapper.readValue(flux, Job_.class);
                // Récupération du score du job
                for (HealthReport job : responseJob.getHealthReport()) {
                    score = job.getScore();
                }
                if (score == null){
                    throw new JenkinsException ("job not build");
                } else {
                    data.setData((int)score);
                }
            } catch (JsonMappingException e) {
                LOGGER.error("JSON Mapping Exception with parameters " + connectorParams.toString());

            } catch (JsonParseException e) {
                LOGGER.error("JSON Parse Exception with parameters " + connectorParams.toString());

            } catch (IOException e) {
                LOGGER.error("IO Exception with parameters " + connectorParams.toString() );
            }

        }

        return data;
    }
}



