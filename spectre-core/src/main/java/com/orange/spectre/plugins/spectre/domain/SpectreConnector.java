package com.orange.spectre.plugins.spectre.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.spectre.model.enumerator.SpectreConnectorParams;
import com.orange.spectre.plugins.spectre.model.enumerator.SpectreIndicatorType;

/**
 * Created by epeg7421 on 03/10/2016.
 */
@Entity
@DiscriminatorValue("SPECTRE")
public class SpectreConnector extends AbstractConnector {

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	@Override
	public List<String> getAvailableIndicators() {
		return Arrays.stream(SpectreIndicatorType.values()).map(Enum::name).collect(Collectors.toList());
	}

	@Override
	public String getType() {
		return "spectre";
	}

	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (SpectreConnectorParams param : SpectreConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				requiredParams.add(newParam);
			}
		}
		return requiredParams;
	}

	/**
	 *
	 * @param indicatorType
	 * @return
	 */
	@Override
	public AbstractIndicator createIndicator(String indicatorType) {
		return SpectreIndicatorType.valueOf(indicatorType).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		return true;
	}
}
