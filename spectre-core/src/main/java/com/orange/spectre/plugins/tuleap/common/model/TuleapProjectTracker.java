package com.orange.spectre.plugins.tuleap.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 27/03/2017.
 */
public class TuleapProjectTracker {

    private String id;
    @JsonProperty("item_name")
    private String itemName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }
}
