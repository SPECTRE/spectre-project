package com.orange.spectre.plugins.jira.model.enumerator;

/**
 * Created by epeg7421 on 07/04/17.
 */

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all qc connection parameters
 */
public enum JiraConnectorParams implements ConnectorParamsInterface {

	login(true, false),
	password(true, true),
	url(true, false),
	projet(true, false),
	release(false, false);

	private final boolean mandatory;
	private final boolean secured;

	private JiraConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}

}
