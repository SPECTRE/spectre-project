package com.orange.spectre.plugins.jira.util;

import com.orange.spectre.plugins.common.util.ProxyConfig;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;

/**
 * Created by epeg7421 on 13/04/17.
 */
public class JiraUtils {

	/**
	 * Get Connection
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static HttpResponse getJiraConnectionData(String username, String password, String url) throws Exception {
		try {

			CloseableHttpClient httpClient;
			HttpGet httpGet = new HttpGet(url);

			// create a string that lookes like:
			// "Basic ((username:password)<as bytes>)<64encoded>"
			String usernamePassword = username + ":" + password;
			String credEncodedString = "Basic " + StringUtils.newStringUtf8(Base64.encodeBase64(usernamePassword.getBytes()));

			httpGet.setHeader(HttpHeaders.AUTHORIZATION, credEncodedString);
			httpGet.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
			httpGet.setHeader(HttpHeaders.ACCEPT, "application/json");

			// Complete url with Proxy Settings
			if (ProxyConfig.getInstance().isUseProxy()) {
				HttpHost proxy = new HttpHost(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort()));
				httpClient = HttpClients.custom().setProxy(proxy).build();
			} else {
				httpClient = HttpClients.custom().build();
			}
			HttpResponse response = null;
			response = httpClient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return response;
			} else {
				throw new Exception("Bad response from tuleap token api");
			}
		} catch (IOException e) {
			throw new Exception("Bad response from tuleap token api");
		}
	}

}
