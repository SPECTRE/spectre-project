package com.orange.spectre.plugins.sonar.common.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Sonar indicator bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MSR {

	private String key;
	private String val;
	private String frmt_val;
	

	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getVal() {
		return val;
	}
	public void setVal(String val) {
		this.val = val;
	}
	public String getFrmt_val() {
		return frmt_val;
	}
	public void setFrmt_val(String frmt_val) {
		this.frmt_val = frmt_val;
	}

	@Override
	public String toString() {
		return "MSR [key=" + key + ", val=" + val + ", frmt_val=" + frmt_val + "]";
	}

}
