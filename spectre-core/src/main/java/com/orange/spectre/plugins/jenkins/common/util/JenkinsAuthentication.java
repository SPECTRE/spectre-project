package com.orange.spectre.plugins.jenkins.common.util;

import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 *
 * @author nblg6551
 */
public class JenkinsAuthentication {
    private CloseableHttpClient httpClient;

    private HttpClientContext localContext ;

    /**
     *
     * @param params
     * @return
     * @throws IOException
     */
    public String scrape(Map <String, String> params) throws IOException {
        HttpResponse response;

        URI uri = URI.create(params.get(JenkinsConnectorParams.url.name()));
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(new AuthScope(uri.getHost(), uri.getPort()), new UsernamePasswordCredentials(params.get(JenkinsConnectorParams.nom_utilisateur.name()), params.get(JenkinsConnectorParams.mot_de_passe.name())));
        // Create AuthCache instance
        AuthCache authCache = new BasicAuthCache();
        // Generate BASIC scheme object and add it to the local auth cache
        BasicScheme basicAuth = new BasicScheme();
        authCache.put(host, basicAuth);
        httpClient = HttpClients.custom().setDefaultCredentialsProvider(credsProvider).build();
        HttpGet httpGet = new HttpGet(uri);
        // Add AuthCache to the execution context
        localContext = HttpClientContext.create();
        localContext.setAuthCache(authCache);


        response = httpClient.execute(host, httpGet, localContext);
        if (response.getStatusLine().getStatusCode() != 200) {
            return null;
        }
        return EntityUtils.toString(response.getEntity());

    }

    /**
     *
     * @param http
     * @param params
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public String request(String http,Map <String, String> params) throws ClientProtocolException, IOException {
//        URI uri = URI.create(http);
        URI uri = URI.create(http);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpGet httpGet = new HttpGet (uri);

        //httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials((String)params.get("nomutilisateur"), (String)params.get("motdepasse")), "UTF-8", false));
        HttpResponse response = httpClient.execute(host,httpGet,this.localContext);
        return EntityUtils.toString(response.getEntity());
    }
}
