package com.orange.spectre.plugins.mingle.util;

import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ludovic on 03/10/2016.
 */
public class HmacAuth {

    private final String login;
    private final String key;


    public HmacAuth(String login, String key) {
        this.login = login;
        this.key = key;
    }

    public String getLogin() {
        return this.login;
    }

    public String signCanonicalString(String canonicalString) {
        try {
            Mac mac = Mac.getInstance("HmacSHA1");
            SecretKeySpec secret = new SecretKeySpec(key.getBytes(), mac.getAlgorithm());
            mac.init(secret);
            byte[] digest = mac.doFinal(canonicalString.getBytes());
            byte[] result = Base64.encodeBase64(digest);
            return new String(result);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
