package com.orange.spectre.plugins.teamcolony.utils;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.MoodData;
import com.orange.spectre.core.model.enumerator.MoodEnum;

import java.util.List;

/**
 * Created by WKNL8600 on 26/04/2016.
 * TeamColony utils
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public final  class TeamColonyResponseUtils {

    /**
     * Compute mood from teamcolony response
     * @param moods Teamcolony response bean
     * @return Average team mood
     */
    public static AbstractData getMoodGlobal(List<String> moods) {
        int happymood = 0;
        int sadmood = 0;
        int normalmood = 0;
        if (moods.size() > 0) {
            for (int j = 0; j < moods.size(); ++j) {
                if (moods.get(j).contains("#happy")) {
                    happymood++;
                } else if (moods.get(j).contains("#sad")) {
                    sadmood++;
                } else if (moods.get(j).contains("#normal")) {
                    normalmood++;
                }

            }
            if(happymood+normalmood+sadmood!=0){
                if ((happymood > sadmood) && (happymood > normalmood)) {
                    return new MoodData(MoodEnum.HAPPY);
                } else if ((sadmood > happymood) && (sadmood > normalmood)) {
                    return new MoodData(MoodEnum.SAD);
                } else {
                    return new MoodData(MoodEnum.NORMAL);
                }
            }
        }
        return null;
    }


}
