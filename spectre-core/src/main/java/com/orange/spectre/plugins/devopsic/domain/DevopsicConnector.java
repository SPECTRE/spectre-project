package com.orange.spectre.plugins.devopsic.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.devopsic.enumerator.DevopsicConnectorParams;
import com.orange.spectre.plugins.devopsic.enumerator.DevopsicIndicatorType;
import com.orange.spectre.plugins.devopsic.util.ComputeDataUtils;
import com.orange.spectre.plugins.devopsic.util.DevopsIcResponse;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of DevopsIc connector
 */
@Entity
@DiscriminatorValue("DEVOPSIC")
public class DevopsicConnector extends AbstractConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(DevopsicConnector.class);

	/**
	 *
	 */
	private static final long	serialVersionUID	= -1238627826968648209L;
	/**
	 * All connector connection params
	 */
	@Transient
	private List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all devopsic indicators
	 * @return All devopsic indicators
	 */
	public List<String> getAvailableIndicators() {
		return Arrays.stream(DevopsicIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all devopsic connection parameters
	 * @return DevopsIc connection parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if(requiredParams == null){
			requiredParams = new ArrayList<>();
			for(DevopsicConnectorParams param : DevopsicConnectorParams.values()){
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return requiredParams;
	}

	/**
	 * Get devopsic string type
	 * @return String type
	 */
	@Override
	public String getType() {
		return "devopsic";
	}

	/**
	 * Create a devopsic indicator from an indicator type
	 * @param s indicator type
	 * @return A new devopsic indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return DevopsicIndicatorType.valueOf(s).createIndicator();
	}


	/**
	 * Test access to the connector
	 *
	 * @param connectorParams weather connection parameters
	 * @return True if connection ok
	 */
	@Override
	public boolean testConnector(Map<String, String> connectorParams) {

		String kpiUrl = connectorParams.get(DevopsicConnectorParams.url.toString());

		try{
			LOGGER.debug("test connector calling extract function");
			DevopsIcResponse DevopsIcResponse = ComputeDataUtils.extract(kpiUrl);

			if (DevopsIcResponse != null) {
				return true;
			}else{
				return false;
			}
		}catch (Exception e){
			return false;
		}

	}
}
