package com.orange.spectre.plugins.qc.util;

/**
 * Created by ludovic on 29/08/2016.
 */
public final class QCConstant {

    public static final String HP_ALM_URL = "https://almdtsi.itn.ftgroup/qcbin";

    public static final String HP_ALM_IS_AUTHENTICATED = "rest/is-authenticated";
    public static final String HP_ALM_SIGN_IN = "api/authentication/sign-in";
    public static final String HP_ALM_SIGN_OUT = "api/authentication/sign-out";
    public static final String HP_ALM_AUTH_POINT = "/authenticate";
    public static final String HP_ALM_REST_DOMAIN = "rest/domains/";
    public static final String HP_ALM_PROJECTS = "/projects/";
    public static final String HP_ALM_ENTITY_DEFECT = "defects";
    public static final String HP_ALM_ENTITY_RELEASE = "releases";

    public static final String HP_ALM_URL_QUERY_BY_NAME = "fields=id,name&query={name['%s']}";
    public static final String HP_ALM_URL_QUERY_DETECTED_IN_RELEASE = "detected-in-rel[";
    public static final String HP_ALM_URL_POINT_VIRGULE = ";";
    public static final String HP_ALM_URL_END_CROCHET = "]";
    public static final String HP_ALM_URL_BEGIN_ACCOLADE = "{";
    public static final String HP_ALM_URL_END_ACCOLADE = "}";
    //BG_STATUS Typical values are: Open, Fixed, Closed, New, Rejected, and Reopen
    public static final String HP_ALM_DEFAULT_URL = "page-size=max";//&fields=x&query=y;
    public static final String HP_ALM_DEFAULT_URL_CLOSED = "page-size=max&fields=status&query={status['Closed' or '09-Closed']";
    public static final String HP_ALM_DEFAULT_URL_FIELD_CLOSED = "&fields=status";
    public static final String HP_ALM_DEFAULT_URL_QUERY_CLOSED = "&query={status['Closed' or '09-Closed']";
    public static final String HP_ALM_DEFAULT_URL_OPENED = "page-size=max&fields=status&query={status[not ('Closed' or '09-Closed')]";
    public static final String HP_ALM_DEFAULT_URL_FIELD_OPENED = "&fields=status";
    public static final String HP_ALM_DEFAULT_URL_QUERY_OPENED = "&query={status[not ('Closed' or '09-Closed')]";
    //BG_SEVERITY Typical values are: 1-Low, 2-Medium, 3-High, 4-Very High, 5-Urgent
    //BG_PRIORITY Typical values are: 1-Low, 2-Medium, 3-High, 4-Very High, 5-Urgent
    public static final String HP_ALM_DEFAULT_URL_SEVERITY_MINOR = "page-size=max&fields=severity&query={severity['1-Low' or '2-Medium' or 'G2-Minor']";
    public static final String HP_ALM_DEFAULT_URL_FIELD_SEVERITY_MINOR = "&fields=severity";
    public static final String HP_ALM_DEFAULT_URL_QUERY_SEVERITY_MINOR = "&query={severity['1-Low' or '2-Medium' or 'G2-Minor']";
    public static final String HP_ALM_DEFAULT_URL_SEVERITY_MAJOR = "page-size=max&fields=severity&query={severity['3-High' or '4-Very High' or 'G1-Major']";
    public static final String HP_ALM_DEFAULT_URL_FIELD_SEVERITY_MAJOR = "&fields=severity";
    public static final String HP_ALM_DEFAULT_URL_QUERY_SEVERITY_MAJOR = "&query={severity['3-High' or '4-Very High' or 'G1-Major']";
    public static final String HP_ALM_DEFAULT_URL_SEVERITY_CRITICAL = "page-size=max&fields=severity&query={severity['5-Urgent' or 'G0-Critical']";
    public static final String HP_ALM_DEFAULT_URL_FIELD_SEVERITY_CRITICAL = "&fields=severity";
    public static final String HP_ALM_DEFAULT_URL_QUERY_SEVERITY_CRITICAL = "&query={severity['5-Urgent' or 'G0-Critical']";


}
