package com.orange.spectre.plugins.sacre.common.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.DossierMOBILE;
import com.orange.spectre.plugins.sacre.common.model.ISacreSubIndicator;

import java.io.IOException;
import java.util.Map;

public class DosReveilMobileIndicator implements ISacreSubIndicator {

    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        IntegerData data = new IntegerData();
        ObjectMapper mapper = new ObjectMapper();
        int nbDosReveil = 0;
        Data myData = mapper.readValue(flux, Data.class);


        for (DossierMOBILE trai : myData.getData().getDossierMOBILE()) {
            if (trai.getReveil() != null && trai.getReveil() != "") {
                nbDosReveil += Integer.parseInt(trai.getReveil());
            }

        }

        data.setData(nbDosReveil);
        return data;
    }

    @Override
    public String getLabel() {
        return "Dossiers reveillés";
    }
}
