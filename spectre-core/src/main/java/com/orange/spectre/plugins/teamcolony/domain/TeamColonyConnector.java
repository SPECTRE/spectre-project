package com.orange.spectre.plugins.teamcolony.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.teamcolony.model.TeamColonyResponse;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyConnectorParams;
import com.orange.spectre.plugins.teamcolony.model.enumerator.TeamColonyIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Jpa entity of TeamColony connector
 */
@Entity
@DiscriminatorValue("TEAMCOLONY")
public class TeamColonyConnector extends AbstractConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(TeamColonyConnector.class);
	private static final String TEAMCOLONY_URL = "http://app.teamcolony.com/teams/";

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all TeamColony indicators
	 *
	 * @return All TeamColony indicators
	 */
	@Override
	public List<String> getAvailableIndicators() {
		return Arrays.stream(TeamColonyIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all TeamColony connection parameters
	 *
	 * @return TeamColony connection parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (TeamColonyConnectorParams param : TeamColonyConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return requiredParams;
	}

	/**
	 * Get TeamColony string type
	 *
	 * @return String type
	 */
	@Override
	public String getType() {
		return "teamcolony";
	}

	/**
	 * Create a TeamColony indicator from an indicator type
	 *
	 * @param s
	 *            indicator type
	 * @return A new TeamColony indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {

		return TeamColonyIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		String token = connectorParams.get(TeamColonyConnectorParams.token.name());
		String teamName = connectorParams.get(TeamColonyConnectorParams.team_name.name());
		String teamcolonyUrl = TEAMCOLONY_URL + teamName + "/reports/from/today/to/today/export.json?token=" + token;

		RestTemplate restTemplate = null;

		if (ProxyConfig.getInstance().isUseProxy()) {
			SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
			Proxy proxy= new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort())));
			requestFactory.setProxy(proxy);
			restTemplate = new RestTemplate(requestFactory);
		} else {
			restTemplate = new RestTemplate();
		}


		ResponseEntity<TeamColonyResponse> response = null;

		try {
			response = restTemplate.getForEntity(teamcolonyUrl, TeamColonyResponse.class);
		} catch (RestClientException e) {
			LOGGER.warn("Erreur lors du test de connection TeamColony sur {} : {}", teamcolonyUrl, e.getMessage());
			return false;
		}

		LOGGER.info("Test de connexion HTTP_STATUS {} sur {}", response.getStatusCode().value(), teamcolonyUrl);
		return true;
	}
}
