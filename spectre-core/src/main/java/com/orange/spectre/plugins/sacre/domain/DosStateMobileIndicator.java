package com.orange.spectre.plugins.sacre.domain;

import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sacre.common.model.ISacreSubIndicator;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sacre.common.model.DosClosMobileIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosOpenMobileIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosReveilMobileIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosSommeilMobileIndicator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@DiscriminatorValue("DOS_STATE_MOBILE")
public class DosStateMobileIndicator extends SacreIndicator {

    /**
     *
     */
    private static final long serialVersionUID = 7206471458959697107L;

    public DosStateMobileIndicator() {
        super();
    }

    @Override
    public IndicatorFamily getFamily() {
        // TODO Auto-generated method stub
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return SacreIndicatorType.DOS_STATE_MOBILE;
    }

    @Override
    public String getLabel() {
        return SacreIndicatorType.DOS_STATE_MOBILE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public TupleData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        TupleData data = new TupleData();

        List<ISacreSubIndicator> sacreIndicatorListe = new ArrayList<ISacreSubIndicator>();
        sacreIndicatorListe.add(new DosOpenMobileIndicator());
        sacreIndicatorListe.add(new DosSommeilMobileIndicator());
        sacreIndicatorListe.add(new DosReveilMobileIndicator());
        sacreIndicatorListe.add(new DosClosMobileIndicator());

        List<String> listData = new ArrayList<String>();
        List<String> listLabel = new ArrayList<String>();
        for (ISacreSubIndicator oneSacreIndicator : sacreIndicatorListe) {
            listData.add(String.valueOf(oneSacreIndicator.computeData(connectorParams, flux).getData()));
            listLabel.add(oneSacreIndicator.getLabel());
        }

        data.setData(listData);
        data.setLabel(listLabel);

        return data;

    }

    @Override
    public String getDomain() {
        return "Metiers";
    }

    @Override
    public String getPurpose() {
        return "Donne les informations sur les changements d'états des dossiers mobiles";
    }

    @Override
    public String getDefinition() {
        return "Retourne les changements sur les dossiers mobiles depuis hier minuit jusqu'à maintenant";
    }

    @Override
    public String getOrigin() {
        return "SACRE";
    }

}
