package com.orange.spectre.plugins.tuleap.scrum.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by ludovic on 25/04/2017.
 */
public class Sprint {

    private String id;
    private String label;
    @JsonProperty("start_date")
    private String startDate;
    @JsonProperty("end_date")
    private String endDate;
    @JsonProperty("number_days_since_start")
    private Integer passedDays;
    @JsonProperty("number_days_until_end")
    private Integer remainingDays;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getPassedDays() {
        return passedDays;
    }

    public void setPassedDays(Integer passedDays) {
        this.passedDays = passedDays;
    }

    public Integer getRemainingDays() {
        return remainingDays;
    }

    public void setRemainingDays(Integer remainingDays) {
        this.remainingDays = remainingDays;
    }

    public boolean isCurrent() {

        try {
            if (this.startDate != null && this.endDate != null) {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                Date start = formatter.parse(this.startDate);
                Date end = formatter.parse(this.endDate);

                Date current = new Date();

                return start.compareTo(current) <= 0 && current.compareTo(end) <= 0;
            } else {
                return false;
            }
        } catch (ParseException e) {
            return false;
        }
    }
}
