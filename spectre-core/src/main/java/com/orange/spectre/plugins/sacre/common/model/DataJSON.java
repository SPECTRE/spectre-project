package com.orange.spectre.plugins.sacre.common.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "listeTRAI",
    "Dossier_FIXE",
    "Dossier_MOBILE"
})
public class DataJSON {

    @JsonProperty("listeTRAI")
    private List<ListeTRAI> listeTRAI = new ArrayList<ListeTRAI>();
    @JsonProperty("Dossier_FIXE")
    private List<DossierFIXE> dossierFIXE = new ArrayList<DossierFIXE>();
    @JsonProperty("Dossier_MOBILE")
    private List<DossierMOBILE> dossierMOBILE = new ArrayList<DossierMOBILE>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The listeTRAI
     */
    @JsonProperty("listeTRAI")
    public List<ListeTRAI> getListeTRAI() {
        return listeTRAI;
    }

    /**
     * 
     * @param listeTRAI
     *     The listeTRAI
     */
    @JsonProperty("listeTRAI")
    public void setListeTRAI(List<ListeTRAI> listeTRAI) {
        this.listeTRAI = listeTRAI;
    }

    /**
     * 
     * @return
     *     The dossierFIXE
     */
    @JsonProperty("Dossier_FIXE")
    public List<DossierFIXE> getDossierFIXE() {
        return dossierFIXE;
    }

    /**
     * 
     * @param dossierFIXE
     *     The Dossier_FIXE
     */
    @JsonProperty("Dossier_FIXE")
    public void setDossierFIXE(List<DossierFIXE> dossierFIXE) {
        this.dossierFIXE = dossierFIXE;
    }

    /**
     * 
     * @return
     *     The dossierMOBILE
     */
    @JsonProperty("Dossier_MOBILE")
    public List<DossierMOBILE> getDossierMOBILE() {
        return dossierMOBILE;
    }

    /**
     * 
     * @param dossierMOBILE
     *     The Dossier_MOBILE
     */
    @JsonProperty("Dossier_MOBILE")
    public void setDossierMOBILE(List<DossierMOBILE> dossierMOBILE) {
        this.dossierMOBILE = dossierMOBILE;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}

