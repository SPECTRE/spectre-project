
package com.orange.spectre.plugins.trello.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.trello.domain.GlobalTaskStateIndicator;
import com.orange.spectre.plugins.trello.domain.GlobalTaskStatePercentIndicator;
import com.orange.spectre.plugins.trello.domain.AllTasksNbIndicator;


/**
 * Enumerator of all available trello indicators
 */
public enum TrelloIndicatorType {


    GLOBAL_TASK_STATE(GlobalTaskStateIndicator.class),
    ALL_TASKS_NB(AllTasksNbIndicator.class),
    GLOBAL_TASK_STATE_PERCENT(GlobalTaskStatePercentIndicator.class);

    /**
     * Current Trello indicator class
     */
    private Class<? extends AbstractIndicator> clazz;
    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    TrelloIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }


    /**
     * Indicator instantiation from it's type
     * @return An Trello indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }
    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "GLOBAL_TASK_STATE":
                return "Etat des taches";
            case "ALL_TASKS_NB":
                return "Nombre total de taches";
            case "GLOBAL_TASK_STATE_PERCENT":
                return "Etat des taches en pourcentages ";
            default: return null;
        }
    }
}


