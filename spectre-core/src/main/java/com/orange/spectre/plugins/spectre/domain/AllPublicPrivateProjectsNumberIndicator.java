package com.orange.spectre.plugins.spectre.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.service.SpectrePluginsService;
import com.orange.spectre.plugins.spectre.model.enumerator.SpectreIndicatorType;

/**
 * Created by epeg7421 on 14/10/2016.
 */
@Entity
@Configurable
@DiscriminatorValue("SPECTRE_ALL_PUBLIC_PRIVATE_PROJECT")
public class AllPublicPrivateProjectsNumberIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(AllPublicPrivateProjectsNumberIndicator.class);

	@Transient
	@Inject
	private SpectrePluginsService spectrePluginsService;

	/**
	 * Fetch data.
	 *
	 * @param connectorParams
	 *            Contains all the params such as URL, token, ... to connect to third providers.
	 * @return The fetched data.
	 * @throws FetchDataException
	 *             indicators must throw their exceptions in this custom core consumed exception.
	 */
	@Override
	public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {
		try {
			TupleData tuple = new TupleData();
			tuple.setMeasure(IndicatorMeasure.NUMERIC.name());
			// Calculate
			Long dataAll = spectrePluginsService.countAllProjects();
			Long dataPublic = spectrePluginsService.countAllProjectsByReachableTrue();
			Long dataPrivate = spectrePluginsService.countAllProjectsByReachableFalse();
			// Populate
			List<String> datas = new ArrayList<>();
			List<String> labels = new ArrayList<>();
			datas.add(dataAll.toString());
			labels.add(SpectreIndicatorType.PROJECT_NB.getLabel());
			datas.add(dataPublic.toString());
			labels.add(SpectreIndicatorType.PUBLIC_PROJECT_NB.getLabel());
			datas.add(dataPrivate.toString());
			labels.add(SpectreIndicatorType.PRIVATE_PROJECT_NB.getLabel());
			// Add
			tuple.setData(datas);
			tuple.setLabel(labels);

			return tuple;

		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
		}
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.MANAGEMENT;
	}

	@Override
	public Enum getType() {
		return SpectreIndicatorType.ALL_PUBLIC_PRIVATE_PROJECT_NB;
	}

	@Override
	public String getLabel() {
		return SpectreIndicatorType.ALL_PUBLIC_PRIVATE_PROJECT_NB.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.TUPLE;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}
}
