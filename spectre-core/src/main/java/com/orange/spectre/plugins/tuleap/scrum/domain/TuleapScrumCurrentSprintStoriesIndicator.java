package com.orange.spectre.plugins.tuleap.scrum.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.tuleap.common.model.*;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapUtils;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumConnectorParams;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumIndicatorType;
import com.orange.spectre.plugins.tuleap.scrum.model.Sprint;
import com.orange.spectre.plugins.tuleap.scrum.model.Story;
import com.orange.spectre.plugins.tuleap.scrum.utils.TuleapScrumUtils;
import org.apache.commons.collections.map.HashedMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ludovic on 03/04/2017.
 */
@Entity
@DiscriminatorValue("TULEAP_SCRUM_CURRENT_SPRINT_STORIES")
public class TuleapScrumCurrentSprintStoriesIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TuleapScrumCurrentSprintStoriesIndicator.class);


    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {

            String login = connectorParams.get(TuleapScrumConnectorParams.login.name());
            String password = connectorParams.get(TuleapScrumConnectorParams.mot_de_passe.name());
            String project = connectorParams.get(TuleapScrumConnectorParams.nom_projet.name());

            TuleapTokenResponse tuleapResponse = TuleapUtils.getTuleapConnectionData(login, password);
            String tuleapProjectId = TuleapUtils.getTuleapProjectIdByName(tuleapResponse.getToken(), tuleapResponse.getUserId(), project);

            TuleapPlanning tuleapScrumPlannings = TuleapScrumUtils.getTuleapScrumPlanning(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapProjectId);
            TuleapSprint[] sprints = TuleapScrumUtils.getTuleapScrumSprints(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapScrumPlannings.getId());

            for(TuleapSprint tuleapSprint : sprints) {
                Sprint sprint = TuleapScrumUtils.getTuleapScrumSprint(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapSprint.getId());
                boolean isCurrent = sprint.isCurrent();
                if (isCurrent) {

                    TuleapMilestoneStory[] milestoneStories = TuleapScrumUtils.getTuleapStories(tuleapResponse.getToken(), tuleapResponse.getUserId(), sprint.getId());

                    List<Story> stories = new ArrayList<>();
                    for (TuleapMilestoneStory milestoneStory : milestoneStories) {
                        TuleapArtifact artifact = TuleapScrumUtils.getTuleapArtifact(tuleapResponse.getToken(), tuleapResponse.getUserId(), milestoneStory.getId());
                        Story story = new Story(milestoneStory.getId(), artifact.getStatus(), artifact.getTitle(), milestoneStory.getInitialEffort());
                        stories.add(story);
                    }

                    Map<String, List<Story>> storyMap = new HashedMap();
                    for(Story story : stories){
                        if(storyMap.containsKey(story.getStatus())){
                            storyMap.get(story.getStatus()).add(story);
                        }else{
                            List<Story> tempBugList = new ArrayList<>();
                            tempBugList.add(story);
                            storyMap.put(story.getStatus(), tempBugList);
                        }
                    }


                    TupleData tuple = new TupleData();
                    tuple.setMeasure("story");
                    List<String> datas = new ArrayList<>();
                    List<String> labels = new ArrayList<>();
                    for(String key : storyMap.keySet()){
                        labels.add(key);
                        datas.add(String.valueOf(storyMap.get(key).size()));
                    }
                    tuple.setData(datas);
                    tuple.setLabel(labels);
                    return tuple;
                }
            }

            throw new Exception();


        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return TuleapScrumIndicatorType.TULEAP_SCRUM_CURRENT_SPRINT_STORIES.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return "stories";
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return TuleapScrumIndicatorType.TULEAP_SCRUM_CURRENT_SPRINT_STORIES;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}
