package com.orange.spectre.plugins.trello.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloConnectorParams;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloIndicatorType;
import com.orange.spectre.plugins.trello.util.TrelloUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Jpa entity for global task state
 */
@Entity
@DiscriminatorValue("TRELLO_GLOBAL_TASK_STATE")
public class GlobalTaskStateIndicator extends AbstractIndicator {


    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalTaskStateIndicator.class);


    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {

            String boardUrl = connectorParams.get(TrelloConnectorParams.url.name());
            String apiKey = connectorParams.get(TrelloConnectorParams.api_key.name());
            String token = connectorParams.get(TrelloConnectorParams.token.name());

            List<String> label = new ArrayList<>();
            List<String> data= new ArrayList<>();

            // Retrieve the board id
            String boardId = TrelloUtils.getBoardId(boardUrl,apiKey,token);

            // Retrieve board lists and cards
            JSONArray myBoard = TrelloUtils.getBoardListsAndCards(boardId, apiKey, token);

            for (int i=0; i<myBoard.length(); i++ ){
                label.add(myBoard.getJSONObject(i).getString("name"));
                data.add(Integer.toString((myBoard.getJSONObject(i).getJSONArray("cards").length())));
            }

            LOGGER.debug(label.toString());
            LOGGER.debug(data.toString());


            TupleData globalTaskState = new TupleData();

            globalTaskState.setLabel(label);
            globalTaskState.setData(data);
            globalTaskState.setMeasure(IndicatorMeasure.NUMERIC.name());

            return globalTaskState;

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return TrelloIndicatorType.GLOBAL_TASK_STATE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return TrelloIndicatorType.GLOBAL_TASK_STATE;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}

