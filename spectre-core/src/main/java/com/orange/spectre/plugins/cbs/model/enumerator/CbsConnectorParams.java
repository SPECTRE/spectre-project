package com.orange.spectre.plugins.cbs.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

public enum CbsConnectorParams implements ConnectorParamsInterface {

	project(true, false);

	private final boolean mandatory;
	private final boolean secured;

	private CbsConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
