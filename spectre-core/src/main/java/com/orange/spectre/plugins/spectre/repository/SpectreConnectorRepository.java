package com.orange.spectre.plugins.spectre.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orange.spectre.plugins.spectre.domain.SpectreConnector;

/**
 * Created by epeg7421 on 03/10/2016.
 *
 * Spectre connector JPA enumerator
 */
public interface SpectreConnectorRepository extends JpaRepository<SpectreConnector, Long> {
}
