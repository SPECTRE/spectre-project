package com.orange.spectre.plugins.jenkins.standard.domain;


import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.plugins.jenkins.common.exception.JenkinsException;
import com.orange.spectre.plugins.jenkins.standard.util.JenkinsFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Jenkins indicator
 * @author nblg6551
 */
public abstract class JenkinsIndicator extends AbstractIndicator {

    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /**
     *
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {

            String outputFlux = JenkinsFluxUtil.getFluxJson(connectorParams);

            return this.computeData(connectorParams, outputFlux);

        } catch (Exception e) {
            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
        }
        return null;
    }

    /**
     *
     * @param connectorParams
     * @return
     * @throws JenkinsException
     */
    public AbstractData computeData(Map<String, String> connectorParams) throws JenkinsException {

            String outputFlux;
        try {
            outputFlux = JenkinsFluxUtil.getFluxJson(connectorParams);
            return this.computeData(connectorParams,outputFlux);
        } catch (UnsupportedEncodingException ex) {
            throw new JenkinsException(ex);
        }catch (Exception ex) {
            LOGGER.error("Error JSON",ex);
            throw new JenkinsException(ex);
        }

    }

    /**
     *
     * @param connectorParams
     * @param jsonFlux
     * @return
     * @throws IOException
     */
    protected AbstractData computeData(Map<String, String> connectorParams , String jsonFlux) throws IOException{
        throw new IOException ("Not yet implemented");
    }


}
