package com.orange.spectre.plugins.sacre.domain;

import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sacre.common.model.ISacreSubIndicator;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sacre.common.model.DosClosFixeIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosOpenFixeIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosReveilFixeIndicator;
import com.orange.spectre.plugins.sacre.common.model.DosSommeilFixeIndicator;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@DiscriminatorValue("DOS_STATE_FIXE")
public class DosStateFixeIndicator extends SacreIndicator {

    /**
     *
     */
    private static final long serialVersionUID = -66409798800900769L;

    public DosStateFixeIndicator() {
        super();
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return SacreIndicatorType.DOS_STATE_FIXE;
    }

    @Override
    public String getLabel() {
        return SacreIndicatorType.DOS_STATE_FIXE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public TupleData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        TupleData data = new TupleData();
        data.setMeasure(IndicatorMeasure.NUMERIC.name());
        List<ISacreSubIndicator> sacreIndicatorListe = new ArrayList<ISacreSubIndicator>();
        sacreIndicatorListe.add(new DosOpenFixeIndicator());
        sacreIndicatorListe.add(new DosSommeilFixeIndicator());
        sacreIndicatorListe.add(new DosReveilFixeIndicator());
        sacreIndicatorListe.add(new DosClosFixeIndicator());

        List<String> listData = new ArrayList<String>();
        List<String> listLabel = new ArrayList<String>();
        for (ISacreSubIndicator oneSacreIndicator : sacreIndicatorListe) {
            listData.add(String.valueOf(oneSacreIndicator.computeData(connectorParams, flux).getData()));
            listLabel.add(oneSacreIndicator.getLabel());
        }

        data.setData(listData);
        data.setLabel(listLabel);

        return data;

    }

    @Override
    public String getDomain() {
        return "Metiers";
    }

    @Override
    public String getPurpose() {
        return "Donne les informations sur les changements d'états des dossiers fixes";
    }

    @Override
    public String getDefinition() {
        return "Retourne les changements sur les dossiers fixes depuis hier minuit jusqu'à maintenant";
    }

    @Override
    public String getOrigin() {
        return "SACRE";
    }

}
