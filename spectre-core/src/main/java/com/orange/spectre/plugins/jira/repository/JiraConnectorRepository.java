package com.orange.spectre.plugins.jira.repository;

import com.orange.spectre.plugins.jira.domain.JiraConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * qc connector JPA enumerator
 */
public interface JiraConnectorRepository extends JpaRepository<JiraConnector, Long> {

}
