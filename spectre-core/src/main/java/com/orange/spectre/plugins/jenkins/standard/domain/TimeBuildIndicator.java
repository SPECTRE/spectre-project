package com.orange.spectre.plugins.jenkins.standard.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;
import com.orange.spectre.plugins.jenkins.common.model.Job_;
import com.orange.spectre.plugins.jenkins.common.model.ResponseJSON;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for time build
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_JOB_TIME_BUILD")
public class TimeBuildIndicator extends JenkinsIndicator {

    @Override
    public String getLabel(){
        return JenkinsIndicatorType.TIME_BUILD.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’optimiser les temps de build et d’identifier quels sont les jobs les plus longs";
    }

    @Override
    public String getDefinition() {
        return "Mesure le temps des différents builds réalisés dans la dernière phase d’intégration continue";
    }

    @Override
    public String getOrigin() {
        return "Jenkins";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return JenkinsIndicatorType.TIME_BUILD;
    }

    @Override
    public FloatData computeData(Map<String, String> connectorParams, String flux) {
        String projectName = (String) connectorParams.get(JenkinsConnectorParams.nom_projet.name());

        float timebuild = 0;
        FloatData data = new FloatData();
        ObjectMapper mapper = new ObjectMapper();

        ResponseJSON responseJob = null;

        try {
            responseJob = mapper.readValue(flux, ResponseJSON.class);
        } catch (JsonMappingException e) {
                super.LOGGER.error("JSON Mapping Exception : " + e );

            } catch (JsonParseException e) {
                super.LOGGER.error("JSON Parse Exception : " + e );

            } catch (IOException e) {
                super.LOGGER.error("IO Exception : " + e );
            }

        if (responseJob != null) {
            for (Job_ job : responseJob.getJobs()) {
                if (job.getLastBuild() != null) {
                    timebuild += job.getLastBuild().getDuration();
                }
            }
        }
        data.setData(timebuild/1000);
        return data;
    }
}


