
package com.orange.spectre.plugins.jenkins.standard.model.enumerator;


import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.jenkins.standard.domain.*;

/**
 *
 * @author nblg6551
 */
public enum JenkinsIndicatorType {

    /**
     *
     */
    JOB_SUCCESS(PercentageJobsSuccessesIndicator.class),
    /**
     *
     */
    TIME_BUILD(TimeBuildIndicator.class),

    /**
     *
     */
    TOTALS_BUILDS(TotalsBuildsIndicator.class),

    /**
     *
     */
    JOB_FAIL(PercentageJobsFailsIndicator.class),
    /**
     *
     */
    HEALTH_REPORT_BUILD(HealthReportBuildIndicator.class),
    /**
     *
     */
    JOB_BUILD_STATUS(JobBuildStatusIndicator.class);


    private Class<? extends AbstractIndicator> clazz;

        JenkinsIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     *
     * @return
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     *
     * @return
     */
    public String getLabel() {
        switch (this.name()) {
            case "JOB_SUCCESS":
                return "Builds réussis";
            case "TIME_BUILD" :
                return "Temps total de build";
            case "TOTALS_BUILDS" :
                return "Nombre de builds";
            case "JOB_FAIL":
                return "Builds échoués";
            case "HEALTH_REPORT_BUILD":
                return "Statut du projet";
            case "JOB_BUILD_STATUS":
                return "Statut du dernier build du projet";

            default: return null;
        }
    }
}


