package com.orange.spectre.plugins.trello.repository;

import com.orange.spectre.plugins.trello.domain.TrelloConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Trello connector JPA enumerator
 */
public interface TrelloConnectorRepository extends JpaRepository<TrelloConnector, Long> {
}
