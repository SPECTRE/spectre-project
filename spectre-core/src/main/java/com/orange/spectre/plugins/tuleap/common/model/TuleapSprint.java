package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 25/04/2017.
 */
public class TuleapSprint {

    private String id;
    private String label;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
