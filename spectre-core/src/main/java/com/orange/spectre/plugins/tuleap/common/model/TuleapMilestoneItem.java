package com.orange.spectre.plugins.tuleap.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 13/04/2017.
 */
public class TuleapMilestoneItem {

    private String id;
    private String label;

    @JsonProperty("status_value")
    private String statusValue;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStatusValue() {
        return statusValue;
    }

    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }
}
