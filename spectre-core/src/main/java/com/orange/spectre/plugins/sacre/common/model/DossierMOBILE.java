
package com.orange.spectre.plugins.sacre.common.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "age_nom_abrege",
    "ouvert",
    "sommeil",
    "reveil",
    "clos"
})
public class DossierMOBILE {

    @JsonProperty("age_nom_abrege")
    private String ageNomAbrege;
    @JsonProperty("ouvert")
    private String ouvert;
    @JsonProperty("sommeil")
    private String sommeil;
    @JsonProperty("reveil")
    private String reveil;
    @JsonProperty("clos")
    private String clos;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The ageNomAbrege
     */
    @JsonProperty("age_nom_abrege")
    public String getAgeNomAbrege() {
        return ageNomAbrege;
    }

    /**
     * 
     * @param ageNomAbrege
     *     The age_nom_abrege
     */
    @JsonProperty("age_nom_abrege")
    public void setAgeNomAbrege(String ageNomAbrege) {
        this.ageNomAbrege = ageNomAbrege;
    }

    /**
     * 
     * @return
     *     The ouvert
     */
    @JsonProperty("ouvert")
    public String getOuvert() {
        return ouvert;
    }

    /**
     * 
     * @param ouvert
     *     The ouvert
     */
    @JsonProperty("ouvert")
    public void setOuvert(String ouvert) {
        this.ouvert = ouvert;
    }

    /**
     * 
     * @return
     *     The sommeil
     */
    @JsonProperty("sommeil")
    public String getSommeil() {
        return sommeil;
    }

    /**
     * 
     * @param sommeil
     *     The sommeil
     */
    @JsonProperty("sommeil")
    public void setSommeil(String sommeil) {
        this.sommeil = sommeil;
    }

    /**
     * 
     * @return
     *     The reveil
     */
    @JsonProperty("reveil")
    public String getReveil() {
        return reveil;
    }

    /**
     * 
     * @param reveil
     *     The reveil
     */
    @JsonProperty("reveil")
    public void setReveil(String reveil) {
        this.reveil = reveil;
    }

    /**
     * 
     * @return
     *     The clos
     */
    @JsonProperty("clos")
    public String getClos() {
        return clos;
    }

    /**
     * 
     * @param clos
     *     The clos
     */
    @JsonProperty("clos")
    public void setClos(String clos) {
        this.clos = clos;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
