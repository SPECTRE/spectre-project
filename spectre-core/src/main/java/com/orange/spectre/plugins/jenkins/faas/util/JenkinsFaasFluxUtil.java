package com.orange.spectre.plugins.jenkins.faas.util;

import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasConnectorParams;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

/**
 * Created by ludovic on 23/08/2016.
 */

public final class JenkinsFaasFluxUtil {




    public static String buildUrlRemoteAPI(Map<String, String> params) throws Exception{

        String JenkinsPartialUrl = params.get(JenkinsFaasConnectorParams.url.name());

        return JenkinsPartialUrl + ":3000/jenkins/faasRemoteAPI/getBuildStats";
    }

    public static String buildUrlHealthReport(Map<String, String> params) throws Exception{

        String JenkinsPartialUrl = params.get(JenkinsFaasConnectorParams.url.name());

        String jobName = params.get(JenkinsFaasConnectorParams.nom_job.name());

        return JenkinsPartialUrl + ":3000/jenkins/view/All/job/"+jobName+"/api/json";
    }

    /*
     * @param url
     * @return
     * @throws IOException
     */
    public static String getFluxJson(String url) throws IOException, Exception {



        CloseableHttpClient httpClient;



        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sc);

        // Complete url with Proxy Settings
        if (ProxyConfig.getInstance().isUseProxy()) {
            HttpHost proxy = new HttpHost(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort()));
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setProxy(proxy).build();
        } else {
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        }

        URI uri = URI.create(url);
        HttpHost host = new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
        HttpGet httpGet = new HttpGet(uri);

        HttpResponse response = httpClient.execute(host, httpGet);

        return EntityUtils.toString(response.getEntity());

    }

}
