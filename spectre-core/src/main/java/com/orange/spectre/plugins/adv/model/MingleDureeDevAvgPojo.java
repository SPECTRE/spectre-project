package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by htxs7368 on 7/6/2017.
 */
public class MingleDureeDevAvgPojo {

    @JsonProperty("Avg Durée DEV")
    private Float avgDuree;

    public Float getAvgDureeTest() {
        return avgDureeTest;
    }

    public void setAvgDureeTest(Float avgDureeTest) {
        this.avgDureeTest = avgDureeTest;
    }

    @JsonProperty("Avg Durée TEST")
    private Float avgDureeTest;


    public Float getAvgDuree() {
        return avgDuree;
    }

    public void setAvgDuree(Float avgDuree) {
        this.avgDuree = avgDuree;
    }
}
