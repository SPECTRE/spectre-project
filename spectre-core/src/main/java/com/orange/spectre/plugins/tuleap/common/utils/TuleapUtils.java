package com.orange.spectre.plugins.tuleap.common.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.plugins.tuleap.common.model.*;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ludovic on 24/03/2017.
 */
public final class TuleapUtils {

    private static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /**
     * Get connexion data to use tuleap API
     * @param login User login
     * @param password User password
     * @return Token response with token value and user id
     * @throws TuleapApiException
     */
    public static TuleapTokenResponse getTuleapConnectionData(String login, String password) throws TuleapApiException{
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder tokenUrl = new StringBuilder();

            tokenUrl.append(TuleapConstants.FORGE_URL);
            tokenUrl.append(TuleapConstants.FORGE_API_TOKENS_URI);

            HttpPost httpPost = new HttpPost(tokenUrl.toString());
            List<NameValuePair> nvps = new ArrayList<NameValuePair>();
            nvps.add(new BasicNameValuePair("username", login));
            nvps.add(new BasicNameValuePair("password", password));
            httpPost.setEntity(new UrlEncodedFormEntity(nvps));

            HttpResponse tokenResponse = httpClient.execute(httpPost);
            if (tokenResponse.getStatusLine().getStatusCode() == HttpStatus.SC_CREATED) {
                return objectMapper.readValue(tokenResponse.getEntity().getContent(), TuleapTokenResponse.class);
            }else{
                throw new TuleapApiException("Bad response from tuleap token api");
            }

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (UnsupportedEncodingException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (TuleapApiException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        }

    }

    /**
     * Get Tuleap project from is shortname
     * @param token Api security token
     * @param userId Api security user id
     * @param project Project shortname
     * @return Tuleap project id
     * @throws TuleapApiException
     */
    public static String getTuleapProjectIdByName(String token, String userId, String project) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PROJECTS_URI);

            projectsUrl.append("?query=%7B%22shortname%22%3A%22"); //{"shortname":"
            projectsUrl.append(project);
            projectsUrl.append("%22%7D"); //"}

            HttpGet httpGet = new HttpGet(projectsUrl.toString().replaceAll(" ", "%20")); //replaceAll(" ", "%20")

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse projectsResponse = httpClient.execute(httpGet);
            if (projectsResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapProject[] tuleapProjectsResponse = objectMapper.readValue(projectsResponse.getEntity().getContent(), TuleapProject[].class);
                if (tuleapProjectsResponse != null && tuleapProjectsResponse.length > 0) {
                    return tuleapProjectsResponse[0].getId();
                }
            }
            throw new TuleapApiException("Bad response from tuleap token api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap token api");
        }
    }

}
