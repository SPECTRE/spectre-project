package com.orange.spectre.plugins.qc.custom.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.qc.custom.model.enumerator.CustomQcConnectorParams;
import com.orange.spectre.plugins.qc.custom.model.enumerator.CustomQcIndicatorType;
import com.orange.spectre.plugins.qc.util.QCConstant;
import com.orange.spectre.plugins.qc.util.QcAuthentification;
import com.orange.spectre.plugins.qc.util.Response;
import com.orange.spectre.plugins.qc.util.RestConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of qc connector
 */
@Entity
@DiscriminatorValue("QC_CUSTOM")
public class CustomQcConnector extends AbstractConnector {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomQcConnector.class);

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all qc indicators
	 *
	 * @return All qc indicators
	 */
	public List<String> getAvailableIndicators() {
		return Arrays.stream(CustomQcIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all qc connection parameters
	 *
	 * @return qc connection parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (CustomQcConnectorParams param : CustomQcConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return requiredParams;
	}

	/**
	 * Get qc string type
	 *
	 * @return String type
	 */
	@Override
	public String getType() {
		return "qc_custom";
	}

	/**
	 * Create a qc indicator from an indicator type
	 *
	 * @param s indicator type
	 * @return A new qc indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return CustomQcIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		String username = connectorParams.get(CustomQcConnectorParams.login.name());
		String password = connectorParams.get(CustomQcConnectorParams.password.name());
		String url = QCConstant.HP_ALM_URL;
		String domain = connectorParams.get(CustomQcConnectorParams.domaine.name());
		String project = connectorParams.get(CustomQcConnectorParams.projet.name());
		String fields = connectorParams.get(CustomQcConnectorParams.fields.name());
		String query = connectorParams.get(CustomQcConnectorParams.query.name());

		RestConnector con = RestConnector.getInstance().init(new HashMap<String, String>(), url, domain, project);
		QcAuthentification login = new QcAuthentification();

		try {
			boolean loginState = login.login(username, password);
			if (loginState) {
				//Test release access
				Map<String, String> requestHeaders = new HashMap<String, String>();
				requestHeaders.put("Accept", "application/json");
				requestHeaders.put("Content-Type", "application/json");
				String releaseUrl = con.buildEntityCollectionUrl(QCConstant.HP_ALM_ENTITY_RELEASE);
				Response responseRelease = con.httpGet(releaseUrl, null, requestHeaders);
				if (HttpURLConnection.HTTP_OK == responseRelease.getStatusCode()) {
					return true;
				}
			} else {
				return false;
			}
			login.logout();
			return false;
		} catch (Exception e) {
			LOGGER.debug("CustomQcConnector.class : Error connection to QC plateform : " + e.getMessage());
			return false;
		}
	}
}
