package com.orange.spectre.plugins.jenkins.standard.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;
import com.orange.spectre.plugins.jenkins.common.model.DownstreamProject;
import com.orange.spectre.plugins.jenkins.common.model.Job_;
import com.orange.spectre.plugins.jenkins.common.model.ResponseJSON;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for percentage jobs fails
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_JOB_FAILS")
public class PercentageJobsFailsIndicator extends JenkinsIndicator {

    @Override
    public String getLabel(){
        return JenkinsIndicatorType.JOB_FAIL.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier la stabilité d’une application durant la phase de développement et la qualité de travail des développeurs";
    }

    @Override
    public String getDefinition() {
        return "Représente le taux de build jenkins qui ont échoué.";
    }


    @Override
    public String getOrigin() {
        return "Jenkins";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return JenkinsIndicatorType.JOB_FAIL;
    }

    @Override
    protected FloatData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        String projectName = (String) connectorParams.get(JenkinsConnectorParams.nom_projet.name());
        float nbBuildSuccess = 0;
        float nbBuildFail = 0;
        FloatData data = new FloatData();
        ObjectMapper mapper = new ObjectMapper();

        if (projectName != null && projectName.length() > 0) {
            Job_ responseJob = null;

            try {
                responseJob = mapper.readValue(flux, Job_.class);
           } catch (JsonMappingException e) {
                super.LOGGER.error("JSON Mapping Exception : " + e );

            } catch (JsonParseException e) {
                super.LOGGER.error("JSON Parse Exception : " + e );

            } catch (IOException e) {
                super.LOGGER.error("IO Exception : " + e );
            }

            if (responseJob != null) {
                // Récupération des valeurs pour un groupe de projet
                if (!responseJob.getDownstreamProjects().isEmpty()) {
                    switch (responseJob.getColor()) {
                        case "blue":
                            nbBuildSuccess++;
                            break;
                        case "red":
                            nbBuildFail++;
                            break;
                    }
                    for (DownstreamProject job : responseJob.getDownstreamProjects()) {
                        if (job.getName() != null) {
                            switch (job.getColor()) {
                                case "blue":
                                    nbBuildSuccess++;
                                    break;
                                case "red":
                                    nbBuildFail++;
                                    break;
                            }
                        }
                    }
                }
                else {
                    throw new IOException ("sélectionné le projet déclencheur du projet que vous venez de rentrer");
                }
            }
        } else {
            ResponseJSON responseJob = null;

            try {
                responseJob = mapper.readValue(flux, ResponseJSON.class);
            } catch (JsonMappingException e) {

            } catch (JsonParseException e) {

            } catch (IOException e) {

            }

            if (responseJob != null) {
                for (Job_ job : responseJob.getJobs()) {
                    if (job.getLastBuild() != null) {
                        switch (job.getColor()) {
                            case "red":
                                nbBuildFail++;
                                break;
                            case "blue":
                                nbBuildSuccess++;
                                break;
                        }
                    }
                }

            }
        }
        data.setData((nbBuildFail/(nbBuildFail+nbBuildSuccess))*100);
        return data;
    }
}

