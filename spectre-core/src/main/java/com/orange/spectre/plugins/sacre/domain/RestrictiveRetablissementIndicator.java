package com.orange.spectre.plugins.sacre.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.ListeTRAI;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity
@DiscriminatorValue("NOMBRE_MESURE_DISE")
public class RestrictiveRetablissementIndicator extends SacreIndicator {

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return SacreIndicatorType.NOMBRE_MESURE_DISE;
    }

    @Override
    public String getLabel() {
        return SacreIndicatorType.NOMBRE_MESURE_DISE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Metier";
    }

    @Override
    public String getPurpose() {
        return "Renvoie le nombre de mesures restrictives ou de rétablissement effectues";
    }

    @Override
    public String getDefinition() {
        return "Retourne 3 valeurs, nombre de mise en IAS puis enlever IAS puis Retablissement";
    }

    @Override
    public String getOrigin() {
        return "SACRE";
    }

    public TupleData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        TupleData retour = new TupleData();
        ObjectMapper mapper = new ObjectMapper();
        List<String> myList = new ArrayList<String>();
        List<String> myLabel = new ArrayList<String>();
        myLabel.add("Mise en IAS DISE");
        myLabel.add("Enlever IAS DISE");
        myLabel.add("retablir Ligne DISE");

        Data myData = mapper.readValue(flux, Data.class);


        for (ListeTRAI trai : myData.getData().getListeTRAI()) {
            if (Integer.parseInt(trai.getTRAIId()) == 41) {
                myList.add(0, trai.getNbLignes());
            }
            if (Integer.parseInt(trai.getTRAIId()) == 73) {
                myList.add(1, trai.getNbLignes());
            }
            if (Integer.parseInt(trai.getTRAIId()) == 75) {
                myList.add(2, trai.getNbLignes());
            }
        }
        retour.setData(myList);
        retour.setLabel(myLabel);
        return retour;
    }

}
