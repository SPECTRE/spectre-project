package com.orange.spectre.plugins.sacre.common.model;

import com.orange.spectre.core.model.data.IntegerData;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ludovic on 18/04/2017.
 */
public interface ISacreSubIndicator {

    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws IOException;

    public String getLabel();
}
