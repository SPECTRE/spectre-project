package com.orange.spectre.plugins.devopsic.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Sonar response bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class DevopsIcResponse {

	private String	dom_id;
	private String	sond_id;
	private String	appli_id;
	private String	appli_basicat;
	private String	appli_name;
	private String	dom_niveau;
	private String	dom_date;
	private String	dom_note_G;
	private String	dom_note_C;
	private String	dom_note_A;
	private String	dom_note_M;
	private String	dom_note_P;
	private String	dev_note_G;
	private String	dev_note_C;
	private String	dev_note_A;
	private String	dev_note_M;
	private String	dev_note_P;
	private String	ops_note_G;
	private String	ops_note_C;
	private String	ops_note_A;
	private String	ops_note_M;
	private String	ops_note_P;
	private String	form_version;
	private String	socle_date;
	private String	socle_note_ICDA;
	private String	socle_note_DC;

	/**
	 * @return the dom_id
	 */
	public String getDom_id() {
		return dom_id;
	}

	/**
	 * @param dom_id
	 *            the dom_id to set
	 */
	public void setDom_id(String dom_id) {
		this.dom_id = dom_id;
	}

	/**
	 * @return the sond_id
	 */
	public String getSond_id() {
		return sond_id;
	}

	/**
	 * @param sond_id
	 *            the sond_id to set
	 */
	public void setSond_id(String sond_id) {
		this.sond_id = sond_id;
	}

	/**
	 * @return the appli_id
	 */
	public String getAppli_id() {
		return appli_id;
	}

	/**
	 * @param appli_id
	 *            the appli_id to set
	 */
	public void setAppli_id(String appli_id) {
		this.appli_id = appli_id;
	}

	/**
	 * @return the appli_basicat
	 */
	public String getAppli_basicat() {
		return appli_basicat;
	}

	/**
	 * @param appli_basicat
	 *            the appli_basicat to set
	 */
	public void setAppli_basicat(String appli_basicat) {
		this.appli_basicat = appli_basicat;
	}

	/**
	 * @return the appli_name
	 */
	public String getAppli_name() {
		return appli_name;
	}

	/**
	 * @param appli_name
	 *            the appli_name to set
	 */
	public void setAppli_name(String appli_name) {
		this.appli_name = appli_name;
	}

	/**
	 * @return the dom_niveau
	 */
	public String getDom_niveau() {
		return dom_niveau;
	}

	/**
	 * @param dom_niveau
	 *            the dom_niveau to set
	 */
	public void setDom_niveau(String dom_niveau) {
		this.dom_niveau = dom_niveau;
	}

	/**
	 * @return the dom_date
	 */
	public String getDom_date() {
		return dom_date;
	}

	/**
	 * @param dom_date
	 *            the dom_date to set
	 */
	public void setDom_date(String dom_date) {
		this.dom_date = dom_date;
	}

	/**
	 * @return the dom_note_G
	 */
	public String getDom_note_G() {
		return dom_note_G;
	}

	/**
	 * @param dom_note_G
	 *            the dom_note_G to set
	 */
	public void setDom_note_G(String dom_note_G) {
		this.dom_note_G = dom_note_G;
	}

	/**
	 * @return the dom_note_C
	 */
	public String getDom_note_C() {
		return dom_note_C;
	}

	/**
	 * @param dom_note_C
	 *            the dom_note_C to set
	 */
	public void setDom_note_C(String dom_note_C) {
		this.dom_note_C = dom_note_C;
	}

	/**
	 * @return the dom_note_A
	 */
	public String getDom_note_A() {
		return dom_note_A;
	}

	/**
	 * @param dom_note_A
	 *            the dom_note_A to set
	 */
	public void setDom_note_A(String dom_note_A) {
		this.dom_note_A = dom_note_A;
	}

	/**
	 * @return the dom_note_M
	 */
	public String getDom_note_M() {
		return dom_note_M;
	}

	/**
	 * @param dom_note_M
	 *            the dom_note_M to set
	 */
	public void setDom_note_M(String dom_note_M) {
		this.dom_note_M = dom_note_M;
	}

	/**
	 * @return the dom_note_P
	 */
	public String getDom_note_P() {
		return dom_note_P;
	}

	/**
	 * @param dom_note_P
	 *            the dom_note_P to set
	 */
	public void setDom_note_P(String dom_note_P) {
		this.dom_note_P = dom_note_P;
	}

	/**
	 * @return the dev_note_G
	 */
	public String getDev_note_G() {
		return dev_note_G;
	}

	/**
	 * @param dev_note_G
	 *            the dev_note_G to set
	 */
	public void setDev_note_G(String dev_note_G) {
		this.dev_note_G = dev_note_G;
	}

	/**
	 * @return the dev_note_C
	 */
	public String getDev_note_C() {
		return dev_note_C;
	}

	/**
	 * @param dev_note_C
	 *            the dev_note_C to set
	 */
	public void setDev_note_C(String dev_note_C) {
		this.dev_note_C = dev_note_C;
	}

	/**
	 * @return the dev_note_A
	 */
	public String getDev_note_A() {
		return dev_note_A;
	}

	/**
	 * @param dev_note_A
	 *            the dev_note_A to set
	 */
	public void setDev_note_A(String dev_note_A) {
		this.dev_note_A = dev_note_A;
	}

	/**
	 * @return the dev_note_M
	 */
	public String getDev_note_M() {
		return dev_note_M;
	}

	/**
	 * @param dev_note_M
	 *            the dev_note_M to set
	 */
	public void setDev_note_M(String dev_note_M) {
		this.dev_note_M = dev_note_M;
	}

	/**
	 * @return the dev_note_P
	 */
	public String getDev_note_P() {
		return dev_note_P;
	}

	/**
	 * @param dev_note_P
	 *            the dev_note_P to set
	 */
	public void setDev_note_P(String dev_note_P) {
		this.dev_note_P = dev_note_P;
	}

	/**
	 * @return the ops_note_G
	 */
	public String getOps_note_G() {
		return ops_note_G;
	}

	/**
	 * @param ops_note_G
	 *            the ops_note_G to set
	 */
	public void setOps_note_G(String ops_note_G) {
		this.ops_note_G = ops_note_G;
	}

	/**
	 * @return the ops_note_C
	 */
	public String getOps_note_C() {
		return ops_note_C;
	}

	/**
	 * @param ops_note_C
	 *            the ops_note_C to set
	 */
	public void setOps_note_C(String ops_note_C) {
		this.ops_note_C = ops_note_C;
	}

	/**
	 * @return the ops_note_A
	 */
	public String getOps_note_A() {
		return ops_note_A;
	}

	/**
	 * @param ops_note_A
	 *            the ops_note_A to set
	 */
	public void setOps_note_A(String ops_note_A) {
		this.ops_note_A = ops_note_A;
	}

	/**
	 * @return the ops_note_M
	 */
	public String getOps_note_M() {
		return ops_note_M;
	}

	/**
	 * @param ops_note_M
	 *            the ops_note_M to set
	 */
	public void setOps_note_M(String ops_note_M) {
		this.ops_note_M = ops_note_M;
	}

	/**
	 * @return the ops_note_P
	 */
	public String getOps_note_P() {
		return ops_note_P;
	}

	/**
	 * @param ops_note_P
	 *            the ops_note_P to set
	 */
	public void setOps_note_P(String ops_note_P) {
		this.ops_note_P = ops_note_P;
	}

	/**
	 * @return the form_version
	 */
	public String getForm_version() {
		return form_version;
	}

	/**
	 * @param form_version
	 *            the form_version to set
	 */
	public void setForm_version(String form_version) {
		this.form_version = form_version;
	}

	/**
	 * @return the socle_date
	 */
	public String getSocle_date() {
		return socle_date;
	}

	/**
	 * @param socle_date
	 *            the socle_date to set
	 */
	public void setSocle_date(String socle_date) {
		this.socle_date = socle_date;
	}

	/**
	 * @return the socle_note_ICDA
	 */
	public String getSocle_note_ICDA() {
		return socle_note_ICDA;
	}

	/**
	 * @param socle_note_ICDA
	 *            the socle_note_ICDA to set
	 */
	public void setSocle_note_ICDA(String socle_note_ICDA) {
		this.socle_note_ICDA = socle_note_ICDA;
	}

	/**
	 * @return the socle_note_DC
	 */
	public String getSocle_note_DC() {
		return socle_note_DC;
	}

	/**
	 * @param socle_note_DC
	 *            the socle_note_DC to set
	 */
	public void setSocle_note_DC(String socle_note_DC) {
		this.socle_note_DC = socle_note_DC;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DevopsIcResponse ["	+ "appli_id=" + this.appli_id + ", appli_basicat=" + this.appli_basicat + ", appli_name=" + this.appli_name
				+ ", socle_date=" + this.socle_date
				+ ", socle_note_ICDA=" + ( this.socle_note_ICDA != null ? this.socle_note_ICDA : "")
				+ ", socle_note_DC=" + ( this.socle_note_DC != null ? this.socle_note_DC : "")
				+ ", dom_id=" + ( this.dom_id != null ? this.dom_id : "")
				+ ", sond_id=" + ( this.sond_id != null ? this.sond_id : "")
				+ ", dom_niveau=" + ( this.dom_niveau != null ? this.dom_niveau : "")
				+ ", dom_date=" + ( this.dom_date != null ? this.dom_date : "")
				+ ", dom_note_G=" + ( this.dom_note_G != null ? this.dom_note_G : "")
				+ ", dom_note_G=" + ( this.dom_note_C != null ? this.dom_note_C : "")
				+ ", dom_note_G=" + ( this.dom_note_A != null ? this.dom_note_A : "")
				+ ", dom_note_G=" + ( this.dom_note_M != null ? this.dom_note_M : "")
				+ ", dom_note_G=" + ( this.dom_note_P != null ? this.dom_note_P : "")
				+ ", dom_form_version=" + ( this.form_version != null ? this.form_version : "")
				+ "]";
	}

}
