package com.orange.spectre.plugins.mingle.repository;

import com.orange.spectre.plugins.mingle.domain.MingleConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * mingle connector JPA enumerator
 */
public interface MingleConnectorRepository extends JpaRepository<MingleConnector, Long> {

}
