package com.orange.spectre.plugins.tuleap.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 13/04/2017.
 */
public class TuleapScrumStory {

    private String id;
    private String status;
    private String title;

    @JsonProperty("initial_effort")
    private String initialEffort;
}
