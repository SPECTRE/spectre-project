package com.orange.spectre.plugins.sonar.faas.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.sonar.faas.domain.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enumerator of all available sonar faas indicators
 */
public enum SonarFaasIndicatorType {

    TECHNICAL_DEBT(TechnicalDebtIndicatorFaas.class),
    TEST_COVERAGE(TestCoverageIndicatorFaas.class),
    TEST_SUCCESS_DENSITY(TestSuccessDensityIndicatorFaas.class),
    TESTS_NUMBER(TestsNumberIndicatorFaas.class),
    DUPLICATED_LINES_DENSITY(DuplicatedLinesDensityIndicatorFaas.class),
    BLOCKER_VIOLATIONS(BlockerViolationsIndicatorFaas.class),
    CRITICAL_VIOLATIONS(CriticalViolationsIndicatorFaas.class),
    MAJOR_VIOLATIONS(MajorViolationsIndicatorFaas.class),
    MINOR_VIOLATIONS(MinorViolationsIndicatorFaas.class),
    COMPLEXITY(ComplexityIndicator.class);

    private static final Logger LOGGER = LoggerFactory.getLogger(SonarFaasIndicatorType.class);

    /**
     * Current sonar faas indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    SonarFaasIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from its type
     * @return A sonar faas indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("Error during indicator creation", e);
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from its type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "TECHNICAL_DEBT":
                return "Dette technique";
            case "TEST_COVERAGE":
                return "Couverture de tests";
            case "TEST_SUCCESS_DENSITY":
                return "Tests en succès";
            case "TESTS_NUMBER":
                return "Nombre de tests";
            case "DUPLICATED_LINES_DENSITY":
                return "Code dupliqué";
            case "BLOCKER_VIOLATIONS":
                return "Erreurs bloquantes";
            case "CRITICAL_VIOLATIONS":
                return "Erreurs critiques";
            case "MAJOR_VIOLATIONS":
                return "Erreurs majeures";
            case "MINOR_VIOLATIONS":
                return "Erreurs mineures";
            case "COMPLEXITY":
                return "Complexité";
            default: return null;
        }
    }
}


