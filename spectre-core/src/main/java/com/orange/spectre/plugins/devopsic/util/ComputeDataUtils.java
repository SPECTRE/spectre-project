package com.orange.spectre.plugins.devopsic.util;

import org.springframework.web.client.RestTemplate;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import org.apache.http.HttpHost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class ComputeDataUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(ComputeDataUtils.class);

	public static DevopsIcResponse extract(String url) throws Exception {

		DevopsIcResponse data = null;

		System.setProperty("jsse.enableSNIExtension", "false");
		CloseableHttpClient httpClient;

		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return null;
					}
					public void checkClientTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
					public void checkServerTrusted(
							java.security.cert.X509Certificate[] certs, String authType) {
					}
				}
		};

		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new java.security.SecureRandom());
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sc);

		// Complete url with Proxy Settings
		if (ProxyConfig.getInstance().isUseProxy()) {
			HttpHost proxy = new HttpHost(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort()));
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setProxy(proxy).build();
		} else {
			httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
		}

		try {

			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
			RestTemplate restTemplate = new RestTemplate(requestFactory);
			DevopsIcResponse[] devopsIcResponseArray = restTemplate.getForObject(url, DevopsIcResponse[].class);
			httpClient.close();
			if (devopsIcResponseArray.length != 0) {
				data = devopsIcResponseArray[0];
			}
			return data;
		} catch (Exception e) {
			LOGGER.error("Exception with http client", e);
			throw new Exception("http client close error");
		}
	}

}
