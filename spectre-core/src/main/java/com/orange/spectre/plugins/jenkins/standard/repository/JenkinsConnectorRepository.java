package com.orange.spectre.plugins.jenkins.standard.repository;


import com.orange.spectre.plugins.jenkins.standard.domain.JenkinsConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author nblg6551
 */
public interface JenkinsConnectorRepository extends JpaRepository<JenkinsConnector, Long> {

}
