package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 28/03/2017.
 */
public class TuleapBugValue {

    private String label;
    private TuleapBugSubValue[] values;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TuleapBugSubValue[] getValues() {
        return values;
    }

    public void setValues(TuleapBugSubValue[] values) {
        this.values = values;
    }
}
