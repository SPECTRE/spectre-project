package com.orange.spectre.plugins.tuleap.bug.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.tuleap.bug.enumerator.TuleapBugsConnectorParams;
import com.orange.spectre.plugins.tuleap.bug.enumerator.TuleapBugsIndicatorType;
import com.orange.spectre.plugins.tuleap.bug.utils.TuleapBugUtils;
import com.orange.spectre.plugins.tuleap.common.model.TuleapTokenResponse;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Jpa entity of Tuleap connector
 */
@Entity
@DiscriminatorValue("TULEAP_BUGS")
public class TuleapBugsConnector extends AbstractConnector {

    /**
     * All connector connection params
     */
    @Transient
    List<ConnectorRequiredParams> requiredParams;


    /**
     * Get list of all Tuleap indicators
     *
     * @return All Tuleap indicators
     */
    public List<String> getAvailableIndicators() {
        return Arrays.stream(TuleapBugsIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

    }

    /**
     * Get list of all Tuleap connection parameters
     *
     * @return Tuleap connection parameters
     */
    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (TuleapBugsConnectorParams param : TuleapBugsConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);

            }
        }
        return requiredParams;
    }

    /**
     * Get Tuleap string type
     *
     * @return String type
     */
    @Override
    public String getType() {
        return "forge_bugs";
    }

    /**
     * Create a Tuleap indicator from an indicator type
     *
     * @param s indicator type
     * @return A new Tuleap indicator
     */
    @Override
    public AbstractIndicator createIndicator(String s) {

        return TuleapBugsIndicatorType.valueOf(s).createIndicator();
    }

    @Override
    public boolean testConnector(Map<String, String> connectorParams) {
        try {

            String login = connectorParams.get(TuleapBugsConnectorParams.login.name());
            String password = connectorParams.get(TuleapBugsConnectorParams.mot_de_passe.name());
            String project = connectorParams.get(TuleapBugsConnectorParams.nom_projet.name());
            String rapport = connectorParams.get(TuleapBugsConnectorParams.nom_rapport.name());

            TuleapTokenResponse tuleapResponse = TuleapUtils.getTuleapConnectionData(login, password);
            String tuleapProjectId = TuleapUtils.getTuleapProjectIdByName(tuleapResponse.getToken(), tuleapResponse.getUserId(), project);
            String tuleapTrackerId = TuleapBugUtils.getTuleapBugTrackerId(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapProjectId);
            String tuleapTrackerReportId = TuleapBugUtils.getTuleapBugTrackerReportId(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapTrackerId, rapport);

            return tuleapTrackerReportId != null;

        } catch (Exception e) {
            return false;
        }

    }

}
