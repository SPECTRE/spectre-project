package com.orange.spectre.plugins.adv.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.adv.model.MingleSprintPojo;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvIndicatorType;
import com.orange.spectre.plugins.adv.util.MingleAdvMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kctl7743 on 02/06/2017.
 */

/**
 * Created by kctl7743 on 01/06/2017.
 */
@Entity
@DiscriminatorValue("ADV_LAST_TEN_SPRINT")
public class SprintLastTenNbIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SprintLastTenNbIndicator.class);

    @Override
    public String getLabel() {
        return MingleAdvIndicatorType.ADV_LAST_TEN_SPRINT.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        String mqlRequest = "SELECT  sprint, count(*) WHERE type = 'Story' and 'Statut de la story' !='terminé' and 'Statut de la story' != 'en rédaction' GROUP BY Sprint";

        List<String> label = new ArrayList<>();
        List<String> data = new ArrayList<>();
        try {
            JSONArray jsonArray = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {

                ObjectMapper mapper = new ObjectMapper();

                for (int i = jsonArray.length() - 2; i > jsonArray.length() - 13; i--) {

                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    MingleSprintPojo mingleSprintPojo = mapper.readValue(jsonObject.toString(), MingleSprintPojo.class);

                    label.add(mingleSprintPojo.getSprint());
                    data.add(mingleSprintPojo.getCount());
                }
                TupleData tupleData = new TupleData();
                tupleData.setLabel(label);
                tupleData.setData(data);
                tupleData.setMeasure(IndicatorMeasure.NUMERIC.name());

                return tupleData;
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }

        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.AUTRE;
    }

    @Override
    public Enum getType() {
        return MingleAdvIndicatorType.ADV_LAST_TEN_SPRINT;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}

