
package com.orange.spectre.plugins.jenkins.standard.domain;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.jenkins.common.model.DownstreamProject;
import com.orange.spectre.plugins.jenkins.common.model.Job_;
import com.orange.spectre.plugins.jenkins.common.model.ResponseJSON;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;
import com.orange.spectre.plugins.jenkins.standard.util.JenkinsFluxUtil;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of Jenkins connector
 */
@Entity
@DiscriminatorValue("JENKINS")
public class JenkinsConnector extends AbstractConnector {

    /**
     * All connector connection params
     */

    @Transient
    List<ConnectorRequiredParams> requiredParams;

    /**
     * Get list of all Jenkins indicators
     *
     * @return All Jenkins indicators
     */
    public List<String> getAvailableIndicators() {
        return Arrays.stream(JenkinsIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

    }

    /**
     * Get list of all Jenkins connection parameters
     *
     * @return Jenkins connection parameters
     */
    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (JenkinsConnectorParams param : JenkinsConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);
            }
        }
        return requiredParams;
    }

    /**
     * Get Jenkins string type
     *
     * @return String type
     */
    @Override
    public String getType() {
        return "jenkins";
    }

    /**
     * Create a Jenkins indicator from an indicator type
     *
     * @param s indicator type
     * @return A new Jenkins indicator
     */
    @Override
    public AbstractIndicator createIndicator(String s) {
        return JenkinsIndicatorType.valueOf(s).createIndicator();
    }

    @Override
    public boolean testConnector(Map<String, String> connectorParams) {
        try {

            String flux = JenkinsFluxUtil.getFluxJson(connectorParams);
            ObjectMapper mapper = new ObjectMapper();


            ResponseJSON responseJob = null;

            try {
                responseJob = mapper.readValue(flux, ResponseJSON.class);
            } catch (JsonMappingException e) {
                return false;
            } catch (JsonParseException e) {
                return false;
            } catch (IOException e) {
                return false;
            }

            if (responseJob != null) {
                return true;

            } else {
                return false;
            }

        } catch (Exception e) {
            return false;
        }
    }
}
