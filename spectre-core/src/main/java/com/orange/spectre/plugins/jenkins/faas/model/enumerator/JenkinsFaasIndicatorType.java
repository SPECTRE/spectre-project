
package com.orange.spectre.plugins.jenkins.faas.model.enumerator;


import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.jenkins.faas.domain.*;

/**
 *
 * @author nblg6551
 */
public enum JenkinsFaasIndicatorType {

    /**
     *
     */
    FAAS_JOB_SUCCESS(PercentageJobsSuccessesIndicatorFaas.class),
    /**
     *
     */
    FAAS_TIME_BUILD(TimeBuildIndicatorFaas.class),

    /**
     *
     */
    FAAS_TOTALS_BUILDS(TotalsBuildsIndicatorFaas.class),

    /**
     *
     */
    FAAS_JOB_FAIL(PercentageJobsFailsIndicatorFaas.class),
    /**
     *
     */
    FAAS_HEALTH_REPORT_BUILD(HealthReportBuildIndicatorFaas.class),
    /**
     *
     */
    FAAS_JOB_BUILD_STATUS(JobBuildStatusIndicatorFaas.class);


    private Class<? extends AbstractIndicator> clazz;

        JenkinsFaasIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     *
     * @return
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     *
     * @return
     */
    public String getLabel() {
        switch (this.name()) {
            case "FAAS_JOB_SUCCESS":
                return "Builds réussis";
            case "FAAS_TIME_BUILD" :
                return "Temps total de build";
            case "FAAS_TOTALS_BUILDS" :
                return "Nombre de builds";
            case "FAAS_JOB_FAIL":
                return "Builds échoués";
            case "FAAS_HEALTH_REPORT_BUILD":
                return "Statut du job";
            case "FAAS_JOB_BUILD_STATUS":
                return "Statut du dernier build du job";

            default: return null;
        }
    }
}


