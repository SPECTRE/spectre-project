package com.orange.spectre.plugins.jira.model;

/**
 * Created by epeg7421 on 14/04/17.
 */
public class JiraStatus {
	private String id;
	private String name;

	private StatusCategory statusCategory;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StatusCategory getStatusCategory() {
		return statusCategory;
	}

	public void setStatusCategory(StatusCategory statusCategory) {
		this.statusCategory = statusCategory;
	}
}
