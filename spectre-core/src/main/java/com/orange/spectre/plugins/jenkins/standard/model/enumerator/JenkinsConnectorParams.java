package com.orange.spectre.plugins.jenkins.standard.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 *
 * @author nblg6551
 */
public enum JenkinsConnectorParams implements ConnectorParamsInterface {

    url (true, false),
    nom_utilisateur (true, false),
    mot_de_passe (true, true),
    nom_projet (false, false),
    nom_vue (false, false);
	
	
	private final boolean mandatory;
	private final boolean secured;
	
	private JenkinsConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}
	
	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}
	
	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
