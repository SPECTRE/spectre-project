package com.orange.spectre.plugins.mingle.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.mingle.model.MingleStoryPojo;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;
import com.orange.spectre.plugins.mingle.util.MingleMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

import static com.orange.spectre.plugins.mingle.model.enumerator.MingleIndicatorType.SPRINT_CLOSED_STORIES;

/**
 * Jpa entity for percentage jobs fails
 *
 * @author nmcf5735
 */
@Entity
@DiscriminatorValue("SPRINT_CLOSED_STORIES")
public class SprintClosedStoriesIndicator extends AbstractIndicator {

    @Override
    public String getLabel() {
        return SPRINT_CLOSED_STORIES.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return "points";
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {


        String sprint = connectorParams.get(MingleConnectorParams.sprint_courant.name());

        if (sprint != null) {

            String mqlRequest = "SELECT complexité, 'planning - sprint', statut WHERE type='story' and 'planning - sprint' != null";

            JSONArray jsonArray = MingleMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {
                try {
                    Integer complexity = getClosedComplexity(jsonArray, sprint);
                    return new IntegerData(complexity);
                } catch (IOException e) {
                    return null;
                }
            }
        }
        return null;
    }

    private Integer getClosedComplexity(JSONArray jsonArray, String sprint) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        int complexitySum = 0;

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject story = (JSONObject) jsonArray.get(i);
            MingleStoryPojo pojo = mapper.readValue(story.toString(), MingleStoryPojo.class);

            if (pojo.getComplexity() != null && pojo.getSprint().contains(sprint) && pojo.getStatus() != null && pojo.getStatus().equals("Terminé")) {
                complexitySum += Integer.valueOf(pojo.getComplexity());
            }
        }
        return complexitySum;

    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return SPRINT_CLOSED_STORIES;
    }


}

