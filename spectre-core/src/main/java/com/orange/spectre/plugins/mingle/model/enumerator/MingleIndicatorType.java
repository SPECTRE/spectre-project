
package com.orange.spectre.plugins.mingle.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.mingle.domain.*;

/**
 * Enumerator of all available mingle indicators
 */
public enum MingleIndicatorType {

    SPRINT_COMPLEXITY(SprintComplexityIndicator.class),
    SPRINT_CLOSED_STORIES(SprintClosedStoriesIndicator.class),
    SPRINT_OPENED_STORIES(SprintOpenedStoriesIndicator.class),
    SPRINT_PROGRESSION(SprintProgressionIndicator.class),
    SPRINT_VELOCITY(SprintVelocityIndicator.class),
    SPRINT_NAME(SprintNameIndicator.class);


    /**
     * Current mingle indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    MingleIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from it's type
     * @return An mingle indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "SPRINT_COMPLEXITY":
                return "Complexité du sprint";
            case "SPRINT_CLOSED_STORIES":
                return "Points terminé";
            case "SPRINT_OPENED_STORIES":
                return "Points en cours";
            case "SPRINT_PROGRESSION":
                return "Avancement du sprint";
            case "SPRINT_VELOCITY":
                return "Vélocité du sprint";
            case "SPRINT_NAME":
                return "Nom du sprint";


            default: return null;
        }

    }
}


