package com.orange.spectre.plugins.weather.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.weather.model.enumerator.WeatherConnectorParams;
import com.orange.spectre.plugins.weather.model.enumerator.WeatherIndicatorType;
import net.aksingh.owmjapis.api.APIException;
import net.aksingh.owmjapis.core.OWM;
import net.aksingh.owmjapis.model.CurrentWeather;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for sample indicator
 */
@Entity
@DiscriminatorValue("WEATHER_DESCRIPTION")
public class DescriptionIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DescriptionIndicator.class);

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {


        String token = connectorParams.get(WeatherConnectorParams.token.name());
        String ville = connectorParams.get(WeatherConnectorParams.ville.name());

        OWM owm = new OWM(token);
        owm.setUnit(OWM.Unit.METRIC);
        owm.setLanguage(OWM.Language.FRENCH);
        owm.setProxy("localhost", Integer.valueOf(ProxyConfig.getInstance().getPort()));
        CurrentWeather cwd = null;
        try {
            cwd = owm.currentWeatherByCityName(ville);

        } catch (APIException e) {
            LOGGER.error("Error during getData call ", e);
            e.printStackTrace();
        }
        String desc = cwd.getWeatherList().get(0).getDescription();
        return new StringData(String.valueOf(desc));

    }
    @Override
    public Enum getType() {
        return WeatherIndicatorType.WEATHER_DESCRIPTION;
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.TEXT ;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.AUTRE;
    }

    @Override
    public String getLabel() {
        return WeatherIndicatorType.WEATHER_DESCRIPTION.getLabel();
    }

}
