package com.orange.spectre.plugins.qc.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.qc.model.enumerator.QcConnectorParams;
import com.orange.spectre.plugins.qc.model.enumerator.QcFieldName;
import com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType;
import com.orange.spectre.plugins.qc.util.ComputeQcResults;
import com.orange.spectre.plugins.qc.util.QCConstant;
import com.orange.spectre.plugins.qc.util.QcAuthentification;
import com.orange.spectre.plugins.qc.util.RestConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

/**
 * Jpa entity for critical severity
 */
@Entity
@DiscriminatorValue("QC_CRITICAL_SEVERITY_NB")
public class CriticalSeverityNbIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(CriticalSeverityNbIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {
		try {
			String url = QCConstant.HP_ALM_URL;
			String domain = connectorParams.get(QcConnectorParams.domaine.name());
			String project = connectorParams.get(QcConnectorParams.projet.name());

			RestConnector con = RestConnector.getInstance().init(new HashMap<String, String>(), url, domain, project);
			QcAuthentification login = new QcAuthentification();
			ComputeQcResults computeQcResults = new ComputeQcResults();
			return computeQcResults.extractSeverityData(login, con, connectorParams, QcFieldName.CRITICAL);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
		}
		return null;
	}

	@Override
	public String getLabel() {
		return QcIndicatorType.CRITICAL_SEVERITY_NB.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.QUALITY;
	}

	@Override
	public Enum getType() {
		return QcIndicatorType.CRITICAL_SEVERITY_NB;
	}

}

