package com.orange.spectre.plugins.trello.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloConnectorParams;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloIndicatorType;
import com.orange.spectre.plugins.trello.util.TrelloUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by KCTL7743 on 03/04/2017.
 *
 * Jpa entity for all tasks number
 */

@Entity
@DiscriminatorValue("TRELLO_ALL_TASKS_NUMBER")
public class AllTasksNbIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllTasksNbIndicator.class);


    @Override
    public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {

        try{

            String boardUrl = connectorParams.get(TrelloConnectorParams.url.name());
            String apiKey = connectorParams.get(TrelloConnectorParams.api_key.name());
            String token = connectorParams.get(TrelloConnectorParams.token.name());

            // Retrieve the board id
            String boardId = TrelloUtils.getBoardId(boardUrl,apiKey,token);

            // Retrieve all task number
            int allTasksNb = TrelloUtils.getAllTaskNumber(boardId, apiKey, token);

            LOGGER.debug(Integer.toString(allTasksNb));

            IntegerData integerData = new IntegerData(new Integer(allTasksNb));

            return  integerData;

        }catch (Exception e){

            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return TrelloIndicatorType.ALL_TASKS_NB;
    }

    @Override
    public String getLabel() {
        return TrelloIndicatorType.ALL_TASKS_NB.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}
