package com.orange.spectre.plugins.sonar.standard.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;


/**
 * Jpa entity for blocker violation indicator
 */
@Entity
@DiscriminatorValue("SONAR_TEST_SUCCESS_DENSITY")
public class TestSuccessDensityIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestSuccessDensityIndicator.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) {

        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "test_success_density";
            String sonarPartialUrl = connectorParams.get("url");
            return ComputeDataUtils.extract(sonarPartialUrl, project, version, metric);

        }catch(Exception e){
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
            return SonarIndicatorType.TEST_SUCCESS_DENSITY.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Le pourcentage de tests en succès permet d’identifier la qualité et la robustesse de son application";
    }

    @Override
    public String getDefinition() {
        return "Il peut s’agir de tests unitaires, de tests fonctionnels ou d’autres types de tests (performance,…)";
    }

    @Override
    public String getOrigin() {
        return "SonarQube";
    }

    @Override
    public Enum getType() {
        return SonarIndicatorType.TEST_SUCCESS_DENSITY;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
