package com.orange.spectre.plugins.sonar.standard.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.orange.spectre.plugins.sonar.standard.domain.SonarConnector;

/**
 * Sonar connector JPA enumerator
 */
public interface SonarConnectorRepository extends JpaRepository<SonarConnector, Long> {

}
