package com.orange.spectre.plugins.trello.util;

import com.orange.spectre.plugins.common.util.ProxyConfig;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.cert.X509Certificate;

/**
 * Created by KCTL7743 on 04/04/2017.
 */
public final class TrelloUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrelloUtils.class);

    public static HttpURLConnection testConnector(String boardUrl, String apiKey, String token) throws Exception {

        String url = boardUrl + ".json?key=" + apiKey + "&token=" + token;

        return request(url);

    }

    public static String getBoardId(String boardUrl, String apiKey, String token) throws Exception {

        String url = boardUrl + ".json?key=" + apiKey + "&token=" + token;

        return retrieveHttpReponseInJSON(request(url)).getString("id");

    }

    public static int getAllTaskNumber (String boardId, String apiKey, String token) throws Exception {

        return retrieveHttpReponseInJSONArray(request(TrelloConstants.TRELLO_API_BOARD_URL+boardId+"/cards?fields=name&key=" + apiKey + "&token=" + token)).length() ;
    }

    public static JSONArray getBoardListsAndCards (String boardId, String apiKey, String token ) throws Exception{

        return retrieveHttpReponseInJSONArray(request(TrelloConstants.TRELLO_API_BOARD_URL+boardId+"/lists?cards=open&card_fields=name&fields=name&key=" + apiKey + "&token=" + token));
    }

    private static HttpURLConnection request(String url) throws Exception {

        TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        }
        };

        // Install the all-trusting trust manager
        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

        Proxy proxy;
        HttpsURLConnection con;

        // Use Proxy Settings if present
        if (ProxyConfig.getInstance().isUseProxy()) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort())));
            con = (HttpsURLConnection) new URL(url).openConnection(proxy);
        } else {
            con = (HttpsURLConnection) new URL(url).openConnection();
        }

        con.setRequestMethod("GET");

        return con;

    }

    /**
     * Parse http reponse in JSON format
     *
     * @param con param
     * @throws IOException
     */
    private static JSONObject retrieveHttpReponseInJSON(HttpURLConnection con) {

        String content = "";

        try {

            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

            String input;

            while ((input = br.readLine()) != null) {
                content += input;
            }
            br.close();

        } catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return new JSONObject(content);

    }

    /**
     * Parse http reponse in JSON format
     *
     * @param con param
     * @throws IOException
     */
    private static JSONArray retrieveHttpReponseInJSONArray(HttpURLConnection con) {

        String content = "";

        try {

            BufferedReader br =
                    new BufferedReader(
                            new InputStreamReader(con.getInputStream()));

            String input;

            while ((input = br.readLine()) != null) {
                content += input;
            }
            br.close();

        } catch (IOException e) {
            LOGGER.error(e.toString());
        }

        return new JSONArray(content);

    }
}
