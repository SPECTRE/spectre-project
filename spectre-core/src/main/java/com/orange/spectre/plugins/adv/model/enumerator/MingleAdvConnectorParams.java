package com.orange.spectre.plugins.adv.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all mingle connection parameters
 */
public enum MingleAdvConnectorParams implements ConnectorParamsInterface {

    url(true, false),
    projet(true, false);

    private final boolean mandatory;
    private final boolean secured;

    private MingleAdvConnectorParams(boolean mandatory, boolean secured) {
        this.mandatory = mandatory;
        this.secured = secured;
    }

    @Override
    public boolean isMandatory() {
        return this.mandatory;
    }

    @Override
    public boolean isSecured() {
        return this.secured;
    }

}
