package com.orange.spectre.plugins.adv.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.adv.model.MingleAnomaliesNbPojo;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvIndicatorType;
import com.orange.spectre.plugins.adv.util.MingleAdvMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by htxs7368 on 6/12/2017.
 */
@Entity
@DiscriminatorValue("ADV_COUNT_US_NONTERMINE_NONENREDACTION")
public class USCountNontermineNnonEnRedaction extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(SprintEncour.class);

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        String mqlRequest = "SELECT  count(*) WHERE type = 'Story' and 'Statut de la story' !='terminé' and 'Statut de la story' != 'en rédaction'";
        try {
            JSONArray jsonArray = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {
                JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                ObjectMapper mapper = new ObjectMapper();
                MingleAnomaliesNbPojo mingleAnomaliesNbPojo = mapper.readValue(jsonObject.toString(), MingleAnomaliesNbPojo.class);

                return new IntegerData(mingleAnomaliesNbPojo.getCount());
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.AUTRE;
    }

    @Override
    public Enum getType() {
        return MingleAdvIndicatorType.ADV_COUNT_US_NONTERMINE_NONENREDACTION;
    }

    @Override
    public String getLabel() {
        return MingleAdvIndicatorType.ADV_COUNT_US_NONTERMINE_NONENREDACTION.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}
