package com.orange.spectre.plugins.spectre.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.spectre.domain.*;

/**
 * Created by epeg7421 on 03/10/2016.
 *
 * Enumerator of all available spectre indicators managed
 */
public enum SpectreIndicatorType {

	PROJECT_NB(ProjectsNumberIndicator.class),
	PRIVATE_PROJECT_NB(PrivateProjectsNumberIndicator.class),
	PUBLIC_PROJECT_NB(PublicProjectsNumberIndicator.class),
	ALL_PUBLIC_PRIVATE_PROJECT_NB(AllPublicPrivateProjectsNumberIndicator.class),
	DASHBOARD_NB(DashboardsNumberIndicator.class),
	PUBLIC_DASHBOARD_NB(PublicDashboardsNumberIndicator.class),
	PRIVATE_DASHBOARD_NB(PrivateDashboardsNumberIndicator.class),
	ALL_PUBLIC_PRIVATE_DASHBOARD_NB(AllPublicPrivateDashboardsNumberIndicator.class),
	CONNECTOR_NB(ConnectorsNumberIndicator.class),
	USER_NB(UsersNumberIndicator.class),
	CONNECTOR_BY_PROJECT_NB(ConnectorByProjectNumberIndicator.class);

	/**
	 * Current Spectre indicator class
	 */
	private Class<? extends AbstractIndicator> clazz;

	/**
	 * Indicator type constructor
	 *
	 * @param clazz
	 *            Current indicator class
	 */
	SpectreIndicatorType(Class<? extends AbstractIndicator> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Indicator instantiation from it's type
	 *
	 * @return An Spectre indicator
	 */
	public AbstractIndicator createIndicator() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(clazz + " has no default constructor");
		}
	}

	/**
	 * Get indicator label from it's type
	 *
	 * @return Indicator label
	 */
	public String getLabel() {
		switch (this.name()) {
		case "USER_NB":
			return "Utilisateurs";
		case "PROJECT_NB":
			return "Projets";
		case "PRIVATE_PROJECT_NB":
			return "Projets privés";
		case "PUBLIC_PROJECT_NB":
			return "Projets publics";
		case "ALL_PUBLIC_PRIVATE_PROJECT_NB":
			return "Projets totals, publics, privés";
		case "DASHBOARD_NB":
			return "Dashboards";
		case "PUBLIC_DASHBOARD_NB":
			return "Dashboards publics";
		case "PRIVATE_DASHBOARD_NB":
			return "Dashboards privés";
		case "ALL_PUBLIC_PRIVATE_DASHBOARD_NB":
			return "Dashboards totals, publics, privés";
		case "CONNECTOR_NB":
			return "Connecteurs";
		case "CONNECTOR_BY_PROJECT_NB":
			return "Connecteurs par projet";
		default:
			return null;
		}
	}
}
