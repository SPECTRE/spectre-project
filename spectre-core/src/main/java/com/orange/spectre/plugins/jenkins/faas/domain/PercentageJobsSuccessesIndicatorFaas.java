package com.orange.spectre.plugins.jenkins.faas.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.common.model.ResponseFaasJSON;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasIndicatorType;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity for percentage jobs successes
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_FAAS_JOB_SUCCESS")
public class PercentageJobsSuccessesIndicatorFaas extends AbstractIndicator {

    public PercentageJobsSuccessesIndicatorFaas() {
        super();
    }

    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getLabel(){
        return JenkinsFaasIndicatorType.FAAS_JOB_SUCCESS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier la stabilité d’une application durant la phase de développement et la qualité de travail des développeurs";
    }

    @Override
    public String getDefinition() {
        return "Représente le taux de build jenkins qui ont réussi.";
    }

    @Override
    public String getOrigin() {
        return "Jenkins FAAS";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return JenkinsFaasIndicatorType.FAAS_JOB_SUCCESS;
    }

    /**
     *
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {
            String outputFlux = JenkinsFaasFluxUtil.getFluxJson(JenkinsFaasFluxUtil.buildUrlRemoteAPI(connectorParams)).replaceAll("\n", ",");

            String fluxJSONArray = "[" + outputFlux.substring(0, outputFlux.length()-1) + "]";

            return this.computeData(connectorParams, fluxJSONArray);

        } catch (Exception e) {

            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
        }
        return null;
    }

    public FloatData computeData(Map<String, String> connectorParams, String fluxJSONArray) throws IOException {

        float nbBuildSuccess = 0;
        float percentSucces;
        FloatData data = new FloatData();
        ObjectMapper mapper = new ObjectMapper();

        List<ResponseFaasJSON> responseJob ;

        List<String> allJobsName =  new ArrayList<>();
        List<ResponseFaasJSON> lastBuildJobs = new ArrayList<>();


            try {

                responseJob = mapper.readValue(fluxJSONArray, new TypeReference<List<ResponseFaasJSON>>(){});

                allJobsName = responseJob.stream().map(ResponseFaasJSON::getJobName).collect(Collectors.toSet())
                        .stream().sorted(String::compareToIgnoreCase).collect(Collectors.toList());

                allJobsName.stream().forEach(
                        job-> lastBuildJobs.add(
                                responseJob.stream().filter(rep -> job.equals(rep.getJobName()))
                                        .max(Comparator.comparing(c -> Double.parseDouble(c.getStarted())))
                                        .get()));

                nbBuildSuccess = (float) lastBuildJobs.stream().filter(jb -> "SUCCESS".equals(jb.getResult())).collect(Collectors.toList()).size();


            } catch (JsonMappingException e) {
                LOGGER.error("JSON Mapping Exception with parameters " + connectorParams.toString());

            } catch (JsonParseException e) {
                LOGGER.error("JSON Parse Exception with parameters " + connectorParams.toString() );

            } catch (IOException e) {
                LOGGER.error("IO Exception with parameters " + connectorParams.toString() );
            }


        percentSucces= ( nbBuildSuccess/(allJobsName.size()))*100;

        percentSucces= (float) Math.round((double) percentSucces*100)/100;

        data.setData(percentSucces);

        return data;
    }
}

