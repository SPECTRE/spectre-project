package com.orange.spectre.plugins.adv.repository;

import com.orange.spectre.plugins.mingle.domain.MingleConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * mingle connector JPA enumerator
 */
public interface MingleAdvConnectorRepository extends JpaRepository<MingleConnector, Long> {
}
