package com.orange.spectre.plugins.tuleap.bug.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.plugins.tuleap.common.model.*;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapApiException;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapConstants;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by ludovic on 24/03/2017.
 */
public final class TuleapBugUtils {

    private static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    /**
     * @param token
     * @param userId
     * @param projectId
     * @return
     * @throws TuleapApiException
     */
    public static String getTuleapBugTrackerId(String token, String userId, String projectId) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PROJECTS_URI);
            projectsUrl.append(projectId);
            projectsUrl.append(TuleapConstants.FORGE_TRACKERS_URI);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapProjectTracker[] tuleapProjectTrackerResponse = objectMapper.readValue(response.getEntity().getContent(), TuleapProjectTracker[].class);
                if (tuleapProjectTrackerResponse != null && tuleapProjectTrackerResponse.length > 0) {
                    for (TuleapProjectTracker tracker : tuleapProjectTrackerResponse) {
                        if (StringUtils.lowerCase(tracker.getItemName()).equals("bugs") ) {
                            return tracker.getId();
                        }
                    }
                }
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    /**
     * @param token
     * @param userId
     * @param trackerId
     * @return
     * @throws TuleapApiException
     */
    public static String getTuleapBugTrackerReportId(String token, String userId, String trackerId, String rapportName) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_TRACKERS_URI);
            projectsUrl.append(trackerId);
            projectsUrl.append(TuleapConstants.FORGE_TRACKER_REPORTS_URI);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapTrackerReport[] reports = objectMapper.readValue(response.getEntity().getContent(), TuleapTrackerReport[].class);
                if (reports != null && reports.length > 0) {
                    for (TuleapTrackerReport report : reports) {
                        if (report.getLabel().equals(rapportName)) {
                            return report.getId();
                        }
                    }
                }
            }
            throw new TuleapApiException("Bad response from tuleap tracker report api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap tracker report api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap tracker report api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap tracker report api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap tracker report api");
        }
    }

    /**
     * @param token
     * @param userId
     * @param trackerReportId
     * @return
     * @throws TuleapApiException
     */
    public static TuleapBug[] getTuleapBugs(String token, String userId, String trackerReportId) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_TRACKER_REPORTS_URI);
            projectsUrl.append(trackerReportId);
            projectsUrl.append(TuleapConstants.FORGE_ARTIFACTS_URI);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapBug[] results = objectMapper.readValue(response.getEntity().getContent(), TuleapBug[].class);

                Integer elementsCount = Integer.valueOf(response.getHeaders("X-Pagination-Size")[0].getValue());
                if (elementsCount > 10) {
                    int iterations  = Math.floorDiv((elementsCount - 10), 10);
                    if(Math.floorMod((elementsCount - 10), 10) > 0){
                        iterations++;
                    }
                    int startIndex = 10;
                    for (int i = 0; i < iterations; i++) {
                        String projectsUrlIndex = projectsUrl.toString().concat("?offset=").concat(String.valueOf(startIndex));
                        httpGet = new HttpGet(projectsUrlIndex.toString());
                        httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
                        httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
                        response = httpClient.execute(httpGet);
                        TuleapBug[] resultsIndex = objectMapper.readValue(response.getEntity().getContent(), TuleapBug[].class);
                        results = ArrayUtils.addAll(results, resultsIndex);
                        startIndex += 10;
                    }
                }
                return results;



            }
            throw new TuleapApiException("Bad response from tuleap bug api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap bug api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap bug api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap bug api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap bug api");
        }
    }

    /**
     * @param token
     * @param userId
     * @param trackerReportId
     * @return
     * @throws TuleapApiException
     */
    public static TuleapFilledBug[] getTuleapFilledBugs(String token, String userId, String trackerReportId) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_TRACKER_REPORTS_URI);
            projectsUrl.append(trackerReportId);
            projectsUrl.append(TuleapConstants.FORGE_ARTIFACTS_URI);
            projectsUrl.append("?values=all");

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            TuleapFilledBug[] results = null;

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                Integer elementsCount = Integer.valueOf(response.getHeaders("X-Pagination-Size")[0].getValue());
                results = objectMapper.readValue(response.getEntity().getContent(), TuleapFilledBug[].class);
                if (elementsCount > 10) {
                    int iterations  = Math.floorDiv((elementsCount - 10), 10);
                    if(Math.floorMod((elementsCount - 10), 10) > 0){
                        iterations++;
                    }
                    int startIndex = 10;
                    for (int i = 0; i < iterations; i++) {
                        String projectsUrlIndex = projectsUrl.toString().concat("&offset=").concat(String.valueOf(startIndex));
                        httpGet = new HttpGet(projectsUrlIndex.toString());
                        httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
                        httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
                        response = httpClient.execute(httpGet);
                        TuleapFilledBug[] resultsIndex = objectMapper.readValue(response.getEntity().getContent(), TuleapFilledBug[].class);
                        results = ArrayUtils.addAll(results, resultsIndex);
                        startIndex += 10;
                    }
                }

            }
            return results;

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap reports artifacts api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap reports artifacts api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap reports artifacts api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap reports artifacts api");
        }
    }

    /**
     * @param bugs
     * @param criterias
     * @return
     */
    public static TupleData getTuleDataFromCriteria(TuleapFilledBug[] bugs, List<String> criterias) {
        Map<String, List<String>> bugsMap = new HashedMap();
        for (TuleapFilledBug bug : bugs) {
            for (TuleapBugValue bugValue : bug.getValues()) {
                if (criterias.contains(bugValue.getLabel()) && bugValue.getValues().length > 0) {
                    String value = bugValue.getValues()[0].getLabel();
                    String id = bugValue.getValues()[0].getId();
                    if (bugsMap.containsKey(value)) {
                        bugsMap.get(value).add(id);
                    } else {
                        List<String> tempBugList = new ArrayList<>();
                        tempBugList.add(id);
                        bugsMap.put(value, tempBugList);
                    }
                }
            }
        }


        TupleData tuple = new TupleData();
        tuple.setMeasure("bugs");
        List<String> datas = new ArrayList<>();
        List<String> labels = new ArrayList<>();
        for (String key : bugsMap.keySet()) {
            labels.add(key);
            datas.add(String.valueOf(bugsMap.get(key).size()));
        }
        tuple.setData(datas);
        tuple.setLabel(labels);
        return tuple;
    }

}
