package com.orange.spectre.plugins.sonar.faas.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.faas.enumerator.SonarFaasIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kctl7743 on 26/06/2017.
 */
@Entity
@DiscriminatorValue("COMPLEXITY_INDICATOR")
public class ComplexityIndicator extends AbstractIndicator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComplexityIndicator.class);

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String sonarPartialUrl = connectorParams.get("url");

            String metric_class = "class_complexity";
            String metric_function = "function_complexity";
            String metric_file = "file_complexity";

            List<String> label = new ArrayList<>();
            List<String> data = new ArrayList<>();

            String classData = ComputeDataUtils.extractFaas(sonarPartialUrl, project, version, metric_class);
            String functionData = ComputeDataUtils.extractFaas(sonarPartialUrl, project, version, metric_function);
            String fileData = ComputeDataUtils.extractFaas(sonarPartialUrl, project, version, metric_file);

            label.add("Complexity of classes");
            data.add(classData);

            label.add("Complexity of Functions");
            data.add(functionData);

            label.add("Complexity of Files");
            data.add(fileData);

            TupleData complexity = new TupleData();

            complexity.setLabel(label);
            complexity.setData(data);
            complexity.setMeasure(IndicatorMeasure.NUMERIC.name());

            return complexity;

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }
        return null;
    }

    @Override
    public String getLabel() {
        return SonarFaasIndicatorType.COMPLEXITY.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre la complexité des fichiers, classes et fonctions";
    }

    @Override
    public String getDefinition() {
        return "Cet indicateur permet de mesurer la complexité de votre code (fichiers, classes et fonctions)";
    }

    @Override
    public String getOrigin() {
        return "Sonar Faas";
    }

    @Override
    public Enum getType() {
        return SonarFaasIndicatorType.COMPLEXITY;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
