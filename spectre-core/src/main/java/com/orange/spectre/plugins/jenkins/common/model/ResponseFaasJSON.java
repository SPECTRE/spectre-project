package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Created by KCTL7743 on 09/05/2017.
 */


@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseFaasJSON {


    @JsonProperty("duration")
    private String duration;
    @JsonProperty("forgeName")
    private String forgeName;
    @JsonProperty("jobName")
    private String jobName;
    @JsonProperty("nodeName")
    private String nodeName;
    @JsonProperty("queued")
    private String queued;
    @JsonProperty("result")
    private String result;
    @JsonProperty("runClass")
    private String runClass;
    @JsonProperty("started")
    private String started;
    @JsonProperty("timeInQueue")
    private String timeInQueue;
    @JsonProperty("rootCauses")
    private String rootCauses;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getForgeName() {
        return forgeName;
    }

    public void setForgeName(String forgeName) {
        this.forgeName = forgeName;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getNodeName() {
        return nodeName;
    }

    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }

    public String getQueued() {
        return queued;
    }

    public void setQueued(String queued) {
        this.queued = queued;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getRunClass() {
        return runClass;
    }

    public void setRunClass(String runClass) {
        this.runClass = runClass;
    }

    public String getStarted() {
        return started;
    }

    public void setStarted(String started) {
        this.started = started;
    }

    public String getTimeInQueue() {
        return timeInQueue;
    }

    public void setTimeInQueue(String timeInQueue) {
        this.timeInQueue = timeInQueue;
    }

    public String getRootCauses() {
        return rootCauses;
    }

    public void setRootCauses(String rootCauses) {
        this.rootCauses = rootCauses;
    }


}
