package com.orange.spectre.plugins.devopsic.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.devopsic.enumerator.DevopsicIndicatorType;
import com.orange.spectre.plugins.devopsic.util.ComputeDataUtils;
import com.orange.spectre.plugins.devopsic.util.DevopsIcResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for global grade indicator
 */
@Entity
@DiscriminatorValue("SOCLE_NOTE_DC_INDICATOR")
public class SocleNoteDcIndicator extends AbstractIndicator {

	/**
	 *
	 */
	private static final long	serialVersionUID	= 3108793270529496569L;
	private static final Logger	LOGGER				= LoggerFactory
															.getLogger(SocleNoteDcIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {

		try {
			String devopsicUrl = connectorParams.get("url");
			DevopsIcResponse response = ComputeDataUtils.extract(devopsicUrl);

			if (response != null)
				return new StringData(response.getSocle_note_DC());

		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval : " + e);
		}

		return null;
	}

	@Override
	public String getLabel() {
		return DevopsicIndicatorType.SOCLE_NOTE_DC.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.PERCENT;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public Enum<DevopsicIndicatorType> getType() {
		return DevopsicIndicatorType.SOCLE_NOTE_DC;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.DEVOPS;
	}
}
