package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 09/06/2017.
 */
@JsonIgnoreProperties
public class MingleSprintDatePojo {

    @JsonProperty("Name")
    private String name;
    @JsonProperty("Date de début")
    private String startTime;
    @JsonProperty("Date de fin")
    private String endTime;

    public int getStoriesNb() {
        return storiesNb;
    }

    public void setStoriesNb(int storiesNb) {
        this.storiesNb = storiesNb;
    }

    private int storiesNb;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
