package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 21/03/2017.
 */
public class TuleapProject {
    private String id;
    private String label;
    private String shortname;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getShortname() {
        return shortname;
    }

    public void setShortname(String shortName) {
        this.shortname = shortName;
    }

}
