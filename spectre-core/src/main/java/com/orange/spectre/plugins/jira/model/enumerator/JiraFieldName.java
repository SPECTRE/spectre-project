package com.orange.spectre.plugins.jira.model.enumerator;

/**
 * Created by epeg7421 on 07/04/17.
 */
public enum JiraFieldName {
	Highest,
	High,
	Medium,
	Low,
	Lowest
}
