package com.orange.spectre.plugins.devopsic.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all devopsic connection parameters
 */
public enum DevopsicConnectorParams implements ConnectorParamsInterface {

    url(true, false);

    private final boolean mandatory;
    private final boolean secured;

    private DevopsicConnectorParams(boolean mandatory, boolean secured) {
        this.mandatory = mandatory;
        this.secured = secured;
    }

    @Override
    public boolean isMandatory() {
        return this.mandatory;
    }

    @Override
    public boolean isSecured() {
        return this.secured;
    }

}
