package com.orange.spectre.plugins.sonar.standard.domain;


import javax.persistence.DiscriminatorValue;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.common.util.SonarResponse;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarConnectorParams;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Jpa entity of Sonar connector
 */
@Entity
@DiscriminatorValue("SONAR")
public class SonarConnector extends AbstractConnector {

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;


	/**
	 * Get list of all sonar indicators
	 * @return All sonar indicators
     */
	public List<String> getAvailableIndicators(){
		return Arrays.stream(SonarIndicatorType.values()).map(Enum::name).collect(Collectors.toList());
	}

	/**
	 * Get list of all sonar connection parameters
	 * @return Sonar connection parameters
     */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters(){
		if(requiredParams == null){
			requiredParams = new ArrayList<>();
			for(SonarConnectorParams param : SonarConnectorParams.values()){
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return 	requiredParams;
	}

	/**
	 * Get sonar string type
	 * @return String type
     */
	@Override
	public String getType() {
		return "sonar";
	}

	/**
	 * Create a sonar indicator from an indicator type
	 * @param s indicator type
	 * @return A new sonar indicator
     */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return SonarIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		String metric = "sqale_index";
		String sonarPartialUrl = connectorParams.get("url");
		String sonarUrl = sonarPartialUrl+"/sonar/api/resources?metrics="+metric+"&format=json";
		RestTemplate restTemplate = new RestTemplate();

		try{
			SonarResponse[] sonarResponseArray = restTemplate.getForObject(sonarUrl, SonarResponse[].class);
			if (sonarResponseArray.length != 0) {
				return true;
			} else {
				return false;
			}
		}catch (RestClientException e){
			return false;
		}

	}
}
