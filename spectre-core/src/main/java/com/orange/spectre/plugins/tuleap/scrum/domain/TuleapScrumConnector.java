package com.orange.spectre.plugins.tuleap.scrum.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.tuleap.common.model.TuleapPlanning;
import com.orange.spectre.plugins.tuleap.common.model.TuleapSprint;
import com.orange.spectre.plugins.tuleap.common.model.TuleapTokenResponse;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapUtils;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumConnectorParams;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumIndicatorType;
import com.orange.spectre.plugins.tuleap.scrum.model.Sprint;
import com.orange.spectre.plugins.tuleap.scrum.utils.TuleapScrumUtils;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Jpa entity of Tuleap scrum connector
 */
@Entity
@DiscriminatorValue("TULEAP_SCRUM")
public class TuleapScrumConnector extends AbstractConnector {

    /**
     * All connector connection params
     */
    @Transient
    List<ConnectorRequiredParams> requiredParams;


    /**
     * Get list of all Tuleap indicators
     *
     * @return All Tuleap indicators
     */
    public List<String> getAvailableIndicators() {
        return Arrays.stream(TuleapScrumIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

    }

    /**
     * Get list of all Tuleap connection parameters
     *
     * @return Tuleap connection parameters
     */
    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (TuleapScrumConnectorParams param : TuleapScrumConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);

            }
        }
        return requiredParams;
    }

    /**
     * Get Tuleap string type
     *
     * @return String type
     */
    @Override
    public String getType() {
        return "forge_scrum";
    }

    /**
     * Create a Tuleap indicator from an indicator type
     *
     * @param s indicator type
     * @return A new Tuleap indicator
     */
    @Override
    public AbstractIndicator createIndicator(String s) {

        return TuleapScrumIndicatorType.valueOf(s).createIndicator();
    }

    @Override
    public boolean testConnector(Map<String, String> connectorParams) {
        try {

            String login = connectorParams.get(TuleapScrumConnectorParams.login.name());
            String password = connectorParams.get(TuleapScrumConnectorParams.mot_de_passe.name());
            String project = connectorParams.get(TuleapScrumConnectorParams.nom_projet.name());

            TuleapTokenResponse tuleapResponse = TuleapUtils.getTuleapConnectionData(login, password);
            String tuleapProjectId = TuleapUtils.getTuleapProjectIdByName(tuleapResponse.getToken(), tuleapResponse.getUserId(), project);

            TuleapPlanning tuleapScrumPlannings = TuleapScrumUtils.getTuleapScrumPlanning(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapProjectId);
            TuleapSprint[] sprints = TuleapScrumUtils.getTuleapScrumSprints(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapScrumPlannings.getId());

            return sprints.length > 0;

        } catch (Exception e) {
            return false;
        }

    }

}
