package com.orange.spectre.plugins.jira.util;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.plugins.jira.model.JiraIssue;
import com.orange.spectre.plugins.jira.model.JiraResponse;
import com.orange.spectre.plugins.jira.model.JiraStatus;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * qc connector JPA enumerator
 */
public class ComputeJiraResults {
	private static final Logger LOGGER = LoggerFactory.getLogger(ComputeJiraResults.class);
	private static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

	/**
	 * Return the query string with or without version filtering
	 *
	 * @param pQuery   : the default query string of url
	 * @param pVersion : the version to search defects   @return queryString with version if present
	 */
	public String computeWithVersion(String pQuery, String pVersion) {
		if (StringUtils.isNotEmpty(pVersion)) {
			if (StringUtils.isNotEmpty(pQuery)) {
				pQuery = String
						.format("%s%s%s", pQuery, JiraConstant.JIRA_FIX_VERSION, pVersion);
			}
		}
		//Close the query
		return pQuery;
	}

	/**
	 * Return the data
	 *
	 * @param responseData
	 * @return
	 */

	public AbstractData extractData(HttpResponse responseData) {
		Integer data = 0;
		try {
			JSONObject jsonObject = new JSONObject(responseData.getEntity().getContent());
			data = jsonObject.getInt("total");
			JSONArray priorities = jsonObject.getJSONArray("priority");

			/*String json = IOUtils.toString(responseData.getEntity().getContent());
			JSONArray array = new JSONArray(json);
			for (int i = 0; i < array.length(); i++) {
				JSONObject object = array.getJSONObject(i);
				data = object.getInt("total");
			}*/
		} catch (Exception e) {
			LOGGER.debug("Unable to get data  -- error message " + e.getMessage());
			return null;
		}
		return new StringData(data.toString());
	}

	/**
	 * Get BUG Url from custom query (by status, priority, resolution, usres....)
	 * example of query
	 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and issuetype=Bug and priority in (Highest, High, Medium) and resolution=Open&maxResults=0
	 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest,High,Medium)&maxResults=0
	 * Pas de sens de ramener des fields si maxResults à 0
	 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest,High,Medium)&fields=id,name&maxResults=0
	 * Du sens si fields d'avoir maxResults > 0
	 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest,High,Medium)&fields=id,name&maxResults=1
	 *
	 * @param url
	 * @param version
	 * @param query
	 * @return
	 */
	public String extractCustomUrl(String url, String version, String query) {
		String queryString = null;
		if (StringUtils.isNotEmpty(query) && !query.contains("&")) {
			queryString = String.format("%s and %s", url, query);
			//Get data filter by fix version
			queryString = this.computeWithVersion(queryString, version);
			//Get data with maxResults to 0 (limit the response json attributs)
			queryString = String.format("%s%s", queryString, JiraConstant.JIRA_MAX_RESULTS);
			//Get data
			return queryString.replaceAll(" ", "%20");
		} else {
			LOGGER.error("Error: the query must be not empty and must not contained & character");
			return null;
		}
	}

	/**
	 * Compute all issuetype (Bug, Epic, Story, Task, Sub-task)
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @param version
	 * @return
	 */
	public AbstractData computeIssueTypeData(String username,
			String password, String url, String version) {
		HttpResponse response = null;
		String query = null;
		JiraResponse jiraResponse = null;
		try {
			query = this.computeWithVersion(url, version);
			// https://orange-village.valiantyscloud.net/jira/rest/api/2/search?jql=project=OEMBF and issuetype=Bug&fields=priority
			query = String.format("%s%s", query, JiraConstant.JIRA_FIELDS_ISSUE_TYPE);
			response = JiraUtils.getJiraConnectionData(username, password, query);
			jiraResponse = this.getJiraResponse(response);

			//Get the count to iterate the full collection
			int maxResults = getJiraResponseCount(jiraResponse);
			query = String.format("%s%s%s", query, JiraConstant.JIRA_MAX_RESULTS_COUNT, maxResults);
			response = JiraUtils.getJiraConnectionData(username, password, query);
			jiraResponse = this.getJiraResponse(response);

			return getTupleDataIssueType(jiraResponse);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
			return null;
		}
	}

	/**
	 * Get tuple data from issue type
	 *
	 * @param response : the http response
	 * @return the tuple data
	 */
	private AbstractData getTupleDataIssueType(JiraResponse response) {
		Map<String, List<String>> bugsMap = new HashedMap();
		int total = 0;
		if (response != null) {
			for (JiraIssue issue : response.getIssues()) {
				total = Integer.parseInt(response.getTotal());
				if (issue.getFields() != null) {
					if (issue.getFields().getIssueType() != null && JiraConstant.ISSUETYPE
							.contains(issue.getFields().getIssueType().getName())) {
						String value = issue.getFields().getIssueType().getName();
						String id = issue.getFields().getIssueType().getId();
						if (bugsMap.containsKey(value)) {
							bugsMap.get(value).add(id);
						} else {
							List<String> tempBugList = new ArrayList<>();
							tempBugList.add(id);
							bugsMap.put(value, tempBugList);
						}
					}
				}
			}
		}

		TupleData tuple = new TupleData();
		List<String> datas = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		tuple.setMeasure("IssueType");
		for (String key : bugsMap.keySet()) {
			labels.add(key);
			datas.add(String.valueOf(bugsMap.get(key).size()));
		}
		tuple.setData(datas);
		tuple.setLabel(labels);
		return tuple;
	}

	/**
	 * Compute all priority (Highest, High, Medium, Low, Lowest, ....)
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @param version
	 * @return
	 */
	public TupleData computePriorityData(String username,
			String password, String url, String version) {
		HttpResponse response = null;
		String query = null;
		JiraResponse jiraResponse = null;
		try {
			query = this.computeWithVersion(url, version);
			// https://orange-village.valiantyscloud.net/jira/rest/api/2/search?jql=project=OEMBF and issuetype=Bug&fields=priority
			query = String.format("%s%s", query, JiraConstant.JIRA_FIELDS_PRIORITY);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);

			//Get the count to iterate the full collection
			int maxResults = getJiraResponseCount(jiraResponse);
			query = String.format("%s%s%s", query, JiraConstant.JIRA_MAX_RESULTS_COUNT, maxResults);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);

			return getTupleDataPriority(jiraResponse);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
			return null;
		}
	}

	private JiraResponse getJiraResponse(HttpResponse response) throws Exception {
		JiraResponse result = null;

		if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
			throw new Exception("Bad response from jira reports artifacts api");
		}
		try {
			result = objectMapper.readValue(response.getEntity().getContent(), JiraResponse.class);
		} catch (JsonParseException e) {
			throw new Exception("Bad response from jira reports artifacts api");
		} catch (JsonMappingException e) {
			throw new Exception("Bad response from jira reports artifacts api");
		} catch (IOException e) {
			throw new Exception("Bad response from jira reports artifacts api");
		}
		return result;

	}

	/**
	 * Get tuple data from priority
	 *
	 * @param jiraResponse : the http response
	 * @return the tuple data
	 */
	private TupleData getTupleDataPriority(JiraResponse jiraResponse) {
		Map<String, List<String>> bugsMap = new HashedMap();
		if (jiraResponse != null) {
			for (JiraIssue issue : jiraResponse.getIssues()) {
				if (issue.getFields() != null) {
					if (issue.getFields().getPriority() != null && JiraConstant.PRIORITY
							.contains(issue.getFields().getPriority().getName())) {
						String value = issue.getFields().getPriority().getName();
						String id = issue.getFields().getPriority().getId();
						if (bugsMap.containsKey(value)) {
							bugsMap.get(value).add(id);
						} else {
							List<String> tempBugList = new ArrayList<>();
							tempBugList.add(id);
							bugsMap.put(value, tempBugList);
						}
					}
				}
			}
		}

		TupleData tuple = new TupleData();
		List<String> datas = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		tuple.setMeasure("numeric");
		for (String key : bugsMap.keySet()) {
			labels.add(key);
			datas.add(String.valueOf(bugsMap.get(key).size()));
		}
		tuple.setData(datas);
		tuple.setLabel(labels);
		return tuple;
	}

	/**
	 * Compute all resolution (Done, Won't Do, Duplicate, Cannot Reproduce)
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @param version
	 * @return
	 */
	public AbstractData computeResolutionData(String username,
			String password, String url, String version) {
		HttpResponse response = null;
		String query = null;
		JiraResponse jiraResponse = null;
		try {
			query = this.computeWithVersion(url, version);
			query = String.format("%s%s", query, JiraConstant.JIRA_FIELDS_RESOLUTION);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);

			//Get the count to iterate the full collection
			int maxResults = getJiraResponseCount(jiraResponse);
			query = String.format("%s%s%s", query, JiraConstant.JIRA_MAX_RESULTS_COUNT, maxResults);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);

			return getTupleDataResolution(jiraResponse);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
			return null;
		}
	}

	/**
	 * Get the tuple data from resolution
	 *
	 * @param jiraResponse : the http response
	 * @return the tuple data
	 */
	private AbstractData getTupleDataResolution(JiraResponse jiraResponse) {
		Map<String, List<String>> bugsMap = new HashedMap();
		if (jiraResponse != null) {
			for (JiraIssue issue : jiraResponse.getIssues()) {
				String value = StringUtils.EMPTY;
				String id = StringUtils.EMPTY;
				if (issue.getFields() != null) {
					if (issue.getFields().getResolution() == null) {
						value = JiraConstant.UNRESOLVED;
						id = issue.getId();
					} else {
						value = issue.getFields().getResolution().getName();
						id = issue.getId();
					}
				}

				if (JiraConstant.RESOLUTION.contains(value)) {
					if (bugsMap.containsKey(value)) {
						bugsMap.get(value).add(id);
					} else {
						List<String> tempBugList = new ArrayList<>();
						tempBugList.add(id);
						bugsMap.put(value, tempBugList);
					}
				}
			}
		}

		TupleData tuple = new TupleData();
		List<String> datas = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		tuple.setMeasure("numeric");
		for (String key : bugsMap.keySet()) {
			labels.add(key);
			datas.add(String.valueOf(bugsMap.get(key).size()));
		}
		tuple.setData(datas);
		tuple.setLabel(labels);
		return tuple;
	}

	/**
	 * Compute all status (Open, In Progress, Reopened, Resolved, Closed, To Do, Done)
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @param version
	 * @return
	 */
	public AbstractData computeStatusData(String username,
			String password, String url, String version) {
		HttpResponse response = null;
		String query = null;
		JiraResponse jiraResponse = null;
		try {
			query = this.computeWithVersion(url, version);
			query = String.format("%s%s", query, JiraConstant.JIRA_FIELDS_STATUS);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);
			//Get the count to iterate the full collection
			int maxResults = getJiraResponseCount(jiraResponse);
			query = String.format("%s%s%s", query, JiraConstant.JIRA_MAX_RESULTS_COUNT, maxResults);
			response = JiraUtils.getJiraConnectionData(username, password, query.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);
			return getTupleDataStatus(jiraResponse);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
			return null;
		}
	}

	/**
	 * Get the total count of search +1
	 *
	 * @param jiraResponse : the response of uri
	 * @return the total count
	 */
	private int getJiraResponseCount(JiraResponse jiraResponse) {
		if (jiraResponse != null) {
			int maxResults = Integer.parseInt(jiraResponse.getTotal());
			return maxResults++;
		}
		return 0;
	}

	/**
	 * Get tuple data from status
	 *
	 * @param jiraResponse : the http response
	 * @return the tuple data
	 */
	private AbstractData getTupleDataStatus(JiraResponse jiraResponse) {
		Map<String, List<String>> bugsMap = new HashedMap();
		if (jiraResponse != null) {
			for (JiraIssue issue : jiraResponse.getIssues()) {
				if (issue.getFields() != null) {
					if (issue.getFields().getStatus() != null) {
						JiraStatus status = issue.getFields().getStatus();
						if (status.getStatusCategory() != null && JiraConstant.STATUS.contains(status.getStatusCategory().getName())) {
							String value = status.getStatusCategory().getName();
							String id = status.getStatusCategory().getId();
							if (bugsMap.containsKey(value)) {
								bugsMap.get(value).add(id);
							} else {
								List<String> tempBugList = new ArrayList<>();
								tempBugList.add(id);
								bugsMap.put(value, tempBugList);
							}
						}
					}
				}
			}
		}

		TupleData tuple = new TupleData();
		List<String> datas = new ArrayList<>();
		List<String> labels = new ArrayList<>();
		tuple.setMeasure("numeric");
		for (String key : bugsMap.keySet()) {
			labels.add(key);
			datas.add(String.valueOf(bugsMap.get(key).size()));
		}
		tuple.setData(datas);
		tuple.setLabel(labels);
		return tuple;
	}

	/**
	 * Custom query :
	 * project%20%3D%20OEMBF%20
	 * AND%20issuetype%20in%20(Action%2C%20Bug%2C%20%22User%20Story%22)%20
	 * AND%20status%20in%20(%22A%20faire%22%2C%20%22A%20valider%22%2C%20%22En%20cours%22)%20
	 * AND%20resolution%20in%20(Unresolved%2C%20R%C3%A9alis%C3%A9e%2C%20Rejet%C3%A9e%2C%20Fixed)
	 * &maxResults=0
	 * <p>
	 * project = OEMBF AND status in ("A faire", "A valider") AND resolution = Unresolved
	 * AND reporter in (membersOf(jira-software-users))
	 * <p>
	 * project = OEMBF AND status in ("A faire", "A valider", "EN DEFINITION")
	 * AND priority = Normal AND resolution = Unresolved ORDER BY created DESC
	 *
	 * @param username
	 * @param password
	 * @param url
	 * @param version
	 * @param query
	 * @return the data of query
	 */
	public AbstractData computeCustomData(String username, String password, String url, String version, String query) {
		HttpResponse response = null;
		String customQuery = null;
		JiraResponse jiraResponse = null;
		try {
			customQuery = this.computeWithVersion(url, version);
			if (StringUtils.isNotEmpty(query)) {
				query = query.replaceAll("\"", "%22");
				query = query.replaceAll(" ", "%20");
				query = query.replaceAll(",", "%2C");
				query = query.replaceAll("=", "%3D");

				customQuery = String.format("%s and %s", customQuery, query);

			} else {
				LOGGER.error("Query must not be empty.");
				return null;
			}
			response = JiraUtils.getJiraConnectionData(username, password, customQuery.replaceAll(" ", "%20"));
			jiraResponse = this.getJiraResponse(response);
			return new IntegerData(this.getJiraResponseCount(jiraResponse));
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
			return null;
		}
	}

}
