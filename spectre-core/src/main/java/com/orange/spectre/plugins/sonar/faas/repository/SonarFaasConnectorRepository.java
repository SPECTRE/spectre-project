package com.orange.spectre.plugins.sonar.faas.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.orange.spectre.plugins.sonar.faas.domain.SonarFaasConnector;

/**
 * Sonar connector JPA enumerator
 */
public interface SonarFaasConnectorRepository extends JpaRepository<SonarFaasConnector, Long> {

}
