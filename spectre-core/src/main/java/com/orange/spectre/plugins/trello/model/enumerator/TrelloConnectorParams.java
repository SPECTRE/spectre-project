
package com.orange.spectre.plugins.trello.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Trello Connection params
 */
public enum TrelloConnectorParams implements ConnectorParamsInterface {

	url(true, false),
	api_key (true, false),
	token (true, false);

//	nom_colonne(false, false)


	private final boolean mandatory;
	private final boolean secured;

	private TrelloConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
