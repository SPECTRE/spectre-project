package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 02/06/2017.
 */
public class MingleAvgUserStoryPojo {

    public Float getAvgPoints() {
        return avgPoints;
    }

    public void setAvgPoints(Float avgPoints) {
        this.avgPoints = avgPoints;
    }

    @JsonProperty("Avg Points")
    private Float avgPoints;
}
