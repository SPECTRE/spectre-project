package com.orange.spectre.plugins.adv.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.adv.model.MingleDureeDevAvgPojo;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvIndicatorType;
import com.orange.spectre.plugins.adv.util.MingleAdvMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by htxs7368 on 7/6/2017.
 */

@Entity
@DiscriminatorValue("ADV_DUREE_DEV")
public class TempsDeMoyenDureeDev extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(PredictabilityNbIndicator.class);

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        String mqlRequest = "SELECT AVG('Durée DEV') WHERE type=Story and 'Statut de la story' = 'Terminé' and 'type de story'!='anomalie'";
        try {
            JSONArray jsonArray = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {

                JSONObject jsonObject = (JSONObject) jsonArray.get(0);
                ObjectMapper mapper = new ObjectMapper();
                MingleDureeDevAvgPojo mingleDureeDevAvgPojo = mapper.readValue(jsonObject.toString(), MingleDureeDevAvgPojo.class);
                return new FloatData(mingleDureeDevAvgPojo.getAvgDuree());
            }
        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }

        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.AUTRE;
    }

    @Override
    public Enum getType() {
        return MingleAdvIndicatorType.ADV_DUREE_DEV;
    }

    @Override
    public String getLabel() {
        return MingleAdvIndicatorType.ADV_DUREE_DEV.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}
