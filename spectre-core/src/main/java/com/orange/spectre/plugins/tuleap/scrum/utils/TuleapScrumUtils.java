package com.orange.spectre.plugins.tuleap.scrum.utils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.plugins.tuleap.common.model.*;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapApiException;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapConstants;
import com.orange.spectre.plugins.tuleap.scrum.model.Sprint;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ludovic on 03/04/2017.
 */
public final class TuleapScrumUtils {

    private static ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    public static final List<String> SCRUM_STATUS_NOT_CURRENT = Collections.unmodifiableList(
            Arrays.asList("planned", "close"));

    public static List<TuleapBacklogItem> getTuleapBacklogItems(String token, String userId, String projectId) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PROJECTS_URI);
            projectsUrl.append(projectId);
            projectsUrl.append(TuleapConstants.FORGE_BACKLOG_URI);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                List<TuleapBacklogItem> stories = new ArrayList<>();

                TuleapBacklogItem[] results;
                Integer elementsCount = Integer.valueOf(response.getHeaders("X-Pagination-Size")[0].getValue());
                results = objectMapper.readValue(response.getEntity().getContent(), TuleapBacklogItem[].class);
                if (elementsCount > 10) {
                    int iterations = Math.floorDiv((elementsCount - 10), 10);
                    if (Math.floorMod((elementsCount - 10), 10) > 0) {
                        iterations++;
                    }
                    int startIndex = 10;
                    for (int i = 0; i < iterations; i++) {
                        String projectsUrlIndex = projectsUrl.toString().concat("?offset=").concat(String.valueOf(startIndex));
                        httpGet = new HttpGet(projectsUrlIndex.toString());
                        httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
                        httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
                        response = httpClient.execute(httpGet);
                        TuleapBacklogItem[] resultsIndex = objectMapper.readValue(response.getEntity().getContent(), TuleapBacklogItem[].class);
                        results = ArrayUtils.addAll(results, resultsIndex);
                        startIndex += 10;
                    }
                }


                if (results != null && results.length > 0) {

                    for (TuleapBacklogItem item : results) {
//                        if (item.getType().equals(TuleapConstants.FORGE_USER_STORY_TYPE)) {
                        stories.add(item);
//                        }
                    }
                }
                return stories;
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static TuleapMilestoneItem[] getTuleapScrumMilestones(String token, String userId, String projectId) throws TuleapApiException {

        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PROJECTS_URI);
            projectsUrl.append(projectId);
            projectsUrl.append(TuleapConstants.FORGE_MILESTONES_URI);
            //Get the top milestones and only the open (which is the current)
            projectsUrl.append("?fields=all&query=%7B%22status%22%3A%22open%22%7D"); //{"status":"open"}

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapMilestoneItem[] results;
                Integer elementsCount = Integer.valueOf(response.getHeaders("X-Pagination-Size")[0].getValue());
                results = objectMapper.readValue(response.getEntity().getContent(), TuleapMilestoneItem[].class);
                if (elementsCount > 10) {
                    int iterations = Math.floorDiv((elementsCount - 10), 10);
                    if (Math.floorMod((elementsCount - 10), 10) > 0) {
                        iterations++;
                    }
                    int startIndex = 10;
                    for (int i = 0; i < iterations; i++) {
                        String projectsUrlIndex = projectsUrl.toString().concat("?offset=").concat(String.valueOf(startIndex));
                        httpGet = new HttpGet(projectsUrlIndex.toString());
                        httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
                        httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
                        response = httpClient.execute(httpGet);
                        TuleapMilestoneItem[] resultsIndex = objectMapper.readValue(response.getEntity().getContent(), TuleapMilestoneItem[].class);
                        results = ArrayUtils.addAll(results, resultsIndex);
                        startIndex += 10;
                    }
                }

                return results;
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static TuleapMilestoneStory[] getTuleapStories(String token, String userId, String sprintId) throws TuleapApiException {
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_MILESTONE_URI);
            projectsUrl.append(sprintId);
            projectsUrl.append(TuleapConstants.FORGE_CONTENT_URI);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapMilestoneStory[] results;
                Integer elementsCount = Integer.valueOf(response.getHeaders("X-Pagination-Size")[0].getValue());
                results = objectMapper.readValue(response.getEntity().getContent(), TuleapMilestoneStory[].class);
                if (elementsCount > 10) {
                    int iterations = Math.floorDiv((elementsCount - 10), 10);
                    if (Math.floorMod((elementsCount - 10), 10) > 0) {
                        iterations++;
                    }
                    int startIndex = 10;
                    for (int i = 0; i < iterations; i++) {
                        String projectsUrlIndex = projectsUrl.toString().concat("?offset=").concat(String.valueOf(startIndex));
                        httpGet = new HttpGet(projectsUrlIndex.toString());
                        httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
                        httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
                        response = httpClient.execute(httpGet);
                        TuleapMilestoneStory[] resultsIndex = objectMapper.readValue(response.getEntity().getContent(), TuleapMilestoneStory[].class);
                        results = ArrayUtils.addAll(results, resultsIndex);
                        startIndex += 10;
                    }
                }

                return results;

            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static TuleapArtifact getTuleapArtifact(String token, String userId, String id) throws TuleapApiException {
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_ARTIFACTS_URI);
            projectsUrl.append(id);

            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return objectMapper.readValue(response.getEntity().getContent(), TuleapArtifact.class);
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static TuleapPlanning getTuleapScrumPlanning(String token, String userId, String tuleapProjectId) throws TuleapApiException {
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PROJECTS_URI);
            projectsUrl.append(tuleapProjectId);
            projectsUrl.append(TuleapConstants.FORGE_PLANNING_URI);


            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                TuleapPlanning[] plannings = objectMapper.readValue(response.getEntity().getContent(), TuleapPlanning[].class);
                for(TuleapPlanning planning : plannings){
                    if(planning.getLabel().contains("Sprint")){
                        return planning;
                    }
                }
                throw new TuleapApiException("Bad response from tuleap project tracker api");
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static TuleapSprint[] getTuleapScrumSprints(String token, String userId, String id) throws TuleapApiException{
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_PLANNINGS_URI);
            projectsUrl.append(id);
            projectsUrl.append(TuleapConstants.FORGE_MILESTONES_URI);
            projectsUrl.append("?limit=99");


            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return objectMapper.readValue(response.getEntity().getContent(), TuleapSprint[].class);
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }

    public static Sprint getTuleapScrumSprint(String token, String userId, String id)throws TuleapApiException {
        try {
            CloseableHttpClient httpClient = HttpClients.custom().build();

            StringBuilder projectsUrl = new StringBuilder();
            projectsUrl.append(TuleapConstants.FORGE_URL);
            projectsUrl.append(TuleapConstants.FORGE_API_MILESTONE_URI);
            projectsUrl.append(id);


            HttpGet httpGet = new HttpGet(projectsUrl.toString());

            httpGet.addHeader(new BasicHeader("X-Auth-Token", token));
            httpGet.addHeader(new BasicHeader("X-Auth-UserId", userId));
            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                return objectMapper.readValue(response.getEntity().getContent(), Sprint.class);
            }
            throw new TuleapApiException("Bad response from tuleap project tracker api");

        } catch (JsonParseException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (JsonMappingException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (ClientProtocolException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        } catch (IOException e) {
            throw new TuleapApiException("Bad response from tuleap project tracker api");
        }
    }
}
