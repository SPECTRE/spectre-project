package com.orange.spectre.plugins.sonar.faas.domain;


import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.sonar.common.util.SonarFaasUtil;
import com.orange.spectre.plugins.sonar.common.util.SonarResponse;
import com.orange.spectre.plugins.sonar.faas.enumerator.SonarFaasConnectorParams;
import com.orange.spectre.plugins.sonar.faas.enumerator.SonarFaasIndicatorType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of Sonar connector
 */
@Entity
@DiscriminatorValue("SONAR_FAAS")
public class SonarFaasConnector extends AbstractConnector {

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all sonar faas indicators
	 * @return All sonar faas indicators
     */
	public List<String> getAvailableIndicators(){
		return Arrays.stream(SonarFaasIndicatorType.values()).map(Enum::name).collect(Collectors.toList());
	}

	/**
	 * Get list of all sonar faas connection parameters
	 * @return Sonar faas connection parameters
     */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters(){
		if(requiredParams == null){
			requiredParams = new ArrayList<>();
			for(SonarFaasConnectorParams param : SonarFaasConnectorParams.values()){
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);
			}
		}
		return 	requiredParams;
	}

	/**
	 * Get sonar faas string type
	 * @return String type
     */
	@Override
	public String getType() {
		return "sonar_faas";
	}

	/**
	 * Create a sonar faas indicator from an indicator type
	 * @param s indicator type
	 * @return A new sonar faas indicator
     */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return SonarFaasIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {

		String metric = "sqale_index";
		String sonarPartialUrl = connectorParams.get("url");

		try{
			String sonarUrl = sonarPartialUrl+":3000/sonar/api/resources?metrics="+metric+"&format=json";
			SonarResponse[] sonarResponseArray = SonarFaasUtil.request(sonarUrl);

			if (sonarResponseArray.length != 0) {
				return true;
			}else{
				return false;
			}
		}catch (Exception e){
			return false;
		}


	}
}
