package com.orange.spectre.plugins.jira.model;

/**
 * Created by epeg7421 on 14/04/17.
 */
public class JiraResponse {

	private String total;
	private JiraIssue[] issues;

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public JiraIssue[] getIssues() {
		return issues;
	}

	public void setIssues(JiraIssue[] issues) {
		this.issues = issues;
	}
}
