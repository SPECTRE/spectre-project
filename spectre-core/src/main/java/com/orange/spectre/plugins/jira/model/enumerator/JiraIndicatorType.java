package com.orange.spectre.plugins.jira.model.enumerator;

/**
 * Created by epeg7421 on 07/04/17.
 */

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.jira.domain.*;

/**
 * Enumerator of all available qc indicators
 */
public enum JiraIndicatorType {

    ISSUE_TYPE_NB(IssueTypeNbIndicator.class),
    BUGS_PRIORITY_NB(BugsPriorityNbIndicator.class),
    BUGS_RESOLUTION_NB(BugsResolutionNbIndicator.class),
    BUGS_STATUS_NB(BugsStatusNbIndicator.class),
    PRIORITY_NB(PriorityNbIndicator.class),
    RESOLUTION_NB(ResolutionNbIndicator.class),
    STATUS_NB(StatusNbIndicator.class);
    /**
     * Current qc indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    /**
     * Indicator type constructor
     *
     * @param clazz Current indicator class
     */
    JiraIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from it's type
     *
     * @return An qc indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from it's type
     *
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "ISSUE_TYPE_NB":
                return "Détails par issue type";
            case "BUGS_PRIORITY_NB":
                return "Bugs par priorité";
            case "BUGS_RESOLUTION_NB":
                return "Bugs par résolution";
            case "BUGS_STATUS_NB":
                return "Bugs par status";
            case "PRIORITY_NB":
                return "Détail par priorité";
            case "RESOLUTION_NB":
                return "Détail par résolution";
            case "STATUS_NB":
                return "Détail par status";
            default:
                return null;
        }
    }
}
