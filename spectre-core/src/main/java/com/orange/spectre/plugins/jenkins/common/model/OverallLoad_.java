
package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "busyExecutors",
    "queueLength",
    "totalExecutors",
    "totalQueueLength"
})
public class OverallLoad_ {

    @JsonProperty("busyExecutors")
    private BusyExecutors_ busyExecutors;
    @JsonProperty("queueLength")
    private QueueLength_ queueLength;
    @JsonProperty("totalExecutors")
    private TotalExecutors_ totalExecutors;
    @JsonProperty("totalQueueLength")
    private TotalQueueLength totalQueueLength;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public BusyExecutors_ getBusyExecutors() {
        return busyExecutors;
    }

    /**
     *
     * @param busyExecutors
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public void setBusyExecutors(BusyExecutors_ busyExecutors) {
        this.busyExecutors = busyExecutors;
    }

    /**
     *
     * @return
     *     The queueLength
     */
    @JsonProperty("queueLength")
    public QueueLength_ getQueueLength() {
        return queueLength;
    }

    /**
     *
     * @param queueLength
     *     The queueLength
     */
    @JsonProperty("queueLength")
    public void setQueueLength(QueueLength_ queueLength) {
        this.queueLength = queueLength;
    }

    /**
     *
     * @return
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public TotalExecutors_ getTotalExecutors() {
        return totalExecutors;
    }

    /**
     *
     * @param totalExecutors
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public void setTotalExecutors(TotalExecutors_ totalExecutors) {
        this.totalExecutors = totalExecutors;
    }

    /**
     *
     * @return
     *     The totalQueueLength
     */
    @JsonProperty("totalQueueLength")
    public TotalQueueLength getTotalQueueLength() {
        return totalQueueLength;
    }

    /**
     *
     * @param totalQueueLength
     *     The totalQueueLength
     */
    @JsonProperty("totalQueueLength")
    public void setTotalQueueLength(TotalQueueLength totalQueueLength) {
        this.totalQueueLength = totalQueueLength;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
