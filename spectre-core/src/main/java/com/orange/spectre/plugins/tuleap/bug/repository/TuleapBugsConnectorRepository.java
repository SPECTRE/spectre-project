package com.orange.spectre.plugins.tuleap.bug.repository;

import com.orange.spectre.plugins.tuleap.bug.domain.TuleapBugsConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Tuleap connector JPA enumerator
 */
public interface TuleapBugsConnectorRepository extends JpaRepository<TuleapBugsConnector, Long> {
}
