package com.orange.spectre.plugins.weather.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.weather.model.enumerator.WeatherConnectorParams;
import com.orange.spectre.plugins.weather.model.enumerator.WeatherIndicatorType;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of weather connector
 */
@Entity
@DiscriminatorValue("WEATHER")
public class WeatherConnector extends AbstractConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(WeatherConnector.class);

	/**
	 * All connector connection params
	 */
	@Transient
	private List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all weather indicators
	 * @return All weather indicators
	 */
	public List<String> getAvailableIndicators(){
		return Arrays.stream(WeatherIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all weather connection parameters
	 * @return weather connections parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (WeatherConnectorParams param : WeatherConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);
			}
		}
		return requiredParams;
	}

	/**
	 * Get weather string type
	 * @return String type
	 */
	@Override
	public String getType() {
		return "weather";
	}

	/**
	 * Create a weather indicator from an indicator type
	 * @param s indicator type
	 * @return A new weather indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return WeatherIndicatorType.valueOf(s).createIndicator();
	}


	/**
	 * Test access to the connector
	 *
	 * @param connectorParams weather connection parameters
	 * @return True if connection ok
	 */
	@Override
	public boolean testConnector(Map<String, String> connectorParams) {

		String token = connectorParams.get(WeatherConnectorParams.token.name());
		String ville = connectorParams.get(WeatherConnectorParams.ville.name());
		String lang = "FR";

		String fullUrl = "http://api.openweathermap.org/data/2.5/weather?q=" + ville + "&APPID=" + token + "&lang=" + lang;

		HttpGet httpGet = new HttpGet(fullUrl);
		HttpHost proxy = new HttpHost("localhost", Integer.valueOf(ProxyConfig.getInstance().getPort()), "http");

		DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
		CloseableHttpClient httpClient = HttpClients.custom().setRoutePlanner(routePlanner).build();

		try {
			HttpResponse response =  httpClient.execute(httpGet);
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return true;
			}
		} catch (IOException e) {
			LOGGER.error("Error during testConnector call ", e);
			return false;
		}

		return false;

	}
}
