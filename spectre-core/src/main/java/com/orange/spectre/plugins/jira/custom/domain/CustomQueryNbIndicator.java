package com.orange.spectre.plugins.jira.custom.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jira.custom.model.enumerator.CustomJiraConnectorParams;
import com.orange.spectre.plugins.jira.custom.model.enumerator.CustomJiraIndicatorType;
import com.orange.spectre.plugins.jira.model.enumerator.JiraConnectorParams;
import com.orange.spectre.plugins.jira.util.ComputeJiraResults;
import com.orange.spectre.plugins.jira.util.JiraConstant;
import com.orange.spectre.plugins.jira.util.JiraUtils;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for highest priority
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority=High and issuetype=Bug&maxResults=0
 * maxResults à 0 permet de ramener que le count
 *
 * https://orange-village.valiantyscloud.net/jira/issues/?filter=-5&jql=project%20%3D%20OEMBF%20
 * 						AND%20issuetype%20in%20(Action%2C%20Bug%2C%20%22User%20Story%22)%20
 * 						AND%20status%20in%20(%22A%20faire%22%2C%20%22A%20valider%22%2C%20%22En%20cours%22)%20
 * 						AND%20resolution%20in%20(Unresolved%2C%20R%C3%A9alis%C3%A9e%2C%20Rejet%C3%A9e%2C%20Fixed)
 * 						&maxResults=0
 *
 * query :
 * 	project = OEMBF AND status in ("A faire", "A valider") AND resolution = Unresolved
 * 	AND reporter in (membersOf(jira-software-users))
 *
 * 	project = OEMBF AND status in ("A faire", "A valider", "EN DEFINITION")
 * 	AND priority = Normal AND resolution = Unresolved ORDER BY created DESC
 *
 */
@Entity
@DiscriminatorValue("JIRA_CUSTOM_QUERY_NB")
public class CustomQueryNbIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomQueryNbIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {
		String username = connectorParams.get(JiraConnectorParams.login.name());
		String password = connectorParams.get(JiraConnectorParams.password.name());
		String project = connectorParams.get(JiraConnectorParams.projet.name());
		String version = connectorParams.get(JiraConnectorParams.release.name());
		String query = connectorParams.get(CustomJiraConnectorParams.query.name());

		String url = String.format("%s%s%s", connectorParams.get(JiraConnectorParams.url.name()), JiraConstant.JIRA_SEARCH, project);

		try {
			ComputeJiraResults computeJiraResults = new ComputeJiraResults();
			return computeJiraResults.computeCustomData(username, password, url, version, query);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
		}
		return null;
	}

	@Override
	public String getLabel() {
		return CustomJiraIndicatorType.BUGS_CUSTOM.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.QUALITY;
	}

	@Override
	public Enum getType() {
		return CustomJiraIndicatorType.BUGS_CUSTOM;
	}

}

