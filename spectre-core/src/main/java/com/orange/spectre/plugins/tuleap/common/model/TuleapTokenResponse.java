package com.orange.spectre.plugins.tuleap.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 21/03/2017.
 */
public class TuleapTokenResponse {
    @JsonProperty("user_id")
    private String userId;
    private String token;
    private String uri;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String pUserId) {
        this.userId = pUserId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
