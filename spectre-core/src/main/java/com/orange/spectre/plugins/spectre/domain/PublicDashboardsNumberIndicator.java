package com.orange.spectre.plugins.spectre.domain;

import java.util.Map;

import javax.inject.Inject;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.LongData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.service.SpectrePluginsService;
import com.orange.spectre.plugins.spectre.model.enumerator.SpectreIndicatorType;

/**
 * Created by epeg7421 on 07/10/2016.
 */
@Entity
@Configurable
@DiscriminatorValue("SPECTRE_PUBLIC_DASHBOARD")
public class PublicDashboardsNumberIndicator extends AbstractIndicator {
	private static final Logger LOGGER = LoggerFactory.getLogger(PublicDashboardsNumberIndicator.class);

	@Inject
	@Transient
	SpectrePluginsService spectrepluginsService;

	/**
	 * Fetch data.
	 *
	 * @param connectorParams
	 *            Contains all the params such as URL, token, ... to connect to third providers.
	 * @return The fetched data.
	 * @throws FetchDataException
	 *             indicators must throw their exceptions in this custom core consumed exception.
	 */
	@Override
	public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {
		LongData data = new LongData();
		data.setData(spectrepluginsService.countAllDashboardsByReachableTrue());
		return data;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.MANAGEMENT;
	}

	@Override
	public Enum getType() {
		return SpectreIndicatorType.PUBLIC_DASHBOARD_NB;
	}

	@Override
	public String getLabel() {
		return SpectreIndicatorType.PUBLIC_DASHBOARD_NB.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}
}
