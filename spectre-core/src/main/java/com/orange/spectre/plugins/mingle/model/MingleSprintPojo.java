package com.orange.spectre.plugins.mingle.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 12/10/2016.
 */
public class MingleSprintPojo {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Vélocité journalière")
    private String velocity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVelocity() {
        return velocity;
    }

    public void setVelocity(String velocity) {
        this.velocity = velocity;
    }
}
