package com.orange.spectre.plugins.qc.custom.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.qc.custom.model.enumerator.CustomQcConnectorParams;
import com.orange.spectre.plugins.qc.custom.model.enumerator.CustomQcIndicatorType;
import com.orange.spectre.plugins.qc.domain.OpenedStatusNbIndicator;
import com.orange.spectre.plugins.qc.util.ComputeQcResults;
import com.orange.spectre.plugins.qc.util.QCConstant;
import com.orange.spectre.plugins.qc.util.QcAuthentification;
import com.orange.spectre.plugins.qc.util.RestConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by WKNL8600 on 05/08/2016.
 */
@Entity
@DiscriminatorValue("QC_CUSTOM_DEFECTS_NB")
public class CustomDefectsNbIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(OpenedStatusNbIndicator.class);

	public AbstractData getData(Map<String, String> connectorParams) {
		Integer data = 0;
		try {
			String url = QCConstant.HP_ALM_URL;
			String domain = connectorParams.get(CustomQcConnectorParams.domaine.name());
			String project = connectorParams.get(CustomQcConnectorParams.projet.name());
			String fields = connectorParams.get(CustomQcConnectorParams.fields.name());
			String query = connectorParams.get(CustomQcConnectorParams.query.name());

			RestConnector con = RestConnector.getInstance().init(new HashMap<String, String>(), url, domain, project);
			QcAuthentification login = new QcAuthentification();
			ComputeQcResults computeQcResults = new ComputeQcResults();
			return computeQcResults.extractCustomData(login, con, connectorParams);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
		}
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.QUALITY;
	}

	@Override
	public Enum getType() {
		return CustomQcIndicatorType.DEFECTS_CUSTOM;
	}

	@Override
	public String getLabel() {
		return CustomQcIndicatorType.DEFECTS_CUSTOM.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}
}
