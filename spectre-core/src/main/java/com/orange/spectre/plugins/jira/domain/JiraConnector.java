package com.orange.spectre.plugins.jira.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.jira.model.enumerator.JiraConnectorParams;
import com.orange.spectre.plugins.jira.model.enumerator.JiraIndicatorType;
import com.orange.spectre.plugins.jira.util.ComputeJiraResults;
import com.orange.spectre.plugins.jira.util.JiraConstant;
import com.orange.spectre.plugins.jira.util.JiraUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of JIRA connector
 * <p>
 * Types de demande 	: issuetype (Bug, Epic, Story, Task, Sub-task)
 * Etats 				: status (Open, In Progress, Reopened, Resolved, Closed, To Do, Done)
 * Priorité				: priority (Highest, High, Medium, Low, Lowest)
 * Resolution			: resolution (Done, Won't Do, Duplicate, Cannot Reproduce)
 * <p>
 * Exemple de query :
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and fixversion=G01R00C00&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest)&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done and status=Done&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done and status=Done&maxResults=0
 */
@Entity
@DiscriminatorValue("JIRA")
public class JiraConnector extends AbstractConnector {
	private static final Logger LOGGER = LoggerFactory.getLogger(JiraConnector.class);

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all jira indicators
	 *
	 * @return All jira indicators
	 */
	public List<String> getAvailableIndicators() {
		return Arrays.stream(JiraIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all jira connection parameters
	 *
	 * @return jira connection parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (JiraConnectorParams param : JiraConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return requiredParams;
	}

	/**
	 * Get jira string type
	 *
	 * @return String type
	 */
	@Override
	public String getType() {
		return "jira";
	}

	/**
	 * Create a jira indicator from an indicator type
	 *
	 * @param s indicator type
	 * @return A new jira indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return JiraIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		String username = connectorParams.get(JiraConnectorParams.login.name());
		String password = connectorParams.get(JiraConnectorParams.password.name());
		String project = connectorParams.get(JiraConnectorParams.projet.name());
		String version = connectorParams.get(JiraConnectorParams.release.name());
		String url = String.format("%s%s%s", connectorParams.get(JiraConnectorParams.url.name()), JiraConstant.JIRA_REST_DOMAIN,
				JiraConstant.JIRA_REST_VERSION);
		String projectUrl = String.format("%s%s%s", url, JiraConstant.JIRA_PROJECT, project);

		try {
//			ComputeJiraResults computeJiraResults = new ComputeJiraResults();
			HttpResponse response = null;
			response = JiraUtils.getJiraConnectionData(username, password, projectUrl.replaceAll("=", "%3D"));
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return true;
			} else {
				LOGGER.debug("JiraConnector.class : Error connection to JIRA plateform : ");
				return false;
			}
		} catch (Exception e) {
			LOGGER.debug("JiraConnector.class : Error connection to JIRA plateform : ");
			return false;
		}
	}
}
