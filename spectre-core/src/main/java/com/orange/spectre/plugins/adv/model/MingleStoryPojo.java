package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ludovic on 12/10/2016.
 */
public class MingleStoryPojo {

    @JsonProperty("Statut")
    private String status;
    @JsonProperty("Planning - Sprint")
    private String sprint;
    @JsonProperty("Complexité")
    private String complexity;
    @JsonProperty("Name")
    private String name;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSprint() {
        return sprint;
    }

    public void setSprint(String sprint) {
        this.sprint = sprint;
    }

    public String getComplexity() {
        return complexity;
    }

    public void setComplexity(String complexity) {
        this.complexity = complexity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
