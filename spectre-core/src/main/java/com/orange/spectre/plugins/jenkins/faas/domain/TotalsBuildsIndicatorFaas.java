package com.orange.spectre.plugins.jenkins.faas.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.common.model.ResponseFaasJSON;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasIndicatorType;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Jpa entity for totals builds
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_FAAS_JOB_TOTALS_BUILDS")
public class TotalsBuildsIndicatorFaas extends AbstractIndicator{

    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getLabel(){
        return JenkinsFaasIndicatorType.FAAS_TOTALS_BUILDS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Il s’agit de connaitre le nombre de jobs";
    }

    @Override
    public String getDefinition() {
        return "Nombre de jobs total";
    }

    @Override
    public String getOrigin() {
        return "Jenkins FAAS";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return JenkinsFaasIndicatorType.FAAS_TOTALS_BUILDS;
    }

    /**
     *
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {
            String outputFlux = JenkinsFaasFluxUtil.getFluxJson(JenkinsFaasFluxUtil.buildUrlRemoteAPI(connectorParams)).replaceAll("\n", ",");

            String fluxJSONArray = "[" + outputFlux.substring(0, outputFlux.length()-1) + "]";

            return this.computeData(connectorParams, fluxJSONArray);

        } catch (Exception e) {

            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
        }
        return null;
    }


    public IntegerData computeData(Map<String, String> connectorParams, String fluxJSONArray) {

        IntegerData data = new IntegerData();

        ObjectMapper mapper = new ObjectMapper();

        List<ResponseFaasJSON> responseJob ;
        Set<String> allJobsName = new HashSet<>();

            try {

                responseJob = mapper.readValue(fluxJSONArray, new TypeReference<List<ResponseFaasJSON>>(){});

                allJobsName = responseJob.stream().map(ResponseFaasJSON::getJobName).collect(Collectors.toSet());

            } catch (JsonMappingException e) {
                LOGGER.error("JSON Mapping Exception with parameters " + connectorParams.toString() );

            } catch (JsonParseException e) {
                LOGGER.error("JSON Parse Exception with parameters " + connectorParams.toString() );

            } catch (IOException e) {
                LOGGER.error("IO Exception with parameters " + connectorParams.toString());
            }

        data.setData(allJobsName.size());

        return data;
    }
}
