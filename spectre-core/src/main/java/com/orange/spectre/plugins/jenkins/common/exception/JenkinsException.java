/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.plugins.jenkins.common.exception;

/**
 *
 * @author NBLG6551
 */
public class JenkinsException extends UnsupportedOperationException{

    /**
     *
     * @param message
     */
    public JenkinsException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public JenkinsException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param cause
     */
    public JenkinsException(Throwable cause) {
        super(cause);
    }

}
