package com.orange.spectre.plugins.sonar.faas.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.faas.enumerator.SonarFaasIndicatorType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for duplicated lines density indicator
 */
@Entity
@DiscriminatorValue("SONAR_FAAS_DUPLIC_LINES_DENSITY")
public class DuplicatedLinesDensityIndicatorFaas extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(DuplicatedLinesDensityIndicatorFaas.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) {

        FloatData floatData = new FloatData();
        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "duplicated_lines_density";
            String sonarPartialUrl = connectorParams.get("url");

            String data = ComputeDataUtils.extractFaas(sonarPartialUrl, project, version, metric);

            floatData.setData(Float.parseFloat(data));

        }catch(Exception e){
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }

        return floatData;
    }

    @Override
    public String getLabel() {
            return SonarFaasIndicatorType.DUPLICATED_LINES_DENSITY.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre le pourcentage de code dupliqué";
    }

    @Override
    public String getDefinition() {
        return "Il s’agit du pourcentage de code “copier-coller” dans votre application, où une suite d’instructions similaires existe en plusieurs endroits du code source.";
    }

    @Override
    public String getOrigin() {
        return "Sonar Faas";
    }

    @Override
    public Enum getType() {
        return SonarFaasIndicatorType.DUPLICATED_LINES_DENSITY;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
