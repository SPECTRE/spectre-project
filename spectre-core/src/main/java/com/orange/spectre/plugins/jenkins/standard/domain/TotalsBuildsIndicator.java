package com.orange.spectre.plugins.jenkins.standard.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.common.model.DownstreamProject;
import com.orange.spectre.plugins.jenkins.common.model.Job_;
import com.orange.spectre.plugins.jenkins.common.model.ResponseJSON;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for totals builds
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_JOB_TOTALS_BUILDS")
public class TotalsBuildsIndicator extends JenkinsIndicator{

    @Override
    public String getLabel(){
        return JenkinsIndicatorType.TOTALS_BUILDS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Il s’agit de connaitre le nombre de jobs";
    }

    @Override
    public String getDefinition() {
        return "Nombre de projets total";
    }

    @Override
    public String getOrigin() {
        return "Jenkins";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return JenkinsIndicatorType.TOTALS_BUILDS;
    }

    @Override
    public IntegerData computeData(Map<String, String> connectorParams, String flux) {
        String projectName = (String) connectorParams.get(JenkinsConnectorParams.nom_projet.name());

        int nbBuildSuccess = 0;
        int nbBuildFail = 0;
        IntegerData data = new IntegerData();
        ObjectMapper mapper = new ObjectMapper();

        if (projectName != null && projectName.length() > 0) {
            Job_ responseJob = null;

            try {
                responseJob = mapper.readValue(flux, Job_.class);
            } catch (JsonMappingException e) {
                super.LOGGER.error("JSON Mapping Exception : " + e );

            } catch (JsonParseException e) {
                super.LOGGER.error("JSON Parse Exception : " + e );

            } catch (IOException e) {
                super.LOGGER.error("IO Exception : " + e );
            }

            if (responseJob != null) {
                // Récupération des valeurs pour un groupe de projet
                if (!responseJob.getDownstreamProjects().isEmpty()) {
                    switch (responseJob.getColor()) {
                        case "blue":
                            nbBuildSuccess++;
                            break;
                        case "red":
                            nbBuildFail++;
                            break;
                    }
                    for (DownstreamProject job : responseJob.getDownstreamProjects()) {
                        if (job.getName() != null) {
                            switch (job.getColor()) {
                                case "blue":
                                    nbBuildSuccess++;
                                    break;
                                case "red":
                                    nbBuildFail++;
                                    break;
                            }
                        }
                    }
                }
            }
        } else {
            ResponseJSON responseJob = null;

            try {
                responseJob = mapper.readValue(flux, ResponseJSON.class);
            } catch (JsonMappingException e) {

            } catch (JsonParseException e) {

            } catch (IOException e) {

            }

            if (responseJob != null) {
                for (Job_ job : responseJob.getJobs()) {
                    switch (job.getColor()) {
                        case "red":
                            nbBuildFail++;
                            break;
                        case "blue":
                            nbBuildSuccess++;
                            break;
                    }
                }

            }
        }
        data.setData(nbBuildFail+nbBuildSuccess);
        return data;
    }
}
