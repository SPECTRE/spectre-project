package com.orange.spectre.plugins.jenkins.standard.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;
import com.orange.spectre.plugins.jenkins.common.model.Job_;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for Job build status
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_JOB_BUILD_STATUS")
public class JobBuildStatusIndicator extends JenkinsIndicator {

    @Override
    public String getLabel(){
        return JenkinsIndicatorType.JOB_BUILD_STATUS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier très rapidement si le dernier build est en erreur ou non";
    }

    @Override
    public String getDefinition() {
        return "Status du dernier build du projet";
    }

    @Override
    public String getOrigin() {
        return "Jenkins";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return JenkinsIndicatorType.JOB_BUILD_STATUS;
    }

    @Override
    public IntegerData computeData(Map<String, String> connectorParams, String flux) {
        String projectName = (String) connectorParams.get(JenkinsConnectorParams.nom_projet.name());

        Integer jobstatut = null;
        ObjectMapper mapper = new ObjectMapper();
        IntegerData data = new IntegerData();

        if (projectName != null && projectName.length() > 0) {
            //Job_ responseJob = restTemplate.getForObject(json, Job_.class);
            Job_ responseJob = null;

            try {
                responseJob = mapper.readValue(flux, Job_.class);
            } catch (JsonMappingException e) {
                super.LOGGER.error("JSON Mapping Exception : " + e );

            } catch (JsonParseException e) {
                super.LOGGER.error("JSON Parse Exception : " + e );

            } catch (IOException e) {
                super.LOGGER.error("IO Exception : " + e );
            }

            if (responseJob != null) {
                // Récupération des valeurs pour un groupe de projet
                if (responseJob.getLastBuild() != null) {
                    switch (responseJob.getColor()) {
                        case "blue":
                            jobstatut = 1;
                            break;
                        case "red":
                            jobstatut = -1;
                            break;
                        case "notbuild":
                            jobstatut = 0;
                            break;
                    }
                }

            }
        }
        data.setData(jobstatut);
        return data;
    }
}
