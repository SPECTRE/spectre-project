package com.orange.spectre.plugins.spectre.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Created by epeg7421 on 03/10/2016.
 *
 * spectre Connection params enum representing all necessary parameters needs accessing the connector --> at this moment, no params needed
 */
public enum SpectreConnectorParams implements ConnectorParamsInterface {
	;

	@Override
	public boolean isMandatory() {
		return false;
	}

	@Override
	public boolean isSecured() {
		return false;
	}
}
