package com.orange.spectre.plugins.jira.model;

/**
 * Created by epeg7421 on 14/04/17.
 */
public class JiraIssueType {
	private String id;
	private String name;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
