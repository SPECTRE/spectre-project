package com.orange.spectre.plugins.qc.util;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by epeg7421 on 15/02/17.
 */
public class QcAuthentification {
	private static final Logger LOGGER = LoggerFactory.getLogger(QcAuthentification.class);
	public QcAuthentification() {

	}

	/**
	 * Gets an instance of RestConnector
	 *
	 * @return
	 */
	private static RestConnector connector() {
		return RestConnector.getInstance();
	}

	/**
	 * @param username
	 * @param password
	 * @return true if authenticated at the end of this method.
	 * @throws Exception convenience method used by other examples to do their login
	 */
	public boolean login(String username, String password) throws Exception {
		String authenticationPoint = QCConstant.HP_ALM_URL.concat("/").concat(QCConstant.HP_ALM_SIGN_IN);
		return this.login(authenticationPoint, username, password);
	}

	/**
	 * @param loginUrl to authenticate at
	 * @param username
	 * @param password
	 * @return true on operation success, false otherwise
	 * @throws Exception Logging in to our system is standard http login (basic
	 *                   authentication), where one must store the returned cookies
	 *                   for further use.
	 */
	public boolean login(String loginUrl, String username, String password) throws Exception {
		LOGGER.debug("QcAuthentification.class : log in, url : " + loginUrl + " ,username : " + username );

		// create a string that lookes like:
		// "Basic ((username:password)<as bytes>)<64encoded>"
		String usernamePassword = username + ":" + password;
		String credEncodedString = "Basic " + StringUtils.newStringUtf8(Base64.encodeBase64(usernamePassword.getBytes()));

		Map<String, String> map = new HashMap<>();
		map.put("Authorization", credEncodedString);
		Response response = null;
		response = connector().httpGet(loginUrl, null, map);
		boolean ret = response.getStatusCode() == HttpURLConnection.HTTP_OK;

		return ret;
	}

	/**
	 * @return true if logout successful
	 * @throws Exception close session on server and clean session cookies on client
	 */
	public boolean logout() throws Exception {
		LOGGER.debug("QcAuthentification.class : log out");

		Response response = connector().httpGet(connector().buildUrl(QCConstant.HP_ALM_SIGN_OUT), null, null);
		return (response.getStatusCode() == HttpURLConnection.HTTP_OK);
	}
}
