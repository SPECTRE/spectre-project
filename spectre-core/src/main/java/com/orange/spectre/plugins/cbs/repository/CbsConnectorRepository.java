package com.orange.spectre.plugins.cbs.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orange.spectre.plugins.cbs.domain.CbsConnector;

public interface CbsConnectorRepository extends JpaRepository<CbsConnector, Long> {

}
