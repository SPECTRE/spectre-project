package com.orange.spectre.plugins.qc.repository;

import com.orange.spectre.plugins.qc.domain.QcConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * qc connector JPA enumerator
 */
public interface QcConnectorRepository extends JpaRepository<QcConnector, Long> {

}
