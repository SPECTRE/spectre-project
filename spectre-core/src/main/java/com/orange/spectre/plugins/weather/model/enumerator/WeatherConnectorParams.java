

package com.orange.spectre.plugins.weather.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all weather connection parameters
 */
public enum WeatherConnectorParams implements ConnectorParamsInterface{

    // TODO: some parameters example, need to be replaced by your owns
    ville(true,false),
    token(true,true);

    private final boolean mandatory;
    private final boolean secured;

    private WeatherConnectorParams(boolean mandatory, boolean secured) {
        this.mandatory = mandatory;
        this.secured = secured;
    }

    @Override
    public boolean isMandatory() {
        return this.mandatory;
    }

    @Override
    public boolean isSecured() {
        return this.secured;
    }

}
