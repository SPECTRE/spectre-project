package com.orange.spectre.plugins.qc.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.qc.domain.*;
import com.orange.spectre.plugins.qc.custom.domain.CustomDefectsNbIndicator;

/**
 * Enumerator of all available qc indicators
 */
public enum QcIndicatorType {

	MINOR_SEVERITY_NB(MinorSeverityNbIndicator.class),
	BUGS_CLOSED_NB(ClosedStatusNbIndicator.class),
	BUGS_NO_CLOSED_NB(OpenedStatusNbIndicator.class),
	MAJOR_SEVERITY_NB(MajorSeverityNbIndicator.class),
	CRITICAL_SEVERITY_NB(CriticalSeverityNbIndicator.class),
	BUGS_OPENED_CLOSED_NB(OpenedClosedStatusNbIndicator.class),
	TEST_CASES_NB(TestCasesNbIndicator.class),
	PASSED_STATUS_NB(PassedStatusNbIndicator.class),
	FAILED_STATUS_NB(FailedStatusNbIndicator.class),
	NO_RUN_STATUS_NB(NoRunStatusNbIndicator.class),
	FUNCTIONAL_TEST_REPORT(FunctionalReportTestIndicator.class);
	/**
	 * Current qc indicator class
	 */
	private Class<? extends AbstractIndicator> clazz;

	/**
	 * Indicator type constructor
	 *
	 * @param clazz Current indicator class
	 */
	QcIndicatorType(Class<? extends AbstractIndicator> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Indicator instantiation from it's type
	 *
	 * @return An qc indicator
	 */
	public AbstractIndicator createIndicator() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(clazz + " has no default constructor");
		}
	}

	/**
	 * Get indicator label from it's type
	 *
	 * @return Indicator label
	 */
	public String getLabel() {
		switch (this.name()) {
		case "MINOR_SEVERITY_NB":
			return "Bugs mineurs";
		case "MAJOR_SEVERITY_NB":
			return "Bugs majeurs";
		case "CRITICAL_SEVERITY_NB":
			return "Bugs critiques";
		case "BUGS_CLOSED_NB":
			return "Bugs fermés";
		case "BUGS_NO_CLOSED_NB":
			return "Bugs ouverts";
		case "BUGS_OPENED_CLOSED_NB":
			return "Bugs ouverts et fermés";
		case "TEST_CASES_NB":
			return "Nombre de cas des Test";
		case "PASSED_STATUS_NB":
			return "Tests reussis";
		case "FAILED_STATUS_NB":
			return "Tests échoués";
		case "NO_RUN_STATUS_NB":
			return "Tests non exécutés";
		case "FUNCTIONAL_TEST_REPORT":
			return "Rapport de Tests";
		default:
			return null;
		}
	}
}


