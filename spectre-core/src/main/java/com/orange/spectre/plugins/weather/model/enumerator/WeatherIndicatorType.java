package com.orange.spectre.plugins.weather.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.weather.domain.*;


/**
 * Enumerator of all available weather indicators
 */
public enum WeatherIndicatorType {

    // TODO: some sample indicators types, need to be replaced by your owns
    WEATHER_TEMPERATURE(TemperatureIndicator.class),
    WEATHER_PRESSURE(PressureIndicator.class),
    WEATHER_HUMIDITY(HumidityIndicator.class),
    WEATHER_WIND(WindIndicator.class),
    WEATHER_DESCRIPTION(DescriptionIndicator.class);

    /**
     * Current weather indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    WeatherIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from it's type
     * @return An weather indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            // TODO: some label examples, need to be replaced by your owns
            case "WEATHER_TEMPERATURE":
                return "Température";
            case "WEATHER_PRESSURE":
                return "Pression";
            case "WEATHER_HUMIDITY":
                return "Humidité relative";
            case "WEATHER_WIND":  // "vitesse moyenne du vent"
                return "Vent";
            case "WEATHER_DESCRIPTION":
                return "Détail";
            default: return null;
        }
    }
}
