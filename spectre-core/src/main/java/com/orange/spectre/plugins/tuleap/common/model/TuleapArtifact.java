package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 13/04/2017.
 */
public class TuleapArtifact {

    private String id;
    private String status;
    private String title;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
