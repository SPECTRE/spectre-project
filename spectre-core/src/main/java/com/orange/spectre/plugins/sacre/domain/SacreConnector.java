package com.orange.spectre.plugins.sacre.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreConnectorParams;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sacre.util.SacreFluxUtil;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@DiscriminatorValue("SACRE")
public class SacreConnector extends AbstractConnector {

    /**
     *
     */
    private static final long serialVersionUID = 3402059788512044508L;

    /**
     * All connector connection params
     */
    @Transient
    List<ConnectorRequiredParams> requiredParams;

    public List<String> getAvailableIndicators() {
        return Arrays.stream(SacreIndicatorType.values()).map(Enum::name).collect(Collectors.toList());
    }

    @Override
    public String getType() {
        return "sacre";
    }

    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (SacreConnectorParams param : SacreConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);
            }
        }
        return requiredParams;
    }

    @Override
    public AbstractIndicator createIndicator(String s) {
        return SacreIndicatorType.valueOf(s).createIndicator();
    }

    @Override
    public boolean testConnector(Map<String, String> connectorParams) {
        try {
            String flux = SacreFluxUtil.getFluxJSON(connectorParams);
            ObjectMapper mapper = new ObjectMapper();

            Data responseJob = mapper.readValue(flux, Data.class);

            return responseJob != null;

        } catch (Exception e) {
            return false;
        }
    }

}
