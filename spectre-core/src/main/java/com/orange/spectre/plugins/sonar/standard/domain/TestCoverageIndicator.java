package com.orange.spectre.plugins.sonar.standard.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;


/**
 * Jpa entity for test coverage indicator
 */
@Entity
@DiscriminatorValue("SONAR_TEST_COVERAGE")
public class TestCoverageIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TestCoverageIndicator.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) throws FetchDataException {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "coverage";
            String sonarPartialUrl = connectorParams.get("url");
            return ComputeDataUtils.extract(sonarPartialUrl, project, version, metric);
    }

    @Override
    public String getLabel() {
            return SonarIndicatorType.TEST_COVERAGE.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de mesurer l'efficacité de la phase de test.";
    }

    @Override
    public String getDefinition() {
        return "La couverture correspond au pourcentage de ligne de code de l’application qui est appelé lors de la phase de test.";
    }

    @Override
    public String getOrigin() {
        return "SonarQube";
    }

    @Override
    public Enum getType() {
        return SonarIndicatorType.TEST_COVERAGE;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
