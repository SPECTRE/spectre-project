package com.orange.spectre.plugins.jira.model;

/**
 * Created by epeg7421 on 14/04/17.
 */
public class JiraIssue {
	private String id;
	private String key;
	private JiraFields fields;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public JiraFields getFields() {
		return fields;
	}

	public void setFields(JiraFields fields) {
		this.fields = fields;
	}
}
