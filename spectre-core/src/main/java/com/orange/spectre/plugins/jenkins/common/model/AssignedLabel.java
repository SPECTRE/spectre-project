
package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "actions",
    "busyExecutors",
    "clouds",
    "description",
    "idleExecutors",
    "loadStatistics",
    "name",
    "nodes",
    "offline",
    "tiedJobs",
    "totalExecutors",
    "propertiesList"
})
public class AssignedLabel {

    @JsonProperty("actions")
    private List<Object> actions = new ArrayList<Object>();
    @JsonProperty("busyExecutors")
    private Integer busyExecutors;
    @JsonProperty("clouds")
    private List<Object> clouds = new ArrayList<Object>();
    @JsonProperty("description")
    private Object description;
    @JsonProperty("idleExecutors")
    private Integer idleExecutors;
    @JsonProperty("loadStatistics")
    private LoadStatistics loadStatistics;
    @JsonProperty("name")
    private String name;
    @JsonProperty("nodes")
    private List<Node> nodes = new ArrayList<Node>();
    @JsonProperty("offline")
    private Boolean offline;
    @JsonProperty("tiedJobs")
    private List<Object> tiedJobs = new ArrayList<Object>();
    @JsonProperty("totalExecutors")
    private Integer totalExecutors;
    @JsonProperty("propertiesList")
    private List<Object> propertiesList = new ArrayList<Object>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The actions
     */
    @JsonProperty("actions")
    public List<Object> getActions() {
        return actions;
    }

    /**
     *
     * @param actions
     *     The actions
     */
    @JsonProperty("actions")
    public void setActions(List<Object> actions) {
        this.actions = actions;
    }

    /**
     *
     * @return
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public Integer getBusyExecutors() {
        return busyExecutors;
    }

    /**
     *
     * @param busyExecutors
     *     The busyExecutors
     */
    @JsonProperty("busyExecutors")
    public void setBusyExecutors(Integer busyExecutors) {
        this.busyExecutors = busyExecutors;
    }

    /**
     *
     * @return
     *     The clouds
     */
    @JsonProperty("clouds")
    public List<Object> getClouds() {
        return clouds;
    }

    /**
     *
     * @param clouds
     *     The clouds
     */
    @JsonProperty("clouds")
    public void setClouds(List<Object> clouds) {
        this.clouds = clouds;
    }

    /**
     *
     * @return
     *     The description
     */
    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(Object description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     The idleExecutors
     */
    @JsonProperty("idleExecutors")
    public Integer getIdleExecutors() {
        return idleExecutors;
    }

    /**
     *
     * @param idleExecutors
     *     The idleExecutors
     */
    @JsonProperty("idleExecutors")
    public void setIdleExecutors(Integer idleExecutors) {
        this.idleExecutors = idleExecutors;
    }

    /**
     *
     * @return
     *     The loadStatistics
     */
    @JsonProperty("loadStatistics")
    public LoadStatistics getLoadStatistics() {
        return loadStatistics;
    }

    /**
     *
     * @param loadStatistics
     *     The loadStatistics
     */
    @JsonProperty("loadStatistics")
    public void setLoadStatistics(LoadStatistics loadStatistics) {
        this.loadStatistics = loadStatistics;
    }

    /**
     *
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The nodes
     */
    @JsonProperty("nodes")
    public List<Node> getNodes() {
        return nodes;
    }

    /**
     *
     * @param nodes
     *     The nodes
     */
    @JsonProperty("nodes")
    public void setNodes(List<Node> nodes) {
        this.nodes = nodes;
    }

    /**
     *
     * @return
     *     The offline
     */
    @JsonProperty("offline")
    public Boolean getOffline() {
        return offline;
    }

    /**
     *
     * @param offline
     *     The offline
     */
    @JsonProperty("offline")
    public void setOffline(Boolean offline) {
        this.offline = offline;
    }

    /**
     *
     * @return
     *     The tiedJobs
     */
    @JsonProperty("tiedJobs")
    public List<Object> getTiedJobs() {
        return tiedJobs;
    }

    /**
     *
     * @param tiedJobs
     *     The tiedJobs
     */
    @JsonProperty("tiedJobs")
    public void setTiedJobs(List<Object> tiedJobs) {
        this.tiedJobs = tiedJobs;
    }

    /**
     *
     * @return
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public Integer getTotalExecutors() {
        return totalExecutors;
    }

    /**
     *
     * @param totalExecutors
     *     The totalExecutors
     */
    @JsonProperty("totalExecutors")
    public void setTotalExecutors(Integer totalExecutors) {
        this.totalExecutors = totalExecutors;
    }

    /**
     *
     * @return
     *     The propertiesList
     */
    @JsonProperty("propertiesList")
    public List<Object> getPropertiesList() {
        return propertiesList;
    }

    /**
     *
     * @param propertiesList
     *     The propertiesList
     */
    @JsonProperty("propertiesList")
    public void setPropertiesList(List<Object> propertiesList) {
        this.propertiesList = propertiesList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
