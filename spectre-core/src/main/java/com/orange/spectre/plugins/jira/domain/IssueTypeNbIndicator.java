package com.orange.spectre.plugins.jira.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jira.model.enumerator.JiraConnectorParams;
import com.orange.spectre.plugins.jira.model.enumerator.JiraIndicatorType;
import com.orange.spectre.plugins.jira.util.ComputeJiraResults;
import com.orange.spectre.plugins.jira.util.JiraConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for issue type
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and issuetype=Bug&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and issuetype=Epic and fixversion=G0R0C0&maxResults=0
 */
@Entity
@DiscriminatorValue("JIRA_ISSUE_TYPE_NB")
public class IssueTypeNbIndicator extends AbstractIndicator {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssueTypeNbIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {
		String username = connectorParams.get(JiraConnectorParams.login.name());
		String password = connectorParams.get(JiraConnectorParams.password.name());
		String project = connectorParams.get(JiraConnectorParams.projet.name());
		String version = connectorParams.get(JiraConnectorParams.release.name());

		String url = String.format("%s%s%s", connectorParams.get(JiraConnectorParams.url.name()), JiraConstant.JIRA_SEARCH, project);
		try {
			ComputeJiraResults computeJiraResults = new ComputeJiraResults();
			//TODO voir pour avoir le même calcul sur le site avec un % par Epic, par Story....
			//TODO comparer avec le total
			return computeJiraResults.computeIssueTypeData(username, password, url, version);
		} catch (Exception e) {
			LOGGER.error("Exception: problem during kpi retrieval :" + e);
		}
		return null;
	}

	@Override
	public String getLabel() {
		return JiraIndicatorType.ISSUE_TYPE_NB.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		return IndicatorMeasure.TUPLE;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		return IndicatorFamily.QUALITY;
	}

	@Override
	public Enum getType() {
		return JiraIndicatorType.ISSUE_TYPE_NB;
	}

}

