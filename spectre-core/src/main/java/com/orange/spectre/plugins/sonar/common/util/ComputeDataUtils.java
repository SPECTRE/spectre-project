package com.orange.spectre.plugins.sonar.common.util;

import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class ComputeDataUtils {
    public static String extractFaas(String sonarPartialUrl, String project, String version, String metric) throws Exception {
        String data = null;
        String sonarUrl = sonarPartialUrl+":3000/sonar/api/resources?metrics="+metric+"&format=json";
        SonarResponse[] sonarResponseArray = SonarFaasUtil.request(sonarUrl);

        if (sonarResponseArray.length != 0) {
        	data = getData(sonarResponseArray, project, version, metric);
        }
        return data;
    }


    /**
     * Build and call a sonar URI to get data.
     *
     * @param sonarPartialUrl The URL to build full URI.
     * @param project The given project.
     * @param version The given Version.
     * @param metric The metric used to extract the desired data.
     * @return The extracted data.
     * @throws RestClientException thrown by 'getForObject' RestTemplate method
     */
    public static AbstractData extract(String sonarPartialUrl, String project, String version, String metric) throws FetchDataException {
        String data;
        String sonarUrl = sonarPartialUrl+"/sonar/api/resources?metrics="+metric+"&format=json";
        RestTemplate restTemplate = new RestTemplate();

        try{
            SonarResponse[] sonarResponseArray = restTemplate.getForObject(sonarUrl, SonarResponse[].class);
            if (sonarResponseArray.length != 0) {
                data = getData(sonarResponseArray, project, version, metric);
                return new StringData(data);
            } else {
                throw new FetchDataException("No data found !");
            }
        }catch (RestClientException e){
            throw new FetchDataException(e);
        }
    }

    private static String getData(SonarResponse[] sonarResponseArray, String project, String version, String metric) throws FetchDataException {
    	String data = null;

    	for (SonarResponse sonarResponse : sonarResponseArray) {
            MSR[] currentMsrArray = sonarResponse.getMsr();
            if (currentMsrArray.length != 0 &&
                    sonarResponse.getName().equals(project) &&
                    sonarResponse.getVersion().equals(version)) {
                for (MSR currentMSR : currentMsrArray) {
                    if(metric.equals(currentMSR.getKey())){
                        data = currentMSR.getVal();
                    }
                }
            }
        }

        if(data == null){
            throw new FetchDataException("No data found !");
        }

    	return data;
    }
}
