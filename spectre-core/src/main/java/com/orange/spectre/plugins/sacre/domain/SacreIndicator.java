package com.orange.spectre.plugins.sacre.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.plugins.sacre.util.SacreFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.Transient;
import java.io.IOException;
import java.util.Map;

public abstract class SacreIndicator extends AbstractIndicator {

    /**
     *
     */
    private static final long serialVersionUID = 5398037165796666734L;


    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    /**
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {
            String outputFlux = SacreFluxUtil.getFluxJSON(connectorParams);
            return this.computeData(connectorParams, outputFlux);
        } catch (Exception e) {
            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
            return null;
        }
    }

//    /**
//     * @param connectorParams
//     * @return
//     * @throws SacreException
//     */
//    public AbstractData computeData(Map<String, String> connectorParams) throws SacreException {
//        try {
//            String outputFlux = SacreFluxUtil.getFluxJSON(connectorParams);
//            return this.computeData(connectorParams, outputFlux);
//        } catch (UnsupportedEncodingException ex) {
//            throw new SacreException(ex);
//        } catch (Exception ex) {
//            LOGGER.error("Error JSON", ex);
//            throw new SacreException(ex);
//        }
//
//    }

    /**
     * @param connectorParams
     * @param jsonFlux
     * @return
     * @throws IOException
     */
    protected AbstractData computeData(Map<String, String> connectorParams, String jsonFlux) throws IOException {
        throw new IOException("Not yet implemented");
    }

}
