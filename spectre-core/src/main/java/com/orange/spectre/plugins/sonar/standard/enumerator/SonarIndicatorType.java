package com.orange.spectre.plugins.sonar.standard.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.sonar.standard.domain.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Enumerator of all available sonar indicators
 */
public enum SonarIndicatorType {

    TECHNICAL_DEBT(TechnicalDebtIndicator.class),
    TEST_COVERAGE(TestCoverageIndicator.class),
    TEST_SUCCESS_DENSITY(TestSuccessDensityIndicator.class),
    TESTS_NUMBER(TestsNumberIndicator.class),
    DUPLICATED_LINES_DENSITY(DuplicatedLinesDensityIndicator.class),
    BLOCKER_VIOLATIONS(BlockerViolationsIndicator.class),
    CRITICAL_VIOLATIONS(CriticalViolationsIndicator.class),
    MAJOR_VIOLATIONS(MajorViolationsIndicator.class),
    MINOR_VIOLATIONS(MinorViolationsIndicator.class);

    private static final Logger LOGGER = LoggerFactory.getLogger(SonarIndicatorType.class);

    /**
     * Current sonar indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    SonarIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from it's type
     * @return An sonar indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("Error during indicator creation", e);
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "TECHNICAL_DEBT":
                return "Dette technique";
            case "TEST_COVERAGE":
                return "Couverture de tests";
            case "TEST_SUCCESS_DENSITY":
                return "Tests en succès";
            case "TESTS_NUMBER":
                return "Nombre de tests";
            case "DUPLICATED_LINES_DENSITY":
                return "Code dupliqué";
            case "BLOCKER_VIOLATIONS":
                return "Erreurs bloquantes";
            case "CRITICAL_VIOLATIONS":
                return "Erreurs critiques";
            case "MAJOR_VIOLATIONS":
                return "Erreurs majeures";
            case "MINOR_VIOLATIONS":
                return "Erreurs mineures";
            default: return null;
        }
    }
}


