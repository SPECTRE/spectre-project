package com.orange.spectre.plugins.tuleap.common.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.StringUtils;

/**
 * Created by ludovic on 13/04/2017.
 */
public class TuleapMilestoneStory {

    private String id;
    private String label;

    @JsonProperty("initial_effort")
    private String initialEffort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getInitialEffort() {
        if (StringUtils.isEmpty(initialEffort)) {
            return new String("0");
        }
        return initialEffort;
    }

    public void setInitialEffort(String initialEffort) {
        this.initialEffort = initialEffort;
    }
}
