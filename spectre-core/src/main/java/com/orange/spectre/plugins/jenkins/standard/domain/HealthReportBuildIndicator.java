package com.orange.spectre.plugins.jenkins.standard.domain;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsIndicatorType;
import com.orange.spectre.plugins.jenkins.common.exception.JenkinsException;
import com.orange.spectre.plugins.jenkins.common.model.HealthReport;
import com.orange.spectre.plugins.jenkins.common.model.Job_;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

/**
 * Jpa entity for health report build
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_HEALTH_REPORT")
public class HealthReportBuildIndicator extends JenkinsIndicator {

    @Override
    public String getLabel(){
        return JenkinsIndicatorType.HEALTH_REPORT_BUILD.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier très rapidement si un job est instable";
    }

    @Override
    public String getDefinition() {
        return "Il s'agit de connaitre le pourcentage de succès d'un projet lors de ses cinq derniers lancements";
    }
    @Override
    public String getOrigin() {
        return "Jenkins";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return JenkinsIndicatorType.HEALTH_REPORT_BUILD;
    }

    @Override
    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws JenkinsException {
        String projectName = (String) connectorParams.get(JenkinsConnectorParams.nom_projet.name());

        ObjectMapper mapper = new ObjectMapper();
        IntegerData data = new IntegerData();
        Integer score = null;

        if (projectName != null && projectName.length() > 0) {
            //Job_ responseJob = restTemplate.getForObject(json, Job_.class);
            Job_ responseJob = null;

            try {
                responseJob = mapper.readValue(flux, Job_.class);
                // Récupération du score pour un projet
                for (HealthReport job : responseJob.getHealthReport()) {
                    score = job.getScore();
                }
                if (score == null){
                    throw new JenkinsException ("job not build");
                } else {
                    data.setData(score);
                }
            } catch (JsonMappingException e) {
                super.LOGGER.error("JSON Mapping Exception : " + e );

            } catch (JsonParseException e) {
                super.LOGGER.error("JSON Parse Exception : " + e );

            } catch (IOException e) {
                super.LOGGER.error("IO Exception : " + e );
            }

        }

        return data;
    }
}



