package com.orange.spectre.plugins.devopsic.enumerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.devopsic.domain.*;

/**
 * Enumerator of all available devopsic indicators
 */
public enum DevopsicIndicatorType {

    DOM_NOTE_GLOBALE(DomNoteGlobaleIndicator.class, "getDom_note_G", "DOM Note Globale"),
    DOM_NOTE_C(DomNoteCultureIndicator.class, "getDom_note_C", "DOM Note C"),
    DOM_NOTE_A(DomNoteAutomatisationIndicator.class, "getDom_note_A", "DOM Note A"),
    DOM_NOTE_M(DomNoteMesureIndicator.class, "getDom_note_M", "DOM Note M"),
    DOM_NOTE_P(DomNotePartageIndicator.class, "getDom_note_P", "DOM Note P"),
    SOCLE_NOTE_ICDA(SocleNoteIcdaIndicator.class, "getSocle_note_ICDA", "Socle Note ICDA"),
    SOCLE_NOTE_DC(SocleNoteDcIndicator.class, "getSocle_note_DC", "Socle Note DC");

    private static final Logger LOGGER = LoggerFactory
            .getLogger(DevopsicIndicatorType.class);

    /**
     * Current devopsic indicator class
     */
    private Class<? extends AbstractIndicator> clazz;
    private final String key;
    private final String label;    // Label displayed

    /**
     * Indicator type constructor
     *
     * @param clazz Current indicator class
     */
    DevopsicIndicatorType(Class<? extends AbstractIndicator> clazz, String key,
                          String label) {
        this.clazz = clazz;
        this.key = key;
        this.label = label;
    }

    /**
     * Indicator instantiation from it's type
     *
     * @return A devopsic indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOGGER.error("Error during indicator creation", e);
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator key
     *
     * @return Indicator key
     */
    public String getKey() {
        return key;
    }

    /**
     * Get indicator label
     *
     * @return Indicator label
     */
    public String getLabel() {
        return label;
    }
}
