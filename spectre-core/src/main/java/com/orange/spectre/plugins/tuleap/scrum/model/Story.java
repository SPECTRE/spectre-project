package com.orange.spectre.plugins.tuleap.scrum.model;

/**
 * Created by ludovic on 13/04/2017.
 */
public class Story {

    public Story() {

    }

    public Story(String id, String status, String title, String initialEffort) {
        this.id = id;
        this.status = status;
        this.title = title;
        this.initialEffort = initialEffort;
    }

    private String id;
    private String status;
    private String title;
    private String initialEffort;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInitialEffort() {
        return initialEffort;
    }

    public void setInitialEffort(String initialEffort) {
        this.initialEffort = initialEffort;
    }
}
