package com.orange.spectre.plugins.sacre.common.model.enumerator;

public enum TraiIdAval {
	
	ORMIS(36),
	DECLARIS(37),
	O2P(38);
	
	
	private int trai_id;
	
	TraiIdAval(int trai) {
		trai_id = trai;
	}
	
	public int getTrai() {
		return trai_id;
	}
	
	public static boolean contains(int trai){
		for(TraiIdAval mytrai : TraiIdAval.values()) {
			if (mytrai.getTrai() == trai) {
				return true;
			}
		}
		return false;
	}
	
}