package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 02/06/2017.
 */
public class MingleAnomaliesNbPojo {

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("Count ")
    private int count;
}
