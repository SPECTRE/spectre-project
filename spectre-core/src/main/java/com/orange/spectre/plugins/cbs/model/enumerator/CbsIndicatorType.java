package com.orange.spectre.plugins.cbs.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.cbs.domain.NbChangesIndicator;
import com.orange.spectre.plugins.cbs.domain.NbPjMepFctIndicator;
import com.orange.spectre.plugins.cbs.domain.TxChangesOkIndicator;
import com.orange.spectre.plugins.cbs.domain.TxIndispoIndicator;
import com.orange.spectre.plugins.cbs.domain.TxT1CIndicator;
import com.orange.spectre.plugins.cbs.domain.TxTrolaP1P22hIndicator;

public enum CbsIndicatorType {

    NB_CHANGES_TYPE(NbChangesIndicator.class),
    NB_PJ_MEP_FCT( NbPjMepFctIndicator.class),
    TX_CHANGE_OK(TxChangesOkIndicator.class),
    TX_INDISPO(TxIndispoIndicator.class),
    TX_TROLA_P1P2_2H(TxTrolaP1P22hIndicator.class),
    TX_T1C(TxT1CIndicator.class);

    private Class<? extends AbstractIndicator> clazz;

    CbsIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    public String getLabel() {
        switch (this.name()) {
            case "NB_CHANGES_TYPE":
                return "Nombre de changes";
            case "NB_PJ_MEP_FCT":
                return "Nombre de projet MEP";
            case "TX_CHANGE_OK":
                return "Taux de change sans impact service";
            case "TX_INDISPO":
                return "Taux d'indisponibilité";
            case "TX_TROLA_P1P2_2H":
                return "Taux de résolution des incidents P1 P2 en moins de 2h";
            case "TX_T1C":
                return "Taux de respect des jalons T1C initial (infra + fonct)";


            default: return null;
        }
    }

    public String getKey() {
        switch (this.name()) {
            case "NB_CHANGES_TYPE":
                return "NB_CHANGES";
            case "NB_PJ_MEP_FCT":
                return "NB_PJ_MEP_FCT";
            case "TX_CHANGE_OK":
                return "TX_CHANGE_OK";
            case "TX_INDISPO":
                return "TX_INDISPO";
            case "TX_TROLA_P1P2_2H":
                return "TROLA_APPLI_P1P2_2H";
            case "TX_T1C":
                return "TR_T1C";


            default: return null;
        }
    }



}
