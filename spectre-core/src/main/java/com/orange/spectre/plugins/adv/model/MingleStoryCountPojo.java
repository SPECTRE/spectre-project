package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 09/06/2017.
 */
public class MingleStoryCountPojo {

    @JsonProperty("Created on")
    private String create;

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("Count ")
    private int count;
}
