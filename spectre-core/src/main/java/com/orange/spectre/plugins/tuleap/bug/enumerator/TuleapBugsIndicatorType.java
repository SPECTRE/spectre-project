
package com.orange.spectre.plugins.tuleap.bug.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.tuleap.bug.domain.TuleapBugsBySeverityIndicator;
import com.orange.spectre.plugins.tuleap.bug.domain.TuleapBugsByStatusIndicator;
import com.orange.spectre.plugins.tuleap.bug.domain.TuleapBugsByUrgencyIndicator;


/**
 * Enumerator of all available tuleap indicators
 */
public enum TuleapBugsIndicatorType {


    TULEAP_BUGS_BY_STATUS(TuleapBugsByStatusIndicator.class),
    TULEAP_BUGS_BY_URGENCY(TuleapBugsByUrgencyIndicator.class),
    TULEAP_BUGS_BY_SEVERITY(TuleapBugsBySeverityIndicator.class)
    ;

    /**
     * Current Trello indicator class
     */
    private Class<? extends AbstractIndicator> clazz;
    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    TuleapBugsIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }


    /**
     * Indicator instantiation from it's type
     * @return An Trello indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }
    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "TULEAP_BUGS_BY_STATUS":
                return "Bugs par statut";
            case "TULEAP_BUGS_BY_URGENCY":
                return "Bugs par priorité";
            case "TULEAP_BUGS_BY_SEVERITY":
                return "Bugs par sévérité";
            default: return null;
        }
    }
}


