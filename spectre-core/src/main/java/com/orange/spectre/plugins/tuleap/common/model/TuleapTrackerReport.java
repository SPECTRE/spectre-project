package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 27/03/2017.
 */
public class TuleapTrackerReport {

    private String id;
    private String label;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
