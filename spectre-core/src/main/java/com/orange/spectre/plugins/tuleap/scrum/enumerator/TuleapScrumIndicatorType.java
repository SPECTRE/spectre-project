
package com.orange.spectre.plugins.tuleap.scrum.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.tuleap.scrum.domain.*;


/**
 * Enumerator of all available tuleap indicators
 */
public enum TuleapScrumIndicatorType {


//    TULEAP_SCRUM_BACKLOG(TuleapScrumBacklogIndicator.class),
    TULEAP_SCRUM_CURRENT_SPRINT(TuleapScrumCurrentSprintIndicator.class),
    TULEAP_SCRUM_CURRENT_SPRINT_STORIES(TuleapScrumCurrentSprintStoriesIndicator.class),
    TULEAP_SCRUM_CURRENT_SPRINT_COMPLEXITY(TuleapScrumCurrentSprintComplexityIndicator.class),
    TULEAP_SCRUM_CURRENT_SPRINT_EVOLUTION(TuleapScrumCurrentSprintEvolutionIndicator.class)
    ;

    /**
     * Current Trello indicator class
     */
    private Class<? extends AbstractIndicator> clazz;
    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
    TuleapScrumIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }


    /**
     * Indicator instantiation from it's type
     * @return An Trello indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }
    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
//            case "TULEAP_SCRUM_BACKLOG":
//                return "Backlog";
            case "TULEAP_SCRUM_CURRENT_SPRINT":
                return "Sprint en cours";
            case "TULEAP_SCRUM_CURRENT_SPRINT_STORIES":
                return "Etat du sprint en stories";
            case "TULEAP_SCRUM_CURRENT_SPRINT_COMPLEXITY":
                return "Etat du sprint en complexité";
            case "TULEAP_SCRUM_CURRENT_SPRINT_EVOLUTION":
                return "Avancement du sprint";
            default: return null;
        }
    }
}


