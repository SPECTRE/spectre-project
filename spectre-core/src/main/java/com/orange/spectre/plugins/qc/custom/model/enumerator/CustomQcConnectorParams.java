package com.orange.spectre.plugins.qc.custom.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all qc connection parameters
 */
public enum CustomQcConnectorParams implements ConnectorParamsInterface {

    login (true, false),
    password (true, true),
    domaine (true, false),
    projet (true, false),
	release (false, false),
	fields(false,false),
	query(true,false);

	private final boolean mandatory;
	private final boolean secured;

	private CustomQcConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
