package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 11/07/2017.
 */
@JsonIgnoreProperties
public class MingleDureeEtudePojo {

    @JsonProperty("Created on")
    private String create;

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    @JsonProperty("Date du passage à l'état à faire")
    private String dob;

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

}
