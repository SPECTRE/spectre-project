package com.orange.spectre.plugins.trello.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloConnectorParams;
import com.orange.spectre.plugins.trello.model.enumerator.TrelloIndicatorType;
import com.orange.spectre.plugins.trello.util.TrelloUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of Trello connector
 */
@Entity
@DiscriminatorValue("TRELLO")
public class TrelloConnector  extends AbstractConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrelloConnector.class);

    /**
     * All connector connection params
     */
    @Transient
    List<ConnectorRequiredParams> requiredParams;


    /**
     * Get list of all Trello indicators
     *
     * @return All Trello indicators
     */
    public List<String> getAvailableIndicators() {
        return Arrays.stream(TrelloIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

    }

    /**
     * Get list of all Trello connection parameters
     *
     * @return Trello connection parameters
     */
    @Override
    public List<ConnectorRequiredParams> getRequiredParameters() {
        if (requiredParams == null) {
            requiredParams = new ArrayList<>();
            for (TrelloConnectorParams param : TrelloConnectorParams.values()) {
                ConnectorRequiredParams newParam = new ConnectorRequiredParams();
                newParam.setName(param.name());
                newParam.setMandatory(param.isMandatory());
                newParam.setSecured(param.isSecured());
                requiredParams.add(newParam);

            }
        }
        return requiredParams;
    }

    /**
     * Get Trello string type
     *
     * @return String type
     */
    @Override
    public String getType() {
        return "trello";
    }

    /**
     * Create a Trello indicator from an indicator type
     *
     * @param s indicator type
     * @return A new Trello indicator
     */
    @Override
    public AbstractIndicator createIndicator(String s) {

        return TrelloIndicatorType.valueOf(s).createIndicator();
    }

    /**
     * Test access to the connector
     *
     * @param connectorParams weather connection parameters
     * @return True if connection ok
     */
    @Override
    public boolean testConnector(Map<String, String> connectorParams) {
        String boardUrl = connectorParams.get(TrelloConnectorParams.url.name());
        String apiKey = connectorParams.get(TrelloConnectorParams.api_key.name());
        String token = connectorParams.get(TrelloConnectorParams.token.name());

        try{
            if(TrelloUtils.testConnector(boardUrl, apiKey, token).getResponseCode()==HttpURLConnection.HTTP_OK){

                LOGGER.debug("Connected");

                return true;

            }

        }catch (Exception e){

            LOGGER.error(e.toString());

        }

        LOGGER.debug("Not Connected");

        return false;

    }

}
