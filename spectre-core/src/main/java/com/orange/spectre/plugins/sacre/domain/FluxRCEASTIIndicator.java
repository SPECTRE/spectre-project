package com.orange.spectre.plugins.sacre.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.ListeTRAI;
import com.orange.spectre.plugins.sacre.common.model.enumerator.SacreIndicatorType;
import com.orange.spectre.plugins.sacre.common.model.enumerator.TraiIdRCEASTI;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

@Entity
@DiscriminatorValue("FLUX_RCE_ASTI_STATUT")
public class FluxRCEASTIIndicator extends SacreIndicator {

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }

    @Override
    public Enum getType() {
        return SacreIndicatorType.FLUX_RCE_ASTI_STATUT;
    }

    @Override
    public String getLabel() {
        return SacreIndicatorType.FLUX_RCE_ASTI_STATUT.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TEXT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Metiers";
    }

    @Override
    public String getPurpose() {
        return "Retourne les status des flux RCE et ASTI";
    }

    @Override
    public String getDefinition() {
        return "Retourne OK si tous les flux RCE et ASTI sont OK, KO sinon";
    }

    @Override
    public String getOrigin() {
        return "SACRE";
    }

    public StringData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        StringData data = new StringData();
        ObjectMapper mapper = new ObjectMapper();
        String StatutAval = "OK";
        Data myData = mapper.readValue(flux, Data.class);


        for (ListeTRAI trai : myData.getData().getListeTRAI()) {

            // prendre que les trai amonts 1,2,3,4,34,35,33,47,37,77
            if (TraiIdRCEASTI.contains(Integer.parseInt(trai.getTRAIId())) && (trai.getEtat().compareToIgnoreCase("KO") == 0 && trai.getRemarques().compareToIgnoreCase("Pas de run ce jour") != 0)) {
                StatutAval = "KO";

            }
        }

        data.setData(StatutAval);
        return data;
    }
}
