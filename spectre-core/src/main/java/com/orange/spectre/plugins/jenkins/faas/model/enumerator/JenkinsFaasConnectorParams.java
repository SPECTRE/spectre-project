package com.orange.spectre.plugins.jenkins.faas.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 *
 * @author nblg6551
 */
public enum JenkinsFaasConnectorParams implements ConnectorParamsInterface {

    url (true, false),
    nom_job (false, false);
	
	
	private final boolean mandatory;
	private final boolean secured;
	
	private JenkinsFaasConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}
	
	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}
	
	@Override
	public boolean isSecured() {
		return this.secured;
	}

}
