package com.orange.spectre.plugins.devopsic.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.orange.spectre.plugins.devopsic.domain.DevopsicConnector;

/**
 * Devopsic connector JPA enumerator
 */
public interface DevopsicConnectorRepository extends
		JpaRepository<DevopsicConnector, Long> {

}
