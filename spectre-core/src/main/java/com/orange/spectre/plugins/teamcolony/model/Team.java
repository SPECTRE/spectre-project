package com.orange.spectre.plugins.teamcolony.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by WKNL8600 on 27/04/2016.
 * TeamColony team bean
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Team {

    private List<Reports> reports = new ArrayList<Reports>();
    private String name;

    public List<Reports> getReports() {
        return reports;
    }
    // private Reports[] reports;

    public void setReports(List<Reports> reports) {
        this.reports = reports;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ClassPojo [reports = , name = " + name + "]";
    }


}
