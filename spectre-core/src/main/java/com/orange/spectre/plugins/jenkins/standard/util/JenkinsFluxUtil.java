package com.orange.spectre.plugins.jenkins.standard.util;

import com.orange.spectre.plugins.jenkins.common.exception.JenkinsException;
import com.orange.spectre.plugins.jenkins.common.util.JenkinsAuthentication;
import com.orange.spectre.plugins.jenkins.standard.model.enumerator.JenkinsConnectorParams;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ludovic on 23/08/2016.
 */
public final class JenkinsFluxUtil {

    // Création du JSON

    /**
     *
     * @param params
     * @return
     * @throws IOException
     */
    public static String getFluxJson(Map<String, String> params) throws IOException, Exception {
        String projectName = (String) params.get(JenkinsConnectorParams.nom_projet.name());
        String viewName = (String) params.get(JenkinsConnectorParams.nom_vue.name());
        String jenkinsUrl;
        String JenkinsPartialUrl = params.get(JenkinsConnectorParams.url.name());

        if (viewName != null && viewName.length() > 0){

            if (projectName != null && projectName.length() > 0) {


                jenkinsUrl = JenkinsPartialUrl + "/view/" + viewName + "/job/" + projectName.replaceAll(" ", "%20") + "/api/json?pretty=true";
                //URLEncoder replace the spaces to "+", out, we want %20
            } else {

                jenkinsUrl = JenkinsPartialUrl+ "/view/" + viewName + "/api/json?depth=2&pretty=true";
            }
        } else {
            if (projectName != null && projectName.length() > 0) {
                jenkinsUrl = JenkinsPartialUrl +"/job/" + projectName.replaceAll(" ", "%20") + "/api/json?pretty=true";

                //URLEncoder replace the spaces to "+", out, we want %20
            } else {
                jenkinsUrl = JenkinsPartialUrl+"/api/json?depth=2&pretty=true";
            }
        }

        String json = null;
        JenkinsAuthentication Authen = new JenkinsAuthentication();
        String authen = Authen.scrape(params);
        if (authen == null){
            throw new JenkinsException("Authentication problem");
        }



        json = Authen.request(jenkinsUrl, params);
        if (json.contains("HTTP ERROR"))
        {
            throw new JenkinsException("project or view does not exist");
        }
        return json;
    }


}
