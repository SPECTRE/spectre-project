package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by htxs7368 on 6/12/2017.
 */
public class MingleCountUserStoriesPojo {

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("Count ")
    private int count;
}
