package com.orange.spectre.plugins.jira.util;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by ludovic on 29/08/2016.
 */
public final class JiraConstant {

	public static final String JIRA_REST_DOMAIN = "/rest/api";
	public static final String JIRA_REST_VERSION = "/2";
	public static final String JIRA_PROJECT = "/project?name="; //%3D vaut =
	public static final String JIRA_SEARCH_PROJECT = "project%3D";
	public static final String JIRA_SEARCH = String.format("%s%s/search?jql=%s", JIRA_REST_DOMAIN, JIRA_REST_VERSION, JIRA_SEARCH_PROJECT);
	public static final String JIRA_FIX_VERSION = " and fixversion=";
	public static final String JIRA_MAX_RESULTS = "&maxResults=0";
	public static final String JIRA_MAX_RESULTS_COUNT = "&maxResults=";
	public static final String JIRA_FIELDS_PRIORITY = "&fields=priority";
	public static final Object JIRA_FIELDS_RESOLUTION = "&fields=resolution";
	public static final Object JIRA_FIELDS_STATUS = "&fields=status";
	public static final String JIRA_FIELDS_ISSUE_TYPE = "&fields=issuetype";
	public static final String JIRA_ISSUETYPE_BUG = " and issuetype=Bug";

	public static final List<String> PRIORITY = Collections.unmodifiableList(
			Arrays.asList("Mandatory", "Highest", "Blocker", "Must", "Critical", "Very urgent", "High", "Urgent", "Major", "Should",
					"Normal", "Medium", "Could", "Wish", "Faible", "Low", "Minor", "Très faible", "Lowest", "Very low", "Trivial",
					"Standby"));

	public static final List<String> STATUS = Collections.unmodifiableList(
			Arrays.asList("Backlog", "Ready for sprint", "In Progress", "Done", "Selected for Development", "D&U conception en cours",
					"D&U graphisme en cours", "D&U priorisé / à faire", "DEV : terminé", "Démarré D&U",
					"Métier : brief en cours", "Métier brief terminé", "Priorisé D&U", "en cours DEV", "prêt pour dev", "A faire",
					"A valider", "En cours", "Bloqué", "Développé", "En définition", "A FAIRE", "A VALIDER", "EN COURS",
					"BLOQUE", "DEVELOPPE", "EN DEFINITION", "FINI", "IN DEFINITION", "Idle", "Pending", "READY", "TO MERGE", "TO TEST"));

	public static final List<String> RESOLUTION = Collections.unmodifiableList(
			Arrays.asList("Unresolved","Done", "Won't Do", "Duplicate", "Cannot Reproduce", "Refused", "1",
					"Terminé", "Non accepté", "Doublon", "Impossible à reproduire",
					"Non résolu", "Réalisée", "Rejetée", "Fixed", "Not yet fixed", "Not yet resolved",
					"Won't Fix"));

	public static final List<String> ISSUETYPE = Collections.unmodifiableList(
			Arrays.asList("Action", "Tâche", "Bug", "Epic", "User Story", "Projet",
					"Avant projet", "Task", "Sub-task", "Story"));

	public static final String UNRESOLVED = "Unresolved";
}
