package com.orange.spectre.plugins.cbs.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.cbs.model.enumerator.CbsConnectorParams;
import com.orange.spectre.plugins.cbs.model.enumerator.CbsIndicatorType;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@DiscriminatorValue("CBS")
public class CbsConnector extends AbstractConnector {

	private static final long						serialVersionUID	= 3910007189761345344L;

	public static Map<String, Map<String, String>>	DATA				= initData();

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	public List<String> getAvailableIndicators() {
		return Arrays.stream(CbsIndicatorType.values()).map(Enum::name)
				.collect(Collectors.toList());
	}

	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if(requiredParams == null){
			requiredParams = new ArrayList<>();
			for(CbsConnectorParams param : CbsConnectorParams.values()){
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}

		return requiredParams;
	}

	@Override
	public String getType() {
		return "cbs";
	}

	@Override
	public AbstractIndicator createIndicator(String s) {
		return CbsIndicatorType.valueOf(s).createIndicator();
	}

	public static Map<String, Map<String, String>> initData() {
//		try {
//			// FIXME: need to get this file remotely
//			InputStream is = CbsConnector.class.getClassLoader()
//					.getResourceAsStream(
//							"TDB_Performance_DESI_vue+panoramique.xlsm");
//
//			return HandleExcelUtils.processOneSheet(is, "rId2");
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
		return null;
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		return true;
	}
}
