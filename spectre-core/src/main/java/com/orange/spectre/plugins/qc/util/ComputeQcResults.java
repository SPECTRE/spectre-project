package com.orange.spectre.plugins.qc.util;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.qc.custom.model.enumerator.CustomQcConnectorParams;
import com.orange.spectre.plugins.qc.model.enumerator.QcConnectorParams;
import com.orange.spectre.plugins.qc.model.enumerator.QcFieldName;
import com.orange.spectre.plugins.qc.model.enumerator.QcIndicatorType;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.HttpURLConnection;
import java.util.*;

/**
 * qc connector JPA enumerator
 */
public class ComputeQcResults {
    private static final Logger LOGGER = LoggerFactory.getLogger(ComputeQcResults.class);

    /**
     * Return the query string with or without version filtering
     *
     * @param pConnector      : the QC Connector
     * @param pRequestHeaders : the header
     * @param pQuery          : the default query string of url
     * @param pVersion        : the version to search defects   @return queryString with version if present
     */
    public String computeWithVersion(RestConnector pConnector, Map<String, String> pRequestHeaders, String pQuery, String pVersion) {
        String versionIds = null;
        if (StringUtils.isNotEmpty(pVersion)) {
            versionIds = extractVersions(pConnector, pRequestHeaders, pVersion);
            if (StringUtils.isNotEmpty(versionIds)) {
                if (StringUtils.isNotEmpty(pQuery) && pQuery.contains("query")) {
                    pQuery = String
                            .format("%s%s%s%s%s", pQuery, QCConstant.HP_ALM_URL_POINT_VIRGULE,
                                    QCConstant.HP_ALM_URL_QUERY_DETECTED_IN_RELEASE, versionIds,
                                    QCConstant.HP_ALM_URL_END_CROCHET);
                } else {
                    pQuery = String
                            .format("%s&query={%s%s%s", pQuery, QCConstant.HP_ALM_URL_QUERY_DETECTED_IN_RELEASE, versionIds,
                                    QCConstant.HP_ALM_URL_END_CROCHET);
                }
            }
        }
        //Close the query with }
        return String
                .format("%s%s", pQuery.replaceAll(" ", "%20"), QCConstant.HP_ALM_URL_END_ACCOLADE);
    }

    /**
     * Get list of id version representing the version to filter
     *
     * @param pConnector
     * @param pRequestHeaders
     * @param pVersion
     * @return String representing all version id for query
     */
    private String extractVersions(RestConnector pConnector, Map<String, String> pRequestHeaders, String pVersion) {
        String versionIds = null;
        String versionUrl = pConnector.buildEntityCollectionUrl(QCConstant.HP_ALM_ENTITY_RELEASE);
        String queryString = String.format(QCConstant.HP_ALM_URL_QUERY_BY_NAME, pVersion);
        Response responseVersions = null;
        try {
            responseVersions = pConnector.httpGet(versionUrl, queryString.replaceAll(" ", "%20"), pRequestHeaders);
            if (HttpURLConnection.HTTP_OK == responseVersions.getStatusCode()) {
                byte[] jsonData = responseVersions.getResponseData();
                ObjectMapper objectMapper = new ObjectMapper();
                JsonNode rootNode = objectMapper.readTree(jsonData);
                JsonNode nodeEntities = rootNode.get("entities");
                if (nodeEntities.isArray()) {
                    for (JsonNode nodeEntity : nodeEntities) {
                        JsonNode nodeFields = nodeEntity.get("Fields");
                        if (nodeFields.isArray()) {
                            for (JsonNode node : nodeFields) {
                                if ("id".equals(node.get("Name").textValue())) {
                                    //									String id = node.get("Name").asText();
                                    String value = node.get("values").get(0).get("value").asText();
                                    if (versionIds == null) {
                                        versionIds = String
                                                .format("'%s'", value);
                                    } else {
                                        versionIds = String
                                                .format("%s or '%s'", versionIds, value);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.debug("Unable to get version  --> " + pVersion + " error message " + e.getMessage());
            return null;
        }
        return versionIds;
    }

    /**
     * Return the data
     *
     * @param pConnector
     * @param pQuery
     * @param pHeaders
     * @return
     */

    public Integer extractData(RestConnector pConnector, String pQuery, Map<String, String> pHeaders) throws Exception {

        String defectUrl = pConnector.buildEntityCollectionUrl(QCConstant.HP_ALM_ENTITY_DEFECT);
        Response responseData = pConnector.httpGet(defectUrl, pQuery.replaceAll(" ", "%20"), pHeaders);
        if (HttpURLConnection.HTTP_OK == responseData.getStatusCode()) {
            //Treat Response
            byte[] jsonData = responseData.getResponseData();
            //create ObjectMapper instance
            ObjectMapper objectMapper = new ObjectMapper();
            //read JSON like DOM Parser
            JsonNode rootNode = objectMapper.readTree(jsonData);
            return Integer.parseInt(rootNode.path("TotalResults").toString());
        }else{
            throw new Exception();
        }
    }

    public AbstractData extractSeverityData(QcAuthentification login, RestConnector con, Map<String, String> connectorParams,
                                            QcFieldName severity) {
        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());
        String version = connectorParams.get(QcConnectorParams.release.name());

        boolean loginState = false;
        String query = null;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC closed status defect
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");

                switch (severity.name()) {
                    case "CRITICAL":
//					query = QCConstant.HP_ALM_DEFAULT_URL_SEVERITY_CRITICAL;
                        query = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_SEVERITY_CRITICAL, QCConstant.HP_ALM_DEFAULT_URL_QUERY_SEVERITY_CRITICAL);

                        break;//CRITICAL
                    case "MAJOR":
//					query = QCConstant.HP_ALM_DEFAULT_URL_SEVERITY_MAJOR;
                        query = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_SEVERITY_MAJOR, QCConstant.HP_ALM_DEFAULT_URL_QUERY_SEVERITY_MAJOR);
                        break;//MAJOR
                    case "MINOR":
//					query = QCConstant.HP_ALM_DEFAULT_URL_SEVERITY_MINOR;
                        query = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_SEVERITY_MINOR, QCConstant.HP_ALM_DEFAULT_URL_QUERY_SEVERITY_MINOR);
                        break;// MINOR
                }
                String queryString = this
                        .computeWithVersion(con, requestHeaders, query, version);
                //Get Defects with severity Major
                StringData data = new StringData(this.extractData(con, queryString, requestHeaders).toString());
                login.logout();
                return data;


            } else {
                LOGGER.error("Exception: problem during connexion :");
                return null;
            }

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }

    }

    public AbstractData extractStatusOpenedClosedData(QcAuthentification login, RestConnector con, Map<String, String> connectorParams) {

        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());
        String version = connectorParams.get(QcConnectorParams.release.name());

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC closed status defect
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");

                String queryString = "";
                List<String> datas = new ArrayList<>();
                List<String> labels = new ArrayList<>();
                //Get Defects Closed
                queryString = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_CLOSED, QCConstant.HP_ALM_DEFAULT_URL_QUERY_CLOSED);
                queryString = this.computeWithVersion(con, requestHeaders, queryString, version);
                Integer dataClosed = this.extractData(con, queryString, requestHeaders);
                datas.add(dataClosed.toString());
                labels.add(QcIndicatorType.BUGS_CLOSED_NB.getLabel());
                //Get Defects Opened
                queryString = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_OPENED, QCConstant.HP_ALM_DEFAULT_URL_QUERY_OPENED);
                queryString = this.computeWithVersion(con, requestHeaders, queryString, version);
                Integer dataOpened = this.extractData(con, queryString, requestHeaders);
                datas.add(dataOpened.toString());
                labels.add(QcIndicatorType.BUGS_NO_CLOSED_NB.getLabel());
                TupleData tuple = new TupleData();
                //set Tuple
                tuple.setMeasure(IndicatorMeasure.NUMERIC.name());
                tuple.setData(datas);
                tuple.setLabel(labels);

                login.logout();
                return tuple;
            } else {
                LOGGER.error("Exception: problem during connexion :");
                login.logout();
                return null;
            }
        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }

    }

    public AbstractData extractStatusOpenedData(QcAuthentification login, RestConnector con, Map<String, String> connectorParams) {

        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());
        String version = connectorParams.get(QcConnectorParams.release.name());

        String query = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_OPENED, QCConstant.HP_ALM_DEFAULT_URL_QUERY_OPENED);

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC closed status defect
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");

                String queryString = this.computeWithVersion(con, requestHeaders, query, version);
                //Get Defects Opened
                StringData data = new StringData(this.extractData(con, queryString, requestHeaders).toString());
                login.logout();
                return data;

            } else {
                LOGGER.error("Exception: problem during connexion :");
                login.logout();
                return null;
            }

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);

            return null;
        }


    }

    public AbstractData extractStatusClosedData(QcAuthentification login, RestConnector con, Map<String, String> connectorParams) {
        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());
        String version = connectorParams.get(QcConnectorParams.release.name());

        String query = String.format("%s%s%s", QCConstant.HP_ALM_DEFAULT_URL, QCConstant.HP_ALM_DEFAULT_URL_FIELD_CLOSED, QCConstant.HP_ALM_DEFAULT_URL_QUERY_CLOSED);

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC closed status defect
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");
                String queryString = this.computeWithVersion(con, requestHeaders, query, version);
                //Get Defects Closed

                StringData data = new StringData(this.extractData(con, queryString, requestHeaders).toString());
                login.logout();
                return data;
            } else {
                LOGGER.error("Exception: problem during connexion :");
                login.logout();
                return null;
            }

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }


    }

    public AbstractData extractCustomData(QcAuthentification login, RestConnector con, Map<String, String> connectorParams) {
        String username = connectorParams.get(CustomQcConnectorParams.login.name());
        String password = connectorParams.get(CustomQcConnectorParams.password.name());
        String version = connectorParams.get(CustomQcConnectorParams.release.name());
        String fields = connectorParams.get(CustomQcConnectorParams.fields.name());
        String query = connectorParams.get(CustomQcConnectorParams.query.name());

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC closed status defect
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");
                String queryString = String.format("%s", QCConstant.HP_ALM_DEFAULT_URL);
                if (StringUtils.isNotEmpty(fields)) {
                    queryString = String.format("%s&fields=%s", queryString, fields);
                }
                if (StringUtils.isNotEmpty(query)) {
                    queryString = String.format("%s&query=%s%s", queryString, QCConstant.HP_ALM_URL_BEGIN_ACCOLADE, query);
                    queryString = this.computeWithVersion(con, requestHeaders, queryString, version);
                    //Get Defects Custom
                    StringData data = new StringData(this.extractData(con, queryString, requestHeaders).toString());
                    login.logout();
                    return data;
                } else {
                    LOGGER.error("Query must not be empty.");
                    login.logout();
                    return null;
                }


            } else {
                LOGGER.error("Exception: problem during connexion :");
                return null;
            }

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }


    }

    public AbstractData extractAllTestCaseNb(QcAuthentification login, RestConnector con, Map<String, String> connectorParams){
        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());

        IntegerData integerData = new IntegerData();
        int data;

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC Test Cases Number
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");
                String queryString = "https://almdtsi.itn.ftgroup/qcbin/rest/domains/dom_i/projects/ihm_adv/test-instances";
                Response responseData = con.httpGet(queryString, "", requestHeaders);
                if (HttpURLConnection.HTTP_OK == responseData.getStatusCode()) {
                    //Treat Response
                    byte[] jsonData = responseData.getResponseData();
                    //create ObjectMapper instance
                    ObjectMapper objectMapper = new ObjectMapper();
                    //read JSON like DOM Parser
                    JsonNode rootNode = objectMapper.readTree(jsonData);
                    data = Integer.parseInt(rootNode.path("TotalResults").toString());
                }else{
                    throw new Exception();
                }
                integerData.setData(data);
                login.logout();
                return integerData;
            } else {
                LOGGER.error("Exception: problem during connexion :");
                login.logout();
                return null;
            }

        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }

    }

    private AbstractData extractStatusNb(QcAuthentification login, RestConnector con, Map<String, String> connectorParams, String status){
        String username = connectorParams.get(QcConnectorParams.login.name());
        String password = connectorParams.get(QcConnectorParams.password.name());

        IntegerData integerData = new IntegerData();
        int data = 0;

        boolean loginState = false;
        try {
            loginState = login.login(username, password);
            if (loginState) {
                //Get All QC Test Passed Number
                Map<String, String> requestHeaders = new HashMap<String, String>();
                requestHeaders.put("Accept", "application/json");
                requestHeaders.put("Content-Type", "application/json");
                String queryString = "https://almdtsi.itn.ftgroup/qcbin/rest/domains/dom_i/projects/ihm_adv/test-instances?fields=status";

                Response responseData = con.httpGet(queryString, "", requestHeaders);

                if (HttpURLConnection.HTTP_OK == responseData.getStatusCode()) {
                    //Treat Response
                    byte[] jsonData = responseData.getResponseData();
                    //create ObjectMapper instance
                    ObjectMapper objectMapper = new ObjectMapper();
                    //read JSON like DOM Parser
                    JsonNode rootNode = objectMapper.readTree(jsonData);
                    Iterator<JsonNode> entities =  rootNode.path("entities").iterator();
                    while(entities.hasNext()){
                        Iterator<JsonNode> fields = entities.next().path("Fields").iterator();
                        while (fields.hasNext()){
                            JsonNode field = fields.next();
                            if(field.path("Name").textValue().equals("status")){
                                if(field.path("values").iterator().next().path("value").textValue().equals(status)){
                                    data++;
                                }
                            }
                        }
                    }
                }else{
                    throw new Exception();
                }
                integerData.setData(data);
                login.logout();
                return integerData;

            } else {
                LOGGER.error("Exception: problem during connexion :");
                login.logout();
                return null;
            }
        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval :" + e);
            return null;
        }
    }

    public AbstractData extractPassedTestNb(QcAuthentification login, RestConnector con, Map<String, String> connectorParams){
       return extractStatusNb(login, con, connectorParams, "Passed");
    }

    public AbstractData extractFailedTestNb(QcAuthentification login, RestConnector con, Map<String, String> connectorParams){
        return extractStatusNb(login, con, connectorParams, "Failed");
    }

    public AbstractData extractNoRunTestNb(QcAuthentification login, RestConnector con, Map<String, String> connectorParams){
        return extractStatusNb(login, con, connectorParams, "No Run");
    }

    public  AbstractData  extractFunctionalReportTest(QcAuthentification login, RestConnector con, Map<String, String> connectorParams){

        TupleData tupleData = new TupleData();

        String passed = this.extractPassedTestNb(login, con, connectorParams).getData().toString();
        String failed = this.extractFailedTestNb(login, con, connectorParams).getData().toString();
        String noRun = this.extractNoRunTestNb(login, con, connectorParams).getData().toString();

        List<String> label = new ArrayList<>();
        List<String> data= new ArrayList<>();

        //Add passed test data
        label.add("Tests reussis");
        data.add(passed);

        //Add failed test data
        label.add("Tests échoués");
        data.add(failed);

        //Add No Run test data
        label.add("Tests non éxécutés");
        data.add(noRun);

        tupleData.setLabel(label);
        tupleData.setData(data);
        tupleData.setMeasure(IndicatorMeasure.NUMERIC.name());

        return tupleData;
    }
}
