
package com.orange.spectre.plugins.teamcolony.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * TeamColony Connection params
 */
public enum TeamColonyConnectorParams implements ConnectorParamsInterface {

   token (true, true),
   team_name (true, false);

	private final boolean mandatory;
	private final boolean secured;

	private TeamColonyConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
