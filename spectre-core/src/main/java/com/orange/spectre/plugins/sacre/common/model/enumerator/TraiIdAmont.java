package com.orange.spectre.plugins.sacre.common.model.enumerator;

public enum TraiIdAmont {
	
	Facture_FE(1),
	Compte_FE(2),
	Facture_50D(3),
	Compte_50D(4),
	Facture_NOVAE(34),
	Compte_NOVAE(35),
	DISE(33),
	COME(47),
	SELF10(51);
	
	
	
	private int trai_id;
	
	TraiIdAmont(int trai) {
		trai_id = trai;
	}
	
	public int getTrai() {
		return trai_id;
	}
	
	public static boolean contains(int trai){
		for(TraiIdAmont mytrai : TraiIdAmont.values()) {
			if (mytrai.getTrai() == trai) {
				return true;
			}
		}
		return false;
	}
}