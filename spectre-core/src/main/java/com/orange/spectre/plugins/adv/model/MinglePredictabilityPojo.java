package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by kctl7743 on 02/06/2017.
 */
public class MinglePredictabilityPojo {

    @JsonProperty("Avg Vélocité")
    private Float avgVelocity;

    @JsonProperty("Avg Capacité")
    private Float avgCapacity;

    public Float getAvgVelocity() {
        return avgVelocity;
    }

    public void setAvgVelocity(Float avgVelocity) {
        this.avgVelocity = avgVelocity;
    }

    public Float getAvgCapacity() {
        return avgCapacity;
    }

    public void setAvgCapacity(Float avgCapacity) {
        this.avgCapacity = avgCapacity;
    }
}
