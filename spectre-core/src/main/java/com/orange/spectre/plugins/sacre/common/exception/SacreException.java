package com.orange.spectre.plugins.sacre.common.exception;

public class SacreException extends UnsupportedOperationException {

    /**
     *
     * @param cause
     */
    public SacreException(Throwable cause) {
        super(cause);
    }

}
