package com.orange.spectre.plugins.mingle.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.FloatData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.mingle.model.MingleStoryPojo;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;
import com.orange.spectre.plugins.mingle.util.MingleMqlResponseUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.IOException;
import java.util.Map;

import static com.orange.spectre.plugins.mingle.model.enumerator.MingleIndicatorType.SPRINT_PROGRESSION;

/**
 * Jpa entity for percentage jobs fails
 *
 * @author nmcf5735
 */
@Entity
@DiscriminatorValue("SPRINT_PROGRESSION")
public class SprintProgressionIndicator extends AbstractIndicator {

    @Override
    public String getLabel() {
        return SPRINT_PROGRESSION.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.PERCENT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {


        String sprint = connectorParams.get(MingleConnectorParams.sprint_courant.name());

        if (sprint != null) {

            String mqlRequest = "SELECT complexité, 'planning - sprint', statut WHERE type='story' and 'planning - sprint' != null";

            JSONArray jsonArray = MingleMqlResponseUtils.getResponseArray(connectorParams, mqlRequest);

            if (jsonArray != null) {
                try {
                    Float progression = getProgression(jsonArray, sprint);
                    if(progression != null){
                        return new FloatData(progression);
                    }

                } catch (IOException e) {
                    return null;
                }
            }
        }
        return null;
    }

    private Float getProgression(JSONArray jsonArray, String sprint) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        float complexityTotalSum = 0;
        float complexityClosedSum = 0;

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject story = (JSONObject) jsonArray.get(i);
            MingleStoryPojo pojo = mapper.readValue(story.toString(), MingleStoryPojo.class);

            if (pojo.getComplexity() != null && pojo.getSprint().contains(sprint)){
                complexityTotalSum += Integer.valueOf(pojo.getComplexity());

                if(pojo.getStatus() != null && pojo.getStatus().equals("Terminé")){
                    complexityClosedSum += Integer.valueOf(pojo.getComplexity());
                }
            }
        }


        if(complexityTotalSum != 0){
            return complexityClosedSum * 100 / complexityTotalSum;
        }else{
            return null;
        }


    }


    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return SPRINT_PROGRESSION;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}

