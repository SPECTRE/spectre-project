package com.orange.spectre.plugins.jira.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by epeg7421 on 14/04/17.
 */
public class JiraFields {
	private JiraResolution resolution;
	private JiraPriority priority;
	private JiraStatus status;
	@JsonProperty("issuetype")
	private JiraIssueType issueType;

	public JiraResolution getResolution() {
		return resolution;
	}

	public void setResolution(JiraResolution resolution) {
		this.resolution = resolution;
	}

	public JiraPriority getPriority() {
		return priority;
	}

	public void setPriority(JiraPriority priority) {
		this.priority = priority;
	}

	public JiraIssueType getIssueType() {
		return issueType;
	}

	public void setIssueType(JiraIssueType issueType) {
		this.issueType = issueType;
	}

	public JiraStatus getStatus() {
		return status;
	}

	public void setStatus(JiraStatus status) {
		this.status = status;
	}
}
