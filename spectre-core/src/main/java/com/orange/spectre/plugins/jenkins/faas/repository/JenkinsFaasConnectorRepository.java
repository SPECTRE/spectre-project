package com.orange.spectre.plugins.jenkins.faas.repository;

import com.orange.spectre.plugins.jenkins.faas.domain.JenkinsFaasConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author nblg6551
 */
public interface JenkinsFaasConnectorRepository extends JpaRepository<JenkinsFaasConnector, Long> {

}
