package com.orange.spectre.plugins.jira.custom.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.jira.custom.domain.CustomQueryNbIndicator;
import com.orange.spectre.plugins.qc.custom.domain.CustomDefectsNbIndicator;

/**
 * Enumerator of all available qc indicators
 */
public enum CustomJiraIndicatorType {

	BUGS_CUSTOM(CustomQueryNbIndicator.class);
	/**
	 * Current qc indicator class
	 */
	private Class<? extends AbstractIndicator> clazz;

	/**
	 * Indicator type constructor
	 *
	 * @param clazz Current indicator class
	 */
	CustomJiraIndicatorType(Class<? extends AbstractIndicator> clazz) {
		this.clazz = clazz;
	}

	/**
	 * Indicator instantiation from it's type
	 *
	 * @return An qc indicator
	 */
	public AbstractIndicator createIndicator() {
		try {
			return clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new RuntimeException(clazz + " has no default constructor");
		}
	}

	/**
	 * Get indicator label from it's type
	 *
	 * @return Indicator label
	 */
	public String getLabel() {
		switch (this.name()) {
		case "BUGS_CUSTOM":
			return "Customize Query";
		default:
			return null;
		}
	}
}


