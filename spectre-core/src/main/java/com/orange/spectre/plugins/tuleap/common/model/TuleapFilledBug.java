package com.orange.spectre.plugins.tuleap.common.model;

/**
 * Created by ludovic on 27/03/2017.
 */
public class TuleapFilledBug {

    private String id;
    private String status;
    private String title;
    private TuleapBugValue[] values;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TuleapBugValue[] getValues() {
        return values;
    }

    public void setValues(TuleapBugValue[] values) {
        this.values = values;
    }
}
