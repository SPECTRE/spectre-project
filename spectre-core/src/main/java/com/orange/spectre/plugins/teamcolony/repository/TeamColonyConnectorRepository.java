package com.orange.spectre.plugins.teamcolony.repository;

import com.orange.spectre.plugins.teamcolony.domain.TeamColonyConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Teamcolony connector JPA enumerator
 */
public interface TeamColonyConnectorRepository extends JpaRepository<TeamColonyConnector, Long> {

}
