
package com.orange.spectre.plugins.sacre.common.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "TRAI_id",
    "Traitement",
    "Date",
    "Etat",
    "Run",
    "Remarques",
    "Debut",
    "Duree",
    "Fin",
    "Nb lignes",
    "Date fichier"
})
public class ListeTRAI {

    @JsonProperty("TRAI_id")
    private String tRAIId;
    @JsonProperty("Traitement")
    private String traitement;
    @JsonProperty("Date")
    private String date;
    @JsonProperty("Etat")
    private String etat;
    @JsonProperty("Run")
    private String run;
    @JsonProperty("Remarques")
    private String remarques;
    @JsonProperty("Debut")
    private String debut;
    @JsonProperty("Duree")
    private String duree;
    @JsonProperty("Fin")
    private String fin;
    @JsonProperty("Nb lignes")
    private String nbLignes;
    @JsonProperty("Date fichier")
    private String dateFichier;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The tRAIId
     */
    @JsonProperty("TRAI_id")
    public String getTRAIId() {
        return tRAIId;
    }

    /**
     * 
     * @param tRAIId
     *     The TRAI_id
     */
    @JsonProperty("TRAI_id")
    public void setTRAIId(String tRAIId) {
        this.tRAIId = tRAIId;
    }

    /**
     * 
     * @return
     *     The traitement
     */
    @JsonProperty("Traitement")
    public String getTraitement() {
        return traitement;
    }

    /**
     * 
     * @param traitement
     *     The Traitement
     */
    @JsonProperty("Traitement")
    public void setTraitement(String traitement) {
        this.traitement = traitement;
    }

    /**
     * 
     * @return
     *     The date
     */
    @JsonProperty("Date")
    public String getDate() {
        return date;
    }

    /**
     * 
     * @param date
     *     The Date
     */
    @JsonProperty("Date")
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * 
     * @return
     *     The etat
     */
    @JsonProperty("Etat")
    public String getEtat() {
        return etat;
    }

    /**
     * 
     * @param etat
     *     The Etat
     */
    @JsonProperty("Etat")
    public void setEtat(String etat) {
        this.etat = etat;
    }

    /**
     * 
     * @return
     *     The run
     */
    @JsonProperty("Run")
    public String getRun() {
        return run;
    }

    /**
     * 
     * @param run
     *     The Run
     */
    @JsonProperty("Run")
    public void setRun(String run) {
        this.run = run;
    }

    /**
     * 
     * @return
     *     The remarques
     */
    @JsonProperty("Remarques")
    public String getRemarques() {
        return remarques;
    }

    /**
     * 
     * @param remarques
     *     The Remarques
     */
    @JsonProperty("Remarques")
    public void setRemarques(String remarques) {
        this.remarques = remarques;
    }

    /**
     * 
     * @return
     *     The debut
     */
    @JsonProperty("Debut")
    public String getDebut() {
        return debut;
    }

    /**
     * 
     * @param debut
     *     The Debut
     */
    @JsonProperty("Debut")
    public void setDebut(String debut) {
        this.debut = debut;
    }

    /**
     * 
     * @return
     *     The duree
     */
    @JsonProperty("Duree")
    public String getDuree() {
        return duree;
    }

    /**
     * 
     * @param duree
     *     The Duree
     */
    @JsonProperty("Duree")
    public void setDuree(String duree) {
        this.duree = duree;
    }

    /**
     * 
     * @return
     *     The fin
     */
    @JsonProperty("Fin")
    public String getFin() {
        return fin;
    }

    /**
     * 
     * @param fin
     *     The Fin
     */
    @JsonProperty("Fin")
    public void setFin(String fin) {
        this.fin = fin;
    }

    /**
     * 
     * @return
     *     The nbLignes
     */
    @JsonProperty("Nb lignes")
    public String getNbLignes() {
        return nbLignes;
    }

    /**
     * 
     * @param nbLignes
     *     The Nb lignes
     */
    @JsonProperty("Nb lignes")
    public void setNbLignes(String nbLignes) {
        this.nbLignes = nbLignes;
    }

    /**
     * 
     * @return
     *     The dateFichier
     */
    @JsonProperty("Date fichier")
    public String getDateFichier() {
        return dateFichier;
    }

    /**
     * 
     * @param dateFichier
     *     The Date fichier
     */
    @JsonProperty("Date fichier")
    public void setDateFichier(String dateFichier) {
        this.dateFichier = dateFichier;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
