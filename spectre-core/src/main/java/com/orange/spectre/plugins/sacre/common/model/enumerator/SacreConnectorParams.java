package com.orange.spectre.plugins.sacre.common.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 *
 * @author nblg6551
 */
public enum SacreConnectorParams implements ConnectorParamsInterface {
	
    url (true, false);
	
	
	private final boolean mandatory;
	private final boolean secured;
	
	private SacreConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}
	
	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}
	
	@Override
	public boolean isSecured() {
		return this.secured;
	}

}
