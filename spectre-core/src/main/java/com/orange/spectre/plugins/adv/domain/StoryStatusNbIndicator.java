package com.orange.spectre.plugins.adv.domain;


import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.TupleData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.adv.model.MingleSprintDatePojo;
import com.orange.spectre.plugins.adv.model.MingleStoryCountPojo;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvIndicatorType;
import com.orange.spectre.plugins.adv.util.MingleAdvMqlResponseUtils;
import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by kctl7743 on 09/06/2017.
 */
@Entity
@DiscriminatorValue("ADV_STATUS_STORY")
public class StoryStatusNbIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoryStatusNbIndicator.class);

    @Override
    public String getLabel() {
        return MingleAdvIndicatorType.ADV_STORY_STATUS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TUPLE;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        String mqlRequestCount = "SELECT 'created on', Count(*) WHERE type = 'Story' and 'Statut de la story' is not null  and 'Sprint' is not NULL";
        String mqlRequestSprint = "SELECT 'name', 'Date de début', 'Date de fin' WHERE type = 'Sprint' and 'statut du sprint' is not null";

        List<String> label = new ArrayList<>();
        List<String> data = new ArrayList<>();

        List<MingleStoryCountPojo> mingleStoryCountPojoList;
        List<MingleSprintDatePojo> mingleSprintDatePojoList;
        List<MingleSprintDatePojo> result;

        try {

            // retrieve the count of user stories based on the creation date
            JSONArray jsonArrayCount = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequestCount);

            // retrieve the created date, terminated date and the name of the sprint
            JSONArray jsonArraySprint = MingleAdvMqlResponseUtils.getResponseArray(connectorParams, mqlRequestSprint);

            if (jsonArrayCount != null) {

                // cumulate the count of user stories
                mingleStoryCountPojoList = MingleAdvMqlResponseUtils.cumulateCount(jsonArrayCount);

                // get the last ten sprint
                mingleSprintDatePojoList = MingleAdvMqlResponseUtils.lastTenSprint(jsonArraySprint);

                // take a sprint, and count how many user stories is between  date de debut, et date de fin
                result = MingleAdvMqlResponseUtils.compute(mingleSprintDatePojoList, mingleStoryCountPojoList);

                for (MingleSprintDatePojo item : result) {
                    label.add(item.getName());
                    data.add(Integer.toString(item.getStoriesNb()));
                }
                TupleData statusStory = new TupleData();
                statusStory.setLabel(label);
                statusStory.setData(data);
                statusStory.setMeasure(IndicatorMeasure.NUMERIC.name());

                return statusStory;
            }

        } catch (Exception e) {
            LOGGER.debug(e.getMessage());
            return null;
        }

        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.AUTRE;
    }

    @Override
    public Enum getType() {
        return MingleAdvIndicatorType.ADV_STORY_STATUS;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }
}