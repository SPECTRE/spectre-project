package com.orange.spectre.plugins.sonar.standard.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all sonar connection parameters
 */
public enum SonarConnectorParams implements ConnectorParamsInterface {

    url (true, false),
    projet (true, false),
    version (true, false);

	private final boolean mandatory;
	private final boolean secured;

	private SonarConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}

}
