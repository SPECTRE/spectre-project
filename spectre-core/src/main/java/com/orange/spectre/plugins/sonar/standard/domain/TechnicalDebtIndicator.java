package com.orange.spectre.plugins.sonar.standard.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;


/**
 * Jpa entity for technical debt indicator
 */
@Entity
@DiscriminatorValue("SONAR_TECHNICAL_DEBT")
public class TechnicalDebtIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalDebtIndicator.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) {

        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "sqale_index";
            String sonarPartialUrl = connectorParams.get("url");
            StringData data = (StringData)ComputeDataUtils.extract(sonarPartialUrl, project, version, metric);
            Float floatData = Float.valueOf(data.getData());
            floatData = floatData/60;

            return new StringData(floatData.toString());


        }catch(Exception e){
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return SonarIndicatorType.TECHNICAL_DEBT.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return "heures";
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre la quantité de travail à fournir pour optimiser la qualité et la performance de l'application";
    }

    @Override
    public String getDefinition() {
        return "La dette technique représente le nombre d'heures nécessaires à la correction des règles de non conformités, à la diminution de la duplication de code, etc...";
    }

    @Override
    public String getOrigin() {
        return "SonarQube";
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return SonarIndicatorType.TECHNICAL_DEBT;
    }

}

