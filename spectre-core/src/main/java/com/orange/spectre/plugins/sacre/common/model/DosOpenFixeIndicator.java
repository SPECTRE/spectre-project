package com.orange.spectre.plugins.sacre.common.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.plugins.sacre.common.model.Data;
import com.orange.spectre.plugins.sacre.common.model.DossierFIXE;
import com.orange.spectre.plugins.sacre.common.model.ISacreSubIndicator;

import java.io.IOException;
import java.util.Map;

public class DosOpenFixeIndicator implements ISacreSubIndicator {


    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        IntegerData data = new IntegerData();
        ObjectMapper mapper = new ObjectMapper();
        int nbDosOpen = 0;
        Data myData = mapper.readValue(flux, Data.class);

        for (DossierFIXE trai : myData.getData().getDossierFIXE()) {
            if (trai.getOuvert() != null && trai.getOuvert() != "") {
                nbDosOpen += Integer.parseInt(trai.getOuvert());
            }

        }

        data.setData(nbDosOpen);
        return data;
    }

    @Override
    public String getLabel() {
        return "Dossiers ouverts";
    }

}
