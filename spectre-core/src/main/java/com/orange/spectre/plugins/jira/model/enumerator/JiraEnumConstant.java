package com.orange.spectre.plugins.jira.model.enumerator;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by epeg7421 on 07/04/17.
 */
public class JiraEnumConstant {

	//priority (Highest, High, Medium, Low, Lowest)
	public enum JiraFieldPriority {
		Mandatory,
		Highest,
		Blocker,
		Must,
		Critical,
		VeryUrgent,
		High,
		Urgent,
		Major,
		Should,
		Normal,
		Medium,
		Could,
		Wish,
		Faible,
		Low,
		Minor,
		TresFaible,
		Lowest,
		VeryLow,
		Trivial,
		StandBy
	}

	//status (Open, In Progress, Reopened, Resolved, Closed, To Do, Done)
	public enum JiraFieldStatus {
		Open,
		InProgress,
		Reopened,
		Resolved,
		Closed,
		Todo,
		Done
	}

	//resolution (Done, Won't Do, Duplicate, Cannot Reproduce)
	public enum JiraFieldResolution {
		Done,
		WontDo,
		Duplicate,
		CannotReproduce
	}

	//issuetype (Bug, Epic, Story, Task, Sub-task)
	public enum JiraFieldIssueType {
		Bug,
		Epic,
		Story,
		Task,
		SubTask
	}

}
