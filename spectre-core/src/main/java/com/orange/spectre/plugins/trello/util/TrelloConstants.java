package com.orange.spectre.plugins.trello.util;

/**
 * Created by KCTL7743 on 04/04/2017.
 */
public final class TrelloConstants {

    public final static String TRELLO_API_BOARD_URL = "https://api.trello.com/1/boards/";

}
