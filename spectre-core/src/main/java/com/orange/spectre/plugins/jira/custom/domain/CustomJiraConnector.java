package com.orange.spectre.plugins.jira.custom.domain;

import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.jira.custom.model.enumerator.CustomJiraConnectorParams;
import com.orange.spectre.plugins.jira.custom.model.enumerator.CustomJiraIndicatorType;
import com.orange.spectre.plugins.jira.model.enumerator.JiraConnectorParams;
import com.orange.spectre.plugins.jira.util.ComputeJiraResults;
import com.orange.spectre.plugins.jira.util.JiraConstant;
import com.orange.spectre.plugins.jira.util.JiraUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Jpa entity of JIRA customizable connector
 * <p>
 * Types de demande 	: issuetype (Bug, Epic, Story, Task, Sub-task)
 * Etats 				: status (Open, In Progress, Reopened, Resolved, Closed, To Do, Done)
 * Priorité				: priority (Highest, High, Medium, Low, Lowest)
 * Resolution			: resolution (Done, Won't Do, Duplicate, Cannot Reproduce)
 * <p>
 * Exemple de query :
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and fixversion=G01R00C00&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest)&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done and status=Done&maxResults=0
 * https://agilespectre.atlassian.net/rest/api/2/search?jql=project=SPECTRE and priority in (Highest) and issuetype=Bug and resolution=Done and status=Done&maxResults=0
 *
 * https://orange-village.valiantyscloud.net/jira/issues/?filter=-5&jql=project%20%3D%20OEMBF%20
 * 						AND%20issuetype%20in%20(Action%2C%20Bug%2C%20%22User%20Story%22)%20
 * 						AND%20status%20in%20(%22A%20faire%22%2C%20%22A%20valider%22%2C%20%22En%20cours%22)%20
 * 						AND%20resolution%20in%20(Unresolved%2C%20R%C3%A9alis%C3%A9e%2C%20Rejet%C3%A9e%2C%20Fixed)
 * 						&maxResults=0
 *
 * query :
 * 	project = OEMBF AND status in ("A faire", "A valider") AND resolution = Unresolved
 * 	AND reporter in (membersOf(jira-software-users))
 *
 */
@Entity
@DiscriminatorValue("JIRA_CUSTOM")
public class CustomJiraConnector extends AbstractConnector {
	private static final Logger LOGGER = LoggerFactory.getLogger(CustomJiraConnector.class);

	/**
	 * All connector connection params
	 */
	@Transient
	List<ConnectorRequiredParams> requiredParams;

	/**
	 * Get list of all jira indicators
	 *
	 * @return All jira indicators
	 */
	public List<String> getAvailableIndicators() {
		return Arrays.stream(CustomJiraIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

	/**
	 * Get list of all jira connection parameters
	 *
	 * @return jira connection parameters
	 */
	@Override
	public List<ConnectorRequiredParams> getRequiredParameters() {
		if (requiredParams == null) {
			requiredParams = new ArrayList<>();
			for (CustomJiraConnectorParams param : CustomJiraConnectorParams.values()) {
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);

			}
		}
		return requiredParams;
	}

	/**
	 * Get jira string type
	 *
	 * @return String type
	 */
	@Override
	public String getType() {
		return "jira_custom";
	}

	/**
	 * Create a jira indicator from an indicator type
	 *
	 * @param s indicator type
	 * @return A new jira indicator
	 */
	@Override
	public AbstractIndicator createIndicator(String s) {
		return CustomJiraIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		String username = connectorParams.get(JiraConnectorParams.login.name());
		String password = connectorParams.get(JiraConnectorParams.password.name());
		String project = connectorParams.get(JiraConnectorParams.projet.name());
		String url = String.format("%s%s%s", connectorParams.get(JiraConnectorParams.url.name()), JiraConstant.JIRA_REST_DOMAIN,
				JiraConstant.JIRA_REST_VERSION);
		String projectUrl = String.format("%s%s%s", url, JiraConstant.JIRA_PROJECT, project);

		try {
			HttpResponse response = null;
			response = JiraUtils.getJiraConnectionData(username, password, projectUrl.replaceAll("=", "%3D"));
			if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				return true;
			} else {
				LOGGER.debug("JiraConnector.class : Error connection to JIRA plateform : ");
				return false;
			}
		} catch (Exception e) {
			LOGGER.debug("JiraConnector.class : Error connection to JIRA plateform : ");
			return false;
		}
	}
}
