package com.orange.spectre.plugins.adv.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.orange.spectre.core.model.data.IntegerData;

/**
 * Created by kctl7743 on 02/06/2017.
 */
public class MingleSprintPojo {
    @JsonProperty("Sprint")
    private String sprint;

    public String getSprint() {
        return sprint;
    }

    public void setSprint(String sprint) {
        this.sprint = sprint;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @JsonProperty("Count ")
    private String count;
}
