package com.orange.spectre.plugins.teamcolony.model;

/**
 * Created by WKNL8600 on 10/05/2016.
 * Teamcolony response
 */
public class TeamColonyResponse {
    private Team team;

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }
}
