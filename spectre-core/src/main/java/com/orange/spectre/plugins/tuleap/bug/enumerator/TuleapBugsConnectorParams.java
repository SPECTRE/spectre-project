
package com.orange.spectre.plugins.tuleap.bug.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Tuleap Connection params
 */
public enum TuleapBugsConnectorParams implements ConnectorParamsInterface {

    login(true, false),
    mot_de_passe(true, true),
    nom_projet(true, false),
    nom_rapport(true, false);

    private final boolean mandatory;
    private final boolean secured;

    private TuleapBugsConnectorParams(boolean mandatory, boolean secured) {
        this.mandatory = mandatory;
        this.secured = secured;
    }

    @Override
    public boolean isMandatory() {
        return this.mandatory;
    }

    @Override
    public boolean isSecured() {
        return this.secured;
    }


}
