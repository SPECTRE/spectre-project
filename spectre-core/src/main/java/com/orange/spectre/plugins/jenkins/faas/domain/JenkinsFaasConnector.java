
package com.orange.spectre.plugins.jenkins.faas.domain;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractConnector;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.domain.ConnectorRequiredParams;
import com.orange.spectre.plugins.jenkins.common.model.ResponseFaasJSON;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasConnectorParams;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasIndicatorType;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Jpa entity of Jenkins connector
 */
@Entity
@DiscriminatorValue("JENKINS_FAAS")
public class JenkinsFaasConnector extends AbstractConnector {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsFaasConnector.class);

	/**
     * All connector connection params
     */

	@Transient
	List<ConnectorRequiredParams> requiredParams;

    /**
     * Get list of all Jenkins indicators
     *
     * @return All Jenkins indicators
     */
    public List<String> getAvailableIndicators(){
		return Arrays.stream(JenkinsFaasIndicatorType.values()).map(Enum::name).collect(Collectors.toList());

	}

    /**
     * Get list of all Jenkins connection parameters
     *
     * @return Jenkins connection parameters
     */
    @Override
	public List<ConnectorRequiredParams> getRequiredParameters(){
		if(requiredParams == null){
			requiredParams = new ArrayList<>();
			for(JenkinsFaasConnectorParams param : JenkinsFaasConnectorParams.values()){
				ConnectorRequiredParams newParam = new ConnectorRequiredParams();
				newParam.setName(param.name());
				newParam.setMandatory(param.isMandatory());
				newParam.setSecured(param.isSecured());
				requiredParams.add(newParam);
			}
		}
		return 	requiredParams;
	}

    /**
     * Get Jenkins string type
     *
     * @return String type
     */
    @Override
	public String getType() {
		return "jenkins_faas";
	}

    /**
     * Create a Jenkins indicator from an indicator type
     *
     * @param s indicator type
     * @return A new Jenkins indicator
     */
    @Override
	public AbstractIndicator createIndicator(String s) {
		return JenkinsFaasIndicatorType.valueOf(s).createIndicator();
	}

	@Override
	public boolean testConnector(Map<String, String> connectorParams) {
		try {

			ObjectMapper mapper = new ObjectMapper();

			String flux = JenkinsFaasFluxUtil.getFluxJson(JenkinsFaasFluxUtil.buildUrlRemoteAPI(connectorParams)).replaceAll("\n", ",");

			String fluxJSONArray = "[" + flux.substring(0, flux.length()-1) + "]";

			List<ResponseFaasJSON> responseJob ;


			try {

				responseJob = mapper.readValue(fluxJSONArray, new TypeReference<List<ResponseFaasJSON>>(){});

			} catch (JsonMappingException e) {
				return false;
			} catch (JsonParseException e) {
				return false;
			} catch (IOException e) {
				return false;
			}

			if (responseJob != null) {
				return true;
			}else{
				return false;
			}

		} catch (Exception e) {
			return false;
		}

	}
}
