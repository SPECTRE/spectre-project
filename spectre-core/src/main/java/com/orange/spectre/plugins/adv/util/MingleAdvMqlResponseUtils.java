package com.orange.spectre.plugins.adv.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;
import com.google.common.net.UrlEscapers;
import com.orange.spectre.plugins.adv.model.MingleSprintDatePojo;
import com.orange.spectre.plugins.adv.model.MingleStoryCountPojo;
import com.orange.spectre.plugins.common.util.ProxyConfig;
import com.orange.spectre.plugins.adv.model.enumerator.MingleAdvConnectorParams;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Comparator;


/**
 * Created by ludovic on 12/10/2016.
 */
public final class MingleAdvMqlResponseUtils<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MingleAdvMqlResponseUtils.class);

    /**
     * @param connectorParams
     * @param mqlRequest
     * @return http response in JSONArray format
     * @throws Exception
     */
    public static JSONArray getResponseArray(Map<String, String> connectorParams, String mqlRequest) throws Exception {

        HttpResponse response = request(connectorParams, mqlRequest);
        if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
            InputStream is = response.getEntity().getContent();
            StringWriter writer = new StringWriter();
            String encoding = "UTF-8";
            IOUtils.copy(is, writer, encoding);
            String decodedJsonResponse = StringEscapeUtils.unescapeJava(writer.toString());

            JSONArray jsonArray = new JSONArray(decodedJsonResponse);

            return jsonArray;
        }
        return null;
    }

    /**
     * @param connectorParams
     * @param mqlRequest
     * @return http response
     * @throws Exception
     */
    public static HttpResponse request(Map<String, String> connectorParams, String mqlRequest) throws Exception {

        String partialUrl = connectorParams.get(MingleAdvConnectorParams.url.name());
        String project = connectorParams.get(MingleAdvConnectorParams.projet.name());

        StringBuilder fullUrl = new StringBuilder();

        fullUrl.append(partialUrl + "/api/v2/projects/");
        fullUrl.append(project);
        fullUrl.append("/cards/execute_mql.json?mql=");

        String escapedMqlRequest = UrlEscapers.urlFragmentEscaper().escape(mqlRequest);
        fullUrl.append(escapedMqlRequest);

        HttpGet httpGet = new HttpGet(fullUrl.toString());

        CloseableHttpClient httpClient;

        TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(
                            java.security.cert.X509Certificate[] certs, String authType) {
                    }
                }
        };

        SSLContext sc = SSLContext.getInstance("SSL");
        sc.init(null, trustAllCerts, new java.security.SecureRandom());
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sc);

        // Complete url with Proxy Settings
        if (ProxyConfig.getInstance().isUseProxy()) {
            HttpHost proxy = new HttpHost(ProxyConfig.getInstance().getHost(), Integer.valueOf(ProxyConfig.getInstance().getPort()));
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).setProxy(proxy).build();
        } else {
            httpClient = HttpClients.custom().setSSLSocketFactory(sslsf).build();
        }

        return httpClient.execute(httpGet);
    }

    /**
     * @param str_date
     * @return date in timestamp
     * @throws ParseException
     */
    public static Timestamp convertStringToTimestamp(String str_date) throws ParseException {
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = formatter.parse(str_date);
        java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());
        return timeStampDate;
    }

    /**
     * @param jsonArray
     * @return List of cumulated story count
     * @throws IOException
     */
    public static List<MingleStoryCountPojo> cumulateCount(JSONArray jsonArray) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        List<MingleStoryCountPojo> mingleStoryCountPojoList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            MingleStoryCountPojo mingleStoryCountPojo = mapper.readValue(jsonObject.toString(), MingleStoryCountPojo.class);
            if (i == 0) {
                mingleStoryCountPojoList.add(mingleStoryCountPojo);
                continue;
            }
            mingleStoryCountPojo.setCount(mingleStoryCountPojoList.get(i - 1).getCount() + mingleStoryCountPojo.getCount());

            mingleStoryCountPojoList.add(mingleStoryCountPojo);
        }

        return mingleStoryCountPojoList;
    }

    /**
     * @param jsonArray
     * @return List of last ten sprints
     * @throws IOException
     */
    public static List<MingleSprintDatePojo> lastTenSprint(JSONArray jsonArray) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        List<MingleSprintDatePojo> mingleSprintDatePojoList = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            MingleSprintDatePojo mingleSprintDatePojo = mapper.readValue(jsonObject.toString(), MingleSprintDatePojo.class);

            if (mingleSprintDatePojo.getStartTime() == null) continue;

            mingleSprintDatePojoList.add(mingleSprintDatePojo);

            if (mingleSprintDatePojoList.size() == 10) break;
        }

        return mingleSprintDatePojoList;

    }

    /**
     * @param mingleSprintDatePojoList
     * @param mingleStoryCountPojoList
     * @return List of sprints with cumulated count
     * @throws ParseException
     */
    public static List<MingleSprintDatePojo> compute(List<MingleSprintDatePojo> mingleSprintDatePojoList, List<MingleStoryCountPojo> mingleStoryCountPojoList) throws ParseException {

        int nb = 0, lastNb = 0;

        List<MingleStoryCountPojo> story = new ArrayList<>();

        List<MingleSprintDatePojo> result = new ArrayList<>();

        for (int i = mingleSprintDatePojoList.size() - 1; i >= 0; i--) {

            MingleSprintDatePojo sprintItem = mingleSprintDatePojoList.get(i);

            for (MingleStoryCountPojo countItem : mingleStoryCountPojoList) {

                if ((convertStringToTimestamp(countItem.getCreate()).getTime() >= convertStringToTimestamp(sprintItem.getStartTime()).getTime())
                        && (convertStringToTimestamp(countItem.getCreate()).getTime() < convertStringToTimestamp(sprintItem.getEndTime()).getTime())) {

                    story.add(countItem);

                    nb = story.stream().max(Comparator.comparing(item -> item.getCount())).get().getCount();
                }
            }
            if (nb == 0) {
                sprintItem.setStoriesNb(lastNb);
            } else {
                sprintItem.setStoriesNb(nb);
                lastNb = nb;
            }
            result.add(sprintItem);

            story.clear();

            nb = 0;
        }

        return Lists.reverse(result);
    }
}
