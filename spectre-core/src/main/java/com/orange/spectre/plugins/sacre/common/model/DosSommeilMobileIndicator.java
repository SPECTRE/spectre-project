package com.orange.spectre.plugins.sacre.common.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.model.data.IntegerData;

import java.io.IOException;
import java.util.Map;

public class DosSommeilMobileIndicator implements ISacreSubIndicator {


    public IntegerData computeData(Map<String, String> connectorParams, String flux) throws IOException {
        IntegerData data = new IntegerData();
        ObjectMapper mapper = new ObjectMapper();
        int nbDosSommeil = 0;
        Data myData = mapper.readValue(flux, Data.class);

        for (DossierMOBILE trai : myData.getData().getDossierMOBILE()) {
            if (trai.getSommeil() != null && trai.getSommeil() != "") {
                nbDosSommeil += Integer.parseInt(trai.getSommeil());
            }

        }

        data.setData(nbDosSommeil);
        return data;
    }

    @Override
    public String getLabel() {
        return "Dossiers en sommeil";
    }

}
