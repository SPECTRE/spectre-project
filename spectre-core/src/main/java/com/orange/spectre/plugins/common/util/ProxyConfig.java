package com.orange.spectre.plugins.common.util;

/**
 * Created by epeg7421 on 12/01/17.
 * Singleton class to access to Configuration outside Spring
 * Bonne pratique :
 * On crée une classe Singleton qui encapsule les propriétés déclarés dans le yaml
 * Cette dernière est instanciée dans un Component Spring
 * On l'utilise via le ProxyConfig.getInstance().use()
 */
public class ProxyConfig {

	private static ProxyConfig instance = new ProxyConfig();

	private boolean useProxy;
	private String host;
	private String port;

	public static ProxyConfig getInstance() {
		return instance;
	}

	public boolean isUseProxy() {
		return useProxy;
	}

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	 public void use(String host, String port){
		this.useProxy = true;
		this.host = host;
		this.port = port;
	}
}
