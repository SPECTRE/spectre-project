
package com.orange.spectre.plugins.adv.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.adv.domain.*;

/**
 * Enumerator of all available mingle indicators
 */
public enum MingleAdvIndicatorType {

    /**
     * Comment the following lines to disabled the indicators
     */
    ADV_PREDICTABILITY_NUMBER(PredictabilityNbIndicator.class),
    ADV_ANOMALIES_NUMBER(AnomaliesNbIndicator.class),
    ADV_AVERAGE_USER_STORY_COMPLETE_NUMBER(AvgUserStoryCompleteNbIndicator.class),
    ADV_LAST_TEN_SPRINT(SprintLastTenNbIndicator.class),
    ADV_SPRINT_ENCOURS(SprintEncour.class),
    ADV_COUNT_US_NONTERMINE_NONENREDACTION(USCountNontermineNnonEnRedaction.class),
    ADV_COUNT_TERMINE_NON_TERMINE(NbOfUsTermineNonTermine.class),
    ADV_NB_US_NON_PREVU(NbUsNonPrevu.class),
    ADV_STORY_STATUS(StoryStatusNbIndicator.class),
    ADV_STATUS_US_FINISH(StatusUSFinish.class),
    ADV_DUREE_DEV(TempsDeMoyenDureeDev.class),
    ADV_DUREE_TEST(TempsMoyenDureeTest.class),
    ADV_TEMPS_MOYEN_DUREE_ETUDES(TempsMoyenDureeEtudes.class);

    /**
     * Current mingle indicator class
     */
    private Class<? extends AbstractIndicator> clazz;

    /**
     * Indicator type constructor
     *
     * @param clazz Current indicator class
     */
    MingleAdvIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }

    /**
     * Indicator instantiation from it's type
     *
     * @return An mingle indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }

    /**
     * Get indicator label from it's type
     *
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "ADV_PREDICTABILITY_NUMBER":
                return "Le predictibilité";
            case "ADV_ANOMALIES_NUMBER":
                return "Le nombre d'anomalies";
            case "ADV_AVERAGE_USER_STORY_COMPLETE_NUMBER":
                return "Points moyen d'US terminé";
            case "ADV_LAST_TEN_SPRINT":
                return "US pas terminé et en redaction";
            case "ADV_SPRINT_ENCOURS":
                return "Sprint en cours";
            case "ADV_COUNT_US_NONTERMINE_NONENREDACTION":
                return "Nb d'US non terminé et en redaction";
            case "ADV_COUNT_TERMINE_NON_TERMINE":
                return "Nb of US Status Termineé et Not Null";
            case "ADV_NB_US_NON_PREVU":
                return "Nb d'US Non Plannifiés";
            case "ADV_STORY_STATUS":
                return "Nb d'US en cours d'étude";
            case "ADV_STATUS_US_FINISH":
                return "Nb d'US Terminé";
            case "ADV_DUREE_DEV":
                return "Temps moyen de réalisation en jour";
            case "ADV_DUREE_TEST":
                return "Temps moyen de testing en jour";
            case "ADV_TEMPS_MOYEN_DUREE_ETUDES":
                return "Temps moyen d'étude en jour";
            default:
                return null;
        }

    }
}


