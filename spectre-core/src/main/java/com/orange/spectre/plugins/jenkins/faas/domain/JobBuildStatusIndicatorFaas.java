package com.orange.spectre.plugins.jenkins.faas.domain;


import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.jenkins.common.model.ResponseFaasJSON;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasConnectorParams;
import com.orange.spectre.plugins.jenkins.faas.model.enumerator.JenkinsFaasIndicatorType;
import com.orange.spectre.plugins.jenkins.faas.util.JenkinsFaasFluxUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Jpa entity for Job build status
 * @author nblg6551
 */
@Entity
@DiscriminatorValue("JENKINS_FAAS_JOB_BUILD_STATUS")
public class JobBuildStatusIndicatorFaas extends AbstractIndicator {

    @Transient
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Override
    public String getLabel(){
        return JenkinsFaasIndicatorType.FAAS_JOB_BUILD_STATUS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        //SELECT THE RIGHT MEASURE VALUE
        return IndicatorMeasure.TEXT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Ops";
    }

    @Override
    public String getPurpose() {
        return "Permet d’identifier très rapidement si le dernier build est en erreur ou non";
    }

    @Override
    public String getDefinition() {
        return "Status du dernier build du job";
    }

    @Override
    public String getOrigin() {
        return "Jenkins FAAS";
    }

    @Override
    public IndicatorFamily getFamily() {
        //SELECT THE RIGHT FAMILY VALUE
        return IndicatorFamily.PERFORMANCE;
    }

    @Override
    public Enum getType() {
        return JenkinsFaasIndicatorType.FAAS_JOB_BUILD_STATUS;
    }

    /**
     *
     * @param connectorParams
     * @return
     */
    @Override
    public AbstractData getData(Map<String, String> connectorParams) {

        try {
            String outputFlux = JenkinsFaasFluxUtil.getFluxJson(JenkinsFaasFluxUtil.buildUrlRemoteAPI(connectorParams)).replaceAll("\n", ",");

            String fluxJSONArray = "[" + outputFlux.substring(0, outputFlux.length()-1) + "]";

            return this.computeData(connectorParams, fluxJSONArray);

        } catch (Exception e) {

            LOGGER.error("Exception: problem in the recovery of kpi: " + e);
        }
        return null;
    }

    public StringData computeData(Map<String, String> connectorParams, String fluxJSONArray) {

        String jobName = (String) connectorParams.get(JenkinsFaasConnectorParams.nom_job.name());

        String jobStatut = null;

        ObjectMapper mapper = new ObjectMapper();

        StringData data = new StringData();

        List<ResponseFaasJSON> responseJob;

        if (jobName != null && jobName.length() > 0) {

            try {

                responseJob = mapper.readValue(fluxJSONArray, new TypeReference<List<ResponseFaasJSON>>() {
                });

                jobStatut = responseJob.stream().filter(rep -> jobName.equals(rep.getJobName()))
                        .max(Comparator.comparing(c -> Double.parseDouble(c.getStarted())))
                        .get().getResult();


            } catch (JsonMappingException e) {
                LOGGER.error("JSON Mapping Exception with parameters " + connectorParams.toString());

            } catch (JsonParseException e) {
                LOGGER.error("JSON Parse Exception with parameters " + connectorParams.toString() );

            } catch (IOException e) {
                LOGGER.error("IO Exception with parameters " + connectorParams.toString() );
            }

        }

        data.setData(jobStatut);
        return data;
    }
}
