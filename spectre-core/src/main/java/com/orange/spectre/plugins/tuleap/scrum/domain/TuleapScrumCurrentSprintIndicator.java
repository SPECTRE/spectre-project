package com.orange.spectre.plugins.tuleap.scrum.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.tuleap.common.model.TuleapPlanning;
import com.orange.spectre.plugins.tuleap.common.model.TuleapSprint;
import com.orange.spectre.plugins.tuleap.common.model.TuleapTokenResponse;
import com.orange.spectre.plugins.tuleap.common.utils.TuleapUtils;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumConnectorParams;
import com.orange.spectre.plugins.tuleap.scrum.enumerator.TuleapScrumIndicatorType;
import com.orange.spectre.plugins.tuleap.scrum.model.Sprint;
import com.orange.spectre.plugins.tuleap.scrum.utils.TuleapScrumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Created by ludovic on 03/04/2017.
 */
@Entity
@DiscriminatorValue("TULEAP_SCRUM_CURRENT_SPRINT")
public class TuleapScrumCurrentSprintIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(TuleapScrumCurrentSprintIndicator.class);


    @Override
    public AbstractData getData(Map<String, String> connectorParams) {
        try {

            String login = connectorParams.get(TuleapScrumConnectorParams.login.name());
            String password = connectorParams.get(TuleapScrumConnectorParams.mot_de_passe.name());
            String project = connectorParams.get(TuleapScrumConnectorParams.nom_projet.name());

            TuleapTokenResponse tuleapResponse = TuleapUtils.getTuleapConnectionData(login, password);
            String tuleapProjectId = TuleapUtils.getTuleapProjectIdByName(tuleapResponse.getToken(), tuleapResponse.getUserId(), project);

            TuleapPlanning tuleapScrumPlannings = TuleapScrumUtils.getTuleapScrumPlanning(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapProjectId);
            TuleapSprint[] sprints = TuleapScrumUtils.getTuleapScrumSprints(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapScrumPlannings.getId());

            for(TuleapSprint tuleapSprint : sprints) {
                Sprint sprint = TuleapScrumUtils.getTuleapScrumSprint(tuleapResponse.getToken(), tuleapResponse.getUserId(), tuleapSprint.getId());
                boolean isCurrent = sprint.isCurrent();
                if (isCurrent) {
                    return new StringData(sprint.getLabel());
                }
            }

            return new StringData("undefined");


        } catch (Exception e) {
            LOGGER.error("Exception: problem during kpi retrieval: " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
        return TuleapScrumIndicatorType.TULEAP_SCRUM_CURRENT_SPRINT.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.TEXT;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return TuleapScrumIndicatorType.TULEAP_SCRUM_CURRENT_SPRINT;
    }

    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}
