package com.orange.spectre.plugins.mingle.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.exception.FetchDataException;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.mingle.model.enumerator.MingleConnectorParams;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

import static com.orange.spectre.plugins.mingle.model.enumerator.MingleIndicatorType.SPRINT_NAME;

/**
 * Jpa entity for percentage jobs fails
 *
 * @author nmcf5735
 */
@Entity
@DiscriminatorValue("SPRINT_NAME")
public class SprintNameIndicator extends AbstractIndicator {

    @Override
    public String getLabel() {
        return SPRINT_NAME.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public AbstractData getData(Map<String, String> connectorParams) throws FetchDataException {


        String sprint = connectorParams.get(MingleConnectorParams.sprint_courant.name());
        if(sprint != null){
            return new StringData(sprint);
        }else{
            return null;
        }
    }



    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.MANAGEMENT;
    }

    @Override
    public Enum getType() {
        return SPRINT_NAME;
    }


    @Override
    public String getDomain() {
        return null;
    }

    @Override
    public String getPurpose() {
        return null;
    }

    @Override
    public String getDefinition() {
        return null;
    }

    @Override
    public String getOrigin() {
        return null;
    }

}

