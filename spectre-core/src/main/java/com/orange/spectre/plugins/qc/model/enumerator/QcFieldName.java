package com.orange.spectre.plugins.qc.model.enumerator;

/**
 * Created by ludovic on 31/08/2016.
 */
public enum QcFieldName {

    CRITICAL,
    MAJOR,
    MINOR,
    OPENED,
    CLOSED
}
