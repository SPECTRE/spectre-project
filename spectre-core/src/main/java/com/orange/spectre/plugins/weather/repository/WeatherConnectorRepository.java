
package com.orange.spectre.plugins.weather.repository;

import com.orange.spectre.plugins.weather.domain.WeatherConnector;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Weather connector JPA enumerator
 */
public interface WeatherConnectorRepository extends JpaRepository<WeatherConnector, Long> {

}
