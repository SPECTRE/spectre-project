package com.orange.spectre.plugins.qc.model.enumerator;

import com.orange.spectre.core.domain.ConnectorParamsInterface;

/**
 * Enumerator of all qc connection parameters
 */
public enum QcConnectorParams implements ConnectorParamsInterface {

    login (true, false),
    password (true, true),
    domaine (true, false),
    projet (true, false),
	release (false, false);

	private final boolean mandatory;
	private final boolean secured;

	private QcConnectorParams(boolean mandatory, boolean secured) {
		this.mandatory = mandatory;
		this.secured = secured;
	}

	@Override
	public boolean isMandatory() {
		return this.mandatory;
	}

	@Override
	public boolean isSecured() {
		return this.secured;
	}


}
