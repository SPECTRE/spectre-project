package com.orange.spectre.plugins.sonar.faas.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.IntegerData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.faas.enumerator.SonarFaasIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;

/**
 * Jpa entity for blocker violation indicator
 */
@Entity
@DiscriminatorValue("SONAR_FAAS_BLOCKER_VIOLATIONS")
public class BlockerViolationsIndicatorFaas extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(BlockerViolationsIndicatorFaas.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) {

        IntegerData integerData = new IntegerData();
        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "blocker_violations";
            String sonarPartialUrl = connectorParams.get("url");

            String data = ComputeDataUtils.extractFaas(sonarPartialUrl, project, version, metric);

            integerData.setData((int)Float.parseFloat(data));

        }catch(Exception e){
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }

        return integerData;
    }

    @Override
    public String getLabel() {
            return SonarFaasIndicatorType.BLOCKER_VIOLATIONS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre le nombre de règles de non confirmités bloquantes";
    }

    @Override
    public String getDefinition() {
        return "Cet indicateur permet de mesurer la qualité de votre code logiciel vis-à-vis des règles Sonar. Elles sont classées selon la sévérité (Bloquante, Critique, Majeur et Mineur)";
    }

    @Override
    public String getOrigin() {
        return "Sonar Faas";
    }

    @Override
    public Enum getType() {
        return SonarFaasIndicatorType.BLOCKER_VIOLATIONS;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
