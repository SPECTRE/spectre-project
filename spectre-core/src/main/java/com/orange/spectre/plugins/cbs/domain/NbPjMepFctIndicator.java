package com.orange.spectre.plugins.cbs.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.data.StringData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.cbs.model.enumerator.CbsIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;

@Entity
@DiscriminatorValue("CBS_NB_PJ_MEP_FCT")
public class NbPjMepFctIndicator extends AbstractIndicator {

	private static final long	serialVersionUID	= -5822635963619843426L;
	@Transient
	private final Logger		LOGGER				= LoggerFactory
															.getLogger(NbPjMepFctIndicator.class);

	@Override
	public AbstractData getData(Map<String, String> connectorParams) {
		try {
			Map<String, Object> params = new HashMap<>();

			params.put("project", connectorParams.get("project"));
			params.put("indicator", CbsIndicatorType.NB_PJ_MEP_FCT.getKey());

			return new StringData(CbsConnector.DATA.get(params.get("project"))
					.get(params.get("indicator")));

		} catch (Exception e) {
			LOGGER.error("Exception: problème lors de la recuperation du kpi : "
					+ e);
		}

		return null;
	}

	@Override
	public String getLabel() {
		return CbsIndicatorType.NB_PJ_MEP_FCT.getLabel();
	}

	@Override
	public IndicatorMeasure getMeasure() {
		// SELECT THE RIGHT MEASURE VALUE
		return IndicatorMeasure.NUMERIC;
	}

	@Override
	public String getMeasureUnity() {
		return null;
	}

	@Override
	public String getDomain() {
		return null;
	}

	@Override
	public String getPurpose() {
		return null;
	}

	@Override
	public String getDefinition() {
		return null;
	}

	@Override
	public String getOrigin() {
		return null;
	}

	@Override
	public IndicatorFamily getFamily() {
		// SELECT THE RIGHT FAMILY VALUE
		return IndicatorFamily.DEVOPS;
	}

	@Override
	public Enum<CbsIndicatorType> getType() {
		return CbsIndicatorType.NB_PJ_MEP_FCT;
	}

}
