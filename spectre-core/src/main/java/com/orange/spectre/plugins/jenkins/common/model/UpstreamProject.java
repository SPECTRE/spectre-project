
package com.orange.spectre.plugins.jenkins.common.model;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "actions",
    "description",
    "displayName",
    "displayNameOrNull",
    "name",
    "url",
    "buildable",
    "builds",
    "color",
    "firstBuild",
    "healthReport",
    "inQueue",
    "keepDependencies",
    "lastBuild",
    "lastCompletedBuild",
    "lastFailedBuild",
    "lastStableBuild",
    "lastSuccessfulBuild",
    "lastUnstableBuild",
    "lastUnsuccessfulBuild",
    "nextBuildNumber",
    "property",
    "queueItem",
    "concurrentBuild",
    "downstreamProjects",
    "scm",
    "upstreamProjects"
})
public class UpstreamProject {

    @JsonProperty("actions")
    private List<Object> actions = new ArrayList<Object>();
    @JsonProperty("description")
    private String description;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("displayNameOrNull")
    private Object displayNameOrNull;
    @JsonProperty("name")
    private String name;
    @JsonProperty("url")
    private String url;
    @JsonProperty("buildable")
    private Boolean buildable;
    @JsonProperty("builds")
    private List<Object> builds = new ArrayList<Object>();
    @JsonProperty("color")
    private String color;
    @JsonProperty("firstBuild")
    private Object firstBuild;
    @JsonProperty("healthReport")
    private List<Object> healthReport = new ArrayList<Object>();
    @JsonProperty("inQueue")
    private Boolean inQueue;
    @JsonProperty("keepDependencies")
    private Boolean keepDependencies;
    @JsonProperty("lastBuild")
    private Object lastBuild;
    @JsonProperty("lastCompletedBuild")
    private Object lastCompletedBuild;
    @JsonProperty("lastFailedBuild")
    private Object lastFailedBuild;
    @JsonProperty("lastStableBuild")
    private Object lastStableBuild;
    @JsonProperty("lastSuccessfulBuild")
    private Object lastSuccessfulBuild;
    @JsonProperty("lastUnstableBuild")
    private Object lastUnstableBuild;
    @JsonProperty("lastUnsuccessfulBuild")
    private Object lastUnsuccessfulBuild;
    @JsonProperty("nextBuildNumber")
    private Integer nextBuildNumber;
    @JsonProperty("property")
    private List<Object> property = new ArrayList<Object>();
    @JsonProperty("queueItem")
    private Object queueItem;
    @JsonProperty("concurrentBuild")
    private Boolean concurrentBuild;
    @JsonProperty("downstreamProjects")
    private List<DownstreamProject> downstreamProjects = new ArrayList<DownstreamProject>();
    @JsonProperty("scm")
    private Scm_ scm;
    @JsonProperty("upstreamProjects")
    private List<Object> upstreamProjects = new ArrayList<Object>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     *     The actions
     */
    @JsonProperty("actions")
    public List<Object> getActions() {
        return actions;
    }

    /**
     *
     * @param actions
     *     The actions
     */
    @JsonProperty("actions")
    public void setActions(List<Object> actions) {
        this.actions = actions;
    }

    /**
     *
     * @return
     *     The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     *     The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     *     The displayName
     */
    @JsonProperty("displayName")
    public String getDisplayName() {
        return displayName;
    }

    /**
     *
     * @param displayName
     *     The displayName
     */
    @JsonProperty("displayName")
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     *
     * @return
     *     The displayNameOrNull
     */
    @JsonProperty("displayNameOrNull")
    public Object getDisplayNameOrNull() {
        return displayNameOrNull;
    }

    /**
     *
     * @param displayNameOrNull
     *     The displayNameOrNull
     */
    @JsonProperty("displayNameOrNull")
    public void setDisplayNameOrNull(Object displayNameOrNull) {
        this.displayNameOrNull = displayNameOrNull;
    }

    /**
     *
     * @return
     *     The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     *     The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     *     The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     *     The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     *
     * @return
     *     The buildable
     */
    @JsonProperty("buildable")
    public Boolean getBuildable() {
        return buildable;
    }

    /**
     *
     * @param buildable
     *     The buildable
     */
    @JsonProperty("buildable")
    public void setBuildable(Boolean buildable) {
        this.buildable = buildable;
    }

    /**
     *
     * @return
     *     The builds
     */
    @JsonProperty("builds")
    public List<Object> getBuilds() {
        return builds;
    }

    /**
     *
     * @param builds
     *     The builds
     */
    @JsonProperty("builds")
    public void setBuilds(List<Object> builds) {
        this.builds = builds;
    }

    /**
     *
     * @return
     *     The color
     */
    @JsonProperty("color")
    public String getColor() {
        return color;
    }

    /**
     *
     * @param color
     *     The color
     */
    @JsonProperty("color")
    public void setColor(String color) {
        this.color = color;
    }

    /**
     *
     * @return
     *     The firstBuild
     */
    @JsonProperty("firstBuild")
    public Object getFirstBuild() {
        return firstBuild;
    }

    /**
     *
     * @param firstBuild
     *     The firstBuild
     */
    @JsonProperty("firstBuild")
    public void setFirstBuild(Object firstBuild) {
        this.firstBuild = firstBuild;
    }

    /**
     *
     * @return
     *     The healthReport
     */
    @JsonProperty("healthReport")
    public List<Object> getHealthReport() {
        return healthReport;
    }

    /**
     *
     * @param healthReport
     *     The healthReport
     */
    @JsonProperty("healthReport")
    public void setHealthReport(List<Object> healthReport) {
        this.healthReport = healthReport;
    }

    /**
     *
     * @return
     *     The inQueue
     */
    @JsonProperty("inQueue")
    public Boolean getInQueue() {
        return inQueue;
    }

    /**
     *
     * @param inQueue
     *     The inQueue
     */
    @JsonProperty("inQueue")
    public void setInQueue(Boolean inQueue) {
        this.inQueue = inQueue;
    }

    /**
     *
     * @return
     *     The keepDependencies
     */
    @JsonProperty("keepDependencies")
    public Boolean getKeepDependencies() {
        return keepDependencies;
    }

    /**
     *
     * @param keepDependencies
     *     The keepDependencies
     */
    @JsonProperty("keepDependencies")
    public void setKeepDependencies(Boolean keepDependencies) {
        this.keepDependencies = keepDependencies;
    }

    /**
     *
     * @return
     *     The lastBuild
     */
    @JsonProperty("lastBuild")
    public Object getLastBuild() {
        return lastBuild;
    }

    /**
     *
     * @param lastBuild
     *     The lastBuild
     */
    @JsonProperty("lastBuild")
    public void setLastBuild(Object lastBuild) {
        this.lastBuild = lastBuild;
    }

    /**
     *
     * @return
     *     The lastCompletedBuild
     */
    @JsonProperty("lastCompletedBuild")
    public Object getLastCompletedBuild() {
        return lastCompletedBuild;
    }

    /**
     *
     * @param lastCompletedBuild
     *     The lastCompletedBuild
     */
    @JsonProperty("lastCompletedBuild")
    public void setLastCompletedBuild(Object lastCompletedBuild) {
        this.lastCompletedBuild = lastCompletedBuild;
    }

    /**
     *
     * @return
     *     The lastFailedBuild
     */
    @JsonProperty("lastFailedBuild")
    public Object getLastFailedBuild() {
        return lastFailedBuild;
    }

    /**
     *
     * @param lastFailedBuild
     *     The lastFailedBuild
     */
    @JsonProperty("lastFailedBuild")
    public void setLastFailedBuild(Object lastFailedBuild) {
        this.lastFailedBuild = lastFailedBuild;
    }

    /**
     *
     * @return
     *     The lastStableBuild
     */
    @JsonProperty("lastStableBuild")
    public Object getLastStableBuild() {
        return lastStableBuild;
    }

    /**
     *
     * @param lastStableBuild
     *     The lastStableBuild
     */
    @JsonProperty("lastStableBuild")
    public void setLastStableBuild(Object lastStableBuild) {
        this.lastStableBuild = lastStableBuild;
    }

    /**
     *
     * @return
     *     The lastSuccessfulBuild
     */
    @JsonProperty("lastSuccessfulBuild")
    public Object getLastSuccessfulBuild() {
        return lastSuccessfulBuild;
    }

    /**
     *
     * @param lastSuccessfulBuild
     *     The lastSuccessfulBuild
     */
    @JsonProperty("lastSuccessfulBuild")
    public void setLastSuccessfulBuild(Object lastSuccessfulBuild) {
        this.lastSuccessfulBuild = lastSuccessfulBuild;
    }

    /**
     *
     * @return
     *     The lastUnstableBuild
     */
    @JsonProperty("lastUnstableBuild")
    public Object getLastUnstableBuild() {
        return lastUnstableBuild;
    }

    /**
     *
     * @param lastUnstableBuild
     *     The lastUnstableBuild
     */
    @JsonProperty("lastUnstableBuild")
    public void setLastUnstableBuild(Object lastUnstableBuild) {
        this.lastUnstableBuild = lastUnstableBuild;
    }

    /**
     *
     * @return
     *     The lastUnsuccessfulBuild
     */
    @JsonProperty("lastUnsuccessfulBuild")
    public Object getLastUnsuccessfulBuild() {
        return lastUnsuccessfulBuild;
    }

    /**
     *
     * @param lastUnsuccessfulBuild
     *     The lastUnsuccessfulBuild
     */
    @JsonProperty("lastUnsuccessfulBuild")
    public void setLastUnsuccessfulBuild(Object lastUnsuccessfulBuild) {
        this.lastUnsuccessfulBuild = lastUnsuccessfulBuild;
    }

    /**
     *
     * @return
     *     The nextBuildNumber
     */
    @JsonProperty("nextBuildNumber")
    public Integer getNextBuildNumber() {
        return nextBuildNumber;
    }

    /**
     *
     * @param nextBuildNumber
     *     The nextBuildNumber
     */
    @JsonProperty("nextBuildNumber")
    public void setNextBuildNumber(Integer nextBuildNumber) {
        this.nextBuildNumber = nextBuildNumber;
    }

    /**
     *
     * @return
     *     The property
     */
    @JsonProperty("property")
    public List<Object> getProperty() {
        return property;
    }

    /**
     *
     * @param property
     *     The property
     */
    @JsonProperty("property")
    public void setProperty(List<Object> property) {
        this.property = property;
    }

    /**
     *
     * @return
     *     The queueItem
     */
    @JsonProperty("queueItem")
    public Object getQueueItem() {
        return queueItem;
    }

    /**
     *
     * @param queueItem
     *     The queueItem
     */
    @JsonProperty("queueItem")
    public void setQueueItem(Object queueItem) {
        this.queueItem = queueItem;
    }

    /**
     *
     * @return
     *     The concurrentBuild
     */
    @JsonProperty("concurrentBuild")
    public Boolean getConcurrentBuild() {
        return concurrentBuild;
    }

    /**
     *
     * @param concurrentBuild
     *     The concurrentBuild
     */
    @JsonProperty("concurrentBuild")
    public void setConcurrentBuild(Boolean concurrentBuild) {
        this.concurrentBuild = concurrentBuild;
    }

    /**
     *
     * @return
     *     The downstreamProjects
     */
    @JsonProperty("downstreamProjects")
    public List<DownstreamProject> getDownstreamProjects() {
        return downstreamProjects;
    }

    /**
     *
     * @param downstreamProjects
     *     The downstreamProjects
     */
    @JsonProperty("downstreamProjects")
    public void setDownstreamProjects(List<DownstreamProject> downstreamProjects) {
        this.downstreamProjects = downstreamProjects;
    }

    /**
     *
     * @return
     *     The scm
     */
    @JsonProperty("scm")
    public Scm_ getScm() {
        return scm;
    }

    /**
     *
     * @param scm
     *     The scm
     */
    @JsonProperty("scm")
    public void setScm(Scm_ scm) {
        this.scm = scm;
    }

    /**
     *
     * @return
     *     The upstreamProjects
     */
    @JsonProperty("upstreamProjects")
    public List<Object> getUpstreamProjects() {
        return upstreamProjects;
    }

    /**
     *
     * @param upstreamProjects
     *     The upstreamProjects
     */
    @JsonProperty("upstreamProjects")
    public void setUpstreamProjects(List<Object> upstreamProjects) {
        this.upstreamProjects = upstreamProjects;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
