
package com.orange.spectre.plugins.teamcolony.model.enumerator;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.plugins.teamcolony.domain.GlobalMoodIndicator;


/**
 * Enumerator of all available teamcolony indicators
 */
public enum TeamColonyIndicatorType {


    GLOBAL_MOOD_TYPE(GlobalMoodIndicator.class);

    /**
     * Current TeamColony indicator class
     */
    private Class<? extends AbstractIndicator> clazz;
    /**
     * Indicator type constructor
     * @param clazz Current indicator class
     */
        TeamColonyIndicatorType(Class<? extends AbstractIndicator> clazz) {
        this.clazz = clazz;
    }
    /**
     * Indicator instantiation from it's type
     * @return An Teamcolony indicator
     */
    public AbstractIndicator createIndicator() {
        try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(clazz + " has no default constructor");
        }
    }
    /**
     * Get indicator label from it's type
     * @return Indicator label
     */
    public String getLabel() {
        switch (this.name()) {
            case "GLOBAL_MOOD_TYPE":
                return "Humeur de l'equipe";

            default: return null;
        }
    }
}


