/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.orange.spectre.plugins.sonar.common.exception;

/**
 *
 * @author NBLG6551
 */
public class SonarException extends UnsupportedOperationException{

    /**
     *
     * @param message
     */
    public SonarException(String message) {
        super(message);
    }

    /**
     *
     * @param message
     * @param cause
     */
    public SonarException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     *
     * @param cause
     */
    public SonarException(Throwable cause) {
        super(cause);
    }

}
