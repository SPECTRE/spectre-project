package com.orange.spectre.plugins.sonar.standard.domain;

import com.orange.spectre.core.domain.AbstractIndicator;
import com.orange.spectre.core.model.data.AbstractData;
import com.orange.spectre.core.model.enumerator.IndicatorFamily;
import com.orange.spectre.core.model.enumerator.IndicatorMeasure;
import com.orange.spectre.plugins.sonar.common.util.ComputeDataUtils;
import com.orange.spectre.plugins.sonar.standard.enumerator.SonarIndicatorType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Map;


/**
 * Jpa entity for major violation indicator
 */
@Entity
@DiscriminatorValue("SONAR_MAJOR_VIOLATIONS")
public class MajorViolationsIndicator extends AbstractIndicator {

    private static final Logger LOGGER = LoggerFactory.getLogger(MajorViolationsIndicator.class);

    @Override
    public AbstractData getData(Map<String,String> connectorParams) {

        try {
            String project = connectorParams.get("projet");
            String version = connectorParams.get("version");
            String metric = "major_violations";
            String sonarPartialUrl = connectorParams.get("url");
            return ComputeDataUtils.extract(sonarPartialUrl, project, version, metric);

        }catch(Exception e){
            LOGGER.error("Exception: problem during kpi retrieval : " + e);
        }

        return null;
    }

    @Override
    public String getLabel() {
            return SonarIndicatorType.MAJOR_VIOLATIONS.getLabel();
    }

    @Override
    public IndicatorMeasure getMeasure() {
        return IndicatorMeasure.NUMERIC;
    }

    @Override
    public String getMeasureUnity() {
        return null;
    }

    @Override
    public String getDomain() {
        return "Dev";
    }

    @Override
    public String getPurpose() {
        return "Il s'agit de connaitre le nombre de règles de non confirmités majeures";
    }

    @Override
    public String getDefinition() {
        return "Cet indicateur permet de mesurer la qualité de votre code logiciel vis-à-vis des règles Sonar. Elles sont classées selon la sévérité (Bloquante, Critique, Majeur et Mineur)";
    }

    @Override
    public String getOrigin() {
        return "SonarQube";
    }

    @Override
    public Enum getType() {
        return SonarIndicatorType.MAJOR_VIOLATIONS;
    }

    @Override
    public IndicatorFamily getFamily() {
        return IndicatorFamily.QUALITY;
    }
}
