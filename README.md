# Welcome on Spectre project!

This repository contains four folders corresponding to the four following components : 
- Spectre-core,
- Spectre-front,
- Spectre-kpi,
- Spectre-engine.

Please refer to the README.md inside of each folder.