package com.orange.spectre.kapi.technical;

import com.orange.spectre.kapi.model.HitSource;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Timestamp;
import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by ludovic on 06/05/2016.
 */
@RunWith(MockitoJUnitRunner.class)
@SpringApplicationConfiguration(classes = CustomComparatorTest.class)
@Transactional
public class CustomComparatorTest {


    @Test
    public void testEquality(){

        CustomComparator comparator = new CustomComparator();

        HitSource source1 = new HitSource();
        source1.setIndicatorId(new Long(1));
        source1.setValue(2);

        Timestamp ts = Timestamp.valueOf(LocalDateTime.now());
        source1.setDate(ts);

        assertThat(comparator.compare(source1, source1)).isEqualTo(0);

    }

    @Test
    public void testSuperior(){

        CustomComparator comparator = new CustomComparator();

        HitSource source1 = new HitSource();
        source1.setIndicatorId(new Long(1));
        source1.setValue(2);
        Timestamp ts = Timestamp.valueOf(LocalDateTime.of(2016,1,1,1,0));
        source1.setDate(ts);

        HitSource source2 = new HitSource();
        source2.setIndicatorId(new Long(2));
        source2.setValue(3);
        Timestamp ts2 = Timestamp.valueOf(LocalDateTime.of(2016,1,1,2,0));
        source2.setDate(ts2);

        assertThat(comparator.compare(source2, source1 )).isGreaterThan(0);

    }

    @Test
    public void testInferior(){

        CustomComparator comparator = new CustomComparator();

        HitSource source1 = new HitSource();
        source1.setIndicatorId(new Long(1));
        source1.setValue(2);
        Timestamp ts = Timestamp.valueOf(LocalDateTime.of(2016,1,1,1,0));
        source1.setDate(ts);

        HitSource source2 = new HitSource();
        source2.setIndicatorId(new Long(2));
        source2.setValue(3);
        Timestamp ts2 = Timestamp.valueOf(LocalDateTime.of(2016,1,1,2,0));
        source2.setDate(ts2);

        assertThat(comparator.compare(source1, source2 )).isLessThan(0);

    }
}
