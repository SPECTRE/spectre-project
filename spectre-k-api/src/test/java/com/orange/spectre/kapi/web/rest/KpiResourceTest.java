package com.orange.spectre.kapi.web.rest;

import com.orange.spectre.SpectreKApiApplication;
import com.orange.spectre.kapi.config.ElasticConfiguration;
import com.orange.spectre.kapi.config.SpectreProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by ludovic on 06/05/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = SpectreKApiApplication.class)
@WebAppConfiguration
@IntegrationTest
public class KpiResourceTest {

	@Mock
	SpectreProperties spectreProperties;
	private MockMvc restWelcomeMockMvc;
	@InjectMocks
	private KpiResource kpiResource;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		this.restWelcomeMockMvc = MockMvcBuilders.standaloneSetup(kpiResource).build();

		ElasticConfiguration elastic = new ElasticConfiguration();
		elastic.setClusterName("cluster");
		elastic.setServerPort(9300);
		elastic.setServerUrl("elasticurl");
		Mockito.when(spectreProperties.getElastic()).thenReturn(elastic);

	}

	@Test
	public void testGetKpisOnUnknownElasticsearch() throws Exception {
		restWelcomeMockMvc
				.perform(
						get("/kpis/indicators/1?connectorType=sonar&indicatorType=technicaldebt&indicatorMeasure=days")
								.contentType("application/json").accept(
								MediaType.APPLICATION_JSON))
				.andExpect(status().isInternalServerError());
	}

}
