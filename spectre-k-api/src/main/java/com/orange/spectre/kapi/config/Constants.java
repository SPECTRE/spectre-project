package com.orange.spectre.kapi.config;

/**
 * Application constants.
 */
public final class Constants {

    public static final String SPRING_PROFILE_LOCAL = "local";
    public static final String SPRING_PROFILE_SERVER = "server";

}


