package com.orange.spectre.kapi.technical;

import com.orange.spectre.kapi.model.HitSource;

import java.util.Comparator;

/**
 * Comparator for KPI data to filter by date
 */
public class CustomComparator implements Comparator<HitSource> {
    @Override
    public int compare(HitSource o1, HitSource o2) {
        return o1.getDate().compareTo(o2.getDate());
    }
}
