package com.orange.spectre.kapi.web.rest;

import com.orange.spectre.kapi.config.SpectreProperties;
import com.orange.spectre.kapi.model.HitSource;
import com.orange.spectre.kapi.technical.CustomComparator;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import javax.ws.rs.PathParam;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.springframework.http.HttpStatus.*;

/**
 * REST Api to retrieve kpi from Frontend
 */
@RestController
@RequestMapping("/kpis")
public class KpiResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(KpiResource.class);

	@Inject
	private SpectreProperties spectreProperties;

	/**
	 * Retrieve all elastic kpis for a specific indicator
	 *
	 * @param indicatorId      Indicator id
	 * @param connectorType    Connector source
	 * @param indicatorType    Indicator source
	 * @param indicatorMeasure Indicator unity
	 * @return HttpStatus OK with kpis, else NOT_FOUND if no kpis in elastic else INTERNAL_SERVER_ERROR if elastic is not reachable.
	 */
	@RequestMapping(value = "/indicators/{indicatorId}", method = RequestMethod.GET)
	public ResponseEntity searchDataByIndicator(@PathVariable String indicatorId,
			@PathParam("connectorType") String connectorType, @PathParam("indicatorType") String indicatorType,
			@PathParam("indicatorMeasure") String indicatorMeasure) {

		LOGGER.debug("get all kpis for {} indicator", indicatorId);
		try {

			Settings settings = Settings.builder()
					.put("cluster.name", spectreProperties.getElastic().getClusterName()).build();

			TransportClient client = new PreBuiltTransportClient(settings)
					.addTransportAddress(
							new InetSocketTransportAddress(InetAddress.getByName(spectreProperties.getElastic().getServerUrl()),
									spectreProperties.getElastic().getServerPort()));
			Timestamp now = new Timestamp(new Date().getTime());
			Date newDate = DateUtils.addMonths(now, -1);
			Timestamp oneMonthEarlier = new Timestamp(newDate.getTime());
			SearchResponse response;
			response = client.prepareSearch(StringUtils.remove((connectorType + indicatorType).toLowerCase(), "_"))
					.setTypes(StringUtils.remove(indicatorMeasure.toLowerCase(), "_"))
					.setQuery(boolQuery()
							.must(QueryBuilders.termQuery("indicatorId", indicatorId))
							.must(QueryBuilders.rangeQuery("exportDate").from(oneMonthEarlier.getTime()).to(now.getTime())))
					.setSize(1000)
					.execute()
					.actionGet();

			client.close();

			SearchHit[] hits = response.getHits().getHits();

			if (hits.length == 0) {
				return new ResponseEntity(null, null, NOT_FOUND);
			}

			List<HitSource> dataList = getDataList(hits, indicatorId);

			Collections.sort(dataList, new CustomComparator());

			return new ResponseEntity<>(dataList, null, OK);

		} catch (UnknownHostException e) {
			LOGGER.error("Error during kpis retrieval : ", e);
			return new ResponseEntity<>(null, null, INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * Build kpi dto for the frontend from elasticsearch data
	 *
	 * @param hits        ElasticConfiguration data
	 * @param indicatorId Indicator id
	 * @return List of formatted kpis
	 */
	List<HitSource> getDataList(SearchHit[] hits, String indicatorId) {
		List<HitSource> dataList = new ArrayList<>();

		for (SearchHit hit : hits) {
			HitSource hitSource = new HitSource();
			Map<String, Object> elasticSource = hit.getSource();
			for (Map.Entry<String, Object> entry : elasticSource.entrySet()) {
				if ("exportDate".equals(entry.getKey())) {
					Timestamp timeStamp;
					try {
						timeStamp = new Timestamp(Long.parseLong((String) entry.getValue()));
					} catch (ClassCastException cce) {
						timeStamp = new Timestamp((Long) entry.getValue());
					} catch (NumberFormatException nfe) {
						String time = (String) entry.getValue();
						timeStamp = Timestamp.valueOf(time);
					}

					hitSource.setDate(timeStamp);
				}
				if ("value".equals(entry.getKey())) {
					hitSource.setValue(entry.getValue());
				}
			}
			hitSource.setIndicatorId(Long.valueOf(indicatorId));
			dataList.add(hitSource);
		}

		return dataList;
	}

}
