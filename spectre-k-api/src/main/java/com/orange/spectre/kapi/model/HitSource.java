package com.orange.spectre.kapi.model;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * KPI dto
 */
public class HitSource {

    private Long indicatorId;
    private Timestamp date;
    private Object value;

    public Long getIndicatorId() {
        return indicatorId;
    }

    public void setIndicatorId(Long indicatorId) {
        this.indicatorId = indicatorId;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HitSource hitSource = (HitSource) o;
        return Objects.equals(indicatorId, hitSource.indicatorId) &&
                Objects.equals(date, hitSource.date) &&
                Objects.equals(value, hitSource.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(indicatorId, date, value);
    }

    @Override
    public String toString() {
        return "HitSource{" +
                "indicatorId=" + indicatorId +
                ", date=" + date +
                ", value='" + value + '\'' +
                '}';
    }
}
