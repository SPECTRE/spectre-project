package com.orange.spectre.kapi.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Created by ludovic on 15/03/2016.
 * Spectre Spring properties bean
 */
@Configuration
@ConfigurationProperties(prefix = "spectre", ignoreUnknownFields = false)
public class SpectreProperties {

    /**
     * CORS parameters
     */
    private CorsConfiguration cors = new CorsConfiguration();

    private ElasticConfiguration elastic = new ElasticConfiguration();


    public CorsConfiguration getCors() {
        return cors;
    }

    public ElasticConfiguration getElastic() {
        return elastic;
    }
}
