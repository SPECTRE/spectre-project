package com.orange.spectre.kapi.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Created by ludovic on 15/03/2016.
 *
 * K-API configurations
 *
 */
@Configuration
public class WebConfigurer implements ServletContextInitializer, EmbeddedServletContainerCustomizer {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebConfigurer.class);

    @Inject
    private SpectreProperties spectreProperties;

    /**
     * CORS web filter instantiation
     * @return CORS filter
     */
    @Bean
    public CorsFilter corsFilter() {
        LOGGER.debug("Init cors filtering");
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = spectreProperties.getCors();
        if (config.getAllowedOrigins() != null && !config.getAllowedOrigins().isEmpty()) {
            source.registerCorsConfiguration("/kpis/**", config);
        }
        return new CorsFilter(source);
    }

    /**
     * Servlet customization
     * @param configurableEmbeddedServletContainer Servlet container
     */
    @Override
    public void customize(ConfigurableEmbeddedServletContainer configurableEmbeddedServletContainer) {
        // no customization needed
    }

    /**
     *  Servlet startup actions
     * @param servletContext Servlet Context
     * @throws ServletException
     */
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        // no startup specialization needed
    }
}
