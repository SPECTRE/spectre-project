package com.orange.spectre.kapi.config;

/**
 * Created by ludovic on 06/05/2016.
 */
public class ElasticConfiguration {

    private String serverUrl;

    private Integer serverPort;

    private String clusterName;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }
}
