package com.orange.spectre;


import com.orange.spectre.kapi.config.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

/**
 * Application main class
 */
@SpringBootApplication
public class SpectreKApiApplication {


	private static final Logger LOGGER = LoggerFactory.getLogger(SpectreKApiApplication.class);

	private static ApplicationContext context;

	/**
	 * Main method
	 *
	 * @param args Execution params
	 */
	public static void main(String[] args) {
		System.setProperty("spring.profiles.default", Constants.SPRING_PROFILE_LOCAL);

		context = SpringApplication.run(SpectreKApiApplication.class, args);

		LOGGER.info("K-API is starting ...");
	}

	/**
	 * Shutdown application method
	 * Used to cleanup akka actors
	 */
	@PreDestroy
	public void terminatingApplicationStep() {
		LOGGER.info("K-API is shutting down");
	}
}

